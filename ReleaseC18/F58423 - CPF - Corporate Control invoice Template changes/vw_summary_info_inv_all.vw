DROP VIEW GFL_MCP_CORE.VW_SUMMARY_INFO_INV_ALL;

/* Formatted on 4/3/2018 5:33:47 PM (QP5 v5.227.12220.39724) */
CREATE OR REPLACE FORCE VIEW GFL_MCP_CORE.VW_SUMMARY_INFO_INV_ALL
(
   INVOICE_NUMBER,
   CLIENT_NAME,
   INVOICE_CREATE_DATE,
   BUSINESS_START_DATE,
   BUSINESS_END_DATE,
   SETTLEMENT_CURRENCY,
   SK_INV_FORMAT_ID,
   COMPANY_LOGO,
   BUSINESS_NAME,
   ADDRESS_LINE_1,
   ADDRESS_LINE_2,
   ADDRESS_LINE_3,
   CITY,
   ZIP_POST_CODE,
   EMAIL,
   COMPANY_NBR,
   VAT_NBR,
   REG_ADDRESS_LINE_1,
   REG_ADDRESS_LINE_2,
   REG_ADDRESS_LINE_3,
   REG_CITY,
   REG_ZIP_POST_CODE,
   BANK_NAME,
   ACCOUNT_NAME,
   ACCOUNT_NUMBER,
   SWIFT_CODE,
   IBAN_NUMBER,
   ROUTING_NUMBER,
   INVOICE_TERMS,
   PAYMENT_REFERENCE,
   SUMMARY_CLIENT_ID,
   FEE_SETTLEMENT_CURRENCY,
   PAYMENT_METHOD,
   SUMMARY_INFO_TEXT
)
AS
   SELECT /*+PARALLEL(4)*/
         invoice_number,
          client_name,
          invoice_create_date,
          business_start_date,
          business_end_date,
          settlement_currency,
          sk_inv_format_id,
          company_logo,
          business_name,
          address_line_1,
          address_line_2,
          address_line_3,
          city,
          zip_post_code,
          email,
          company_nbr,
          vat_nbr,
          reg_address_line_1,
          reg_address_line_2,
          reg_address_line_3,
          reg_city,
          reg_zip_post_code,
          bank_name,
          account_name,
          account_number,
          swift_code,
          iban_number,
          routing_number,
          CASE
             WHEN NVL (net_total_value_due, 0) < 0
             THEN
                'Payments will be made to your Account with'
             ELSE
                'Payments should be made to our Account with'
          END
             invoice_terms,
             TO_CHAR (invoice_create_date, 'YYMMDD')
          || '-'
          || processor_client_id
             payment_reference,
          invoice_summary.client_id summary_client_id,
          NVL (fee.fee_settlement_currency, 'UNKN') fee_settlement_currency,
          UPPER (PAYMENT_METHOD) PAYMENT_METHOD, --F58423 corporate Control adding new column PAYMENT_METHOD
          CASE
             WHEN payment_method = 'PREPAID' THEN 'For Information Only'
             ELSE 'Payment Is Now Due'
          END
             summary_info_text
     FROM invoice_summary
          LEFT JOIN legal_entity
             ON legal_entity.code = invoice_summary.client_legal_entity
          LEFT JOIN
          (SELECT DISTINCT client_id, fee_settlement_currency
             FROM invoice_client_fee
            WHERE INVOICE_FREQUENCY = 'D') fee
             ON     invoice_summary.settlement_currency =
                       fee.fee_settlement_currency
                AND invoice_summary.client_id = fee.client_id;
