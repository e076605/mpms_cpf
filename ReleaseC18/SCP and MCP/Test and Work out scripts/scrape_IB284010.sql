REPORT  NUMBER: IB284010-BB                                                                                              PAGE:          42
COMP/DIST ID 0000122 - 0013340                         PREPAID POOL BALANCE REPORT                                   RUN DATE: 09 JAN 2018
ICA:  00000013340                                   RECONCILIATION WORK OF       01/09/2018                                               
PURSE CURRENCY        036                                                                                                              
                                                                                                                                    
BEGINNING POOL BALANCE                                10064859.21                                                                         
                                                                                                                                    
CARDS LOAD                                                  40.00                                                                         
CASHOUT (UNLOAD)                                             0.00                                                                         
TRANSFER CREDIT                                              0.00                                                                         
TRANSFER DEBIT                                               0.00                                                                         
ACCOUNT REPLENISHMENT (RELOAD)                               0.00                                                                         
CARDHOLDER ADJUSTMENTS - CREDIT (EXCP)                       0.00                                                                         
CARDHOLDER ADJUSTMENTS - CREDIT (MISC)                       0.00                                                                         
CARDHOLDER ADJUSTMENTS - CREDIT P2P TRF                      0.00                                                                         
CARDHOLDER ADJUSTMENTS - CREDIT P2P TRF FEE                  0.00                                                                         
CARDHOLDER ADJUSTMENTS - CREDIT (RECON)                      0.00                                                                         
CARDHOLDER ADJUSTMENTS - DEBIT (EXCP)                        0.00                                                                         
CARDHOLDER ADJUSTMENTS - DEBIT (MISC)                        0.00                                                                         
CARDHOLDER ADJUSTMENTS - DEBIT P2P TRF                       0.00                                                                         
CARDHOLDER ADJUSTMENTS - DEBIT P2P TRF FEE                   0.00                                                                         
CARDHOLDER ADJUSTMENTS - DEBIT (RECON)                       0.00                                                                         
CARDHOLDER FEES.                                            -4.00                                                                         
FOREIGN EXCHANGE CONVERSION FEE                              0.00                                                                         
FOREIGN EXCH CROSS BORDER FEE                                0.00                                                                         
TRANSACTIONS PROCESSED                                       0.00                                                                         
NETWORK LOAD                                                 0.00                                                                      
TRANSACTIONS PROCESSED OTP REJ                               0.00                                                                         
                                                                                                                                    
                                               ----------------                                                                     
ENDING POOL BALANCE                                   10064895.21                                                                         
                                                                                                                                    
TODAYS OTP REJECTS - TRAN AMT                                0.00                                                                         
TODAYS OTP REJECTS - CCA  AMT                                0.00


---IB284010

/**************************************************************/

--Transactiosn to check
--Auto Recon Adj
--ORIGINAL TRANS-TRAN
--NET TOTALS

--IB284010-BB

/**************************************************************/

select * from global_staging_area.parsing_col_layout where report_template_id like '%IB284010%';

select * from global_staging_area.parsing_col_layout where report_template_id like '%IB284010%';

PREPAID_POOL_BAL_RECONC

select * from global_staging_area.parsing_row_layout where report_template_id like '%IB284010%';

select * from global_staging_area.raw_report_data  where report_id like '%IB284010%' and  run_date > sysdate-1;


select * from global_staging_area.tmp$_raw_report_data;


select distinct ENTITY_NAME from global_staging_area.tmp$_raw_report_data;


select * from GFL_COMMON_DATA.MCP_ENTITY_LIST
where entity_name in 
(
'PREPAID_POOL_BAL_RECONC'
);

'MCP_NEG_BAL'
MCP_NEG_BAL_DTL


--- -- 1.ORIGINAL TRANS-TRAN

select * from global_staging_area.PREPAID_POOL_BAL_RECONC where run_date = '2015-10-15'

select * from GFL_COMMON_DATA.MCP_ENTITY_LIST where entity_name = 'PREPAID_POOL_BAL_RECONC'

select * from gfl_mcp_data.PREPAID_POOL_BAL_RECONC_DTL

truncate table PREPAID_POOL_BAL_RECONC

truncate table raw_report_data

select distinct REPORT_ID from raw_report_data

execute generic_report_to_stage

select * from PREPAID_POOL_BAL_RECONC


------Below queries for developers only
select *
 from global_staging_area.raw_report_data where report_id like ('%IB267030-BB%')
and header_value6 in ('BUSINESS_DATE=2015-05-14','BUSINESS_DATE=2015-05-15')
and header_value8 = 'OWNER_LEVEL_1=21403' and header_value4 In ('PURSE_CURRENCY=784','PURSE_CURRENCY=756');
select * from global_staging_area.MCP_NEG_BAL where run_date in( '14 MAY 2015','15 MAY 2015') and purse_currency in (784,756)
and owner_level_1 = 21403;
select * from gfl_mcp_data.MCP_NEG_BAL_DTL where etl_date in ('14-MAY-2015', '15-MAY-2015') 
and owner_level_1 = 21403;
SELECT a.REPORT_NUMBER,
A.BUSINESS_DATE,
A.ICA AS ICA_NUMBER,
A.PURSE_CURRENCY_ALPHA AS PURSE_CURRENCY,
SUM (A.NET_BALANCE) AS NET_BALANCE
FROM gfl_mcp_data.mcp_neg_bal_dtl A
where etl_date in ('14-MAY-2015', '15-MAY-2015') and purse_currency in (784,756)
and owner_level_1 = 21403
GROUP BY a.REPORT_NUMBER,
A.BUSINESS_DATE,
A.ICA,
A.PURSE_CURRENCY_ALPHA
ORDER BY A.BUSINESS_DATE, A.ICA, A.PURSE_CURRENCY_ALPHA
VW_MCP_NEG_BAL_TXNS
TMP$_REPORT_TRANSACTIONS
select * from gfl_mcp_data.IF_REPORT_TRANSACTIONS where "BUSINESS_DATE" IN( TO_DATE('2015-05-15' , 'YYYY-MM-DD'),
TO_DATE('2015-05-14' , 'YYYY-MM-DD'))
and BIN_SPONSOR_ID =100204 and upper(transaction_description) like '%NEGATIVE BALANCE MOVEMENT%' and currency_alpha = 'CHF'
UNION
select * from gfl_mcp_data.IF_REPORT_TRANSACTIONS where "BUSINESS_DATE" IN( TO_DATE('2015-05-15' , 'YYYY-MM-DD'),
TO_DATE('2015-05-14' , 'YYYY-MM-DD'))
and BIN_SPONSOR_ID =100204 and upper(transaction_description) like '%NEGATIVE BALANCE MOVEMENT%' and currency_alpha = 'AED'


CPI_OWNER
input_data
execute check_to5abn_format(595319,'TQ5ABN','TQ5ABN');
execute input_data_utilities.export_input_data_to_file(595319,'TQ5ABN','TQ5ABN');
execute MCP_FILE_SPLIT(595319,'TQ5ABN','TQ5ABN');

--Table raw_reports_data_int -- cpi_owner
--table global_staging_area.raw_reports_data
execute input_data_utilities.remove_export_file(595319,'TQ5ABN','TQ5ABN');
commit;

--table global_staging_area.raw_reports_data

truncate table PREPAID_POOL_BAL_RECONC
truncate table PREPAID_POOL_BAL_RECONC_DTL

execute global_staging_area.generic_report_to_stage

select * from PREPAID_POOL_BAL_RECONC -- IB284 and IB184

execute cleaning package GFL_MCP_DATA

select * from gfl_mcp_data.PREPAID_POOL_BAL_RECONC_DTL

Run and check the VW_POOL_RECON...

Run the ODI mapping

IF_REPORT_TRANSACTION

VW_FD_SCRAPRE_REPORTS -- This will trigger one ODI job

FD_SCRAPE_REPORTS

------------------------------------ 

IF_REPORT_TRANSACTION-- Source
It has to insert in to Self issuer tables and escrow tables
Target tables 
----

vw_calc_scrape_report_txn - This view will use Self issuer tables opening and closing balance


TJF :

CPI_OWNER - File_import_data
TJF_RECORDS_STG - GLOBAL_STAGING_AREA
ACOF_RECORDS_STG - GLOABL_STAGING_AREA

