DROP VIEW MCP_ISSUER_REP.VW_CLIENT_HIERARCHY;

/* Formatted on 2/4/2018 10:38:47 PM (QP5 v5.227.12220.39724) */
CREATE OR REPLACE FORCE VIEW MCP_ISSUER_REP.VW_CLIENT_HIERARCHY
(
   KEY_INDENTED,
   CLIENT_LEVEL,
   ID,
   NAME,
   CLIENT_TYPE_CODE,
   L2_CLIENT_ID,
   L2_CLIENT_NAME,
   TOP_CLIENT_ID,
   TOP_CLIENT_NAME,
   CLIENT_MCP_FLAG,
   CLIENT_GROUP_NAME,
   ORG_REGION_NAME,
   COUNTRY_NAME,
   COUNTRY_ISO_CODE,
   COUNTRY_ISO_NUM_CODE,
   PROCESSOR_CLIENT_CODE,
   GEO_REGION_ID,
   GEO_SUBREGION_ID,
   GEO_REGION_NAME,
   GEO_SUBREGION_NAME,
   SUB_COUNTRY_ID,
   SUB_COUNTRY_NAME,
   MIGRATED_SCP,
   EXCLUDE_MATCHING
)
AS
   SELECT key_indented,
          client_hrchy.client_level,
          client_hrchy.id,
          client_hrchy.name,
          client_hrchy.client_type_code,
          CASE
             WHEN client_hrchy.client_type_code = 'CL' THEN -1
             WHEN client_hrchy.client_type_code = 'L2' THEN client_hrchy.id
             ELSE client_hrchy.parent_client_id
          END
             l2_client_id,
          CASE
             WHEN client_hrchy.client_type_code = 'CL' THEN 'NA'
             WHEN client_hrchy.client_type_code = 'L2' THEN client_hrchy.name
             ELSE client.name
          END
             l2_client_name,
          client_hrchy.top_client_id,
          client_hrchy.top_client_name,
          'Y' client_mcp_flag,
          client_group.name AS client_group_name,
          org_region.name AS org_region_name,
          ra_country.country_name AS country_name,
          ra_country.country_iso_code AS country_iso_code,
          TO_CHAR (ra_country.country_iso_num) AS country_iso_num_code,
          ra_client.processor_client_id AS processor_client_code,
          ra_geo_region.geo_region_id AS geo_region_id,
          ra_geo_subregion.geo_sub_region_id AS geo_subregion_id,
          ra_geo_region.geo_region_name AS geo_region_name,
          ra_geo_subregion.sub_region_name AS geo_subregion_name,
          ra_org_sub_country.sub_country_id AS sub_country_id,
          ra_org_sub_country.sub_country_name AS sub_country_name,
          client.migrated_scp,
          client.exclude_matching
     FROM (    SELECT LPAD (' ', LEVEL - 1) || id AS key_indented,
                      LEVEL AS client_level,
                      id,
                      name,
                      parent_client_id,
                      client_type_code,
                      CONNECT_BY_ROOT id AS top_client_id,
                      CONNECT_BY_ROOT name AS top_client_name,
                      CONNECT_BY_ROOT CLIENT_GROUP_ID AS client_group_id,
                      client_mcp_flag
                 FROM client
           START WITH id = parent_client_id AND client_mcp_flag = 'Y'
           CONNECT BY NOCYCLE parent_client_id = PRIOR id) client_hrchy,
          client_group,
          org_region,
          ra_country,
          ra_geo_region,
          ra_geo_subregion,
          ra_org_sub_country,
          ra_org_group,
          ra_client,
          client
    WHERE     client_hrchy.client_group_id = client_group.id(+)
          AND client_group.org_region_code = org_region.code(+)
          AND org_region.country_code = ra_country.country_iso_code(+)
          AND ra_country.geo_sub_region_id =
                 ra_geo_subregion.geo_sub_region_id(+)
          AND ra_geo_subregion.geo_region_id = ra_geo_region.geo_region_id(+)
          AND client_hrchy.id = ra_client.rds_client_id
          AND client.id = client_hrchy.parent_client_id
          AND client_group.id = ra_org_group.GROUP_ID(+)
          AND ra_org_group.sub_country_id =
                 ra_org_sub_country.sub_country_id(+)
   UNION ALL
   SELECT '-1' AS key_indented,
          -1 AS client_level,
          -1 AS id,
          'N/A' AS name,
          'BR' AS client_type_code,
          -1 AS l2_client_id,
          'N/A' AS l2_client_name,
          -1 AS top_client_id,
          'N/A' AS top_client_name,
          'Y' AS client_mcp_flag,
          'N/A' AS client_group_name,
          'N/A' AS org_region_name,
          'N/A' AS country_name,
          '-1' AS country_iso_code,
          '000' AS country_iso_num_code,
          -1 AS processor_client_code,
          -1 AS geo_region_id,
          -1 AS geo_subregion_id,
          'N/A' AS geo_region_name,
          'N/A' AS geo_subregion_name,
          -1 AS sub_country_id,
          'N/A' AS sub_country_name,
           NULL migrated_scp,
           NULL exclude_matching
     FROM DUAL;


CREATE OR REPLACE SYNONYM MCP_ISSUER_REP.D_CLIENT_HIERARCHY FOR MCP_ISSUER_REP.VW_CLIENT_HIERARCHY;

