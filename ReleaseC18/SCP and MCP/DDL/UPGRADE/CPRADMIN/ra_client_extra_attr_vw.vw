/* Formatted on 2/9/2018 1:24:01 PM (QP5 v5.227.12220.39724) */
DROP VIEW CPRADMIN.RA_CLIENT_EXTRA_ATTR_VW;


CREATE OR REPLACE FORCE VIEW CPRADMIN.RA_CLIENT_EXTRA_ATTR_VW
/* *********************************************************
 REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.1        08/02/2018  Swati      Added two columns MIGRATED_SCP,EXCLUDE_MATCHING  for Feature F57475 SCP and MCP Merge
************************************************************/
(
   ID,
   RA_CLIENT_ID,
   BIN_SPONSOR_ID,
   CLIENT_MCP_FLAG,
   CLIENT_EXCEL_INDICATOR,
   CLIENT_SETTLEMENT_FORMAT,
   INCLUDE_REV_NET_SETTLE,
   FEES_COMM_INV_TEMPLATE,
   ALTERNATE_ID,
   CARDNUMBER_REQ,
   PROXY_CARDNBR_REQ,
   TRANSACTION_TIMESTAMP_REQ,
   LAST_UPDATED,
   LAST_UPDATED_BY,
   AUDIT_GROUP_ID,
   MIGRATED_SCP,
   EXCLUDE_MATCHING
)
AS
   SELECT ID,
          RA_CLIENT_ID,
          BIN_SPONSOR_ID,
          CLIENT_MCP_FLAG,
          CLIENT_EXCEL_INDICATOR,
          CLIENT_SETTLEMENT_FORMAT,
          INCLUDE_REV_NET_SETTLE,
          FEES_COMM_INV_TEMPLATE,
          ALTERNATE_ID,
          CARDNUMBER_REQ,
          PROXY_CARDNBR_REQ,
          TRANSACTION_TIMESTAMP_REQ,
          LAST_UPDATED,
          LAST_UPDATED_BY,
          AUDIT_GROUP_ID,
          MIGRATED_SCP,
          EXCLUDE_MATCHING
     FROM RA_CLIENT_EXTRA_ATTR;