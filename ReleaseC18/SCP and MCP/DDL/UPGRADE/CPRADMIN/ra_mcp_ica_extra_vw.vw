DROP VIEW CPRADMIN.RA_MCP_ICA_EXTRA_VW;

/* Formatted on 2/9/2018 1:12:23 PM (QP5 v5.227.12220.39724) */
CREATE OR REPLACE FORCE VIEW CPRADMIN.RA_MCP_ICA_EXTRA_VW
/* *********************************************************
 REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.1        08/02/2018  Swati      Added one column MIGRATED_SCP  for Feature F57475 SCP and MCP Merge
************************************************************/
(
   ID,
   BIN_SPONSOR_ID,
   ICA_NBR,
   IS_PRIMARY_ICA,
   COUNTRY_CODE,
   CREATED_DATE,
   LAST_UPDATED_DATE,
   LAST_UPDATED_BY,
   MIGRATED_SCP
)
AS
   SELECT id,
          bin_sponsor_id,
          ica_nbr,
          is_primary_ica,
          country_code,
          created_date,
          last_updated_date,
          last_updated_by,
          MIGRATED_SCP
     FROM ra_mcp_ica_extra;