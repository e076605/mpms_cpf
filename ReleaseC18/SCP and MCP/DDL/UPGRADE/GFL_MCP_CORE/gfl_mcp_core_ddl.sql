DECLARE
v_cnt_check NUMBER;
BEGIN
SELECT count(1) into v_cnt_check
from all_tab_columns
where column_name = 'MIGRATED_SCP'
and table_name = 'ICA_AGREEMENT'
and OWNER = 'GFL_MCP_CORE';

IF v_cnt_check = 0 THEN


EXECUTE IMMEDIATE 'ALTER TABLE gfl_mcp_core.ica_agreement ADD ( migrated_scp  VARCHAR2(1) default ''N'')';


END IF;
exception
When others then
dbms_output.put_line(SQLERRM||'  Column creation failed for table  GFL_MCP_CORE.ICA_AGREEMENT');
End;

/



DECLARE
v_cnt_check NUMBER;
BEGIN
SELECT count(1) into v_cnt_check
from all_tab_columns
where column_name = 'MIGRATED_SCP'
and table_name = 'CLIENT'
and OWNER = 'GFL_MCP_CORE';

IF v_cnt_check = 0 THEN


EXECUTE IMMEDIATE 'ALTER TABLE gfl_mcp_core.client  ADD (MIGRATED_SCP VARCHAR2(1) default ''N'')';


END IF;
exception
When others then
dbms_output.put_line(SQLERRM||'  Column creation failed for table  GFL_MCP_CORE.CLIENT');
End;

/


DECLARE
v_cnt_check NUMBER;
BEGIN
SELECT count(1) into v_cnt_check
from all_tab_columns
where column_name = 'EXCLUDE_MATCHING'
and table_name = 'CLIENT'
and OWNER = 'GFL_MCP_CORE';

IF v_cnt_check = 0 THEN


EXECUTE IMMEDIATE 'ALTER TABLE gfl_mcp_core.client  ADD (EXCLUDE_MATCHING VARCHAR2(1) default ''N'')';


END IF;
exception
When others then
dbms_output.put_line(SQLERRM||'  Column creation failed for table  GFL_MCP_CORE.CLIENT');
End;

/




