DROP VIEW GFL_MCP_DATA.VW_PREPAID_POOL_BAL_REC_TXNS;

/* Formatted on 2/8/2018 11:30:01 AM (QP5 v5.227.12220.39724) */
CREATE OR REPLACE FORCE VIEW GFL_MCP_DATA.VW_PREPAID_POOL_BAL_REC_TXNS
(
   DATA_SOURCE,
   ICA,
   CURRENCY_ALPHA,
   RECONCILIATION_DATE,
   TRANSACTION_DESC,
   TRANSACTION_AMOUNT,
   PARSER_RUN_ID,
   PARSER_RUN_DATE
)
AS
   SELECT report_number AS DATA_SOURCE,
          ica,
          purse_currency_alpha AS currency_alpha,
          reconciliation_date,
          (CASE
              WHEN TRANSACTION_DESC = 'ACCNT_REPLSHMNT_RELOAD_AMOUNT'
              THEN
                 'ACCOUNT REPLENISHMENT (RELOAD)'
              WHEN TRANSACTION_DESC = 'TRANSNS_PROCESS_OTP_REJ_AMNT'
              THEN
                 'TRANSACTIONS PROCESSED OTP REJ'
              WHEN TRANSACTION_DESC = 'CARDHLDR_ADJ_CRED_RECON_AMNT'
              THEN
                 'CARDHOLDER ADJUSTMENTS - CREDIT (RECON)'
              WHEN TRANSACTION_DESC = 'CARDHLDR_ADJ_CRED_EXCP_AMOUNT'
              THEN
                 'CARDHOLDER ADJUSTMENTS - CREDIT (EXCP)'
              WHEN TRANSACTION_DESC = 'CARDHLDR_ADJ_CRED_MISC_AMOUNT'
              THEN
                 'CARDHOLDER ADJUSTMENTS - CREDIT (MISC)'
              WHEN TRANSACTION_DESC = 'CARDHLDR_ADJ_CRED_P2P_TRF_AMNT'
              THEN
                 'CARDHOLDER ADJUSTMENTS - CREDIT P2P TRF'
              WHEN TRANSACTION_DESC = 'CARDHLDR_ADJ_CRED_P2P_TRF_F_AM'
              THEN
                 'CARDHOLDER ADJUSTMENTS - CREDIT P2P TRF FEE'
              WHEN TRANSACTION_DESC = 'CARDHLDR_ADJ_DEB_EXCP_AMNT'
              THEN
                 'CARDHOLDER ADJUSTMENTS - DEBIT (EXCP)'
              WHEN TRANSACTION_DESC = 'CARDHLDR_ADJ_DEB_MISC_AMNT'
              THEN
                 'CARDHOLDER ADJUSTMENTS - DEBIT (MISC)'
              WHEN TRANSACTION_DESC = 'CARDHLDR_ADJ_DEB_RECON_AMNT'
              THEN
                 'CARDHOLDER ADJUSTMENTS - DEBIT (RECON)'
              WHEN TRANSACTION_DESC = 'CARDHLDR_ADJ_DEB_P2P_TRF_AMNT'
              THEN
                 'CARDHOLDER ADJUSTMENTS - DEBIT P2P TRF'
              WHEN TRANSACTION_DESC = 'CARDHLDR_ADJ_DEB_P2P_TRF_F_AMT'
              THEN
                 'CARDHOLDER ADJUSTMENTS - DEBIT P2P TRF FEE'
              WHEN TRANSACTION_DESC = 'CARDHOLDER_FEES_AMNT'
              THEN
                 'CARDHOLDER FEES.'
              WHEN TRANSACTION_DESC = 'CARDS_LOAD_AMOUNT'
              THEN
                 'CARDS LOAD'
              WHEN TRANSACTION_DESC = 'CASHOUT_UNLOAD_AMOUNT'
              THEN
                 'CASHOUT (UNLOAD)'
              WHEN TRANSACTION_DESC = 'ENDING_POOL_BALANCE_AMNT'
              THEN
                 'ENDING POOL BALANCE'
              WHEN TRANSACTION_DESC = 'FOREIGN_EXC_CONV_FEE_AMNT'
              THEN
                 'FOREIGN EXCHANGE CONVERSION FEE'
              WHEN TRANSACTION_DESC = 'FORGN_EXCH_CROSS_BRDR_FEE_AMNT'
              THEN
                 'FOREIGN EXCH CROSS BORDER FEE'
              WHEN TRANSACTION_DESC = 'NETWORK_LOAD_AMNT'
              THEN
                 'NETWORK LOAD'
              WHEN TRANSACTION_DESC = 'TODAYS_OTP_REJ_CCA_AMT_AMNT'
              THEN
                 'TODAYS OTP REJECTS - CCA  AMT'
              WHEN TRANSACTION_DESC = 'TODAYS_OTP_REJ_TRAN_AMT_AMNT'
              THEN
                 'TODAYS OTP REJECTS - TRAN AMT'
              WHEN TRANSACTION_DESC = 'TRANSACTIONS_PROCESSED_AMNT'
              THEN
                 'TRANSACTIONS PROCESSED'
              WHEN TRANSACTION_DESC = 'TRANSFERS_AMOUNT'
              THEN
                 'TRANSFERS'
              WHEN TRANSACTION_DESC = 'BEG_POOL_BALANCE_AMOUNT'
              THEN
                 'BEGINNING POOL BALANCE'
              WHEN TRANSACTION_DESC = 'TRANSFERS_CREDIT_AMNT'
              THEN
                 'TRANSFERS_CREDIT_DESC'
              WHEN TRANSACTION_DESC = 'TRANSFERS_DEBIT_AMNT'
              THEN
                 'TRANSFERS_DEBIT_DESC'
           END)
             TRANSACTION_DESC,
          NVL (transaction_amount, 0) AS transaction_amount,
          run_id AS parser_run_id,
          RUN_DATE_TIME AS PARSER_RUN_DATE
     FROM (SELECT /*+ no_merge */
                 run_id,
                  report_number,
                  COMP_DIST_ID,
                  ica,
                  RECONCILIATION_DATE,
                  purse_currency_alpha,
                  NVL (ACCNT_REPLSHMNT_RELOAD_AMOUNT, 0)
                     AS ACCNT_REPLSHMNT_RELOAD_AMOUNT,
                  NVL (BEG_POOL_BALANCE_AMOUNT, 0) AS BEG_POOL_BALANCE_AMOUNT,
                  NVL (CARDHLDR_ADJ_CRED_RECON_AMNT, 0)
                     AS CARDHLDR_ADJ_CRED_RECON_AMNT,
                  NVL (CARDHLDR_ADJ_CRED_EXCP_AMOUNT, 0)
                     AS CARDHLDR_ADJ_CRED_EXCP_AMOUNT,
                  NVL (CARDHLDR_ADJ_CRED_MISC_AMOUNT, 0)
                     AS CARDHLDR_ADJ_CRED_MISC_AMOUNT,
                  NVL (CARDHLDR_ADJ_CRED_P2P_TRF_AMNT, 0)
                     AS CARDHLDR_ADJ_CRED_P2P_TRF_AMNT,
                  NVL (CARDHLDR_ADJ_CRED_P2P_TRF_F_AM, 0)
                     AS CARDHLDR_ADJ_CRED_P2P_TRF_F_AM,
                  NVL (CARDHLDR_ADJ_DEB_EXCP_AMNT, 0)
                     AS CARDHLDR_ADJ_DEB_EXCP_AMNT,
                  NVL (CARDHLDR_ADJ_DEB_MISC_AMNT, 0)
                     AS CARDHLDR_ADJ_DEB_MISC_AMNT,
                  NVL (CARDHLDR_ADJ_DEB_RECON_AMNT, 0)
                     AS CARDHLDR_ADJ_DEB_RECON_AMNT,
                  NVL (CARDHLDR_ADJ_DEB_P2P_TRF_AMNT, 0)
                     AS CARDHLDR_ADJ_DEB_P2P_TRF_AMNT,
                  NVL (CARDHLDR_ADJ_DEB_P2P_TRF_F_AMT, 0)
                     AS CARDHLDR_ADJ_DEB_P2P_TRF_F_AMT,
                  NVL (CARDHOLDER_FEES_AMNT, 0) AS CARDHOLDER_FEES_AMNT,
                  NVL (CARDS_LOAD_AMOUNT, 0) AS CARDS_LOAD_AMOUNT,
                  NVL (CASHOUT_UNLOAD_AMOUNT, 0) AS CASHOUT_UNLOAD_AMOUNT,
                  NVL (ENDING_POOL_BALANCE_AMNT, 0)
                     AS ENDING_POOL_BALANCE_AMNT,
                  NVL (FOREIGN_EXC_CONV_FEE_AMNT, 0)
                     AS FOREIGN_EXC_CONV_FEE_AMNT,
                  NVL (FORGN_EXCH_CROSS_BRDR_FEE_AMNT, 0)
                     AS FORGN_EXCH_CROSS_BRDR_FEE_AMNT,
                  NVL (NETWORK_LOAD_AMNT, 0) AS NETWORK_LOAD_AMNT,
                  NVL (TODAYS_OTP_REJ_CCA_AMT_AMNT, 0)
                     AS TODAYS_OTP_REJ_CCA_AMT_AMNT,
                  NVL (TODAYS_OTP_REJ_TRAN_AMT_AMNT, 0)
                     AS TODAYS_OTP_REJ_TRAN_AMT_AMNT,
                  NVL (TRANSACTIONS_PROCESSED_AMNT, 0)
                     AS TRANSACTIONS_PROCESSED_AMNT,
                  NVL (TRANSFERS_AMOUNT, 0) AS TRANSFERS_AMOUNT,
                  NVL (TRANSNS_PROCESS_OTP_REJ_AMNT, 0)
                     AS TRANSNS_PROCESS_OTP_REJ_AMNT,
                  NVL (TRANSFERS_CREDIT_AMNT, 0) AS TRANSFERS_CREDIT_AMNT,
                  NVL (TRANSFERS_DEBIT_AMNT, 0) AS TRANSFERS_DEBIT_AMNT,
                  run_date_time
             FROM (SELECT src.*,
                          RANK ()
                          OVER (PARTITION BY RECONCILIATION_DATE, run_id
                                ORDER BY RECONCILIATION_DATE, run_id DESC)
                             rnk
                     FROM PREPAID_POOL_BAL_RECONC_DTL src           --IB284010
                                                         )
            WHERE rnk = 1) UNPIVOT (transaction_amount
                           FOR transaction_desc
                           IN  (accnt_replshmnt_reload_amount,
                               beg_pool_balance_amount,
                               cardhldr_adj_cred_recon_amnt,
                               cardhldr_adj_cred_excp_amount,
                               cardhldr_adj_cred_misc_amount,
                               cardhldr_adj_cred_p2p_trf_amnt,
                               cardhldr_adj_cred_p2p_trf_f_am,
                               cardhldr_adj_deb_excp_amnt,
                               cardhldr_adj_deb_misc_amnt,
                               cardhldr_adj_deb_recon_amnt,
                               cardhldr_adj_deb_p2p_trf_amnt,
                               cardhldr_adj_deb_p2p_trf_f_amt,
                               cardholder_fees_amnt,
                               cards_load_amount,
                               cashout_unload_amount,
                               ending_pool_balance_amnt,
                               foreign_exc_conv_fee_amnt,
                               forgn_exch_cross_brdr_fee_amnt,
                               network_load_amnt,
                               todays_otp_rej_cca_amt_amnt,
                               todays_otp_rej_tran_amt_amnt,
                               transactions_processed_amnt,
                               transfers_amount,
                               transns_process_otp_rej_amnt,
                               transfers_credit_amnt,
                               transfers_debit_amnt));


CREATE OR REPLACE SYNONYM RAL_MCP_DATA.VW_PREPAID_POOL_BAL_REC_TXNS FOR GFL_MCP_DATA.VW_PREPAID_POOL_BAL_REC_TXNS;


GRANT SELECT ON GFL_MCP_DATA.VW_PREPAID_POOL_BAL_REC_TXNS TO GFL_MCP_DATA_READ_RL;

GRANT SELECT ON GFL_MCP_DATA.VW_PREPAID_POOL_BAL_REC_TXNS TO GFL_MCP_DATA_USR_RL;

GRANT SELECT ON GFL_MCP_DATA.VW_PREPAID_POOL_BAL_REC_TXNS TO RAL_MCP_DATA;
