CREATE OR REPLACE PROCEDURE CPRADMIN.pr_syn_ra_mcp_ica_extra
(
  i_unique_id NUMBER,
  i_audit_group_id NUMBER
)
IS

  v_pk_column_name VARCHAR2(30);
BEGIN
  FOR c1 IN
  (
    SELECT *
      FROM ra_mcp_ica_extra_a
     WHERE id = i_unique_id
      AND audit_group_id = i_audit_group_id
  )
  LOOP
    IF c1.dml_type = 'I' THEN
      INSERT INTO ra_mcp_ica_extra
      (
        id,
        bin_sponsor_id,
        ica_nbr,
        is_primary_ica,
        country_code,
        audit_group_id
    )
      VALUES
      (
        c1.id,
        c1.bin_sponsor_id,
        c1.ica_nbr,
        c1.is_primary_ica,
        c1.country_code,
        c1.audit_group_id
     );
    ELSIF c1.dml_type = 'U' THEN
      UPDATE ra_mcp_ica_extra
      SET audit_group_id = c1.audit_group_id,
          bin_sponsor_id = c1.bin_sponsor_id,
          ica_nbr = c1.ica_nbr,
          is_primary_ica = c1.is_primary_ica,
          country_code = c1.country_code
    WHERE id = i_unique_id;
    
    ELSIF c1.dml_type = 'D' THEN
      DELETE FROM ra_mcp_ica_extra
        WHERE id = i_unique_id;
    END IF;
  END LOOP;
END;
/
