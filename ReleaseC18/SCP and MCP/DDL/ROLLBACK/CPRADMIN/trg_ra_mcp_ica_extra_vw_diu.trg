DROP TRIGGER CPRADMIN.TRG_RA_MCP_ICA_EXTRA_VW_DIU;

CREATE OR REPLACE TRIGGER CPRADMIN.trg_ra_mcp_ica_extra_vw_diu
INSTEAD OF INSERT OR UPDATE OR DELETE ON CPRADMIN.RA_MCP_ICA_EXTRA_VW
FOR EACH ROW
DECLARE
  v_dml_type VARCHAR2(1);
  v_locked VARCHAR2(1);
BEGIN
  IF INSERTING THEN
    v_dml_type := 'I';
  ELSIF UPDATING THEN
    v_dml_type := 'U';
    v_locked := static_data_auth_utils.get_locked_status('RA_MCP_ICA_EXTRA_VW', :NEW.id);
  ELSIF DELETING THEN
    v_dml_type := 'D';
    v_locked := static_data_auth_utils.get_locked_status('RA_MCP_ICA_EXTRA_VW', :OLD.id);
  END IF;

  IF v_locked='Y' THEN
    RAISE_APPLICATION_ERROR(-20001,'RECORD IS LOCKED, THERE IS A PENDING CHANGE');
  END IF;

  IF INSERTING OR UPDATING THEN
    INSERT INTO ra_mcp_ica_extra_a
    (
      id,
      bin_sponsor_id,
      ica_nbr,
      is_primary_ica,
      country_code,
      dml_type
    )
    VALUES
    (
      :NEW.id,
      :NEW.bin_sponsor_id,
      :NEW.ica_nbr,
      :NEW.is_primary_ica,
      :NEW.country_code,
      v_dml_type
    );
  ELSIF DELETING THEN
    INSERT INTO ra_mcp_ica_extra_a
    (
      id,
      bin_sponsor_id,
      ica_nbr,
      is_primary_ica,
      country_code,
      dml_type
    )
    VALUES
    (
      :NEW.id,
      :NEW.bin_sponsor_id,
      :NEW.ica_nbr,
      :NEW.is_primary_ica,
      :NEW.country_code,
      v_dml_type
    );
  END IF;
END;
/
