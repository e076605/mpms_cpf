DROP VIEW CPRADMIN.RA_MCP_ICA_EXTRA_VW;

/* Formatted on 2/9/2018 8:02:13 PM (QP5 v5.227.12220.39724) */
CREATE OR REPLACE FORCE VIEW CPRADMIN.RA_MCP_ICA_EXTRA_VW
(
   ID,
   BIN_SPONSOR_ID,
   ICA_NBR,
   IS_PRIMARY_ICA,
   COUNTRY_CODE,
   CREATED_DATE,
   LAST_UPDATED_DATE,
   LAST_UPDATED_BY
)
AS
   SELECT id,
          bin_sponsor_id,
          ica_nbr,
          is_primary_ica,
          country_code,
          created_date,
          last_updated_date,
          last_updated_by
     FROM ra_mcp_ica_extra;
