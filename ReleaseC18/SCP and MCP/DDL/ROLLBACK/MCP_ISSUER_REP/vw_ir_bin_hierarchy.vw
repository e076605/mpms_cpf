DROP VIEW MCP_ISSUER_REP.VW_IR_BIN_HIERARCHY;

/* Formatted on 2/8/2018 12:41:10 PM (QP5 v5.227.12220.39724) */
CREATE OR REPLACE FORCE VIEW MCP_ISSUER_REP.VW_IR_BIN_HIERARCHY
(
   BIN,
   ICA_NBR,
   PARENT_BUSINESS_UNIT_NAME,
   CHILD_BUSINESS_UNIT_NAME,
   BIN_SPONSOR_NAME,
   BIN_SPONSOR_ID,
   IS_MCP_BIN_SPONSOR_FLAG,
   BIN_SPONSOR_TYPE_CODE,
   BIN_SPONSOR_COUNTRY_CODE,
   ICA_COUNTRY_CODE,
   BIN_COUNTRY_CODE,
   IS_PRIMARY_ICA,
   TOP_LEVEL_ICA,
   IS_PRIMARY_BIN
)
AS
   SELECT b.bin,
          i.ica_nbr,
          i.parent_business_unit_name,
          b.child_business_unit_name,
          bs.NAME bin_sponsor_name,
          bs.ID bin_sponsor_id,
          bs.mcp_flag is_mcp_bin_sponsor_flag,
          bs.bin_sponsor_type_code,
          bs.country_code AS bin_sponsor_country_code,
          i.country_code AS ica_country_code,                    --APWAS-17559
          b.country_code AS bin_country_code,                    --APWAS-17559
          NVL (i.is_primary_ica, 'N') AS is_primary_ica,
          MAX (CASE WHEN is_primary_ica = 'Y' THEN i.ica_nbr ELSE 0 END)
             OVER (PARTITION BY bs.ID)
             top_level_ica,
          b.is_primary_bin
     FROM ica_agreement i, bin b, bin_sponsor bs
    WHERE     bs.ID = i.bin_sponsor_id(+)
          AND i.bin_sponsor_id = b.bin_sponsor_id(+)
          AND i.ica_nbr = b.ica_nbr(+)
          -- As few Bin sponsor are there without ICA
          AND i.ica_nbr IS NOT NULL
          -- only mcp bin sponsors
          AND NVL (bs.mcp_flag, 'N') = 'Y'
   UNION ALL
   SELECT -1 AS bin,
          -1 AS ICA_nbr,
          'N/A' AS parent_business_unit_name,
          'N/A' AS child_bunsiness_unit_name,
          'N/A' AS bin_sponsor_name,
          -1 AS bin_sponsor_id,
          'Y' AS is_mcp_bin_sponsor_flag,
          '-1' AS bin_sponsor_type_code,
          '-1' AS bin_sponsor_country_code,
          '-1' AS ica_country_code,
          '-1' AS bin_country_code,
          'Y' AS is_primary_ica,
          -1 AS top_level_ica,
          'N' AS is_primary_bin
     FROM DUAL;


CREATE OR REPLACE SYNONYM MCP_ISSUER_REP.D_BIN_HIERARCHY FOR MCP_ISSUER_REP.VW_IR_BIN_HIERARCHY;


GRANT SELECT ON MCP_ISSUER_REP.VW_IR_BIN_HIERARCHY TO MCP_ISSUER_REP_READ_RL;

GRANT SELECT ON MCP_ISSUER_REP.VW_IR_BIN_HIERARCHY TO MCP_ISSUER_REP_USR_RL;
