CREATE OR REPLACE PACKAGE BODY GFL_MCP_CORE.client_data_etl AS

/********************************************************************************************************************
   PURPOSE:    To ETL data into MCP static data tables

   REVISIONS:
   Ver        Date        Author            Description
   ---------  ----------  ---------------  ------------------------------------
   2.0        04/10/2013    JT               Change Primary ICA to be set if any of its BINs have the flag set to Y.
   3.0        04/10/2013    JT               Added country code to bin_sponsor.
   4.0        26/02/2014    JT               Added 3 new columns BIN_SPONSORETL procedure (parent_business_unit,destination_id and is_mcp)
   5.0        04/10/2013    Srini            Added country code to bin_sponsor.
   6.0        17/04/2015    Madhu            Changed logic to populate ica_agreement's is primary ica flag, country code and moved the logic
                                             of populating child business unit from ica agreement to bin. Added logic to populate primary bin flag,
                                             country code in bin.
   7.0        11/07/2015    Dillip           Added alternate_id in the CLIENTETL procedure.
   8.0        27/07/2015    Renukapathy      Added new procedure CHANNEL_TYPEETL as part of APWAS-14632
   11.0       28/09/2015    Lavanya          Added the GFT contact method in BIN_SPONSOR_CONTACT_DATAETL-APWAS-15767
   4.0        30/11/2015    Srinivas         Added p_etl_date for pr_control to run it adhoc
   12.0       17/12/2015    Lavanya          Made changes to bin_sponsor_contact_dataetl as part of APWAS-16359
   13.0       05/02/2015    Lavanya          Made changes to populate cardnumber_req and proxy_card_nbr_req as part of CMP1182
   14.0       03/03/2016    Swapna           Made changes to populate the transaction_timestamp_req as part of CMP1191 Canada POST
   15.0       28/03/2017    Dillip           Made changes to populate the static table as part of SCP-MCP merger PI Feature P57.
   16.0       06/06/2017    Dillip           Made changes to Control Procedure as part of OWB to ODI Migration .
********************************************************************************************************************/

  ----------------------------------------------------------------------------------------------------------------------------------

  vEMessage      varchar2(32767);
  vRecordsRead   number(15)     := 0;
  vRecordsUpd    number(15)     := 0;
  vRecordsIns    number(15)     := 0;
  vRecordsErr    number(15)     := 0;
  vLastAmount    number(14,2)   := 0;
  vOffset        number         := 0;

  procedure control is

  begin

    standard_audit.open_audit_trail (
     p_namespace   => 'CLIENT_DATA_ETL',
     p_program     => 'client_data_etl.control',
     p_override    => TRUE);

    begin
     -- if p_etl_date IS NULL THEN
        vETL_DATE := utl_etl_control.get_current_etl_date;
     -- elsif p_etl_date IS NOT NULL THEN
     --   vETL_DATE := p_etl_date;
     -- end if;
        IF vETL_DATE IS NULL THEN
          SELECT MAX(etl_date) + 1
          into vETL_DATE
          from gfl_common_data.etl_control
          where etl_status = 'COMPLETED';
        END IF;
    exception
    When no_data_found then
      SELECT MAX(etl_date) + 1
      into vETL_DATE
      from gfl_common_data.etl_control
      where etl_status = 'COMPLETED';
    end;

    if vETL_DATE is null then
      standard_error.raise_program_error
      (
        p_program         => 'client_data_etl.control',
        p_message         => 'ETL date has not been set',
        p_additional_info => 'If running manually set client_data_etl.etl_date = required ETL date'
      );
    end if;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before RATESETL',
     p_log_entry   => 'Before RATESETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    RATESETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before CLIENT_CONTACTETL',
     p_log_entry   => 'Before CLIENT_CONTACTETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    CLIENT_CONTACTETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before CLIENT_CONTACT_DATAETL',
     p_log_entry   => 'Before CLIENT_CONTACT_DATAETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    CLIENT_CONTACT_DATAETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before BANKETL',
     p_log_entry   => 'Before BANKETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    BANKETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before BIN_SPONSORETL',
     p_log_entry   => 'Before BIN_SPONSORETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    BIN_SPONSORETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before LEGAL_ENTITYETL',
     p_log_entry   => 'Before LEGAL_ENTITYETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    LEGAL_ENTITYETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before ORG_REGIONETL',
     p_log_entry   => 'Before ORG_REGIONETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    ORG_REGIONETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before BIN_SPONSOR_BANK_ACCOUNTETL',
     p_log_entry   => 'Before BIN_SPONSOR_BANK_ACCOUNTETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    BIN_SPONSOR_BANK_ACCOUNTETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before BIN_SPONSOR_CONTACTETL',
     p_log_entry   => 'Before BIN_SPONSOR_CONTACTETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    BIN_SPONSOR_CONTACTETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before CLIENT_GROUPETL',
     p_log_entry   => 'Before CLIENT_GROUPETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    CLIENT_GROUPETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before ICA_AGREEMENTETL',
     p_log_entry   => 'Before ICA_AGREEMENTETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    ICA_AGREEMENTETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before LEGAL_ENTITY_BANK_ACCOUNTETL',
     p_log_entry   => 'Before LEGAL_ENTITY_BANK_ACCOUNTETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    LEGAL_ENTITY_BANK_ACCOUNTETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before BINETL',
     p_log_entry   => 'Before BINETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    BINETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before BIN_SPONSOR_CONTACT_DATAETL',
     p_log_entry   => 'Before BIN_SPONSOR_CONTACT_DATAETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    BIN_SPONSOR_CONTACT_DATAETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before CLIENTETL',
     p_log_entry   => 'Before CLIENTETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    CLIENTETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before CLIENT_BANK_ACCOUNTETL',
     p_log_entry   => 'Before CLIENT_BANK_ACCOUNTETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    CLIENT_BANK_ACCOUNTETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before CLIENT_CODINGETL',
     p_log_entry   => 'Before CLIENT_CODINGETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    CLIENT_CODINGETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before CLIENT_CONTACT_GROUPETL',
     p_log_entry   => 'Before CLIENT_CONTACT_GROUPETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    CLIENT_CONTACT_GROUPETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before CLIENT_CONTRACTETL',
     p_log_entry   => 'Before CLIENT_CONTRACTETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    CLIENT_CONTRACTETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before CLIENT_CONTRACT_PARTETL',
     p_log_entry   => 'Before CLIENT_CONTRACT_PARTETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    CLIENT_CONTRACT_PARTETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before invoice_consolidationETL',
     p_log_entry   => 'Before invoice_consolidationETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    invoice_consolidationETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before TRUSTETL',
     p_log_entry   => 'Before TRUSTETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    TRUSTETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before MASTERCARD_HEADERETL',
     p_log_entry   => 'Before MASTERCARD_HEADERETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    MASTERCARD_HEADERETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before INVOICE_FEE_GROUPETL',
     p_log_entry   => 'Before INVOICE_FEE_GROUPETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    INVOICE_FEE_GROUPETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before BUDGET_FORECAST_EX_RATESETL',
     p_log_entry   => 'Before BUDGET_FORECAST_EX_RATESETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    BUDGET_FORECAST_EX_RATESETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before SIX_MONTHLY_CONFIG',
     p_log_entry   => 'Before SIX_MONTHLY_CONFIG - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    SIX_MONTHLY_CONFIG;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before COUNTRY_ETL',
     p_log_entry   => 'Before COUNTRY_ETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    COUNTRY_ETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before INVOICE_CLIENT_FEE_ETL',
     p_log_entry   => 'Before INVOICE_CLIENT_FEE_ETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);


    INVOICE_CLIENT_FEE_ETL;


    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before INV_CLIENT_FEE_COMM_SHARE_ETL',
     p_log_entry   => 'Before INV_CLIENT_FEE_COMM_SHARE_ETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    INV_CLIENT_FEE_COMM_SHARE_ETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before CURRENCY_ETL',
     p_log_entry   => 'Before CURRENCY_ETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);


    CURRENCY_ETL;  --CMP0769 Changes

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before CFG_IPS_CURRENCY_CODES_ETL',
     p_log_entry   => 'Before CFG_IPS_CURRENCY_CODES_ETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    CFG_IPS_CURRENCY_CODES_ETL; --CMP0769 Changes

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before CLEAN_SHARE_AGREEMENT',
     p_log_entry   => 'Before CLEAN_SHARE_AGREEMENT - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    -- Revenue Share Routines
    CLEAN_SHARE_AGREEMENT;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before REV_SHARE_AGREEMENTETL',
     p_log_entry   => 'Before REV_SHARE_AGREEMENTETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    REV_SHARE_AGREEMENTETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before REV_SHARE_AGMT_ICAETL',
     p_log_entry   => 'Before REV_SHARE_AGMT_ICAETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    REV_SHARE_AGMT_ICAETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before STD_REV_COMPONENTETL',
     p_log_entry   => 'Before STD_REV_COMPONENTETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    STD_REV_COMPONENTETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before STD_REV_ITEMETL',
     p_log_entry   => 'Before STD_REV_ITEMETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    STD_REV_ITEMETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before STD_REV_ITEM_COMPONENTETL',
     p_log_entry   => 'Before STD_REV_ITEM_COMPONENTETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    STD_REV_ITEM_COMPONENTETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before REV_SHARE_AGMT_ITEMETL',
     p_log_entry   => 'Before REV_SHARE_AGMT_ITEMETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    REV_SHARE_AGMT_ITEMETL;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before create_invoice_groups_daily',
     p_log_entry   => 'Before create_invoice_groups_daily - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    -- Re-create the invoice_groups_daily
    create_invoice_groups_daily;

    standard_audit.log_entry (
     p_program     => 'client_data_etl.control',
     p_step_name   => 'Before CHANNEL_TYPEETL',
     p_log_entry   => 'Before CHANNEL_TYPEETL - vETL_DATE : '||vETL_DATE,
     p_level       => 3);

    -- Added new procedure CHANNEL_TYPEETL as part of APWAS-14632
    CHANNEL_TYPEETL;
   --OWB to ODI Migration
    UPDATE STORED_PARAMETER_VALUE
    SET VALUE = vETL_DATE
    WHERE  NAMESPACE ='MCP_CLIENT_DATA_ETL'
    AND NAME ='LAST_PROCESSED_CLIENT_ETL_DATE';

   --OWB to ODI Migration

    standard_audit.close_audit_trail (
         p_program => 'client_data_etl.control');

  exception
    when others then
    standard_audit.close_audit_trail (
         p_program => 'client_data_etl.control');
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.control',
        p_additional_info => 'vETL_DATE: '||vETL_DATE
      );

  end control;

  procedure invoice_consolidationETL is

   cursor acur is
   select
     IG.INVOICE_GROUP_ID         AS group_id
     ,c.id                       AS client_id
     ,ig.group_name              AS description
     ,ig.primary_client_id       AS client_to_bill
     ,ig.last_updated            AS last_updated
     ,ig.last_updated_by         AS last_updated_by
   from
     CLIENT C
    ,RA_CLIENT RC
    ,RA_INVOICE_GROUP IG
   where
         C.ID = RC.RDS_CLIENT_ID
     and RC.INVOICE_GROUP_ID = IG.INVOICE_GROUP_ID;

    fdata          acur%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

    execute immediate 'truncate table invoice_consolidation';

    open acur;
    fetch acur into fdata;
    while acur%found
    loop
      vRecordsRead := vRecordsRead + 1;
      begin

        insert into invoice_consolidation
          (GROUP_ID,CLIENT_ID,DESCRIPTION,CLIENT_TO_BILL,LAST_UPDATED, LAST_UPDATED_BY)
        values
          (fdata.GROUP_ID,fdata.CLIENT_ID,fdata.DESCRIPTION,fdata.CLIENT_TO_BILL,fdata.LAST_UPDATED, fdata.LAST_UPDATED_BY);

        commit;

        vRecordsIns := vRecordsIns + 1;

      exception
          when others then
            vEMessage := 'INVOICE_CONSOLIDATION - ' || sqlerrm;
            ins_txn_err(fdata.group_id,vEMessage);
            vRecordsErr := vRecordsErr + 1;
      end;

      fetch acur into fdata;
    end loop;
    close acur;
    ins_extract_recon('INVOICE_CONSOLIDATION',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
    commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.invoice_consolidationETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end invoice_consolidationETL;

  procedure RATESETL
  is
    IncorrectRates   exception;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

    select
      count(1)
    into
      vRecordsRead
    from
      re_fx_spot_rates
    where
      date_rate_applicable = vETL_DATE;

    if vRecordsRead = 0 then
      raise IncorrectRates;
      vRecordsErr := 40;
    else
      delete from fx_rate
      where rate_date = vETL_DATE;

      insert into fx_rate
      (RATE_DATE
        ,RATE
        ,CURRENCY_FOR_CODE
        ,CURRENCY_AGAINST_CODE
        ,RATE_TYPE_CODE)
      select
        DATE_RATE_APPLICABLE
        ,GRS_RATE
        ,CURRENCY_FOR
        ,CURRENCY_AGAINST
        ,'G'
      from
        re_fx_spot_rates
      where
        date_rate_applicable = vETL_DATE;

      vRecordsIns := SQL%ROWCOUNT;
    end if;

    ins_extract_recon('RATES',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
    commit;

  exception
    when IncorrectRates then
      vEMessage := 'Rates - Wrong number of rates: ' || vRecordsRead;
      ins_txn_err(to_char(vETL_DATE),vEMessage);
      vRecordsErr := vRecordsErr + 1;
      standard_error.raise_program_error
      (
        p_program         => 'client_data_etl.RATESETL',
        p_message         => vEMessage,
        p_additional_info => 'vETL_DATE: '||vETL_DATE
      );

    when others then
      vEMessage := 'Rates - ' || sqlerrm;
      ins_txn_err(to_char(vETL_DATE),vEMessage);
      vRecordsErr := vRecordsErr + 1;
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.RATESETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE
      );

  end RATESETL;

  procedure BINETL
  is
    cursor acur is
    select
      src.bin
     ,nvl(trg.bin,0)                 as target_bin
     ,trg.rowid                      as target_rowid
     ,src.reference_name
     ,src.status_flag
     ,src.settelement_type_flag
     ,src.bin_division_flag
     ,src.use_margin_flag
     ,src.margin
     ,src.mc_report_link
     ,src.card_network_provider_code
     ,src.org_region_code
     ,src.bin_sponsor_id
     ,src.processor_id
     ,src.ica_nbr
     ,src.child_business_unit_name
     ,src.is_primary_bin
     ,src.country_iso
     ,src.bin_currency  -- SCP-MCP merger PI Feature P57
     ,src.gl_project_id  -- SCP-MCP merger PI Feature P57
     ,src.top_level_ica  -- SCP-MCP merger PI Feature P57
     ,src.last_updated  -- SCP-MCP merger PI Feature P57
     ,src.last_updated_by  -- SCP-MCP merger PI Feature P57
    from
    (
      SELECT
        b.bin bin
        ,b.bin_reference_name reference_name
        ,SUBSTR(b.active_status,1,1) status_flag
        ,SUBSTR(b.settlement_type,1,1) settelement_type_flag
        ,SUBSTR(b.bin_division_name,1,1) bin_division_flag
        ,b.use_bin_margin_flag use_margin_flag
        ,b.bin_margin margin
        ,b.mastercard_legal_entity2 mc_report_link
        ,DECODE(b.scheme_id,1,'VS',2,'MC','3','CU') card_network_provider_code
        ,CASE s.ra_country_iso
           WHEN 'GB' THEN UPPER(REPLACE(SUBSTR(s.sub_country_name,1,5),' ',''))
           ELSE s.ra_country_iso
         END  org_region_code
        ,b.bin_sponsor_id bin_sponsor_id
        ,b.processor_id processor_id
        ,b.ica_number ica_nbr
        ,rbe.child_business_unit_name
        ,rbe.is_primary_bin
        ,b.country_iso
        ,b.bin_currency  -- SCP-MCP merger PI Feature P57
        ,b.gl_project_id -- SCP-MCP merger PI Feature P57
        ,b.top_level_ica  -- SCP-MCP merger PI Feature P57
        ,b.last_updated  -- SCP-MCP merger PI Feature P57
        ,b.last_updated_by     -- SCP-MCP merger PI Feature P57
      FROM ra_bin b
        ,ra_org_sub_country s
        ,ra_bin_extra_attr rbe
      WHERE b.bin_sponsor_id IS NOT NULL
      AND b.sub_country_id = s.sub_country_id
      AND rbe.bin(+) = b.bin
    ) src
      ,bin   trg
    where
      src.bin = trg.bin(+)
      and not exists
      (
        select 1
        from   bin
        where
          bin                             = src.bin
          and reference_name              = src.reference_name
          and status_flag                 = src.status_flag
          and settelement_type_flag       = src.settelement_type_flag
          and bin_division_flag           = src.bin_division_flag
          and use_margin_flag             = src.use_margin_flag
          and nvl(margin,-999)              = nvl(src.margin,-999)
          and nvl(mc_report_link,' ')     = nvl(src.mc_report_link,' ')
          and card_network_provider_code  = src.card_network_provider_code
          and org_region_code             = src.org_region_code
          and bin_sponsor_id              = src.bin_sponsor_id
          and processor_id                = src.processor_id
          and nvl(ica_nbr,0)              = nvl(src.ica_nbr,0)
          and nvl(child_business_unit_name, ' ') = nvl(src.child_business_unit_name, ' ')
          and nvl(is_primary_bin, ' ')    = nvl(src.is_primary_bin, ' ')
          and nvl(country_code, ' ')      = nvl(src.country_iso, ' ')
          and bin_currency                = src.bin_currency    -- SCP-MCP merger PI Feature P57
          and gl_project_id               = src.gl_project_id    -- SCP-MCP merger PI Feature P57
          and top_level_ica               = src.top_level_ica   -- SCP-MCP merger PI Feature P57
          and last_updated                = src.last_updated     -- SCP-MCP merger PI Feature P57
          and last_updated_by             = src.last_updated_by   -- SCP-MCP merger PI Feature P57
      );

    fdata          acur%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

    open acur;
    fetch acur into fdata;
    while acur%found
    loop

      vRecordsRead := vRecordsRead + 1;

      if fdata.target_bin <> 0
      then
        begin

          update
            bin t
          set
             t.reference_name          = fdata.reference_name
            ,t.status_flag             = fdata.status_flag
            ,t.settelement_type_flag   = fdata.settelement_type_flag
            ,t.bin_division_flag       = fdata.bin_division_flag
            ,t.use_margin_flag         = fdata.use_margin_flag
            ,t.margin                  = fdata.margin
            ,t.mc_report_link          = fdata.mc_report_link
            ,t.card_network_provider_code = fdata.card_network_provider_code
            ,t.org_region_code         = fdata.org_region_code
            ,t.bin_sponsor_id          = fdata.bin_sponsor_id
            ,t.processor_id            = fdata.processor_id
            ,t.ica_nbr                 = fdata.ica_nbr
            ,t.child_business_unit_name = fdata.child_business_unit_name
            ,t.is_primary_bin          = fdata.is_primary_bin
            ,t.country_code            = fdata.country_iso
            ,t.bin_currency            = fdata.bin_currency   -- SCP-MCP merger PI Feature P57
            ,t.gl_project_id           = fdata.gl_project_id  -- SCP-MCP merger PI Feature P57
            ,t.top_level_ica           = fdata.top_level_ica  -- SCP-MCP merger PI Feature P57
            ,t.last_updated            = fdata.last_updated   -- SCP-MCP merger PI Feature P57
            ,t.last_updated_by         = fdata.last_updated_by  -- SCP-MCP merger PI Feature P57
          where
            rowid = fdata.target_rowid;

          vRecordsUpd := vRecordsUpd + 1;

        exception
          when others then
            vEMessage := 'BIN - ' || sqlerrm;
            ins_txn_err(fdata.bin,vEMessage);
            vRecordsErr := vRecordsErr + 1;

        end;

      else

        begin
          insert into BIN
            (bin,reference_name,status_flag,settelement_type_flag
            ,bin_division_flag,use_margin_flag,margin,mc_report_link
            ,card_network_provider_code,org_region_code,bin_sponsor_id
            ,processor_id,ica_nbr, child_business_unit_name, is_primary_bin, country_code
            ,bin_currency,gl_project_id,top_level_ica,last_updated,last_updated_by)
          values
            (fdata.bin,fdata.reference_name,fdata.status_flag,fdata.settelement_type_flag
            ,fdata.bin_division_flag,fdata.use_margin_flag,fdata.margin,fdata.mc_report_link
            ,fdata.card_network_provider_code,fdata.org_region_code,fdata.bin_sponsor_id
            ,fdata.processor_id,fdata.ica_nbr, fdata.child_business_unit_name, fdata.is_primary_bin, fdata.country_iso
            ,fdata.bin_currency,fdata.gl_project_id,fdata.top_level_ica,fdata.last_updated,fdata.last_updated_by);  --SCP-MCP merger PI Feature P57

          vRecordsIns := vRecordsIns + 1;

        exception
          when others then
            vEMessage := 'BIN - ' || sqlerrm;
            ins_txn_err(fdata.bin,vEMessage);
            vRecordsErr := vRecordsErr + 1;

        end;

      end if;
      fetch acur into fdata;
    end loop;
    close acur;
    ins_extract_recon('BIN',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
    commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.BINETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end BINETL;

  procedure BANKETL
  is
    cursor acur is
    select
      xrf.id                     as xrf_id
     ,src.id                     as id
     ,nvl(trg.id,0)              as target_bank_id
     ,trg.rowid                  as target_rowid
     ,src.typ
     ,src.name
     ,src.business_name
     ,src.address_line_1
     ,src.address_line_2
     ,src.address_line_3
     ,src.city
     ,src.zip_post_code
     ,src.country_code
     ,src.sub_region_code
     ,src.sort_code
     ,src.swift_code
     ,src.specific_nacha_name
     ,src.routing_nbr
    from
      (select
         'MST'                           as typ
         ,RA_TRAVELEX_BANK_ACCOUNT_ID    as id
         ,BANK_NAME                      as name
         ,bank_name                      as business_name
         ,null                           as address_line_1
         ,null                           as address_line_2
         ,null                           as address_line_3
         ,null                           as city
         ,null                           as zip_post_code
         ,null                           as country_code
         ,null                           as sub_region_code
         ,BANK_ACCOUNT_SORT_CODE         as sort_code
         ,SWIFT_CODE                     as swift_code
         ,null                           as specific_nacha_name
         ,ROUTING_NUMBER                 as routing_nbr
       from
         ra_travelex_bank_account
       union
       select
         'CLI'                           as typ
         ,RA_ACCOUNT_ID                  as id
         ,BANK_NAME                      as name
         ,BANK_NAME                      as business_name
         ,null                           as address_line_1
         ,null                           as address_line_2
         ,null                           as address_line_3
         ,null                           as city
         ,null                           as zip_post_code
         ,null                           as country_code
         ,null                           as sub_region_code
         ,BANK_ACCOUNT_SORT_CODE         as sort_code
         ,SWIFT_CODE                     as swift_code
         ,SORTNAME                       as specific_nacha_name --code changed for CMP0720
         ,ROUTING_NUMBER                 as routing_nbr
       from
         ra_client_bank_accounts
       union
       select
         'BIN'                           as typ
         ,RA_BIN_SPONSOR_BANK_ACCOUNT_ID as id
         ,BANK_NAME                      as name
         ,BANK_NAME                      as business_name
         ,null                           as address_line_1
         ,null                           as address_line_2
         ,null                           as address_line_3
         ,null                           as city
         ,null                           as zip_post_code
         ,null                           as country_code
         ,null                           as sub_region_code
         ,BANK_ACCOUNT_SORT_CODE         as sort_code
         ,SWIFT_CODE                     as swift_code
         ,null                           as specific_nacha_name
         ,ROUTING_NUMBER                 as routing_nbr
       from
        ra_bin_sponsors_bank_account) src
        ,bank         trg
        ,bank_xrf     xrf
      where src.typ       = xrf.typ(+)
      and   src.id        = xrf.src_id(+)
      and   nvl(xrf.id,0) = trg.id(+)
      and   not exists
      (
        select  1
        from    bank
        where id                            = nvl(xrf.id,0)
        and   nvl(name,' ')                 = nvl(src.name,' ')
        and   nvl(business_name,' ')        = nvl(src.business_name,' ')
        and   nvl(address_line_1,' ')       = nvl(src.address_line_1,' ')
        and   nvl(address_line_2,' ')       = nvl(src.address_line_2,' ')
        and   nvl(address_line_3,' ')       = nvl(src.address_line_3,' ')
        and   nvl(city,' ')                 = nvl(src.city,' ')
        and   nvl(zip_post_code,' ')        = nvl(src.zip_post_code,' ')
        and   nvl(country_code,' ')         = nvl(src.country_code,' ')
        and   nvl(sub_region_code,' ')      = nvl(src.sub_region_code,' ')
        and   nvl(sort_code,' ')            = nvl(src.sort_code,' ')
        and   nvl(swift_code,' ')           = nvl(src.swift_code,' ')
        and   nvl(specific_nacha_name,' ')  = nvl(src.specific_nacha_name,' ')
        and   nvl(routing_nbr,' ')          = nvl(src.routing_nbr,' ')
      );

    fdata          acur%rowtype;
    newkey         number(9);
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

    -- Broken X-ref links can cause duplicate bank records so clean
    delete
    from bank_xrf
    where not exists
    (
      select 1
      from bank
      where id=bank_xrf.id
    );

    commit;

    open acur;
    fetch acur into fdata;
    while acur%found
    loop

    vRecordsRead := vRecordsRead + 1;

    if fdata.target_bank_id <> 0
    then

      begin

        update
          bank t
        set
           t.name                = fdata.name
          ,t.business_name       = fdata.business_name
          ,t.address_line_1      = fdata.address_line_1
          ,t.address_line_2      = fdata.address_line_2
          ,t.address_line_3      = fdata.address_line_3
          ,t.city                = fdata.city
          ,t.zip_post_code       = fdata.zip_post_code
          ,t.country_code        = fdata.country_code
          ,t.sub_region_code     = fdata.sub_region_code
          ,t.sort_code           = fdata.sort_code
          ,t.swift_code          = fdata.swift_code
          ,t.specific_nacha_name = fdata.specific_nacha_name
          ,t.routing_nbr         = fdata.routing_nbr
        where
          rowid = fdata.target_rowid;

        vRecordsUpd := vRecordsUpd + 1;

      exception
        when others then
          vEMessage := 'BANK - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    else

      begin

        newkey := bank_xrf_seq.nextval;

        insert into bank_xrf
          (TYP
          ,SRC_ID
          ,ID
          ,DATESTAMP)
        values
          (fdata.typ
          ,fdata.id
          ,newkey
          ,vETL_DATE);

        insert into BANK
          (id, name, business_name, address_line_1, address_line_2
          ,address_line_3, city, zip_post_code, country_code
          ,sub_region_code, sort_code, swift_code, specific_nacha_name
          ,routing_nbr)
        values
          (newkey,fdata.name,fdata.business_name,fdata.address_line_1,fdata.address_line_2
          ,fdata.address_line_3,fdata.city,fdata.zip_post_code,fdata.country_code
          ,fdata.sub_region_code,fdata.sort_code,fdata.swift_code,fdata.specific_nacha_name
          ,fdata.routing_nbr);

          vRecordsIns := vRecordsIns + 1;

      exception
        when others then
          vEMessage := 'BANK - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    end if;

    commit;

    fetch acur into fdata;
  end loop;
  close acur;
  ins_extract_recon('BANK',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.BANKETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end BANKETL;

  procedure BIN_SPONSORETL is

  cursor acur is
  select
    src.id
   ,nvl(trg.id,0)             as target_bin_sponsor_id
   ,trg.rowid                 as target_rowid
   ,src.name
   ,src.business_name
   ,src.address_line_1
   ,src.address_line_2
   ,src.address_line_3
   ,src.city
   ,src.zip_post_code
   ,src.country_code
   ,src.sub_region_code
   ,src.mcp_flag
   ,src.coda_ref_rev
   ,src.coda_ref_cr
   ,src.coda_short_name
   ,src.bs_scheme_payable
   ,src.bs_scheme_receivable
   ,src.legal_entity_code
   ,src.bin_sponsor_type_code
   ,src.parent_business_unit
   ,src.destination_id
   ,src.is_mcp
   ,src.toplevel_issuer_id
   ,src.last_updated
   ,src.last_updated_by
  from
    (
        select rds_bin_sponsor_id id
        ,bin_sponsor_name name
        ,null business_name
        ,'tbd' address_line_1
        ,null address_line_2
        ,null address_line_3
        ,'tbd' city
        ,'tbd' zip_post_code
        ,country_code
       -- ,(select min(country_iso) from ra_bin where bin_sponsor_id = rds_bin_sponsor_id) country_code   -- Note: this is a temporary compromise and should be corrected in the future.
        ,null sub_region_code
        ,is_mcp mcp_flag
        ,coda_element3_rev coda_ref_rev
        ,coda_element3_cr coda_ref_cr
        ,coda_short_name coda_short_name
        ,bs_scheme_payable
        ,bs_scheme_receivable
        ,'TCS' legal_entity_code
        ,bin_sponsor_type_code
        ,parent_business_unit
        ,destination_id
        ,is_mcp
        ,toplevel_issuer_id  -- SCP-MCP merger PI Feature P57
        ,ra_bin_sponsor.last_updated  -- SCP-MCP merger PI Feature P57
        ,ra_bin_sponsor.last_updated_by  -- SCP-MCP merger PI Feature P57
      FROM ra_bin_sponsor,
        ra_bin_sponsor_extra_attr
    where ra_bin_sponsor.rds_bin_sponsor_id = ra_bin_sponsor_extra_attr.bin_sponsor_id(+)
    ) src
    ,bin_sponsor        trg
  where src.id = trg.id(+)
  and not exists
  (
    select  1
    from    bin_sponsor
    where id                              = src.id
    and   name                            = src.name
    and   nvl(business_name,' ')          = nvl(src.business_name,' ')
    and   address_line_1                  = src.address_line_1
    and   nvl(address_line_2,' ')         = nvl(src.address_line_2,' ')
    and   nvl(address_line_3,' ')         = nvl(src.address_line_3,' ')
    and   nvl(city,' ')                   = nvl(src.city,' ')
    and   nvl(zip_post_code,' ')          = nvl(src.zip_post_code,' ')
    and   country_code                    = src.country_code
    and   nvl(sub_region_code,' ')        = nvl(src.sub_region_code,' ')
    and   nvl(mcp_flag,' ')               = nvl(src.mcp_flag,' ')
    and   nvl(coda_ref_rev,' ')           = nvl(src.coda_ref_rev,' ')
    and   nvl(coda_ref_cr,' ')            = nvl(src.coda_ref_cr,' ')
    and   nvl(coda_short_name,' ')        = nvl(src.coda_short_name,' ')
    and   nvl(bs_scheme_payable,' ')      = nvl(src.bs_scheme_payable,' ')
    and   nvl(bs_scheme_receivable,' ')   = nvl(src.bs_scheme_receivable,' ')
    and   nvl(legal_entity_code,' ')      = nvl(src.legal_entity_code,' ')
    and   nvl(bin_sponsor_type_code,' ')  = nvl(src.bin_sponsor_type_code,' ')
    and   nvl(parent_business_unit,' ')   = nvl(src.parent_business_unit,' ')
    and   nvl(destination_id,' ')         = nvl(src.destination_id,' ')
    and   nvl(is_mcp,' ')                 = nvl(src.is_mcp,' ')
    and   nvl(toplevel_issuer_id, '999')     = nvl(src.toplevel_issuer_id,'999')   -- SCP-MCP merger PI Feature P57
    and   nvl(last_updated, SYSDATE)          = nvl(src.last_updated, SYSDATE)     -- SCP-MCP merger PI Feature P57
    and   nvl(last_updated_by, ' ')       = nvl(src.last_updated_by, ' ')   -- SCP-MCP merger PI Feature P57
    );

  fdata          acur%rowtype;

  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

  open acur;
  fetch acur into fdata;
  while acur%found
  loop

    vRecordsRead := vRecordsRead + 1;

    if fdata.target_bin_sponsor_id <> 0
    then

      begin

        update
          bin_sponsor t
        set
           t.name                  = fdata.name
          ,t.business_name         = fdata.business_name
          ,t.address_line_1        = fdata.address_line_1
          ,t.address_line_2        = fdata.address_line_2
          ,t.address_line_3        = fdata.address_line_3
          ,t.city                  = fdata.city
          ,t.zip_post_code         = fdata.zip_post_code
          ,t.country_code          = fdata.country_code
          ,t.sub_region_code       = fdata.sub_region_code
          ,t.mcp_flag              = fdata.mcp_flag
          ,t.coda_ref_rev          = fdata.coda_ref_rev
          ,t.coda_ref_cr           = fdata.coda_ref_cr
          ,t.coda_short_name       = fdata.coda_short_name
          ,t.bs_scheme_payable     = fdata.bs_scheme_payable
          ,t.bs_scheme_receivable  = fdata.bs_scheme_receivable
          ,t.legal_entity_code     = fdata.legal_entity_code
          ,t.bin_sponsor_type_code = fdata.bin_sponsor_type_code
          ,t.parent_business_unit  = fdata.parent_business_unit
          ,t.destination_id        = fdata.destination_id
          ,t.is_mcp                = fdata.is_mcp
          ,t.toplevel_issuer_id    = fdata.toplevel_issuer_id   -- SCP-MCP merger PI Feature P57
          ,t.last_updated          = fdata.last_updated  -- SCP-MCP merger PI Feature P57
          ,t.last_updated_by       = fdata.last_updated_by  -- SCP-MCP merger PI Feature P57
        where
          rowid = fdata.target_rowid;

          vRecordsUpd := vRecordsUpd + 1;

      exception
        when others then
          vEMessage := 'BIN_SPONSOR - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;


      end;

    else

      begin
        insert into BIN_SPONSOR
          (id, name, business_name, address_line_1, address_line_2
          ,address_line_3, city, zip_post_code, country_code
          ,sub_region_code, mcp_flag, coda_ref_rev, coda_ref_cr
          ,coda_short_name, bs_scheme_payable, bs_scheme_receivable
          ,legal_entity_code,bin_sponsor_type_code
          ,parent_business_unit,destination_id,is_mcp
          ,toplevel_issuer_id,last_updated, last_updated_by   -- SCP-MCP merger PI Feature P57
          )
        values
          (fdata.id,fdata.name,fdata.business_name,fdata.address_line_1,fdata.address_line_2
          ,fdata.address_line_3,fdata.city,fdata.zip_post_code,fdata.country_code
          ,fdata.sub_region_code,fdata.mcp_flag,fdata.coda_ref_rev,fdata.coda_ref_cr
          ,fdata.coda_short_name,fdata.bs_scheme_payable,fdata.bs_scheme_receivable
          ,fdata.legal_entity_code,fdata.bin_sponsor_type_code
          ,fdata.parent_business_unit,fdata.destination_id,fdata.is_mcp
          ,fdata.toplevel_issuer_id,fdata.last_updated, fdata.last_updated_by  -- SCP-MCP merger PI Feature P57
          );

          vRecordsIns := vRecordsIns + 1;

      exception
        when others then
          vEMessage := 'BIN_SPONSOR - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;

           end;

    end if;

    fetch acur into fdata;

  end loop;
  close acur;
  ins_extract_recon('BIN_SPONSOR',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.BIN_SPONSORETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end BIN_SPONSORETL;

  procedure BIN_SPONSOR_BANK_ACCOUNTETL
  is
  cursor acur is
  select
    src.id
    ,nvl(trg.id,0)                         as target_bin_sponsor_bank_id
    ,trg.rowid                             as target_rowid
    ,src.iban
    ,src.account_nbr
    ,src.bank_id
    ,src.reference_field
    ,src.currency_code
    ,src.bin_sponsor_id
    ,src.name
    ,src.bank_account_purpose_code
  from
  (
    select
     ra_bin_sponsor_bank_account_id     as id
     ,bank_account_iban                 as iban
     ,bank_account_number               as account_nbr
     ,(
          select max(id)       -- Returns null if it doesn't exist
          from bank_xrf
          where typ = 'BIN'    -- Bin Sponsor
          and   src_id = ra_bin_sponsor_bank_account_id
      )                                 as bank_id
     ,null                              as reference_field
     ,account_currency                  as currency_code
     ,rds_bin_sponsor_id                as bin_sponsor_id
     ,bank_account_name                 as name
     ,SUBSTR(bank_account_purpose,1,1)  as bank_account_purpose_code
    from
      ra_bin_sponsors_bank_account
  )  src
    ,bin_sponsor_bank_account       trg
  where
    src.id  = trg.id(+)
  and not exists
  (
    select  1
    from    bin_sponsor_bank_account
    where   id = src.id
    and     nvl(iban,' ') = nvl(src.iban,' ')
    and     nvl(account_nbr,' ') = nvl(src.account_nbr,' ')
    and     bank_id = src.bank_id
    and     nvl(reference_field,' ') = nvl(src.reference_field,' ')
    and     currency_code = src.currency_code
    and     bin_sponsor_id = src.bin_sponsor_id
    and     nvl(name,' ') = nvl(src.name,' ')
    and     nvl(bank_account_purpose_code,' ') = nvl(src.bank_account_purpose_code,' ')
  );

  fdata          acur%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

  open acur;
  fetch acur into fdata;
  while acur%found
  loop

    vRecordsRead := vRecordsRead + 1;

    if fdata.target_bin_sponsor_bank_id <> 0
    then

      begin

        update
          bin_sponsor_bank_account t
        set
           t.iban                   = fdata.iban
          ,t.account_nbr            = fdata.account_nbr
          ,t.bank_id                = fdata.bank_id
          ,t.reference_field        = fdata.reference_field
          ,t.currency_code          = fdata.currency_code
          ,t.bin_sponsor_id         = fdata.bin_sponsor_id
          ,t.name                   = fdata.name
          ,t.bank_account_purpose_code = fdata.bank_account_purpose_code
        where
          rowid = fdata.target_rowid;

          vRecordsUpd := vRecordsUpd + 1;

      exception
        when others then
          vEMessage := 'BIN_SPONSOR_BANK_ACCOUNT - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    else

      begin
        insert into BIN_SPONSOR_BANK_ACCOUNT
          (id,iban,account_nbr,bank_id,reference_field
          ,currency_code,bin_sponsor_id,name
          ,bank_account_purpose_code)
        values
          (fdata.id,fdata.iban,fdata.account_nbr,fdata.bank_id,fdata.reference_field
          ,fdata.currency_code,fdata.bin_sponsor_id,fdata.name
          ,fdata.bank_account_purpose_code);

          vRecordsIns := vRecordsIns + 1;

      exception
        when others then
          vEMessage := 'BIN_SPONSOR_BANK_ACCOUNT - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    end if;

    fetch acur into fdata;
  end loop;
  close acur;
  ins_extract_recon('BIN_SPONSOR_BANK_ACCOUNT',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.BIN_SPONSOR_BANK_ACCOUNTETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end BIN_SPONSOR_BANK_ACCOUNTETL;

  procedure BIN_SPONSOR_CONTACTETL
  is
  cursor acur is
  select
    src.id
   ,nvl(trg.id,0)             as target_bin_sponsor_contact_id
   ,trg.rowid                 as target_rowid
   ,src.title
   ,src.name
   ,src.comments
   ,src.bin_sponsor_id
   ,src.contact_type_id   --SCP-MCP merger PI Feature P57
   ,src.contact_method   --SCP-MCP merger PI Feature P57
   ,src.last_updated     --SCP-MCP merger PI Feature P57
   ,src.last_updated_by   --SCP-MCP merger PI Feature P57
  from
  (
    select
      bin_sponsor_contact_id id,
      CASE WHEN LENGTH(contact_title) > 4 THEN NULL ELSE contact_title END title,
      trim(REGEXP_REPLACE(contact_first_name||'  '||contact_middle_name||'  '||contact_last_name,' {2,}',' ')) name,
      case when length(contact_title) > 4 then contact_title||' '||trim(description) else description end comments,
      bin_sponsor_id,contact_type_id,contact_method,last_updated,last_updated_by   --SCP-MCP merger PI Feature P57
    from ra_bin_sponsor_contacts
   ) src
    ,bin_sponsor_contact          trg
  where
    src.id = trg.id(+)
  and not exists
  (
    select  1
    from    bin_sponsor_contact
    where   id                = src.id
    and     nvl(title,' ')    = nvl(src.title,' ')
    and     nvl(name,' ')     = nvl(src.name,' ')
    and     nvl(comments,' ') = nvl(src.comments,' ')
    and     bin_sponsor_id    = src.bin_sponsor_id
    and     contact_type_id   = src.contact_type_id  --SCP-MCP merger PI Feature P57
    and     contact_method    = src.contact_method   --SCP-MCP merger PI Feature P57
    and     last_updated       = src.last_updated   --SCP-MCP merger PI Feature P57
    and     last_updated_by   = src.last_updated_by   --SCP-MCP merger PI Feature P57
  );

  fdata          acur%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

  open acur;
  fetch acur into fdata;
  while acur%found
  loop

    vRecordsRead := vRecordsRead + 1;

    if fdata.target_bin_sponsor_contact_id <> 0
    then

      begin

        update
          bin_sponsor_contact t
        set
           t.title          = fdata.title
          ,t.name           = fdata.name
          ,t.comments       = fdata.comments
          ,t.bin_sponsor_id = fdata.bin_sponsor_id
          ,t.contact_type_id = fdata.contact_type_id   --SCP-MCP merger PI Feature P57
          ,t.contact_method  = fdata.contact_method    --SCP-MCP merger PI Feature P57
          ,t.last_updated   = fdata.last_updated       --SCP-MCP merger PI Feature P57
          ,t.last_updated_by = fdata.last_updated_by   --SCP-MCP merger PI Feature P57
        where
          rowid = fdata.target_rowid;

          vRecordsUpd := vRecordsUpd + 1;

      exception
        when others then
          vEMessage := 'BIN_SPONSOR_CONTACT - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    else

      begin
        insert into BIN_SPONSOR_CONTACT
          (id, title, name, comments, bin_sponsor_id,
          contact_type_id,contact_method,last_updated,last_updated_by )  --SCP-MCP merger PI Feature P57
        values
          (fdata.id, fdata.title, fdata.name, fdata.comments, fdata.bin_sponsor_id,
          fdata.contact_type_id,fdata.contact_method,fdata.last_updated , fdata.last_updated_by );  --SCP-MCP merger PI Feature P57

          vRecordsIns := vRecordsIns + 1;

      exception
        when others then
          vEMessage := 'BIN_SPONSOR_CONTACT - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    end if;

    fetch acur into fdata;
  end loop;
  close acur;
  ins_extract_recon('BIN_SPONSOR_CONTACT',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);

  commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.BIN_SPONSOR_CONTACTETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end BIN_SPONSOR_CONTACTETL;

  procedure BIN_SPONSOR_CONTACT_DATAETL
  is
  cursor acur is
  select
    src.contact
   ,nvl(trg.contact,'0')             as target_contact
   ,trg.rowid                        as target_rowid
   ,src.contact_type_code
   ,src.bin_sponsor_contact_id
  from
  (
    select
      v.bin_sponsor_contact_id
      ,v.contact_type             contact_type_code
      ,v.contact
    from
    (
      select  bin_sponsor_contact_id,contact_type,contact
      from
      (
        select distinct bin_sponsor_contact_id,contact_mobile,contact_email,contact_phone, DECODE (contact_method, 'GFT', 'GFT', NULL) AS contact_method
        from ra_bin_sponsor_contacts
      ) --where bin_sponsor_contact_id=100109)
       unpivot exclude nulls
       (
        contact
        FOR contact_type IN (contact_mobile AS 'M', contact_email AS 'E', contact_phone AS 'L' ,contact_method AS 'G')
      )
   ) v
  ) src
    ,bin_sponsor_contact_data           trg
  where
        src.contact                = trg.contact(+)
    AND src.contact_type_code      = trg.contact_type_code(+)
    AND src.bin_sponsor_contact_id = trg.bin_sponsor_contact_id(+);      -- All field are joined so only inserts are done

  fdata          acur%rowtype;
  begin

  vRecordsRead := 0;
  vRecordsUpd  := 0;
  vRecordsIns  := 0;
  vRecordsErr  := 0;

  begin
    execute immediate 'truncate table bin_sponsor_contact_data';

    dbms_output.put_line('truncate');
  exception
    when others then
      vEMessage := 'BIN_SPONSOR_CONTACT_DATA (Truncate Table) - ' || sqlerrm;
      ins_txn_err(fdata.contact,vEMessage);
  end;

  open acur;
  fetch acur into fdata;
  while acur%found
  loop

    vRecordsRead := vRecordsRead + 1;

    if fdata.target_contact = '0'
    then
      begin
        insert into BIN_SPONSOR_CONTACT_DATA
          (contact, contact_type_code, bin_sponsor_contact_id)
        values
          (fdata.contact, fdata.contact_type_code, fdata.bin_sponsor_contact_id);

          vRecordsIns := vRecordsIns + 1;

      exception
        when others then
          vEMessage := 'BIN_SPONSOR_CONTACT_DATA - ' || sqlerrm;
          ins_txn_err(fdata.contact,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    end if;

    fetch acur into fdata;
  end loop;
  close acur;
  ins_extract_recon('BIN_SPONSOR_CONTACT_DATA',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.BIN_SPONSOR_CONTACT_DATAETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end BIN_SPONSOR_CONTACT_DATAETL;
  procedure CLIENTETL
  is

  cursor acur is
  select
    src.id
   ,nvl(trg.id,0)                as target_client
   ,trg.rowid                    as target_rowid
   ,src.name
   ,src.business_name
   ,src.address_line_1
   ,src.address_line_2
   ,src.address_line_3
   ,src.city
   ,src.zip_post_code
   ,src.country_code
   ,src.sub_region_code
   ,src.client_type_code
   ,src.client_group_id
   ,src.parent_client_id
   ,src.invoice_timezone_offset
   ,src.invoice_type_flag
   ,src.status_flag
   ,src.front_end_sales_data_flag
   ,src.backing_invoice_format
   ,src.settlement_method
   ,src.client_mcp_flag
   ,src.include_rev_net_settle
   ,src.fees_comm_inv_template
   ,src.alternate_id
   ,src.cardnumber_req
   ,src.proxy_cardnbr_req
   ,src.transaction_timestamp_req  --Added for CMP1191 Canada POST
   ,src.credit_grade   -- SCP-MCP merger PI Feature P57
   ,src.credit_limit    -- SCP-MCP merger PI Feature P57
   ,src.forex_rate_type   -- SCP-MCP merger PI Feature P57
   ,src.gl_customer_group
   ,src.gl_customer_reference  -- SCP-MCP merger PI Feature P57
   ,src.gl_extract_customer  -- SCP-MCP merger PI Feature P57
   ,src.gl_member_code  -- SCP-MCP merger PI Feature P57
   ,src.gl_project_id  -- SCP-MCP merger PI Feature P57
   ,src.invoice_flag  -- SCP-MCP merger PI Feature P57
   ,src.rds_division_id  -- SCP-MCP merger PI Feature P57
   ,src.timezone_offset  -- SCP-MCP merger PI Feature P57
   ,src.last_updated  -- SCP-MCP merger PI Feature P57
   ,src.last_updated_by  -- SCP-MCP merger PI Feature P57
  from
    (
      select
        rds_client_id                                                               as id
        ,nvl(nvl(substr(client_name,1,instr(client_name,'*') - 1),client_name),' ') as name
        ,client_name                                                                as business_name
        ,nvl(client_address_line1, ' ')                                             as address_line_1
        ,client_address_line2                                                       as address_line_2
        ,null                                                                       as address_line_3
        ,client_city                                                                as city
        ,substr(client_postcode,1,12)                                               as zip_post_code
        ,ra_country_id                                                              as country_code
        ,null                                                                       as sub_region_code
        ,decode(client_level,'CLIENT','CL','LEVEL2','L2','BRANCH','BR','BR')        as client_type_code
        ,geo_group_id                                                               as client_group_id
        ,parent_client_id                                                           as parent_client_id
        ,null                                                                       as invoice_timezone_offset
        ,nvl(substr(ra_invoice_type,1,1),decode(invoice_flag,'Y','S'))              as invoice_type_flag
        ,substr(active_status,1,1)                                                  as status_flag
        ,gpf_flag                                                                   as front_end_sales_data_flag
        ,decode(client_excel_indicator,'Y','E',null)                                as backing_invoice_format
        ,client_settlement_format                                                   as settlement_method
        ,nvl(client_mcp_flag,DECODE(client_level,'CLIENT','N',NULL))                as client_mcp_flag
        ,include_rev_net_settle                                                     as include_rev_net_settle
        ,fees_comm_inv_template                                                     as fees_comm_inv_template
        ,alternate_id                                                               as alternate_id
        ,cardnumber_req                                                             as cardnumber_req
        ,proxy_cardnbr_req                                                          as proxy_cardnbr_req
        ,transaction_timestamp_req                                                  as transaction_timestamp_req  --Added for CMP1191 Canada POST
        ,credit_grade                                                               as credit_grade        -- SCP-MCP merger PI Feature P57
        ,credit_limit                                                               as credit_limit        -- SCP-MCP merger PI Feature P57
        ,forex_rate_type                                                            as forex_rate_type      -- SCP-MCP merger PI Feature P57
        ,gl_customer_group                                                          as gl_customer_group    -- SCP-MCP merger PI Feature P57
        ,gl_customer_reference                                                      as gl_customer_reference  -- SCP-MCP merger PI Feature P57
        ,gl_extract_customer                                                        as gl_extract_customer    -- SCP-MCP merger PI Feature P57
        ,gl_member_code                                                             as gl_member_code         -- SCP-MCP merger PI Feature P57
        ,gl_project_id                                                              as gl_project_id           -- SCP-MCP merger PI Feature P57
        ,invoice_flag                                                               as invoice_flag            -- SCP-MCP merger PI Feature P57
        ,rds_division_id                                                            as rds_division_id         -- SCP-MCP merger PI Feature P57
        ,timezone_offset                                                            as timezone_offset         -- SCP-MCP merger PI Feature P57
        ,ra_client.last_updated                                                               as last_updated           -- SCP-MCP merger PI Feature P57
        ,ra_client.last_updated_by                                                            as last_updated_by        -- SCP-MCP merger PI Feature P57
      from  ra_client,
            ra_client_extra_attr
      where rds_client_id = ra_client_id(+)
    )src
   ,client              trg
  where src.id = trg.id(+)
  and   src.country_code is not null
  and   not exists
  (
    select  1
    from    client
    where   id                                  = src.id
    and     name                                = src.name
    and     nvl(business_name,' ')              = nvl(src.business_name,' ')
    and     address_line_1                      = src.address_line_1
    and     nvl(address_line_2,' ')             = nvl(src.address_line_2,' ')
    and     nvl(address_line_3,' ')             = nvl(src.address_line_3,' ')
    and     nvl(city,' ')                       = nvl(src.city,' ')
    and     nvl(zip_post_code,' ')              = nvl(src.zip_post_code,' ')
    and     country_code                        = src.country_code
    and     nvl(sub_region_code,' ')            = nvl(src.sub_region_code,' ')
    and     client_type_code                    = src.client_type_code
    and     nvl(client_group_id,0)              = nvl(src.client_group_id,0)
    and     nvl(parent_client_id,0)             = nvl(src.parent_client_id,0)
    and     nvl(invoice_timezone_offset,' ')    = nvl(src.invoice_timezone_offset,' ')
    and     nvl(invoice_type_flag,' ')          = nvl(src.invoice_type_flag,' ')
    and     nvl(status_flag,' ')                = nvl(src.status_flag,' ')
    and     nvl(front_end_sales_data_flag,' ')  = nvl(src.front_end_sales_data_flag,' ')
    and     nvl(backing_invoice_format,' ')     = nvl(src.backing_invoice_format,' ')
    and     nvl(settlement_method,' ')          = nvl(src.settlement_method,' ')
    and     nvl(client_mcp_flag,' ')            = nvl(src.client_mcp_flag,' ')
    and     nvl(include_rev_net_settle,' ')     = nvl(src.include_rev_net_settle,' ')
    and     nvl(fees_comm_inv_template,' ')     = nvl(src.fees_comm_inv_template,' ')
    and     nvl(alternate_id,' ')               = nvl(src.alternate_id,' ')
    and     nvl(cardnumber_req,' ')             = nvl(src.cardnumber_req,' ')
    and     nvl(proxy_cardnbr_req,' ')          = nvl(src.proxy_cardnbr_req,' ')
    and     nvl(transaction_timestamp_req,' ')  = nvl(src.transaction_timestamp_req,' ') --Added for CMP1191 Canada POST
    and     nvl(credit_grade,' ')               = nvl(src.credit_grade,' ')  -- SCP-MCP merger PI Feature P57
    and     nvl(credit_limit,'99')               = nvl(src.credit_limit,'99')   -- SCP-MCP merger PI Feature P57
    and     nvl(forex_rate_type,' ')            = nvl(src.forex_rate_type,' ')  -- SCP-MCP merger PI Feature P57
    and     nvl(gl_customer_group,' ')          = nvl(src.gl_customer_group,' ')  -- SCP-MCP merger PI Feature P57
    and     nvl(gl_customer_reference,' ')      = nvl(src.gl_customer_reference,' ')  -- SCP-MCP merger PI Feature P57
    and     nvl(gl_extract_customer,' ')        = nvl(src.gl_extract_customer,' ')  -- SCP-MCP merger PI Feature P57
    and     nvl(gl_member_code,'99')             = nvl(src.gl_member_code,'99')  -- SCP-MCP merger PI Feature P57
    and     nvl(gl_project_id,'99')              = nvl(src.gl_project_id,'99 ')   -- SCP-MCP merger PI Feature P57
    and     nvl(invoice_flag,' ')               = nvl(src.invoice_flag,' ')   -- SCP-MCP merger PI Feature P57
    and     nvl(rds_division_id,'99')            = nvl(src.rds_division_id,'99')  -- SCP-MCP merger PI Feature P57
    and     nvl(timezone_offset,' ')            = nvl(src.timezone_offset,' ')  -- SCP-MCP merger PI Feature P57
    and     nvl(last_updated,sysdate)               = nvl(src.last_updated, sysdate)  -- SCP-MCP merger PI Feature P57
    and     nvl(last_updated_by,' ')            = nvl(src.last_updated_by,' ')   -- SCP-MCP merger PI Feature P57
  );
  fdata          acur%rowtype;

  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

  open acur;
  fetch acur into fdata;
  while acur%found
  loop

    execute immediate 'alter table client disable constraint CLI_CLI_FK5';

    vRecordsRead := vRecordsRead + 1;

    if fdata.target_client <> 0
    then

      begin

        update
          client t
        set
           t.name                       = fdata.name
          ,t.business_name              = fdata.business_name
          ,t.address_line_1             = fdata.address_line_1
          ,t.address_line_2             = fdata.address_line_2
          ,t.address_line_3             = fdata.address_line_3
          ,t.city                       = fdata.city
          ,t.zip_post_code              = fdata.zip_post_code
          ,t.country_code               = fdata.country_code
          ,t.sub_region_code            = fdata.sub_region_code
          ,t.client_type_code           = fdata.client_type_code
          ,t.client_group_id            = fdata.client_group_id
          ,t.parent_client_id           = fdata.parent_client_id
          ,t.invoice_timezone_offset    = fdata.invoice_timezone_offset
          ,t.invoice_type_flag          = fdata.invoice_type_flag
          ,t.status_flag                = fdata.status_flag
          ,t.front_end_sales_data_flag  = fdata.front_end_sales_data_flag
          ,t.backing_invoice_format     = fdata.backing_invoice_format
          ,t.settlement_method          = fdata.settlement_method
          ,t.client_mcp_flag            = fdata.client_mcp_flag
          ,t.include_rev_net_settle     = fdata.include_rev_net_settle
          ,t.fees_comm_inv_template     = fdata.fees_comm_inv_template
          ,t.alternate_id               = fdata.alternate_id
          ,t.cardnumber_req             = fdata.cardnumber_req
          ,t.proxy_cardnbr_req          = fdata.proxy_cardnbr_req
          ,t.transaction_timestamp_req  = fdata.transaction_timestamp_req --Added for CMP1191 Canada POST
          ,t.credit_grade               = fdata.credit_grade  -- SCP-MCP merger PI Feature P57
          ,t.credit_limit               = fdata.credit_limit  -- SCP-MCP merger PI Feature P57
          ,t.forex_rate_type            = fdata.forex_rate_type  -- SCP-MCP merger PI Feature P57
          ,t.gl_customer_group          = fdata.gl_customer_group  -- SCP-MCP merger PI Feature P57
          ,t.gl_customer_reference      = fdata.gl_customer_reference  -- SCP-MCP merger PI Feature P57
          ,t.gl_extract_customer        = fdata.gl_extract_customer  -- SCP-MCP merger PI Feature P57
          ,t.gl_member_code             = fdata.gl_member_code  -- SCP-MCP merger PI Feature P57
          ,t.gl_project_id              = fdata.gl_project_id  -- SCP-MCP merger PI Feature P57
          ,t.invoice_flag               = fdata.invoice_flag  -- SCP-MCP merger PI Feature P57
          ,t.rds_division_id            = fdata.rds_division_id  -- SCP-MCP merger PI Feature P57
          ,t.timezone_offset            = fdata.timezone_offset  -- SCP-MCP merger PI Feature P57
          ,t.last_updated               = fdata.last_updated  -- SCP-MCP merger PI Feature P57
          ,t.last_updated_by            = fdata.last_updated_by -- SCP-MCP merger PI Feature P57
        where
          rowid = fdata.target_rowid;

        vRecordsUpd := vRecordsUpd + 1;

      exception
        when others then
          vEMessage := 'CLIENT - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    else

      begin
        insert into CLIENT
          (id, name, business_name, address_line_1, address_line_2
          ,address_line_3, city, zip_post_code, country_code
          ,sub_region_code, client_type_code, client_group_id
          ,parent_client_id,invoice_timezone_offset,invoice_type_flag
          ,status_flag,front_end_sales_data_flag,backing_invoice_format
          ,settlement_method,client_mcp_flag,include_rev_net_settle
          ,fees_comm_inv_template,alternate_id,cardnumber_req,proxy_cardnbr_req
          ,transaction_timestamp_req --Added for CMP1191 Canada POST
          ,credit_grade,credit_limit,forex_rate_type,gl_customer_group  -- SCP-MCP merger PI Feature P57
          ,gl_customer_reference,gl_extract_customer,gl_member_code  --  SCP-MCP merger PI Feature P57
          ,gl_project_id,invoice_flag,rds_division_id,timezone_offset,last_updated,last_updated_by)  -- SCP-MCP merger PI Feature P57
        values
          (fdata.id,fdata.name,fdata.business_name,fdata.address_line_1,fdata.address_line_2
          ,fdata.address_line_3,fdata.city,fdata.zip_post_code,fdata.country_code
          ,fdata.sub_region_code,fdata.client_type_code,fdata.client_group_id
          ,fdata.parent_client_id,fdata.invoice_timezone_offset,fdata.invoice_type_flag
          ,fdata.status_flag,fdata.front_end_sales_data_flag,fdata.backing_invoice_format
          ,fdata.settlement_method,fdata.client_mcp_flag,fdata.include_rev_net_settle
          ,fdata.fees_comm_inv_template,fdata.alternate_id,fdata.cardnumber_req,fdata.proxy_cardnbr_req
          ,fdata.transaction_timestamp_req --Added for CMP1191 Canada POST
          ,fdata.credit_grade,fdata.credit_limit,fdata.forex_rate_type,fdata.gl_customer_group  -- SCP-MCP merger PI Feature P57
          ,fdata.gl_customer_reference,fdata.gl_extract_customer,fdata.gl_member_code  --  SCP-MCP merger PI Feature P57
          ,fdata.gl_project_id,fdata.invoice_flag,fdata.rds_division_id,fdata.timezone_offset,fdata.last_updated,fdata.last_updated_by);  -- SCP-MCP merger PI Feature P57

          vRecordsIns := vRecordsIns + 1;

      exception
        when others then
          vEMessage := 'CLIENT - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;

      end;

    end if;

    fetch acur into fdata;

  end loop;
  close acur;
  ins_extract_recon('CLIENT',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);

  commit;

  begin
    execute immediate 'alter table client enable validate constraint CLI_CLI_FK5';

  exception
    when others then
      vEMessage := 'CLIENT Enable Constraint - ' || sqlerrm;
      ins_txn_err(fdata.id,vEMessage);
      vRecordsErr := vRecordsErr + 1;
      commit;
  end;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.CLIENTETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end CLIENTETL;

  procedure CLIENT_BANK_ACCOUNTETL
  is
  cursor acur is
  select
    src.id
   ,nvl(trg.id,0)              as target_client_bank_acc_id
   ,trg.rowid                  as target_rowid
   ,src.iban
   ,src.account_nbr
   ,src.bank_id
   ,src.reference_field
   ,src.currency_code
   ,src.client_id
   ,src.name
   ,src.ach_code
   ,src.bank_account_purpose_code
   ,src.last_updated  -- SCP-MCP merger PI Feature P57
   ,src.last_updated_by  -- SCP-MCP merger PI Feature P57
  from
  (
    select
      ra_account_id                         as id
     ,bank_account_iban                     as iban
     ,client_bank_account_number            as account_nbr
     ,(
        select max(id)       -- Returns null if it doesn't exist
        from bank_xrf
        where typ = 'CLI'    -- Client
        and   src_id = ra_account_id
      )                                     as bank_id
     ,null                                  as reference_field
     ,settlement_currency                   as currency_code
     ,ra_client_id                          as client_id
     ,bank_account_name                     as name
     ,REPLACE(ach_code,'NULL')              as ach_code
     ,SUBSTR(bank_account_purpose,1,1)      as bank_account_purpose_code
     ,last_updated    -- SCP-MCP merger PI Feature P57
     ,last_updated_by   -- SCP-MCP merger PI Feature P57
    from
      ra_client_bank_accounts
  ) src
    ,client_bank_account        trg
  where src.id = trg.id(+)
  and   not exists
  (
    select  1
    from    client_bank_account
    where   id                                  = src.id
    and     nvl(iban,' ')                       = nvl(src.iban,' ')
    and     nvl(account_nbr,' ')                = nvl(src.account_nbr,' ')
    and     bank_id                             = src.bank_id
    and     nvl(reference_field,' ')            = nvl(src.reference_field,' ')
    and     currency_code                       = src.currency_code
    and     client_id                           = src.client_id
    and     nvl(name,' ')                       = nvl(src.name,' ')
    and     nvl(ach_code,' ')                   = nvl(src.ach_code,' ')
    and     nvl(bank_account_purpose_code,' ')  = nvl(src.bank_account_purpose_code,' ')
    and     nvl(last_updated,SYSDATE)               = nvl(src.last_updated,SYSDATE)   -- SCP-MCP merger PI Feature P57
    and     nvl(last_updated_by, ' ')           = nvl(src.last_updated_by,' ')  -- SCP-MCP merger PI Feature P57
  );

  fdata          acur%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

  open acur;
  fetch acur into fdata;
  while acur%found
  loop

    vRecordsRead := vRecordsRead + 1;

    if fdata.target_client_bank_acc_id <> 0
    then

      begin

        update
          client_bank_account t
        set
           t.iban                      = fdata.iban
          ,t.account_nbr               = fdata.account_nbr
          ,t.bank_id                   = fdata.bank_id
          ,t.reference_field           = fdata.reference_field
          ,t.currency_code             = fdata.currency_code
          ,t.client_id                 = fdata.client_id
          ,t.name                      = fdata.name
          ,t.ach_code                  = fdata.ach_code
          ,t.bank_account_purpose_code = fdata.bank_account_purpose_code
          ,t.last_updated              = fdata.last_updated  -- SCP-MCP merger PI Feature P57
          ,t.last_updated_by           = fdata.last_updated_by  -- SCP-MCP merger PI Feature P57
        where
          rowid = fdata.target_rowid;

          vRecordsUpd := vRecordsUpd + 1;

      exception
        when others then
          vEMessage := 'CLIENT_BANK_ACCOUNT - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    else

      begin
        insert into client_bank_account
          (id,iban,account_nbr,bank_id,reference_field
          ,currency_code,client_id,name,ach_code
          ,bank_account_purpose_code
          ,last_updated, last_updated_by) -- SCP-MCP merger PI Feature P57
        values
          (fdata.id,fdata.iban,fdata.account_nbr,fdata.bank_id,fdata.reference_field
          ,fdata.currency_code,fdata.client_id,fdata.name,fdata.ach_code
          ,fdata.bank_account_purpose_code
          ,fdata.last_updated, fdata.last_updated_by);  -- SCP-MCP merger PI Feature P57

          vRecordsIns := vRecordsIns + 1;

      exception
        when others then
          vEMessage := 'CLIENT_BANK_ACCOUNT - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;

      end;

    end if;

    fetch acur into fdata;
  end loop;
  close acur;
  ins_extract_recon('CLIENT_BANK_ACCOUNT',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.CLIENT_BANK_ACCOUNTETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end CLIENT_BANK_ACCOUNTETL;

  procedure CLIENT_CODINGETL is
  cursor acur is
  select
    src.processor_client_code
    ,nvl(trg.processor_client_code,0)               as target_processor_client_code
    ,trg.rowid                                      as target_rowid
    ,src.client_id
    ,src.processor_id
    ,src.channel_code
  from
  (
    select
    rds_client_id                                 as client_id
    ,processor_client_id                          as processor_client_code
    ,client_level
    ,processor_id
    ,nvl(branch_channel_type_id,1) as channel_code
    ,row_number() over (partition by processor_client_id
                        order by processor_client_id
                                 ,decode(client_level,'CLIENT',1,'LEVEL2',2,'BRANCH',3,1)
                       ) as rank
    from
      ra_client
    order by
      processor_client_id
      ,rds_client_id
  ) src
    ,client_coding          trg
  where src.rank                  = 1
  and   src.processor_client_code = trg.processor_client_code(+);

  fdata          acur%rowtype;

  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

    begin
      execute immediate 'truncate table client_coding';
    exception
      when others then
        vEMessage := 'CLIENT_CODING (Truncate Table) - ' || sqlerrm;
        ins_txn_err(fdata.processor_client_code,vEMessage);
    end;

    open acur;
    fetch acur into fdata;
    while acur%found
    loop

    vRecordsRead := vRecordsRead + 1;

    if fdata.target_processor_client_code = 0 then

      begin
        insert into client_coding
          (processor_client_code,client_id,processor_id,channel_code)
        values
          (fdata.processor_client_code,fdata.client_id,fdata.processor_id,fdata.channel_code);

          vRecordsIns := vRecordsIns + 1;

      exception
        when others then
          vEMessage := 'CLIENT_CODING - ' || sqlerrm;
          ins_txn_err(fdata.processor_client_code,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    else
      begin

        update
          client_coding t
        set
           t.client_id    = fdata.client_id
          ,t.processor_id = fdata.processor_id
          ,t.channel_code = fdata.channel_code
        where
          rowid = fdata.target_rowid;

        vRecordsUpd := vRecordsUpd + 1;

      exception
        when others then
          vEMessage := 'CLIENT_CODING - ' || sqlerrm;
          ins_txn_err(fdata.processor_client_code,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    end if;

    fetch acur into fdata;
  end loop;
  close acur;
  ins_extract_recon('CLIENT_CODING',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.CLIENT_CODINGETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end CLIENT_CODINGETL;

  procedure CLIENT_CONTACTETL
  is

  cursor acur is
  select
    src.id
   ,nvl(trg.id,0)                as target_client_contact_id
   ,trg.rowid                    as target_rowid
   ,src.title
   ,src.name
   ,src.comments
   ,src.last_updated   -- SCP-MCP merger PI Feature P57
   ,src.last_updated_by   -- SCP-MCP merger PI Feature P57
  from
  (
    select distinct
        client_contact_id id,
        CASE WHEN LENGTH(contact_title) > 4 THEN NULL ELSE contact_title END title,
        trim(REGEXP_REPLACE(contact_first_name||'  '||contact_middle_name||'  '||contact_last_name,' {2,}',' ')) name,
        case when length(contact_title) > 4 then contact_title||' '||trim(description) else description end comments,
        last_updated ,last_updated_by   -- SCP-MCP merger PI Feature P57
    from ra_client_contacts
   )src
  ,client_contact        trg
  where src.id = trg.id(+)
  and   not exists
  (
    select  1
    from    client_contact
    where   id                = src.id
    and     nvl(title,' ')    = nvl(src.title,' ')
    and     nvl(name,' ')     = nvl(src.name,' ')
    and     nvl(comments,' ') = nvl(src.comments,' ')
    and     nvl (last_updated, SYSDATE) = nvl(src.last_updated, SYSDATE)  -- SCP-MCP merger PI Feature P57
    and     nvl (last_updated_by, ' ') = nvl(src.last_updated_by, ' ')  -- SCP-MCP merger PI Feature P57
  );

  fdata          acur%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

  open acur;
  fetch acur into fdata;
  while acur%found
  loop

    vRecordsRead := vRecordsRead + 1;

    if fdata.target_client_contact_id <> 0
    then

      begin

        update
          client_contact t
        set
           t.title    = fdata.title
          ,t.name     = fdata.name
          ,t.comments = fdata.comments
          ,t.last_updated = fdata.last_updated  -- SCP-MCP merger PI Feature P57
          ,t.last_updated_by = fdata.last_updated_by -- SCP-MCP merger PI Feature P57

        where
          rowid = fdata.target_rowid;

          vRecordsUpd := vRecordsUpd + 1;

      exception
        when others then
          vEMessage := 'CLIENT_CONTACT - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    else

      begin
        insert into client_contact
          (id, title, name, comments,last_updated, last_updated_by)  --SCP-MCP merger PI Feature P57
        values
          (fdata.id,fdata.title,fdata.name,fdata.comments, fdata.last_updated, fdata.last_updated_by);  --SCP-MCP merger PI Feature P57

          vRecordsIns := vRecordsIns + 1;

      exception
        when others then
          vEMessage := 'CLIENT_CONTACT - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;

      end;

    end if;

    commit;

    fetch acur into fdata;
  end loop;
  close acur;
  ins_extract_recon('CLIENT_CONTACT',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.CLIENT_CONTACTETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end CLIENT_CONTACTETL;

  procedure CLIENT_CONTACT_DATAETL
  is

  cursor acur is
  select
    src.contact
   ,nvl(trg.contact,'0')                as target_client_contact_data
   ,src.contact_type_code
   ,src.client_contact_id
   ,src.contact_type_id --Added this as part of CMP0630
   ,src.last_updated   -- SCP-MCP merger PI Feature P57
   ,src.last_updated_by  -- SCP-MCP merger PI Feature P57
  from
    (
        select
            v.client_contact_id client_contact_id
            ,v.contact_type contact_type_code
            ,SUBSTR(v.contact,1,64) contact
            ,contact_type_id
            ,last_updated   -- SCP-MCP merger PI Feature P57
            ,last_updated_by   -- SCP-MCP merger PI Feature P57
        from
        (
            select distinct client_contact_id, contact_type, contact,contact_type_id, last_updated, last_updated_by  -- SCP-MCP merger PI Feature P57
            from ra_client_contacts
                 unpivot exclude nulls
                    (
                        contact
                        FOR contact_type IN (contact_mobile AS 'M', contact_email AS 'E', contact_phone AS 'L')
                    )
          ) v
        where v.contact != '0'
    )   src
    ,client_contact_data      trg
  where
        src.contact           = trg.contact(+)
    AND src.contact_type_code = trg.contact_type_code(+)
    AND src.contact_type_id   = trg.contact_type_id(+)          --Added this as part of CMP0630
    AND src.client_contact_id = trg.client_contact_id(+);      -- All field are joined so only inserts are done

  fdata          acur%rowtype;
  begin

  vRecordsRead := 0;
  vRecordsUpd  := 0;
  vRecordsIns  := 0;
  vRecordsErr  := 0;

  begin
    execute immediate 'truncate table client_contact_data';
  exception
    when others then
      vEMessage := 'CLIENT_CONTACT_DATA (Truncate Table) - ' || sqlerrm;
      ins_txn_err(fdata.contact,vEMessage);
  end;

  open acur;
  fetch acur into fdata;
  while acur%found
  loop

    vRecordsRead := vRecordsRead + 1;

    if fdata.target_client_contact_data = '0'
    then

      begin
        insert into client_contact_data
          (contact, contact_type_code, client_contact_id,contact_type_id
          ,last_updated, last_updated_by)  -- SCP-MCP merger PI Feature P57
        values
          (fdata.contact, fdata.contact_type_code, fdata.client_contact_id,fdata.contact_type_id
          ,fdata.last_updated, fdata.last_updated_by);  --SCP-MCP merger PI Feature P57

          vRecordsIns := vRecordsIns + 1;

      exception
        when others then
          vEMessage := 'CLIENT_CONTACT_DATA - ' || sqlerrm;
          ins_txn_err(fdata.contact,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    else
      vRecordsUpd := vRecordsUpd  + 1;

    end if;

    fetch acur into fdata;

  end loop;
  close acur;
  ins_extract_recon('CLIENT_CONTACT_DATA',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.CLIENT_CONTACT_DATAETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end CLIENT_CONTACT_DATAETL;

  procedure LEGAL_ENTITYETL
  is
  cursor acur is
  select
    src.code
   ,src.id
   ,nvl(trg.code,'0')              as target_legal_entity_code
   ,trg.rowid                    as target_rowid
   ,src.name
   ,src.business_name
   ,src.address_line_1
   ,src.address_line_2
   ,src.address_line_3
   ,src.city
   ,src.zip_post_code
   ,src.country_code
   ,src.sub_region_code
   ,src.vat_nbr
   ,src.company_nbr
   ,src.reg_address_line_1
   ,src.reg_address_line_2
   ,src.reg_address_line_3
   ,src.reg_city
   ,src.reg_zip_post_code
   ,src.reg_sub_region_code
   ,src.reg_country_code
   ,src.company_logo
   ,src.invoice_prefix
   ,src.jp_morgan_company_id
   ,src.email
   ,src.notice
   ,src.last_updated  -- SCP-MCP merger PI Feature P57
   ,src.last_updated_by  -- SCP-MCP merger PI Feature P57
  from
  (
    SELECT
      ra_company_details_id id
      ,company_name                                                       as name
      ,company_name                                                       as business_name
      ,site_address_1                                                     as address_line_1
      ,site_address_2                                                     as address_line_2
      ,site_address_3                                                     as address_line_3  -- SCP-MCP merger PI Feature P57
      ,'Peterborough'                                                     as city
      ,site_postcode                                                      as zip_post_code
      ,'GB'                                                               as country_code
      ,'CAM'                                                              as sub_region_code
      ,legal_entity                                                       as code
      ,DECODE(ra_company_details_id,4,NULL,company_vat_no)                as vat_nbr
      ,DECODE(ra_company_details_id,4,NULL,company_registered_no)         as company_nbr ----Added as part US partners invoice
      ,registered_address_1                                               as reg_address_line_1
      ,registered_address_2                                               as reg_address_line_2
      ,registered_address_3                                               as reg_address_line_3
      ,registered_address_4                                               as reg_city
      ,REPLACE(registered_postcode,'Delaware ')                           as reg_zip_post_code
      ,NULL                                                               as reg_sub_region_code
      ,DECODE(ra_company_details_id,3,'US',4,'CN','GB')                   as reg_country_code
      ,company_logo
      ,invoice_prefix
      ,jpmorgan_company_id                                                as jp_morgan_company_id
      ,site_email_address                                                 as email
      ,DECODE
      (
        ra_company_details_id,
        4,'Registered address is c/o the registered agent: The Corporation Trust Company',
        NULL
      )                                                                    as notice,
      last_updated,  -- SCP-MCP merger PI Feature P57
      last_updated_by  -- SCP-MCP merger PI Feature P57
     FROM ra_company_details
  )  src
    ,legal_entity              trg
  where
    src.code = trg.code(+)
    and not exists
    (
      select  1
      from    legal_entity
      where   id                            = src.id
      and     name                          = src.name
      and     nvl(business_name,' ')        = nvl(src.business_name,' ')
      and     address_line_1                = src.address_line_1
      and     nvl(address_line_2,' ')       = nvl(src.address_line_2,' ')
      and     nvl(address_line_3,' ')       = nvl(src.address_line_3,' ')
      and     nvl(city,' ')                 = nvl(src.city,' ')
      and     nvl(zip_post_code,' ')        = nvl(src.zip_post_code,' ')
      and     country_code                  = src.country_code
      and     nvl(sub_region_code,' ')      = nvl(src.sub_region_code,' ')
      and     code                          = src.code
      and     nvl(vat_nbr,0)                = nvl(src.vat_nbr,0)
      and     nvl(company_nbr,0)            = nvl(src.company_nbr,0)
      and     nvl(reg_address_line_1,' ')   = nvl(src.reg_address_line_1,' ')
      and     nvl(reg_address_line_2,' ')   = nvl(src.reg_address_line_2,' ')
      and     nvl(reg_address_line_3,' ')   = nvl(src.reg_address_line_3,' ')
      and     nvl(reg_city,' ')             = nvl(src.reg_city,' ')
      and     nvl(reg_zip_post_code,' ')    = nvl(src.reg_zip_post_code,' ')
      and     nvl(reg_sub_region_code,' ')  = nvl(src.reg_sub_region_code,' ')
      and     reg_country_code              = src.reg_country_code
      and     ((company_logo is null and src.company_logo is null)
        or     dbms_lob.compare(company_logo,src.company_logo)=0)
      and     nvl(invoice_prefix,' ')       = nvl(src.invoice_prefix,' ')
      and     nvl(jp_morgan_company_id,0)   = nvl(src.jp_morgan_company_id,0)
      and     nvl(email,' ')                = nvl(src.email,' ')
      and     nvl(notice,' ')               = nvl(src.notice,' ')
      --and     nvl(last_updated, SYSDATE)        = nvl(src.last_updated_by, SYSDATE)  -- SCP-MCP merger PI Feature P57
      and     nvl(last_updated, SYSDATE)        = nvl(src.last_updated, SYSDATE)-- added by nagesh to fix above line
      and     nvl(last_updated_by, ' ')     = nvl(src.last_updated_by, ' ')        -- SCP-MCP merger PI Feature P57
    );

  fdata          acur%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

  open acur;
  fetch acur into fdata;
  while acur%found
  loop

    vRecordsRead := vRecordsRead + 1;

    if fdata.target_legal_entity_code <> '0'
    then

      begin

        update
          legal_entity t
        set
           t.name                  = fdata.name
          ,t.business_name         = fdata.business_name
          ,t.address_line_1        = fdata.address_line_1
          ,t.address_line_2        = fdata.address_line_2
          ,t.address_line_3        = fdata.address_line_3
          ,t.city                  = fdata.city
          ,t.zip_post_code         = fdata.zip_post_code
          ,t.country_code          = fdata.country_code
          ,t.sub_region_code       = fdata.sub_region_code
          ,t.vat_nbr               = fdata.vat_nbr
          ,t.company_nbr           = fdata.company_nbr
          ,t.reg_address_line_1    = fdata.reg_address_line_1
          ,t.reg_address_line_2    = fdata.reg_address_line_2
          ,t.reg_address_line_3    = fdata.reg_address_line_3
          ,t.reg_city              = fdata.reg_city
          ,t.reg_zip_post_code     = fdata.reg_zip_post_code
          ,t.reg_sub_region_code   = fdata.reg_sub_region_code
          ,t.reg_country_code      = fdata.reg_country_code
          ,t.invoice_prefix        = fdata.invoice_prefix
          ,t.jp_morgan_company_id  = fdata.jp_morgan_company_id
          ,t.email                 = fdata.email
          ,t.notice                = fdata.notice
          ,t.last_updated          = fdata.last_updated  --SCP-MCP merger PI Feature P57
          ,t.last_updated_by       = fdata.last_updated_by  --SCP-MCP merger PI Feature P57
        where
          rowid = fdata.target_rowid;

          vRecordsUpd := vRecordsUpd + 1;

      exception
        when others then
          vEMessage := 'LEGAL_ENTITY - ' || sqlerrm;
          ins_txn_err(fdata.code,vEMessage);
          vRecordsErr := vRecordsErr + 1;


      end;

    else

      begin
        insert into legal_entity
          (id, name, business_name, address_line_1, address_line_2
          ,address_line_3, city, zip_post_code, country_code
          ,sub_region_code, code, vat_nbr, company_nbr, reg_address_line_1
          ,reg_address_line_2, reg_address_line_3, reg_city
          ,reg_zip_post_code, reg_sub_region_code, reg_country_code
          ,company_logo, invoice_prefix, jp_morgan_company_id
          ,email, notice,last_updated, last_updated_by)  --SCP-MCP merger PI Feature P57
         values
          (fdata.id, fdata.name, fdata.business_name, fdata.address_line_1, fdata.address_line_2
          ,fdata.address_line_3, fdata.city, fdata.zip_post_code, fdata.country_code
          ,fdata.sub_region_code, fdata.code, fdata.vat_nbr, fdata.company_nbr, fdata.reg_address_line_1
          ,fdata.reg_address_line_2, fdata.reg_address_line_3, fdata.reg_city
          ,fdata.reg_zip_post_code, fdata.reg_sub_region_code, fdata.reg_country_code
          ,fdata.company_logo, fdata.invoice_prefix, fdata.jp_morgan_company_id
          ,fdata.email, fdata.notice,fdata.last_updated, fdata.last_updated_by);   --SCP-MCP merger PI Feature P57

          vRecordsIns := vRecordsIns + 1;

      exception
        when others then
          vEMessage := 'LEGAL_ENTITY - ' || sqlerrm;
          ins_txn_err(fdata.code,vEMessage);
          vRecordsErr := vRecordsErr + 1;

      end;

    end if;

    fetch acur into fdata;
  end loop;
  close acur;
  ins_extract_recon('LEGAL_ENTITY',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.LEGAL_ENTITYETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end LEGAL_ENTITYETL;

  procedure ORG_REGIONETL
  is
  cursor acur is
  select
    src.code
   ,nvl(trg.code,'0')            as target_ord_region_code
   ,trg.rowid                    as target_rowid
   ,src.name
   ,src.country_code
   ,src.coda_ref
   ,src.last_updated
   ,src.last_updated_by
  from
  (
select
      case ra_country_iso
        when 'GB' then
          upper(replace(substr(sub_country_name,1,5),' ',''))
        else
          ra_country_iso
        end  code
      ,sub_country_name name
      ,ra_country_iso country_code
      ,coda_element3 coda_ref
      ,src.LAST_UPDATED   --SCP-MCP merger PI Feature P57
     ,src.last_updated_by   --SCP-MCP merger PI Feature P57
    from ra_org_sub_country src
  ) src
    ,org_region           trg
  where
    src.code = trg.code(+)
    and not exists
    (
      select  1
      from    org_region
      where   code = src.code
      and     name = src.name
      and     country_code = src.country_code
      and     nvl(coda_ref,' ') = nvl(src.coda_ref,' ')
      and     nvl(last_updated,sysdate) =nvl(last_updated,sysdate)  ----SCP-MCP merger PI Feature P57
      and     nvl(last_updated_by,'XX') = nvl(last_updated_by , 'XX')  --SCP-MCP merger PI Feature P57
    );

  fdata          acur%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

  open acur;
  fetch acur into fdata;
  while acur%found
  loop

    vRecordsRead := vRecordsRead + 1;

    if fdata.target_ord_region_code <> '0'
    then

      begin

        update
          org_region t
        set
           t.name         = fdata.name
          ,t.country_code = fdata.country_code
          ,t.coda_ref     = fdata.coda_ref
          ,t.last_updated = fdata.last_updated  --SCP-MCP merger PI Feature P57
          ,t.last_updated_by = fdata.last_updated_by  --SCP-MCP merger PI Feature P57
        where
          rowid = fdata.target_rowid;

        vRecordsUpd := vRecordsUpd + 1;

      exception
        when others then
          vEMessage := 'ORG_REGION - ' || sqlerrm;
          ins_txn_err(fdata.code,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    else

      begin
        insert into org_region
          (code, name, country_code, coda_ref,last_updated, last_updated_by) --SCP-MCP merger PI Feature P57
        values
          (fdata.code, fdata.name, fdata.country_code, fdata.coda_ref,fdata.last_updated,fdata.last_updated_by); --SCP-MCP merger PI Feature P57

          vRecordsIns := vRecordsIns + 1;

      exception
        when others then
          vEMessage := 'ORG_REGION - ' || sqlerrm;
          ins_txn_err(fdata.code,vEMessage);
          vRecordsErr := vRecordsErr + 1;

      end;

    end if;

    fetch acur into fdata;
  end loop;
  close acur;
  ins_extract_recon('ORG_REGION',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.ORG_REGIONETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end ORG_REGIONETL;

  procedure CLIENT_GROUPETL
  is
  cursor acur is
  select
    src.id
   ,nvl(trg.id,0)              as target_client_group
   ,trg.rowid                  as target_rowid
   ,src.name
   ,src.description
   ,src.org_region_code
   ,src.last_updated   --SCP-MCP merger PI Feature P57
   ,src.last_updated_by  --SCP-MCP merger PI Feature P57
  from
    (
      select
        group_id                                                    as id
        ,group_name                                                 as name
        ,null                                                       as description
        ,case s.ra_country_iso
           when 'GB' then
            upper(replace(substr(s.sub_country_name,1,5),' ',''))
           else
            s.ra_country_iso
         end                                                        as org_region_code
         ,nvl(g.last_updated,s.last_updated)                        as last_updated  --SCP-MCP merger PI Feature P57
         , nvl(g.last_updated_by,s.last_updated_by)                 as last_updated_by  --SCP-MCP merger PI Feature P57
    from ra_org_group g
        ,ra_org_sub_country s
   where g.sub_country_id = s.sub_country_id
   )src
    ,client_group         trg
  where
    src.id = trg.id(+)
  and not exists
  (
    select  1
    from    client_group
    where   id = src.id
    and     nvl(name,' ') = nvl(src.name,' ')
    and     nvl(description,' ') = nvl(src.description,' ')
    and     org_region_code = src.org_region_code
    );

  fdata          acur%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

  open acur;
  fetch acur into fdata;
  while acur%found
  loop

    vRecordsRead := vRecordsRead + 1;

    if fdata.target_client_group <> 0
    then

      begin

        update
          client_group t
        set
           t.name            = fdata.name
          ,t.description     = fdata.description
          ,t.org_region_code = fdata.org_region_code
          ,t.last_updated    = fdata.last_updated    --SCP-MCP merger PI Feature P57
          ,t.last_updated_by = fdata.last_updated_by  --SCP-MCP merger PI Feature P57
        where
          rowid = fdata.target_rowid;

        vRecordsUpd := vRecordsUpd + 1;

      exception
        when others then
          vEMessage := 'CLIENT_GROUP - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    else

      begin
        insert into client_group
          (id, name, description, org_region_code,last_updated,last_updated_by)  --SCP-MCP merger PI Feature P57
        values
          (fdata.id, fdata.name, fdata.description, fdata.org_region_code,fdata.last_updated,fdata.last_updated_by);   --SCP-MCP merger PI Feature P57

          vRecordsIns := vRecordsIns + 1;

      exception
        when others then
          vEMessage := 'CLIENT_GROUP - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    end if;

    fetch acur into fdata;
  end loop;
  close acur;
  ins_extract_recon('CLIENT_GROUP',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.CLIENT_GROUPETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end CLIENT_GROUPETL;

  procedure ICA_AGREEMENTETL is

  cursor acur is
  select
    src.ica_nbr
   ,nvl(trg.ica_nbr,0)         as target_ica_nbr
   ,nvl(trg.bin_sponsor_id,0)  as target_bin_sponsor_id
   ,trg.rowid                  as target_rowid
   ,src.description
   ,src.bin_sponsor_id
   ,src.parent_business_unit_name
   --,src.child_business_unit_name
   ,src.distribution_nbr
   ,src.transfer_agent_flag
   ,src.is_primary_ica
   ,nvl(src.mcp_extra_id, 0)   as mcp_extra_id
   ,src.country_code
  from
  (
    select
      ra_bin.ica_number                         ica_nbr
     ,max(bin_sponsor_name)                     description
     ,max(ra_bin.bin_sponsor_id)                       bin_sponsor_id
     --,ra_bin.bin_sponsor_id                       bin_sponsor_id
     ,max(parent_business_unit)                 parent_business_unit_name
     --,max(child_business_unit_name)             child_business_unit_name
     ,max(distribution_id)                      distribution_nbr
     ,null                                      transfer_agent_flag
     ,max(is_primary_ica)                       is_primary_ica
     ,max(ra_mcp_ica_extra.id)                  mcp_extra_id
     ,max(ra_mcp_ica_extra.country_code)        country_code
    from  ra_bin,
          ra_bin_sponsor,
          ra_bin_extra_attr,
          ra_mcp_ica_extra
    where ra_bin.bin_sponsor_id is not null
    and   ra_bin_sponsor.rds_bin_sponsor_id = ra_bin.bin_sponsor_id
    and   ra_bin_extra_attr.bin(+) = ra_bin.bin
    and   ra_mcp_ica_extra.ica_nbr(+) = ra_bin.ica_number
    and   ra_mcp_ica_extra.bin_sponsor_id(+) = ra_bin.bin_sponsor_id
    and   ra_bin.ica_number is not null
    group by ra_bin.ica_number
  ) src
    ,ica_agreement           trg
  where src.ica_nbr = trg.ica_nbr(+)
  and   not exists
  (
    select  1
    from    ica_agreement
    where   ica_nbr                             = src.ica_nbr
    and     nvl(description,' ')                = nvl(src.description,' ')
    and     bin_sponsor_id                      = src.bin_sponsor_id
    and     nvl(parent_business_unit_name,' ')  = nvl(src.parent_business_unit_name,' ')
    --and     nvl(child_business_unit_name,' ')   = nvl(src.child_business_unit_name,' ')
    and     nvl(distribution_nbr,0)             = nvl(src.distribution_nbr,0)
    and     nvl(transfer_agent_flag,' ')        = nvl(src.transfer_agent_flag,' ')
    and     nvl(is_primary_ica,' ')             = nvl(src.is_primary_ica,' ')
    and     nvl(country_code, ' ')              = nvl(src.country_code, ' ')
  );

  fdata          acur%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

  open acur;
  fetch acur into fdata;
  while acur%found
  loop

    vRecordsRead := vRecordsRead + 1;

    if (fdata.target_ica_nbr <> 0 AND fdata.target_bin_sponsor_id <> 0)
    then

      begin

        update
          ica_agreement t
        set
           t.description               = fdata.description
          ,t.bin_sponsor_id            = fdata.bin_sponsor_id
          ,t.parent_business_unit_name = fdata.parent_business_unit_name
          --,t.child_business_unit_name  = fdata.child_business_unit_name
          ,t.distribution_nbr          = fdata.distribution_nbr
          ,t.transfer_agent_flag       = fdata.transfer_agent_flag
          ,t.is_primary_ica            = DECODE(fdata.mcp_extra_id, 0, t.is_primary_ica, fdata.is_primary_ica)
          ,t.country_code              = DECODE(fdata.mcp_extra_id, 0, t.country_code, fdata.country_code)
        where
          rowid = fdata.target_rowid;

        vRecordsUpd := vRecordsUpd + 1;

      exception
        when others then
          vEMessage := 'ICA_AGREEMENT - ' || sqlerrm;
          ins_txn_err(fdata.ica_nbr,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    else

      begin
        insert into ica_agreement
          (ica_nbr, description, bin_sponsor_id,  parent_business_unit_name
          , distribution_nbr, transfer_agent_flag,is_primary_ica, country_code)
        values
          (fdata.ica_nbr, fdata.description, fdata.bin_sponsor_id,  fdata.parent_business_unit_name
          , fdata.distribution_nbr, fdata.transfer_agent_flag,NVL(fdata.is_primary_ica, 'N'), fdata.country_code);

          vRecordsIns := vRecordsIns + 1;

      exception
        when others then
          vEMessage := 'ICA_AGREEMENT - ' || sqlerrm;
          ins_txn_err(fdata.ica_nbr,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    end if;

    fetch acur into fdata;
  end loop;
  close acur;
  ins_extract_recon('ICA_AGREEMENT',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.ICA_AGREEMENTETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end ICA_AGREEMENTETL;

  procedure LEGAL_ENTITY_BANK_ACCOUNTETL
  is
  cursor acur is
  select
    src.id
   ,nvl(trg.id,0)              as target_legal_bank_id
   ,trg.rowid                  as target_rowid
   ,src.iban
   ,src.account_nbr
   ,src.bank_id
   ,src.reference_field
   ,src.currency_code
   ,src.legal_entity_code
   ,src.name
   ,src.bank_account_purpose_code
   ,src.last_updated   ----SCP-MCP merger PI Feature P57
   ,src.last_updated_by  --SCP-MCP merger PI Feature P57
  from
  (
    select
        v.ra_travelex_bank_account_id           id
       ,v.bank_account_iban                     iban
       ,trim(v.travelex_bank_account_number)    account_nbr
       ,v.bank_account_name                     name
       ,v.currency                              currency_code
       ,null                                    reference_field
       ,substr(bank_account_purpose,1,1)        bank_account_purpose_code
        ,v.travelex_legal_entity                legal_entity_code
        ,
        (
            select max(id)       -- Returns null if it doesn't exist
            from bank_xrf
            where typ = 'MST'    -- Mastercard
            and   src_id = v.ra_travelex_bank_account_id
        )                                       bank_id
        ,last_updated   --SCP-MCP merger PI Feature P57
        ,last_updated_by  --SCP-MCP merger PI Feature P57
    from ra_travelex_bank_account v
    where trim(v.travelex_bank_account_number) is not null
   )  src
    ,legal_entity_bank_account       trg
  where src.id = trg.id(+)
  and   not exists
  (
    select  1
    from    legal_entity_bank_account
    where   id                                  = src.id
    and     legal_entity_code                   = src.legal_entity_code
    and     account_nbr                         = src.account_nbr
    and     bank_id                             = src.bank_id
    and     currency_code                       = src.currency_code
    and     nvl(iban,' ')                       = nvl(src.iban,' ')
    and     nvl(name,' ')                       = nvl(src.name,' ')
    and     nvl(reference_field,' ')            = nvl(src.reference_field,' ')
    and     nvl(bank_account_purpose_code,' ')  = nvl(src.bank_account_purpose_code,' ')
    and     nvl(last_updated,sysdate)           = nvl(last_updated,sysdate)  --SCP-MCP merger PI Feature P57
    and     nvl(last_updated_by, 'xx')          = nvl(last_updated_by, 'xx')  --SCP-MCP merger PI Feature P57
  );

    fdata          acur%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

  open acur;
  fetch acur into fdata;
  while acur%found
  loop

    vRecordsRead := vRecordsRead + 1;

    if fdata.target_legal_bank_id <> 0
    then
      begin

        update
          legal_entity_bank_account t
        set
           t.iban               = fdata.iban
          ,t.account_nbr        = fdata.account_nbr
          ,t.bank_id            = fdata.bank_id
          ,t.reference_field    = fdata.reference_field
          ,t.currency_code      = fdata.currency_code
          ,t.legal_entity_code  = fdata.legal_entity_code
          ,t.name               = fdata.name
          ,t.bank_account_purpose_code = fdata.bank_account_purpose_code
          ,t.last_updated       = fdata.last_updated   --SCP-MCP merger PI Feature P57
          ,t.last_updated_by    = fdata.last_updated_by  --SCP-MCP merger PI Feature P57
        where
          rowid = fdata.target_rowid;

        vRecordsUpd := vRecordsUpd + 1;

      exception
        when others then
          vEMessage := 'LEGAL_ENTITY_BNKACC - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;

      end;

    else

      begin
        insert into legal_entity_bank_account
          (id, iban, account_nbr, bank_id, reference_field
          ,currency_code, legal_entity_code, name, bank_account_purpose_code,last_updated,last_updated_by)  --SCP-MCP merger PI Feature P57
        values
          (fdata.id, fdata.iban, fdata.account_nbr, fdata.bank_id, fdata.reference_field
          ,fdata.currency_code, fdata.legal_entity_code, fdata.name, fdata.bank_account_purpose_code,fdata.last_updated,fdata.last_updated_by);  --SCP-MCP merger PI Feature P57

          vRecordsIns := vRecordsIns + 1;

      exception
        when others then
          vEMessage := 'LEGAL_ENTITY_BNKACC - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;

      end;

    end if;

    fetch acur into fdata;
  end loop;
  close acur;
  ins_extract_recon('LEGAL_ENTITY_BNKACC',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.LEGAL_ENTITY_BANK_ACCOUNTETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end LEGAL_ENTITY_BANK_ACCOUNTETL;

  procedure CLIENT_CONTACT_GROUPETL
  is
  cursor acur is
  select
    src.client_id
   ,nvl(trg.client_id,0)    as target_client_contact_group
   ,trg.rowid               as target_rowid
   ,src.client_contact_id
   ,src.last_updated  -- SCP-MCP merger PI Feature P57
   ,src.last_updated_by  -- SCP-MCP merger PI Feature P57
  from
  (
    select distinct
        client_id,
        client_contact_id,
        last_updated,  -- SCP-MCP merger PI Feature P57
        last_updated_by  -- SCP-MCP merger PI Feature P57
    from ra_client_contacts
  ) src
    ,client_contact_group          trg
  where src.client_id         = trg.client_id(+)
  and   src.client_contact_id = trg.client_contact_id(+)
  and   trg.client_id is null;                        -- All fields are in the key

  fdata          acur%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

  open acur;
  fetch acur into fdata;
  while acur%found
  loop

    vRecordsRead := vRecordsRead + 1;

    if fdata.target_client_contact_group = 0 then

      begin
        insert into client_contact_group
          (client_id, client_contact_id,last_updated, last_updated_by)  -- SCP-MCP merger PI Feature P57
        values
          (fdata.client_id, fdata.client_contact_id, fdata.last_updated, fdata.last_updated_by);  -- SCP-MCP merger PI Feature P57

          vRecordsIns := vRecordsIns + 1;

      exception
        when others then
          vEMessage := 'CLIENT_CONTACT_GROUP - ' || sqlerrm;
          ins_txn_err(fdata.client_contact_id,vEMessage);
          vRecordsErr := vRecordsErr + 1;

      end;

    end if;

    commit;

    fetch acur into fdata;
  end loop;
  close acur;
  ins_extract_recon('CLIENT_CONTACT_GROUP',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.CLIENT_CONTACT_GROUPETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end CLIENT_CONTACT_GROUPETL;

  procedure CLIENT_CONTRACTETL
  is
  cursor acur is
  select
    src.id
    ,nvl(trg.id,0)              as target_client_contract
    ,trg.rowid                  as target_rowid
    ,src.name
    ,src.start_date
    ,src.end_date
    ,src.status_flag
    ,src.settlement_frequency_flag
    ,src.settlement_method_code
    ,src.client_id
    ,src.legal_entity_code
    ,src.processor_id
    ,src.previous_contract_id
    ,src.margin_rate
    ,src.margin_split
    ,src.comments
  from
  (
    select
      rds_client_id                                                             as id
     ,'Contract for '||client_name||' ('||rds_client_id||')'                    as name
     ,case
      when contract_start_date <= to_date('31-12-1999','DD-MM-YYYY')
          or contract_start_date is null then
        to_date('01-01-2000','DD-MM-YYYY')
      else
        contract_start_date
      end                                                                       as start_date
     ,case when contract_end_date > to_date('31-DEC-2040','DD-MM-YYYY')
          or contract_end_date is null then
        to_date('31/12/2040','DD/MM/YYYY')
      else
        contract_end_date
      end                                                                       as end_date
      ,substr(active_status,1,1)                                                as status_flag
      ,replace(settlement_frequency,'tba')                                      as settlement_frequency_flag
      ,(
        select decode(max(ra_settlement_method),
          'PREPAID','PP',
          'DORA','DR',
          'ACH','AC',
          'EFT','EF',
          'MICROBANK','MB',
          'NACHA','NC',
          'SWIFT','SW')
        from ra_client_currency
        where ra_client_id = rds_client_id
       )                                                                        as settlement_method_code
      ,rds_client_id                                                            as client_id
      ,(
        select max(legal_entity)
        from ra_client_currency
        where ra_client_id = rds_client_id
       )                                                                        as legal_entity_code
      ,processor_id
      ,null                                                                     as previous_contract_id
      ,margin_rate
      ,margin_split
      ,null                                                                     as comments
    from
      ra_client
    where client_level in ('CLIENT','LEVEL2')
  )                   src
    ,client_contract  trg
  where src.id = trg.id(+)
    and not exists
    (
      select  1
      from    client_contract
      where   id = src.id
      and     name = src.name
      and     start_date = src.start_date
      and     nvl(end_date,src.start_date-1) = nvl(src.end_date,src.start_date-1)
      and     nvl(status_flag,' ') = nvl(src.status_flag,' ')
      and     nvl(settlement_frequency_flag,' ') = nvl(src.settlement_frequency_flag,' ')
      and     nvl(settlement_method_code,' ') = nvl(src.settlement_method_code,' ')
      and     client_id = src.client_id
      and     nvl(legal_entity_code,' ') = nvl(src.legal_entity_code,' ')
      and     processor_id = src.processor_id
      and     nvl(previous_contract_id,-999) = nvl(src.previous_contract_id,-999)
      and     nvl(margin_rate,-999) = nvl(src.margin_rate,-999)
      and     nvl(margin_split,-999) = nvl(src.margin_split,-999)
      and     nvl(comments,' ') = nvl(src.comments,' ')
    );

  fdata          acur%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

  open acur;
  fetch acur into fdata;
  while acur%found
  loop

    vRecordsRead := vRecordsRead + 1;

    if fdata.target_client_contract <> 0
    then

      begin

        update
          client_contract t
        set
           t.name                       = fdata.name
          ,t.start_date                 = fdata.start_date
          ,t.end_date                   = fdata.end_date
          ,t.status_flag                = fdata.status_flag
          ,t.settlement_frequency_flag  = fdata.settlement_frequency_flag
          ,t.settlement_method_code     = fdata.settlement_method_code
          ,t.client_id                  = fdata.client_id
          ,t.legal_entity_code          = fdata.legal_entity_code
          ,t.processor_id               = fdata.processor_id
          ,t.previous_contract_id       = fdata.previous_contract_id
          ,t.margin_rate                = fdata.margin_rate
          ,t.margin_split               = fdata.margin_split
          ,t.comments                   = fdata.comments
        where
          rowid = fdata.target_rowid;

        vRecordsUpd := vRecordsUpd + 1;

      exception
        when others then
          vEMessage := 'CLIENT_CONTRACT - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    else

      begin
        insert into client_contract
          (id, name, start_date, end_date, status_flag
          ,settlement_frequency_flag, settlement_method_code
          ,client_id, legal_entity_code, processor_id
          ,previous_contract_id, margin_rate, margin_split, comments)
        values
          (fdata.id, fdata.name, fdata.start_date, fdata.end_date, fdata.status_flag
          ,fdata.settlement_frequency_flag, fdata.settlement_method_code
          ,fdata.client_id, fdata.legal_entity_code, fdata.processor_id
          ,fdata.previous_contract_id, fdata.margin_rate, fdata.margin_split, fdata.comments);

          vRecordsIns := vRecordsIns + 1;

      exception
        when others then
          vEMessage := 'CLIENT_CONTRACT - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    end if;

    fetch acur into fdata;
  end loop;
  close acur;
  ins_extract_recon('CLIENT_CONTRACT',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.CLIENT_CONTRACTETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end CLIENT_CONTRACTETL;

  procedure CLIENT_CONTRACT_PARTETL
  is
  cursor acur is
  select
    src.client_contract_id
   ,nvl(trg.client_contract_id,0)           as target_client_contract_part
   ,trg.rowid                               as target_rowid
   ,src.currency_code
   ,src.client_bank_account_id
   ,src.legal_entity_bank_account_id
   ,src.status_flag
   ,src.smart_stream_account_nbr
   ,src.currency_settl_code
   ,src.margin_rate
   ,src.margin_split
   ,src.fx_revenue_client_share
   ,src.fees_comm_client_share
   ,src.p2p_fx_revenue_client_share
   ,src.p2p_fees_client_share
   ,src.revenue_settlement_margin
   ,src.customer_markup_rate  -- SCP-MCP merger PI Feature P57
   ,src.ra_settlement_method -- SCP-MCP merger PI Feature P57
   ,src.last_updated   -- SCP-MCP merger PI Feature P57
   ,src.last_updated_by  -- SCP-MCP merger PI Feature P57

  from
  (
        select
            c.ra_client_id                                            as client_contract_id ,
            c.ra_client_id                                            as client_id ,
            c.ra_currency_deals                                       as currency_code ,
            c.ra_travelex_account_id                                  as legal_entity_bank_account_id,
            (
              select max(ra_account_id)
              from  ra_client_bank_accounts
              where ra_client_id = c.ra_client_id
              and   settlement_currency = c.ra_settlement_currency
            )                                                         as client_bank_account_id,
            substr(c.active_status,1,1)                               as status_flag,
            smart_stream_acct_num                                     as smart_stream_account_nbr,
            c.ra_settlement_currency                                  as currency_settl_code,
            c.settlement_markup_rate                                  as margin_rate,
            c.customer_markup_cli_share                               as margin_split,
            e.fx_revenue_client_share                                 as fx_revenue_client_share,
            e.fees_comm_client_share                                  as fees_comm_client_share,
            e.p2p_fx_revenue_client_share                             as p2p_fx_revenue_client_share,
            e.p2p_fees_client_share                                   as p2p_fees_client_share,
            e.revenue_settlement_margin                               as revenue_settlement_margin,
            c.customer_markup_rate,  -- SCP-MCP merger PI Feature P57
            c.ra_settlement_method, -- SCP-MCP merger PI Feature P57
            c.last_updated,   -- SCP-MCP merger PI Feature P57
            c.last_updated_by  -- SCP-MCP merger PI Feature P57
        from ra_client_currency c ,
             ra_client_currency_extra e,
             ra_client m
        where c.ra_client_id = m.rds_client_id
        and e.ra_client_cur_id(+) = c.ra_client_cur_id
        and m.client_level  in ('CLIENT','LEVEL2')
        and c.ra_client_id  is not null
        and c.ra_client_cur_id =
        (
            select nvl(max(decode(active_status,'DEA',null,ra_client_cur_id)),max(ra_client_cur_id))
            from  ra_client_currency
            where ra_client_id = c.ra_client_id
            and   ra_currency_deals = c.ra_currency_deals
        )
  )  src
    ,client_contract_part            trg
  where
        src.currency_code       = trg.currency_code(+)
    and src.client_contract_id  = trg.client_contract_id(+)
    and (src.status_flag != 'D' OR trg.client_contract_id IS NOT NULL)
    and not exists
    (
      select  1
      from    client_contract_part
      where   currency_code                         = src.currency_code
      and     client_contract_id                    = src.client_contract_id
      and     currency_settl_code                   = src.currency_settl_code
      and     nvl(client_bank_account_id,0)         = nvl(src.client_bank_account_id,0)
      and     nvl(legal_entity_bank_account_id,0)   = nvl(src.legal_entity_bank_account_id,0)
      and     nvl(status_flag,' ')                  = nvl(src.status_flag,' ')
      and     nvl(smart_stream_account_nbr,' ')     = nvl(src.smart_stream_account_nbr,' ')
      and     nvl(margin_rate,-999)                 = nvl(src.margin_rate,-999)
      and     nvl(margin_split,-999)                = nvl(src.margin_split,-999)
      and     nvl(fx_revenue_client_share,-999)     = nvl(src.fx_revenue_client_share,-999)
      and     nvl(fees_comm_client_share,-999)      = nvl(src.fees_comm_client_share,-999)
      and     nvl(p2p_fx_revenue_client_share,-999) = nvl(src.p2p_fx_revenue_client_share,-999)
      and     nvl(p2p_fees_client_share,-999)       = nvl(src.p2p_fees_client_share,-999)
      and     nvl(revenue_settlement_margin,-999)   = nvl(src.revenue_settlement_margin,-999)
      and     nvl(customer_markup_rate,-999)        = nvl(src.customer_markup_rate,-999)  -- SCP-MCP merger PI Feature P57
      and     nvl(ra_settlement_method,' ')         = nvl(src.ra_settlement_method,' ')  -- SCP-MCP merger PI Feature P57
      and     nvl(last_updated, SYSDATE)                = nvl(last_updated, SYSDATE)   -- SCP-MCP merger PI Feature P57
      and     nvl(last_updated_by, ' ')             = nvl(last_updated_by, ' ')   -- SCP-MCP merger PI Feature P57
    );

  fdata          acur%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

  open acur;
  fetch acur into fdata;
  while acur%found
  loop

    vRecordsRead := vRecordsRead + 1;

    if fdata.target_client_contract_part <> 0
    then

      begin

        update
          client_contract_part t
        set
           t.status_flag                  = fdata.status_flag
          ,t.smart_stream_account_nbr     = fdata.smart_stream_account_nbr
          ,t.legal_entity_bank_account_id = fdata.legal_entity_bank_account_id
          ,t.client_bank_account_id       = fdata.client_bank_account_id
          ,t.currency_settl_code          = fdata.currency_settl_code
          ,t.margin_rate                  = fdata.margin_rate
          ,t.margin_split                 = fdata.margin_split
          ,t.fx_revenue_client_share      = fdata.fx_revenue_client_share
          ,t.fees_comm_client_share       = fdata.fees_comm_client_share
          ,t.p2p_fx_revenue_client_share  = fdata.p2p_fx_revenue_client_share
          ,t.p2p_fees_client_share        = fdata.p2p_fees_client_share
          ,t.revenue_settlement_margin    = fdata.revenue_settlement_margin
          ,t.customer_markup_rate         = fdata.customer_markup_rate
          ,t.ra_settlement_method         = fdata.ra_settlement_method
          ,t.last_updated                 = fdata.last_updated
          ,t.last_updated_by              = fdata.last_updated_by
        where
          rowid = fdata.target_rowid;

        vRecordsUpd := vRecordsUpd + 1;

      exception
        when others then
          vEMessage := 'CLIENT_CONTRACT_PART - ' || sqlerrm;
          ins_txn_err(fdata.client_contract_id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    else

      begin
        insert into client_contract_part
          (client_contract_id, currency_code, client_bank_account_id
          ,legal_entity_bank_account_id, status_flag, smart_stream_account_nbr
          ,currency_settl_code,margin_rate,margin_split
          ,fx_revenue_client_share,fees_comm_client_share,p2p_fx_revenue_client_share
          ,p2p_fees_client_share,revenue_settlement_margin
          ,customer_markup_rate,ra_settlement_method,last_updated,last_updated_by)  -- SCP-MCP merger PI Feature P57
        values
          (fdata.client_contract_id, fdata.currency_code, fdata.client_bank_account_id
          ,fdata.legal_entity_bank_account_id, fdata.status_flag, fdata.smart_stream_account_nbr
          ,fdata.currency_settl_code,fdata.margin_rate,fdata.margin_split
          ,fdata.fx_revenue_client_share,fdata.fees_comm_client_share,fdata.p2p_fx_revenue_client_share
          ,fdata.p2p_fees_client_share,fdata.revenue_settlement_margin
          ,fdata.customer_markup_rate,fdata.ra_settlement_method,fdata.last_updated,fdata.last_updated_by);   -- SCP-MCP merger PI Feature P57

          vRecordsIns := vRecordsIns + 1;

      exception
        when others then
          vEMessage := 'CLIENT_CONTRACT_PART - ' || sqlerrm;
          ins_txn_err(fdata.client_contract_id,vEMessage);
          vRecordsErr := vRecordsErr + 1;

      end;

    end if;

    fetch acur into fdata;
  end loop;
  close acur;
  ins_extract_recon('CLIENT_CONTRACT_PART',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.CLIENT_CONTRACT_PARTETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end CLIENT_CONTRACT_PARTETL;

  procedure TRUSTETL
  is
  cursor acur is
  select
    src.ra_trust_invoice_detail_id
    ,nvl(trg.ra_trust_invoice_detail_id,0)     as target_id
    ,trg.rowid                                 as target_rowid
    ,src.trust_currency_code
    ,src.bin_sponsor_name
    ,src.trust_company_name
    ,src.trust_contact_name
    ,src.trust_contact_phone
    ,src.trust_contact_email
    ,src.trust_bank_name
    ,src.trust_account_number
    ,src.trust_iban_number
    ,src.trust_swift_id
    ,src.shell_company_name
    ,src.shell_contact_name
    ,src.shell_contact_phone
    ,src.shell_contact_email
    ,src.shell_bank_name
    ,src.shell_account_number
    ,src.shell_iban_number
    ,src.shell_swift_id
    ,src.access_company_name
    ,src.access_contact_name
    ,src.access_contact_phone
    ,src.access_contact_email
    ,src.access_bank_name
    ,src.access_account_number
    ,src.access_iban_number
    ,src.access_swift_id
    ,substr(src.ms_phone,1,13) ms_phone
    ,substr(src.ms_email,1,35) ms_email
    ,src.bin_sponsor_id
  from
  (
    select
      ra_trust_invoice_detail_id
      ,trust_currency_code
      ,bin_sponsor_name
      ,trust_company_name
      ,trust_contact_name
      ,trust_contact_phone
      ,trust_contact_email
      ,trust_bank_name
      ,trust_account_number
      ,trust_iban_number
      ,trust_swift_id
      ,shell_company_name
      ,shell_contact_name
      ,shell_contact_phone
      ,shell_contact_email
      ,shell_bank_name
      ,shell_account_number
      ,shell_iban_number
      ,shell_swift_id
      ,access_company_name
      ,access_contact_name
      ,access_contact_phone
      ,access_contact_email
      ,access_bank_name
      ,access_account_number
      ,access_iban_number
      ,access_swift_id
      ,substr(ms_phone,1,13) ms_phone
      ,substr(ms_email,1,35) ms_email
      ,bin_sponsor_id
    from
      ra_trust_invoice_details
  ) src
    ,trust_invoice_static_details     trg
  where src.ra_trust_invoice_detail_id = trg.ra_trust_invoice_detail_id(+)
  and   not exists
  (
    select  1
    from    trust_invoice_static_details
    where   ra_trust_invoice_detail_id      = src.ra_trust_invoice_detail_id
    and     nvl(trust_currency_code,' ')    = nvl(src.trust_currency_code,' ')
    and     bin_sponsor_name                = src.bin_sponsor_name
    and     nvl(trust_company_name,' ')     = nvl(src.trust_company_name,' ')
    and     nvl(trust_contact_name,' ')     = nvl(src.trust_contact_name,' ')
    and     nvl(trust_contact_phone,' ')    = nvl(src.trust_contact_phone,' ')
    and     nvl(trust_contact_email,' ')    = nvl(src.trust_contact_email,' ')
    and     nvl(trust_bank_name,' ')        = nvl(src.trust_bank_name,' ')
    and     nvl(trust_account_number,' ')   = nvl(src.trust_account_number,' ')
    and     nvl(trust_iban_number,' ')      = nvl(src.trust_iban_number,' ')
    and     nvl(trust_swift_id,' ')         = nvl(src.trust_swift_id,' ')
    and     shell_company_name              = src.shell_company_name
    and     nvl(shell_contact_name,' ')     = nvl(src.shell_contact_name,' ')
    and     nvl(shell_contact_phone,' ')    = nvl(src.shell_contact_phone,' ')
    and     nvl(shell_contact_email,' ')    = nvl(src.shell_contact_email,' ')
    and     nvl(shell_bank_name,' ')        = nvl(src.shell_bank_name,' ')
    and     nvl(shell_account_number,' ')   = nvl(src.shell_account_number,' ')
    and     nvl(shell_iban_number,' ')      = nvl(src.shell_iban_number,' ')
    and     nvl(shell_swift_id,' ')         = nvl(src.shell_swift_id,' ')
    and     access_company_name             = src.access_company_name
    and     nvl(access_contact_name,' ')    = nvl(src.access_contact_name,' ')
    and     nvl(access_contact_phone,' ')   = nvl(src.access_contact_phone,' ')
    and     nvl(access_contact_email,' ')   = nvl(src.access_contact_email,' ')
    and     nvl(access_bank_name,' ')       = nvl(src.access_bank_name,' ')
    and     nvl(access_account_number,' ')  = nvl(src.access_account_number,' ')
    and     nvl(access_iban_number,' ')     = nvl(src.access_iban_number,' ')
    and     nvl(access_swift_id,' ')        = nvl(src.access_swift_id,' ')
    and     nvl(ms_phone,' ')               = nvl(src.ms_phone,' ')
    and     nvl(ms_email,' ')               = nvl(src.ms_email,' ')
    and     nvl(bin_sponsor_id,0)           = nvl(src.bin_sponsor_id,0)
  );

  fdata          acur%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

  open acur;
  fetch acur into fdata;
  while acur%found
  loop

    vRecordsRead := vRecordsRead + 1;

    if fdata.target_id <> 0
    then

      begin

        update
          TRUST_INVOICE_STATIC_DETAILS t
        set
          t.trust_currency_code = fdata.trust_currency_code
          ,t.bin_sponsor_name   = fdata.bin_sponsor_name
          ,t.trust_company_name = fdata.trust_company_name
          ,t.trust_contact_name = fdata.trust_contact_name
          ,t.trust_contact_phone = fdata.trust_contact_phone
          ,t.trust_contact_email = fdata.trust_contact_email
          ,t.trust_bank_name     = fdata.trust_bank_name
          ,t.trust_account_number = fdata.trust_account_number
          ,t.trust_iban_number    = fdata.trust_iban_number
          ,t.trust_swift_id       = fdata.trust_swift_id
          ,t.shell_company_name   = fdata.shell_company_name
          ,t.shell_contact_name   = fdata.shell_contact_name
          ,t.shell_contact_phone  = fdata.shell_contact_phone
          ,t.shell_contact_email  = fdata.shell_contact_email
          ,t.shell_bank_name      = fdata.shell_bank_name
          ,t.shell_account_number = fdata.shell_account_number
          ,t.shell_iban_number    = fdata.shell_iban_number
          ,t.shell_swift_id       = fdata.shell_swift_id
          ,t.access_company_name  = fdata.access_company_name
          ,t.access_contact_name  = fdata.access_contact_name
          ,t.access_contact_phone = fdata.access_contact_phone
          ,t.access_contact_email = fdata.access_contact_email
          ,t.access_bank_name     = fdata.access_bank_name
          ,t.access_account_number = fdata.access_account_number
          ,t.access_iban_number    = fdata.access_iban_number
          ,t.access_swift_id      = fdata.access_swift_id
          ,t.ms_phone             = fdata.ms_phone
          ,t.ms_email             = fdata.ms_email
          ,t.bin_sponsor_id       = fdata.bin_sponsor_id
        where
          rowid = fdata.target_rowid;

        vRecordsUpd := vRecordsUpd + 1;

      exception
        when others then
          vEMessage := 'TRUST - ' || sqlerrm;
          ins_txn_err(fdata.RA_TRUST_INVOICE_DETAIL_ID,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    else

      begin
        insert into trust_invoice_static_details
          (ra_trust_invoice_detail_id
           ,trust_currency_code
           ,bin_sponsor_name
           ,trust_company_name
           ,trust_contact_name
           ,trust_contact_phone
           ,trust_contact_email
           ,trust_bank_name
           ,trust_account_number
           ,trust_iban_number
           ,trust_swift_id
           ,shell_company_name
           ,shell_contact_name
           ,shell_contact_phone
           ,shell_contact_email
           ,shell_bank_name
           ,shell_account_number
           ,shell_iban_number
           ,shell_swift_id
           ,access_company_name
           ,access_contact_name
           ,access_contact_phone
           ,access_contact_email
           ,access_bank_name
           ,access_account_number
           ,access_iban_number
           ,access_swift_id
           ,ms_phone
           ,ms_email
           ,bin_sponsor_id)
        values
          (fdata.ra_trust_invoice_detail_id
           ,fdata.trust_currency_code
           ,fdata.bin_sponsor_name
           ,fdata.trust_company_name
           ,fdata.trust_contact_name
           ,fdata.trust_contact_phone
           ,fdata.trust_contact_email
           ,fdata.trust_bank_name
           ,fdata.trust_account_number
           ,fdata.trust_iban_number
           ,fdata.trust_swift_id
           ,fdata.shell_company_name
           ,fdata.shell_contact_name
           ,fdata.shell_contact_phone
           ,fdata.shell_contact_email
           ,fdata.shell_bank_name
           ,fdata.shell_account_number
           ,fdata.shell_iban_number
           ,fdata.shell_swift_id
           ,fdata.access_company_name
           ,fdata.access_contact_name
           ,fdata.access_contact_phone
           ,fdata.access_contact_email
           ,fdata.access_bank_name
           ,fdata.access_account_number
           ,fdata.access_iban_number
           ,fdata.access_swift_id
           ,fdata.ms_phone
           ,fdata.ms_email
           ,fdata.bin_sponsor_id);

          vRecordsIns := vRecordsIns + 1;

          commit;

      exception
        when others then
          vEMessage := 'TRUST - ' || sqlerrm;
          ins_txn_err(fdata.RA_TRUST_INVOICE_DETAIL_ID,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    end if;

    fetch acur into fdata;
  end loop;
  close acur;
  ins_extract_recon('TRUST',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.TRUSTETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end TRUSTETL;

  procedure MASTERCARD_HEADERETL
  is
    cursor acur is
    select
    src.company_name
     ,trg.company_name          as target_company_name
     ,trg.rowid                 as target_rowid
    ,src.company_address_line1
    ,src.company_address_line2
    ,src.country
    ,src.company_phone_number
    ,src.email_address
    from
      ra_mastercard_header_extra  src
      left join mastercard_header trg on 1=1 -- Note that this is a single record
    where not exists
    (
      select  1
      from    mastercard_header
      where   nvl(company_name,' ')           = nvl(src.company_name,' ')
      and     nvl(company_address_line1,' ')  = nvl(src.company_address_line1,' ')
      and     nvl(company_address_line2,' ')  = nvl(src.company_address_line2,' ')
      and     nvl(country,' ')                = nvl(src.country,' ')
      and     nvl(company_phone_number,' ')   = nvl(src.company_phone_number,' ')
      and     nvl(email_address,' ')          = nvl(src.email_address,' ')
    );

    fdata          acur%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

    open acur;
    fetch acur into fdata;
    while acur%found
    loop

      vRecordsRead := vRecordsRead + 1;

      if fdata.target_company_name is not null
      then

        begin

          update
            mastercard_header t
          set
             t.company_name           = fdata.company_name
            ,t.company_address_line1  = fdata.company_address_line1
            ,t.company_address_line2  = fdata.company_address_line2
            ,t.country                = fdata.country
            ,t.company_phone_number   = fdata.company_phone_number
            ,t.email_address          = fdata.email_address
          where
            rowid = fdata.target_rowid;

          vRecordsUpd := vRecordsUpd + 1;

        exception
          when others then
            vEMessage := 'MASTERCARD_HEADER - ' || sqlerrm;
            ins_txn_err(fdata.company_name,vEMessage);
            vRecordsErr := vRecordsErr + 1;
        end;

      else

        begin
          insert into mastercard_header
          (
            company_name
            ,company_address_line1
            ,company_address_line2
            ,country
            ,company_phone_number
            ,email_address
          )
          values
          (
            fdata.company_name
            ,fdata.company_address_line1
            ,fdata.company_address_line2
            ,fdata.country
            ,fdata.company_phone_number
            ,fdata.email_address
          );

          vRecordsIns := vRecordsIns + 1;

        exception
          when others then
            vEMessage := 'MASTERCARD_HEADER - ' || sqlerrm;
            ins_txn_err(fdata.company_name,vEMessage);
            vRecordsErr := vRecordsErr + 1;
        end;

      end if;
      fetch acur into fdata;
    end loop;
    close acur;
    ins_extract_recon('MASTERCARD_HEADER',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
    commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.MASTERCARD_HEADERETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end MASTERCARD_HEADERETL;

  procedure INVOICE_FEE_GROUPETL is

  cursor acur is
  select
    src.sk_client_fee_id
   ,nvl(trg.sk_client_fee_id,0)     as target_sk_client_fee_id
   ,trg.rowid                       as target_rowid
   ,src.client_id
   ,src.fee_group
   ,src.fee_name
   ,src.transaction_currency
   ,src.transaction_type
   ,src.channel_type
   ,src.fixed_or_percent
   ,src.fee_value
   ,src.invoice_frequency
   ,src.tax_discount
  from
  (
    select
      id                        sk_client_fee_id,
      ra_client_id              client_id,
      fee_group,
      fee_name,
      transaction_currency,
      transaction_type,
      channel_type,
      fixed_or_percent,
      value                     fee_value,
      frequency                 invoice_frequency,
      tax_discount
    from  ra_client_fees_mcp_ext
  ) src
    ,invoice_fee_group           trg
  where src.sk_client_fee_id = trg.sk_client_fee_id(+)
  and   not exists
  (
    select  1
    from    invoice_fee_group
    where   sk_client_fee_id              = src.sk_client_fee_id
    and     nvl(client_id,0)              = nvl(src.client_id,0)
    and     nvl(fee_group,' ')            = nvl(src.fee_group,' ')
    and     nvl(fee_name,' ')             = nvl(src.fee_name,' ')
    and     nvl(transaction_currency,' ') = nvl(src.transaction_currency,' ')
    and     nvl(transaction_type,' ')     = nvl(src.transaction_type,' ')
    and     nvl(channel_type,0)           = nvl(src.channel_type,0)
    and     nvl(fixed_or_percent,' ')     = nvl(src.fixed_or_percent,' ')
    and     nvl(fee_value,-999)           = nvl(src.fee_value,-999)
    and     nvl(invoice_frequency,' ')    = nvl(src.invoice_frequency,' ')
    and     nvl(tax_discount,-999)        = nvl(src.tax_discount,-999)
  );

  fdata          acur%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

  open acur;
  fetch acur into fdata;
  while acur%found
  loop

    vRecordsRead := vRecordsRead + 1;

    if fdata.target_sk_client_fee_id <> 0
    then

      begin

        update
          invoice_fee_group t
        set
          t.sk_client_fee_id        = fdata.sk_client_fee_id,
          t.client_id               = fdata.client_id,
          t.fee_group               = fdata.fee_group,
          t.fee_name                = fdata.fee_name,
          t.transaction_currency    = fdata.transaction_currency,
          t.transaction_type        = fdata.transaction_type,
          t.channel_type            = fdata.channel_type,
          t.fixed_or_percent        = fdata.fixed_or_percent,
          t.fee_value               = fdata.fee_value,
          t.invoice_frequency       = fdata.invoice_frequency,
          t.tax_discount            = fdata.tax_discount
        where
          rowid = fdata.target_rowid;

        vRecordsUpd := vRecordsUpd + 1;

      exception
        when others then
          vEMessage := 'INVOICE_FEE_GROUP - ' || sqlerrm;
          ins_txn_err(fdata.sk_client_fee_id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    else

      begin
        insert into invoice_fee_group
        (
          sk_client_fee_id,
          client_id,
          fee_group,
          fee_name,
          transaction_currency,
          transaction_type,
          channel_type,
          fixed_or_percent,
          fee_value,
          invoice_frequency,
          tax_discount
        )
        values
        (
          fdata.sk_client_fee_id,
          fdata.client_id,
          fdata.fee_group,
          fdata.fee_name,
          fdata.transaction_currency,
          fdata.transaction_type,
          fdata.channel_type,
          fdata.fixed_or_percent,
          fdata.fee_value,
          fdata.invoice_frequency,
          fdata.tax_discount
        );

        vRecordsIns := vRecordsIns + 1;

      exception
        when others then
          vEMessage := 'INVOICE_FEE_GROUP - ' || sqlerrm;
          ins_txn_err(fdata.sk_client_fee_id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    end if;

    commit;

    fetch acur into fdata;

  end loop;
  close acur;

  ins_extract_recon('INVOICE_FEE_GROUP',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.INVOICE_FEE_GROUPETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end INVOICE_FEE_GROUPETL;


  procedure BUDGET_FORECAST_EX_RATESETL is

  cursor acur is
  select
    src.year_rate_applicable
   ,src.currency_for
   ,src.currency_against
   ,trg.year_rate_applicable        as target_year_rate_applicable
   ,trg.rowid                       as target_rowid
   ,src.fx_rate
   ,src.last_updated   --SCP-MCP merger PI Feature P57
   ,src.last_updated_by  --SCP-MCP merger PI Feature P57
  from
  (
    select
      year_rate_applicable
      ,currency_for
      ,currency_against
      ,fx_rate
      ,last_updated     --SCP-MCP merger PI Feature P57
      ,last_updated_by   --SCP-MCP merger PI Feature P57
    from  ra_budget_forecast_ex_rates
    where exists
    (
      select  1
      from    currency
      where   iso_code = currency_for
    )
    and exists
    (
      select  1
      from    currency
      where   iso_code = currency_against
    )
  ) src
    ,budget_forecast_ex_rates   trg
  where src.year_rate_applicable  = trg.year_rate_applicable(+)
  and   src.currency_for          = trg.currency_for(+)
  and   src.currency_against      = trg.currency_against(+)
  and   not exists
  (
    select  1
    from    budget_forecast_ex_rates
    where   year_rate_applicable  = src.year_rate_applicable
    and     currency_for          = src.currency_for
    and     currency_against      = src.currency_against
    and     nvl(fx_rate,-999)     = nvl(src.fx_rate,-999)
  );

  fdata          acur%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

  open acur;
  fetch acur into fdata;
  while acur%found loop

    vRecordsRead := vRecordsRead + 1;

    if fdata.target_year_rate_applicable IS NOT NULL then

      begin

        update
          budget_forecast_ex_rates t
        set
          t.fx_rate        = fdata.fx_rate
          , t.last_updated   = fdata.last_updated    --SCP-MCP merger PI Feature P57
          , t.last_updated_by  = fdata.last_updated_by   --SCP-MCP merger PI Feature P57
        where
          rowid = fdata.target_rowid;

        vRecordsUpd := vRecordsUpd + 1;

      exception
        when others then
          vEMessage := 'BUDGET_FORECAST_EX_RATES - ' || sqlerrm;
          ins_txn_err(fdata.year_rate_applicable||' '||fdata.currency_for||' '||fdata.currency_against,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    else

      begin
        insert into budget_forecast_ex_rates
        (
          year_rate_applicable,
          currency_for,
          currency_against,
          fx_rate,
          last_updated,   --SCP-MCP merger PI Feature P57
          last_updated_by  --SCP-MCP merger PI Feature P57
        )
        values
        (
          fdata.year_rate_applicable,
          fdata.currency_for,
          fdata.currency_against,
          fdata.fx_rate,
          fdata.last_updated,   --SCP-MCP merger PI Feature P57
          fdata.last_updated_by    --SCP-MCP merger PI Feature P57
        );


        vRecordsIns := vRecordsIns + 1;

      exception
        when others then
          vEMessage := 'BUDGET_FORECAST_EX_RATES - ' || sqlerrm;
          ins_txn_err(fdata.year_rate_applicable||' '||fdata.currency_for||' '||fdata.currency_against,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    end if;

    commit;

    fetch acur into fdata;

  end loop;
  close acur;

  ins_extract_recon('BUDGET_FORECAST_EX_RATES',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.BUDGET_FORECAST_EX_RATESETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end BUDGET_FORECAST_EX_RATESETL;

  procedure SIX_MONTHLY_CONFIG
 is
cursor acur is
   select trgt.rowid target_rowid,
        src.id src_id,
        trgt.id trgt_id,
        src.ica,
        src.stmt_flag,
        src.frequency_in_months,
        src.last_stmt_end_date,
        src.next_stmt_end_date,
        src.last_stmt_run_date,
        src.next_stmt_run_date,
        src.stmt_message,
        src.last_updated,
        src.last_updated_by,
        src.last_filename,
        src.from_address,
        src.to_address,
        src.cc_address,
        src.bcc_address,
        src.mail_subject,
        src.mail_content
    from
      RA_MCP_SIX_MONTH_STMT_CONFIG src,
      MCP_SIX_MONTH_STMT_CONFIG TRGT
    where src.id = trgt.id(+);

    fdata          acur%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

    open acur;
    fetch acur into fdata;
    while acur%found
    loop

      vRecordsRead := vRecordsRead + 1;

      if fdata.trgt_id is not null
      then

    begin
          update
            mcp_six_month_stmt_config t
          set
            ica = fdata.ica,
            stmt_flag = fdata.stmt_flag,
            frequency_in_months = fdata.frequency_in_months,
            last_stmt_end_date = fdata.last_stmt_end_date,
            next_stmt_end_date = fdata.next_stmt_end_date,
            last_stmt_run_date = fdata.last_stmt_run_date,
            next_stmt_run_date = fdata.next_stmt_run_date,
            stmt_message = fdata.stmt_message,
            last_updated = fdata.last_updated,
            last_updated_by = fdata.last_updated_by,
            last_filename = fdata.last_filename,
            mail_subject = fdata.mail_subject,
            mail_content = fdata.mail_content,
            from_address = fdata.from_address,
            to_address = fdata.to_address,
            cc_address = fdata.cc_address,
            bcc_address = fdata.bcc_address
          where
            rowid = fdata.target_rowid;

          vRecordsUpd := vRecordsUpd + 1;

        exception
          when others then
            vEMessage := 'SIX_MONTH_STMT_CONFIG - ' || sqlerrm;
            ins_txn_err(fdata.ICA,vEMessage);
            vRecordsErr := vRecordsErr + 1;
        end;

      elsif fdata.trgt_id is null then
        begin
          insert into MCP_SIX_MONTH_STMT_CONFIG
          ( id,
            ica,
            stmt_flag,
            frequency_in_months,
            last_stmt_end_date,
            next_stmt_end_date,
            last_stmt_run_date,
            next_stmt_run_date,
            stmt_message,
            last_updated,
            last_updated_by,
            last_filename,
            from_address,
            to_address,
            cc_address,
            bcc_address,
            mail_subject,
            mail_content
          )
          values
          (fdata.src_id,
            fdata.ICA,
            fdata.stmt_flag,
            fdata.frequency_in_months,
            fdata.last_stmt_end_date,
            fdata.next_stmt_end_date,
            fdata.last_stmt_run_date,
            fdata.next_stmt_run_date,
            fdata.stmt_message,
            fdata.last_updated,
            fdata.last_updated_by,
            fdata.last_filename,
            fdata.from_address,
            fdata.to_address,
            fdata.cc_address,
            fdata.bcc_address,
            fdata.mail_subject,
            fdata.mail_content
          );
          commit;

          vRecordsIns := vRecordsIns + 1;

        exception
          when others then
            vEMessage := 'SIX_MONTH_STMT_CONFIG - ' || sqlerrm;
            ins_txn_err(fdata.ICA,vEMessage);
            vRecordsErr := vRecordsErr + 1;
        end;

      end if;
      fetch acur into fdata;
    end loop;
    close ACUR;
    ins_extract_recon('SIX_MONTH_STMT_CONFIG',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
    commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.SIX_MONTHLY_CONFIG',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end SIX_MONTHLY_CONFIG;

  --Added for CR0433
procedure COUNTRY_ETL is
  cursor acur is
  SELECT trg.rowid                              AS target_rowid ,
      NVL(trg.ISO_CODE,'X')                     AS trg_ISO_CODE,
      src.COUNTRY_ISO_CODE                      AS ISO_CODE ,
      src.country_iso_code3                     AS iso_code_3 ,
      lpad(TO_CHAR(src.COUNTRY_ISO_NUM),3,'0')  AS ISO_NUM_CODE ,
      src.COUNTRY_NAME                          AS NAME ,
      src.currency_iso_code                     AS currency_code ,
      src.country_name                          AS full_name ,
      src.manage_country_tax                    AS manage_country_tax,
      src.geo_sub_region_id                     AS geo_sub_region_id,  -- SCP-MCP merger PI Feature P57
      src.last_updated                          AS last_updated,   -- SCP-MCP merger PI Feature P57
      src.last_updated_by                      AS  last_updated_by   -- SCP-MCP merger PI Feature P57
    FROM  cpradmin.RA_COUNTRY src ,
          COUNTRY trg
    WHERE src.COUNTRY_ISO_CODE = trg.ISO_CODE(+)
    AND NOT EXISTS
    (
      SELECT 1
      from COUNTRY
      WHERE ISO_CODE                 = src.COUNTRY_ISO_CODE
      and nvl(iso_code_3,'X')        = nvl(src.country_iso_code3,'X')
      AND NVL(ISO_NUM_CODE,'X')      = NVL(lpad(TO_CHAR(src.COUNTRY_ISO_NUM),3,'0'),'X')
      and nvl(name,'X')              = nvl(src.country_name,'X')
      and nvl(currency_code,'X')     = nvl(src.currency_iso_code,'X')
      and nvl(full_name,'X')         = nvl(src.country_name,'X')
      and nvl(manage_country_tax,99) = nvl(src.manage_country_tax,99)
      and nvl(geo_sub_region_id,'99') = nvl(geo_sub_region_id,'99')  -- SCP-MCP merger PI Feature P57
      and nvl(last_updated, sysdate) = nvl(last_updated, sysdate)  -- SCP-MCP merger PI Feature P57
      and nvl(last_updated_by, 'X')  = nvl(last_updated_by, 'X')  -- SCP-MCP merger PI Feature P57
    );

  fdata          acur%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

    open acur;
    fetch acur into fdata;
    while acur%found
    loop

    vRecordsRead := vRecordsRead + 1;

    IF fdata.trg_ISO_CODE <> 'X' THEN

      BEGIN

        UPDATE COUNTRY t
        SET t.iso_code_3       = fdata.iso_code_3 ,
          t.iso_num_code       = fdata.iso_num_code ,
          t.name               = fdata.name ,
          t.currency_code      = fdata.currency_code ,
          t.full_name          = fdata.full_name ,
          t.manage_country_tax = fdata.manage_country_tax,
          t.geo_sub_region_id  = fdata.geo_sub_region_id,
          t.last_updated       = fdata.last_updated,  -- SCP-MCP merger PI Feature P57
          t.last_updated_by    = fdata.last_updated_by   -- SCP-MCP merger PI Feature P57
        WHERE t.rowid          = fdata.target_rowid;  -- SCP-MCP merger PI Feature P57

        vRecordsUpd := vRecordsUpd + 1;

      EXCEPTION
        when others then
          vEMessage := 'COUNTRY - ' || sqlerrm;
          ins_txn_err(fdata.ISO_CODE,vEMessage);
          vRecordsErr := vRecordsErr + 1;

      END;

    ELSE

      begin
        INSERT INTO COUNTRY
        (
          iso_code,
          iso_code_3,
          iso_num_code,
          name,
          currency_code,
          full_name,
          manage_country_tax,
          geo_sub_region_id,  -- SCP-MCP merger PI Feature P57
          last_updated,  -- SCP-MCP merger PI Feature P57
          last_updated_by  -- SCP-MCP merger PI Feature P57
        )
        VALUES
        (
          fdata.iso_code,
          fdata.iso_code_3,
          fdata.iso_num_code,
          fdata.name,
          fdata.currency_code,
          fdata.full_name,
          fdata.manage_country_tax,
          fdata.geo_sub_region_id,  -- SCP-MCP merger PI Feature P57
          fdata.last_updated,   -- SCP-MCP merger PI Feature P57
          fdata.last_updated_by   -- SCP-MCP merger PI Feature P57
        );

        vRecordsIns := vRecordsIns + 1;

      EXCEPTION
        WHEN OTHERS THEN
          vEMessage := 'COUNTRY - ' || sqlerrm;
          ins_txn_err(fdata.ISO_CODE,vEMessage);
          vRecordsErr := vRecordsErr + 1;


      END;

    END IF;

    FETCH acur INTO fdata;
  END LOOP;
  CLOSE acur;
  ins_extract_recon('COUNTRY',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  COMMIT;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.COUNTRY_ETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  END COUNTRY_ETL;


----Added as part CMP0630
PROCEDURE INVOICE_CLIENT_FEE_ETL IS
CURSOR c_get_cli_fee IS
 SELECT trg.rowid                              AS target_rowid ,
        nvl(trg.sk_client_fee_id,0) sk_client_fee_id,
        src.sk_client_fee_id        src_sk_client_fee_id,
        src.client_id    ,
        src.transaction_currency    ,
        src.channel_type    ,
        src.fixed_or_percent    ,
        src.fee_value    ,
        src.invoice_frequency    ,
        src.tax_discount    ,
        src.fee_type_id    ,
        src.fee_settlement_currency    ,
        src.order_or_purse    ,
        src.min_value    ,
        src.max_value    ,
        src.tier_band_start    ,
        src.tier_band_end    ,
        src.tier_flag    ,
        src.tier_period    ,
        src.value_conversion_rule    ,
        src.status    ,
        src.fees_comm_client_share
    FROM  RA_invoice_client_fee src ,
          invoice_client_fee trg
    WHERE src.SK_CLIENT_FEE_ID = trg.SK_CLIENT_FEE_ID(+)
    AND NOT EXISTS
   (
        SELECT 1
        from invoice_client_fee
        WHERE
        NVL(CLIENT_ID,0)                 = NVL(src.CLIENT_ID,0)
        AND NVL(TRANSACTION_CURRENCY,'X')     = NVL(src.TRANSACTION_CURRENCY,'X')
        AND         NVL(CHANNEL_TYPE,0)             = NVL(src.CHANNEL_TYPE,0)
                AND NVL(FIXED_OR_PERCENT,'X')        = NVL(src.FIXED_OR_PERCENT,'X')
                AND NVL(FEE_VALUE,0)                = NVL(src.FEE_VALUE,0)
                AND NVL(INVOICE_FREQUENCY,'X')        = NVL(src.INVOICE_FREQUENCY,'X')
              AND   NVL(TAX_DISCOUNT,0)                = NVL(src.TAX_DISCOUNT,0)
                AND NVL(FEE_TYPE_ID,'X')            = NVL(src.FEE_TYPE_ID,'X')
               AND  NVL(FEE_SETTLEMENT_CURRENCY,'X')= NVL(src.FEE_SETTLEMENT_CURRENCY,'X')
                AND NVL(ORDER_OR_PURSE,'X')            = NVL(src.ORDER_OR_PURSE,'X')
                AND NVL(MIN_VALUE,0)                = NVL(src.MIN_VALUE,0)
               AND  NVL(MAX_VALUE,0)                = NVL(src.MAX_VALUE,0)
                AND NVL(TIER_BAND_START,0)            = NVL(src.TIER_BAND_START,0)
               AND  NVL(TIER_BAND_END,0)            = NVL(src.TIER_BAND_END,0)
               AND  NVL(TIER_FLAG,'X')              = NVL(src.TIER_FLAG,'X')
               AND  NVL(TIER_PERIOD,'X')            = NVL(src.TIER_PERIOD,'X')
                AND NVL(VALUE_CONVERSION_RULE,'X')  = NVL(src.VALUE_CONVERSION_RULE,'X')
               AND  NVL(STATUS,'X')                 = NVL(src.STATUS,'X')
                AND NVL(FEE_COMM_SHARE_PER,0)   = NVL(src.FEES_COMM_CLIENT_SHARE,0)
    );

  fdata          c_get_cli_fee%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

    open c_get_cli_fee;
    fetch c_get_cli_fee into fdata;
    while c_get_cli_fee%found
    loop

    vRecordsRead := vRecordsRead + 1;

    IF fdata.sk_client_fee_id <> 0 THEN

      BEGIN

        UPDATE INVOICE_CLIENT_FEE t
        SET
        t.CLIENT_ID=fdata.CLIENT_ID,
        t.TRANSACTION_CURRENCY=fdata.TRANSACTION_CURRENCY,
        t.CHANNEL_TYPE=fdata.CHANNEL_TYPE,
        t.FIXED_OR_PERCENT=fdata.FIXED_OR_PERCENT,
        t.FEE_VALUE=fdata.FEE_VALUE,
        t.INVOICE_FREQUENCY=fdata.INVOICE_FREQUENCY,
        t.TAX_DISCOUNT=fdata.TAX_DISCOUNT,
        t.FEE_TYPE_ID=fdata.FEE_TYPE_ID,
        t.FEE_SETTLEMENT_CURRENCY=fdata.FEE_SETTLEMENT_CURRENCY,
        t.ORDER_OR_PURSE=fdata.ORDER_OR_PURSE,
        t.MIN_VALUE=fdata.MIN_VALUE,
        t.MAX_VALUE=fdata.MAX_VALUE,
        t.TIER_BAND_START=fdata.TIER_BAND_START,
        t.TIER_BAND_END=fdata.TIER_BAND_END,
        t.TIER_FLAG=fdata.TIER_FLAG,
        t.TIER_PERIOD=fdata.TIER_PERIOD,
        t.VALUE_CONVERSION_RULE=fdata.VALUE_CONVERSION_RULE,
        t.STATUS=fdata.STATUS,
        t.FEE_COMM_SHARE_PER=fdata.FEES_COMM_CLIENT_SHARE,
        t.LAST_UPDATED = SYSDATE
        WHERE t.rowid          = fdata.target_rowid;

        vRecordsUpd := vRecordsUpd + 1;

      EXCEPTION
        when others then
          vEMessage := 'INVOICE_CLIENT_FEE - ' || sqlerrm;
          ins_txn_err(fdata.sk_client_fee_id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      END;

    ELSE

      begin
        INSERT INTO INVOICE_CLIENT_FEE
        (
        sk_client_fee_id    ,
        CLIENT_ID    ,
        TRANSACTION_CURRENCY    ,
        CHANNEL_TYPE    ,
        FIXED_OR_PERCENT    ,
        FEE_VALUE    ,
        INVOICE_FREQUENCY    ,
        TAX_DISCOUNT    ,
        FEE_TYPE_ID    ,
        FEE_SETTLEMENT_CURRENCY    ,
        ORDER_OR_PURSE    ,
        MIN_VALUE    ,
        MAX_VALUE    ,
        TIER_BAND_START    ,
        TIER_BAND_END    ,
        TIER_FLAG    ,
        TIER_PERIOD    ,
        VALUE_CONVERSION_RULE    ,
        STATUS    ,
        FEE_COMM_SHARE_PER    ,
        DATE_INSERTED
        )
        VALUES
        (
        fdata.src_sk_client_fee_id,
        fdata.CLIENT_ID,
        fdata.TRANSACTION_CURRENCY,
        fdata.CHANNEL_TYPE,
        fdata.FIXED_OR_PERCENT,
        fdata.FEE_VALUE,
        fdata.INVOICE_FREQUENCY,
        fdata.TAX_DISCOUNT,
        fdata.FEE_TYPE_ID,
        fdata.FEE_SETTLEMENT_CURRENCY,
        fdata.ORDER_OR_PURSE,
        fdata.MIN_VALUE,
        fdata.MAX_VALUE,
        fdata.TIER_BAND_START,
        fdata.TIER_BAND_END,
        fdata.TIER_FLAG,
        fdata.TIER_PERIOD,
        fdata.VALUE_CONVERSION_RULE,
        fdata.STATUS,
        fdata.FEES_COMM_CLIENT_SHARE,
        SYSDATE
        );
        vRecordsIns := vRecordsIns + 1;

      EXCEPTION
        WHEN OTHERS THEN
          vEMessage := 'INVOICE_CLIENT_FEE - ' || sqlerrm;
          ins_txn_err(fdata.sk_client_fee_id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      END;

    END IF;

    FETCH c_get_cli_fee INTO fdata;
  END LOOP;
  CLOSE c_get_cli_fee;
  ins_extract_recon('INVOICE_CLIENT_FEE',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  COMMIT;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.INVOICE_CLIENT_FEE_ETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

 END INVOICE_CLIENT_FEE_ETL;



 ----Added as part CMP0630
PROCEDURE INV_CLIENT_FEE_COMM_SHARE_ETL IS
CURSOR c_get_cli_fee_comm IS
 SELECT trg.rowid                              AS target_rowid ,
        nvl(trg.id,0) id,
        src.id  src_id,
        src.client_id,
        src.transaction_currency,
        src.fee_type_id,
        src.fee_settlement_currency,
        src.order_or_purse,
        src.fee_comm_share_per,
        src.status
    FROM  RA_INV_CLI_FEE_COMM_SHARE src ,
          INV_CLIENT_FEE_COMM_SHARE trg
    WHERE src.id = trg.id(+)
    AND NOT EXISTS
   (
        SELECT 1
        from INV_CLIENT_FEE_COMM_SHARE
        WHERE
        NVL(CLIENT_ID,0)                 = NVL(src.CLIENT_ID,0)
        AND NVL(transaction_currency,'X')     = NVL(src.transaction_currency,'X')
        AND NVL(fee_type_id,'X')     = NVL(src.fee_type_id,'X')
        AND NVL(fee_settlement_currency,'X')     = NVL(src.fee_settlement_currency,'X')
        AND NVL(order_or_purse,'X')     = NVL(src.order_or_purse,'X')
        AND NVL(fee_comm_share_per, 0)     = NVL(src.fee_comm_share_per,0)
        AND NVL(status,'X')     = NVL(src.status,'X')
    );

  fdata          c_get_cli_fee_comm%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

    open c_get_cli_fee_comm;
    fetch c_get_cli_fee_comm into fdata;
    while c_get_cli_fee_comm%found
    loop

    vRecordsRead := vRecordsRead + 1;

    IF fdata.id <> 0 THEN

      BEGIN

        UPDATE INV_CLIENT_FEE_COMM_SHARE t
        SET
        t.CLIENT_ID=fdata.CLIENT_ID,
        t.TRANSACTION_CURRENCY=fdata.TRANSACTION_CURRENCY,
        t.fee_type_id=fdata.fee_type_id,
        t.fee_settlement_currency=fdata.fee_settlement_currency,
        t.order_or_purse=fdata.order_or_purse,
        t.fee_comm_share_per=fdata.fee_comm_share_per,
        t.status=fdata.status,
        t.LAST_UPDATED = SYSDATE
        WHERE t.rowid          = fdata.target_rowid;

        vRecordsUpd := vRecordsUpd + 1;

      EXCEPTION
        when others then
          vEMessage := 'INV_CLIENT_FEE_COMM_SHARE_ETL - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      END;

    ELSE

      begin
        INSERT INTO INV_CLIENT_FEE_COMM_SHARE
        (
        id,
        client_id,
        transaction_currency,
        fee_type_id,
        fee_settlement_currency,
        order_or_purse,
        fee_comm_share_per,
        status,
        DATE_INSERTED
        )
        VALUES
        (
        fdata.src_id,
        fdata.CLIENT_ID,
        fdata.TRANSACTION_CURRENCY,
        fdata.FEE_TYPE_ID,
        fdata.FEE_SETTLEMENT_CURRENCY,
        fdata.ORDER_OR_PURSE,
        fdata.fee_comm_share_per,
        fdata.STATUS,
        SYSDATE
        );
        vRecordsIns := vRecordsIns + 1;

      EXCEPTION
        WHEN OTHERS THEN
          vEMessage := 'INV_CLIENT_FEE_COMM_SHARE_ETL - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      END;

    END IF;

    FETCH c_get_cli_fee_comm INTO fdata;
  END LOOP;
  CLOSE c_get_cli_fee_comm;
  ins_extract_recon('INV_CLIENT_FEE_COMM_SHARE_ETL',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  COMMIT;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.INV_CLIENT_FEE_COMM_SHARE_ETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

 END INV_CLIENT_FEE_COMM_SHARE_ETL;

 --CMP0769 Changes Starts
 procedure CURRENCY_ETL IS
  cursor acur is
  SELECT trg.rowid                              AS target_rowid ,
      nvl(trg.iso_code,'X')                     as trg_iso_code,
      src.currency_iso_code                     as iso_code ,
      src.currency_name                         as name ,
      src.iso_num_code                          as iso_num_code ,
      src.exponent                              as exponent ,
      src.mc_settlement_flag                    as mc_settlement_flag,
      src.pref_deal_curr_flag                   as pref_deal_curr_flag,  --SCP-MCP merger PI Feature P57
      src.pref_settlement_curr_flag             as pref_settlement_curr_flag,  --SCP-MCP merger PI Feature P57
      src.last_updated                          as last_updated,   --SCP-MCP merger PI Feature P57
      src.last_updated_by                       as last_updated_by   --SCP-MCP merger PI Feature P57
   FROM  RA_CURRENCY src ,
          CURRENCY trg
    WHERE src.CURRENCY_ISO_CODE = trg.ISO_CODE(+)
    AND NOT EXISTS
    (
      SELECT 1
      from CURRENCY
      WHERE iso_code                 = src.currency_iso_code
      and nvl(iso_num_code,'X')      = nvl(src.iso_num_code,'X')
      and nvl(name,'X')              = nvl(src.currency_name,'X')
      and nvl(exponent,'0')          = nvl(src.exponent,'0')
      and nvl(mc_settlement_flag,'X') = nvl(src.mc_settlement_flag,'X')
      and nvl(pref_deal_curr_flag,'X') = nvl(src.pref_deal_curr_flag,'X')   --SCP-MCP merger PI Feature P57
      and nvl(pref_settlement_curr_flag,'X') = nvl(src.pref_settlement_curr_flag,'X')  --SCP-MCP merger PI Feature P57
      and nvl(last_updated, sysdate)   = nvl(src.last_updated,sysdate)  --SCP-MCP merger PI Feature P57
      and nvl(last_updated_by,'X')     = nvl(src.last_updated_by,'X')    --SCP-MCP merger PI Feature P57
    );

  fdata          acur%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

    open acur;
    fetch acur into fdata;
    while acur%found
    loop

    vRecordsRead := vRecordsRead + 1;

    IF fdata.trg_ISO_CODE <> 'X' THEN

      BEGIN

        UPDATE CURRENCY t
        SET t.iso_code              = fdata.iso_code ,
          t.iso_num_code            = fdata.iso_num_code ,
          t.name                    = fdata.name ,
          t.exponent                = fdata.exponent ,
          t.mc_settlement_flag      = fdata.mc_settlement_flag,
          t.pref_deal_curr_flag     = fdata.pref_deal_curr_flag,
          t.pref_settlement_curr_flag = fdata.pref_settlement_curr_flag,  --SCP-MCP merger PI Feature P57
          t.last_updated            = fdata.last_updated,  --SCP-MCP merger PI Feature P57
          t.last_updated_by         = fdata.last_updated_by    --SCP-MCP merger PI Feature P57
        where t.rowid               = fdata.target_rowid;   --SCP-MCP merger PI Feature P57

        vRecordsUpd := vRecordsUpd + 1;

      EXCEPTION
        when others then
          vEMessage := 'CURRENCY - ' || sqlerrm;
          ins_txn_err(fdata.ISO_CODE,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      END;

    ELSE

      begin
        INSERT INTO CURRENCY
        (
          iso_code,
          iso_num_code,
          name,
          exponent,
          mc_settlement_flag,
          pref_deal_curr_flag,  --SCP-MCP merger PI Feature P57
          pref_settlement_curr_flag,  --SCP-MCP merger PI Feature P57
          last_updated,    --SCP-MCP merger PI Feature P57
          last_updated_by  --SCP-MCP merger PI Feature P57
        )
        VALUES
        (
          fdata.iso_code,
          fdata.iso_num_code,
          fdata.name,
          fdata.exponent,
          fdata.mc_settlement_flag,
          fdata.pref_deal_curr_flag,  --SCP-MCP merger PI Feature P57
          fdata.pref_settlement_curr_flag,  --SCP-MCP merger PI Feature P57
          fdata.last_updated,  --SCP-MCP merger PI Feature P57
          fdata.last_updated_by  --SCP-MCP merger PI Feature P57
        );

        vRecordsIns := vRecordsIns + 1;

      EXCEPTION
        WHEN OTHERS THEN
          vEMessage := 'CURRENCY - ' || sqlerrm;
          ins_txn_err(fdata.ISO_CODE,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      END;

    END IF;

    FETCH acur INTO fdata;
  END LOOP;
  CLOSE acur;
  ins_extract_recon('CURRENCY',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  COMMIT;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.CURRENCY_ETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  END CURRENCY_ETL;

  procedure CFG_IPS_CURRENCY_CODES_ETL IS
  cursor acur is
  SELECT trg.rowid                              AS target_rowid ,
      NVL(trg.ALPHA,'X')                     AS trg_ALPHA,
      src.ISO_NUM_CODE                         AS NUMCODE ,
      src.CURRENCY_ISO_CODE                     AS ALPHA ,
      src.CURRENCY_NAME                         AS CURRENCY_NAME ,
      src.EXPONENT                          AS EXPONENT
    FROM  RA_CURRENCY src ,
          CFG_IPS_CURRENCY_CODES trg
    WHERE src.CURRENCY_ISO_CODE = trg.ALPHA(+)
    AND NOT EXISTS
    (
      SELECT 1
      from CPRADMIN.CFG_IPS_CURRENCY_CODES
      WHERE NUMCODE                 = src.ISO_NUM_CODE
      and nvl(ALPHA,'X')        = nvl(src.CURRENCY_ISO_CODE,'X')
     AND NVL(CURRENCY_NAME,'X')  =nvl(src.CURRENCY_NAME,'X')
      and nvl(EXPONENT,'0')              = nvl(src.EXPONENT,'0')
      );

  fdata          acur%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

    open acur;
    fetch acur into fdata;
    while acur%found
    loop

    vRecordsRead := vRecordsRead + 1;

    IF fdata.trg_ALPHA <> 'X' THEN

      BEGIN

        UPDATE CFG_IPS_CURRENCY_CODES t
        SET t.NUMCODE       = fdata.NUMCODE ,
          t.ALPHA       = fdata.ALPHA ,
          t.CURRENCY_NAME               = fdata.CURRENCY_NAME ,
          t.EXPONENT      = fdata.EXPONENT
        WHERE t.rowid          = fdata.target_rowid;

        vRecordsUpd := vRecordsUpd + 1;

      EXCEPTION
        when others then
          vEMessage := 'COUNTRY - ' || sqlerrm;
          ins_txn_err(fdata.ALPHA,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      END;

    ELSE

      begin
        INSERT INTO CFG_IPS_CURRENCY_CODES
        (
          NUMCODE,
          ALPHA,
          CURRENCY_NAME,
          EXPONENT
        )
        VALUES
        (
          fdata.NUMCODE,
          fdata.ALPHA,
          fdata.CURRENCY_NAME,
          fdata.EXPONENT
        );

        vRecordsIns := vRecordsIns + 1;

      EXCEPTION
        WHEN OTHERS THEN
          vEMessage := 'COUNTRY - ' || sqlerrm;
          ins_txn_err(fdata.ALPHA,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      END;

    END IF;

    FETCH acur INTO fdata;
  END LOOP;
  CLOSE acur;
  ins_extract_recon('COUNTRY',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  COMMIT;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.COUNTRY_ETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );
  END CFG_IPS_CURRENCY_CODES_ETL;


 --CMP0769 Changes Ends


  -- Remove share agreements that have been deleted from the cpradmin table
  -- but have not started gathering data.
  -- To remove this as well the revenue_share_details table will need to be cleaned

  procedure CLEAN_SHARE_AGREEMENT is

  begin
    delete from rev_share_agmt_item
    where not exists
    (
      select  1
      from    ra_mcp_rev_share_agmt_item
      where   id = rev_share_agmt_item.id
    )
    and not exists
    (
      select  1
      from    revenue_share_item_details
      where   std_rev_item_id = rev_share_agmt_item.id
    );

    delete from std_rev_item_component
    where not exists
    (
      select  1
      from    ra_mcp_std_rev_item_component
      where   std_rev_item_id       = std_rev_item_component.std_rev_item_id
      and     std_rev_component_id  = std_rev_item_component.std_rev_component_id
    );

    delete from std_rev_item
    where not exists
    (
      select  1
      from    ra_mcp_std_rev_item
      where   id = std_rev_item.id
    )
    and not exists
    (
      select  1
      from    rev_share_agmt_item
      where   std_rev_item_id = std_rev_item.id
    );

    delete from std_rev_component
    where not exists
    (
      select  1
      from    ra_mcp_std_rev_component
      where   id = std_rev_component.id
    )
    and not exists
    (
      select  1
      from    std_rev_item_component
      where   std_rev_component_id=std_rev_component.id
    );

    delete from rev_share_agmt_ica
    where not exists
    (
      select  1
      from    ra_mcp_rev_share_agmt_ica
      where   id = rev_share_agmt_ica.id
    );

    delete from rev_share_agreement
    where not exists
    (
      select  1
      from    ra_mcp_rev_share_agreement
      where   id = rev_share_agreement.id
    )
    and not exists
    (
      select  1
      from    revenue_share_item_details
      where   rev_share_agreement_id = rev_share_agreement.id
    );

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.CLEAN_SHARE_AGREEMENT',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end CLEAN_SHARE_AGREEMENT;


  procedure REV_SHARE_AGREEMENTETL is

  cursor acur is
  select
    src.id
    ,trg.id                          as target_id
    ,trg.rowid                       as target_rowid
    ,src.name
    ,src.start_date
    ,src.end_date
    ,src.rate_type
  from
  (
    select
      id
      ,name
      ,start_date
      ,end_date
      ,rate_type
    from  ra_mcp_rev_share_agreement
  ) src
    ,rev_share_agreement   trg
  where src.id  = trg.id(+)
  and   not exists
  (
    select  1
    from    rev_share_agreement
    where   id                  = src.id
    and     name                = src.name
    and     start_date          = src.start_date
    and     end_date            = src.end_date
    and     nvl(rate_type,' ')  = nvl(src.rate_type,' ')
  );

  fdata          acur%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

  open acur;
  fetch acur into fdata;
  while acur%found loop

    vRecordsRead := vRecordsRead + 1;

    if fdata.target_id IS NOT NULL then

      begin

        update
          rev_share_agreement t
        set
          t.name             = fdata.name,
          t.start_date       = fdata.start_date,
          t.end_date         = fdata.end_date,
          t.rate_type        = fdata.rate_type
        where
        rowid = fdata.target_rowid;

        vRecordsUpd := vRecordsUpd + 1;

      exception
        when others then
          vEMessage := 'REV_SHARE_AGREEMENT - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    else

      begin
        insert into rev_share_agreement
        (
          id,
          name,
          start_date,
          end_date,
          rate_type
        )
        values
        (
          fdata.id,
          fdata.name,
          fdata.start_date,
          fdata.end_date,
          fdata.rate_type
        );

        vRecordsIns := vRecordsIns + 1;

      exception
        when others then
          vEMessage := 'REV_SHARE_AGREEMENT - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    end if;

    commit;

    fetch acur into fdata;

  end loop;
  close acur;

  ins_extract_recon('REV_SHARE_AGREEMENT',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.REV_SHARE_AGREEMENTETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end REV_SHARE_AGREEMENTETL;

  procedure REV_SHARE_AGMT_ICAETL is

  cursor acur is
  select
    src.id
    ,trg.id                          as target_id
    ,trg.rowid                       as target_rowid
    ,src.rev_share_agreement_id
    ,src.ica
  from
  (
    select
      id
      ,rev_share_agreement_id
      ,ica
    from  ra_mcp_rev_share_agmt_ica
  ) src
    ,rev_share_agmt_ica   trg
  where src.id  = trg.id(+)
  and   not exists
  (
    select  1
    from    rev_share_agmt_ica
    where   id                      = src.id
    and     rev_share_agreement_id  = src.rev_share_agreement_id
    and     ica                     = src.ica
  );

  fdata          acur%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

  open acur;
  fetch acur into fdata;
  while acur%found loop

    vRecordsRead := vRecordsRead + 1;

    if fdata.target_id IS NOT NULL then

      begin

        update
          rev_share_agmt_ica t
        set
          t.rev_share_agreement_id = fdata.rev_share_agreement_id
          ,t.ica                    = fdata.ica
        where
        rowid = fdata.target_rowid;

        vRecordsUpd := vRecordsUpd + 1;

      exception
        when others then
          vEMessage := 'REV_SHARE_AGMT_ICA - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    else

      begin
        insert into rev_share_agmt_ica
        (
          id,
          rev_share_agreement_id,
          ica
        )
        values
        (
          fdata.id,
          fdata.rev_share_agreement_id,
          fdata.ica
        );

        vRecordsIns := vRecordsIns + 1;

      exception
        when others then
          vEMessage := 'REV_SHARE_AGMT_ICA - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    end if;

    commit;

    fetch acur into fdata;

  end loop;
  close acur;

  ins_extract_recon('REV_SHARE_AGMT_ICA',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.REV_SHARE_AGMT_ICAETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end REV_SHARE_AGMT_ICAETL;


  procedure STD_REV_COMPONENTETL is

  cursor acur is
  select
    src.id
    ,trg.id                          as target_id
    ,trg.rowid                       as target_rowid
    ,src.source
    ,src.transaction_type
  from
  (
    select
      id
      ,source
      ,transaction_type
    from  ra_mcp_std_rev_component
  ) src
    ,std_rev_component   trg
  where src.id  = trg.id(+)
  and   not exists
  (
    select  1
    from    std_rev_component
    where   id                = src.id
    and     source            = src.source
    and     transaction_type  = src.transaction_type
  );

  fdata          acur%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

  open acur;
  fetch acur into fdata;
  while acur%found loop

    vRecordsRead := vRecordsRead + 1;

    if fdata.target_id IS NOT NULL then

      begin

        update
          std_rev_component t
        set
          t.source = fdata.source
          ,t.transaction_type = fdata.transaction_type
        where
        rowid = fdata.target_rowid;

        vRecordsUpd := vRecordsUpd + 1;

      exception
        when others then
          vEMessage := 'STD_REV_COMPONENT - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    else

      begin
        insert into std_rev_component
        (
          id,
          source,
          transaction_type
        )
        values
        (
          fdata.id,
          fdata.source,
          fdata.transaction_type
        );

        vRecordsIns := vRecordsIns + 1;

      exception
        when others then
          vEMessage := 'STD_REV_COMPONENT - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    end if;

    commit;

    fetch acur into fdata;

  end loop;
  close acur;

  ins_extract_recon('STD_REV_COMPONENT',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.STD_REV_COMPONENTETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end STD_REV_COMPONENTETL;

  procedure STD_REV_ITEMETL is

  cursor acur is
  select
    src.id
    ,trg.id                          as target_id
    ,trg.rowid                       as target_rowid
    ,src.name
    ,src.usage_type
  from
  (
    select
      id
      ,name
      ,usage_type
    from  ra_mcp_std_rev_item
  ) src
    ,std_rev_item   trg
  where src.id  = trg.id(+)
  and   not exists
  (
    select  1
    from    std_rev_item
    where   id                        = src.id
    and     name                      = src.name
    and     usage_type                = src.usage_type
  );

  fdata          acur%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

  open acur;
  fetch acur into fdata;
  while acur%found loop

    vRecordsRead := vRecordsRead + 1;

    if fdata.target_id IS NOT NULL then

      begin

        update
          std_rev_item t
        set
          t.name = fdata.name
          ,t.usage_type                 = fdata.usage_type
        where
        rowid = fdata.target_rowid;

        vRecordsUpd := vRecordsUpd + 1;

      exception
        when others then
          vEMessage := 'STD_REV_ITEM - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    else

      begin
        insert into std_rev_item
        (
          id,
          name,
          usage_type
        )
        values
        (
          fdata.id,
          fdata.name,
          fdata.usage_type
        );

        vRecordsIns := vRecordsIns + 1;

      exception
        when others then
          vEMessage := 'STD_REV_ITEM - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    end if;

    commit;

    fetch acur into fdata;

  end loop;
  close acur;

  ins_extract_recon('STD_REV_ITEM',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.STD_REV_ITEMETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end STD_REV_ITEMETL;

  procedure STD_REV_ITEM_COMPONENTETL is

  cursor acur is
  select
    src.id
    ,trg.id                          as target_id
    ,trg.rowid                       as target_rowid
    ,src.std_rev_item_id
    ,src.std_rev_component_id
    ,src.signage
  from
  (
    select
      id
      ,std_rev_item_id
      ,std_rev_component_id
      ,signage
    from  ra_mcp_std_rev_item_component
  ) src
    ,std_rev_item_component   trg
  where src.id  = trg.id(+)
  and   not exists
  (
    select  1
    from    std_rev_item_component
    where   id                    = src.id
    and     std_rev_item_id       = src.std_rev_item_id
    and     std_rev_component_id  = src.std_rev_component_id
    and     signage               = src.signage
  );

  fdata          acur%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

  open acur;
  fetch acur into fdata;
  while acur%found loop

    vRecordsRead := vRecordsRead + 1;

    if fdata.target_id IS NOT NULL then

      begin

        update
          std_rev_item_component t
        set
          t.std_rev_item_id  = fdata.std_rev_item_id
          ,t.std_rev_component_id   = fdata.std_rev_component_id
          ,t.signage                = fdata.signage
        where
        rowid = fdata.target_rowid;

        vRecordsUpd := vRecordsUpd + 1;

      exception
        when others then
          vEMessage := 'STD_REV_ITEM_COMPONENT - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    else

      begin
        insert into std_rev_item_component
        (
          id,
          std_rev_item_id,
          std_rev_component_id,
          signage
        )
        values
        (
          fdata.id,
          fdata.std_rev_item_id,
          fdata.std_rev_component_id,
          fdata.signage
        );

        vRecordsIns := vRecordsIns + 1;

      exception
        when others then
          vEMessage := 'STD_REV_ITEM_COMPONENT - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    end if;

    commit;

    fetch acur into fdata;

  end loop;
  close acur;

  ins_extract_recon('STD_REV_ITEM_COMPONENT',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.STD_REV_ITEM_COMPONENTETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end STD_REV_ITEM_COMPONENTETL;


  procedure REV_SHARE_AGMT_ITEMETL is

  cursor acur is
  select
    src.id
    ,trg.id                          as target_id
    ,trg.rowid                       as target_rowid
    ,src.rev_share_agreement_id
    ,src.std_rev_item_id
    ,src.currency
    ,src.percentage_indicator
    ,src.amount
    ,src.revenue_item_currency
  from
  (
    select
      id
      ,rev_share_agreement_id
      ,std_rev_item_id
      ,currency
      ,percentage_indicator
      ,amount
      ,revenue_item_currency
    from  ra_mcp_rev_share_agmt_item
  ) src
    ,rev_share_agmt_item   trg
  where src.id  = trg.id(+)
  and   not exists
  (
    select  1
    from    rev_share_agmt_item
    where   id                              = src.id
    and     rev_share_agreement_id          = src.rev_share_agreement_id
    and     std_rev_item_id                 = src.std_rev_item_id
    and     currency                        = src.currency
    and     nvl(percentage_indicator,' ')   = nvl(src.percentage_indicator,' ')
    and     nvl(amount,0)                   = nvl(src.amount,0)
    and     nvl(revenue_item_currency,' ')  = nvl(src.revenue_item_currency,' ')
  );

  fdata          acur%rowtype;
  begin

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

  open acur;
  fetch acur into fdata;
  while acur%found loop

    vRecordsRead := vRecordsRead + 1;

    if fdata.target_id IS NOT NULL then

      begin

        update
          rev_share_agmt_item t
        set
          t.rev_share_agreement_id  = fdata.rev_share_agreement_id
          ,t.std_rev_item_id        = fdata.std_rev_item_id
          ,t.currency               = fdata.currency
          ,t.percentage_indicator   = fdata.percentage_indicator
          ,t.amount                 = fdata.amount
          ,t.revenue_item_currency  = fdata.revenue_item_currency
        where
        rowid = fdata.target_rowid;

        vRecordsUpd := vRecordsUpd + 1;

      exception
        when others then
          vEMessage := 'REV_SHARE_AGMT_ITEM - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    else

      begin
        insert into rev_share_agmt_item
        (
          id,
          rev_share_agreement_id,
          std_rev_item_id,
          currency,
          percentage_indicator,
          amount,
          revenue_item_currency
        )
        values
        (
          fdata.id,
          fdata.rev_share_agreement_id,
          fdata.std_rev_item_id,
          fdata.currency,
          fdata.percentage_indicator,
          fdata.amount,
          fdata.revenue_item_currency
        );

        vRecordsIns := vRecordsIns + 1;

      exception
        when others then
          vEMessage := 'REV_SHARE_AGMT_ITEM - ' || sqlerrm;
          ins_txn_err(fdata.id,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      end;

    end if;

    commit;

    fetch acur into fdata;

  end loop;
  close acur;

  ins_extract_recon('REV_SHARE_AGMT_ITEM',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.REV_SHARE_AGMT_ITEMETL',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end REV_SHARE_AGMT_ITEMETL;

  ----------------------------------------------------------------------------------------------------------------------------------

  PROCEDURE create_invoice_groups_daily IS

    CURSOR  cur_invoice_client_hier IS
    SELECT  rowno,
            cli_id,
            parent_id,
            root_settlement_method,
            hier_level,
            business_name,
            NVL(inv_flag,'X') inv_flag,
            ic.client_to_bill
    FROM    inv_hier
    LEFT JOIN invoice_consolidation ic
      ON ic.client_id = inv_hier.cli_id
    ORDER BY parent_id,rowno;

    TYPE typ_invoice_client_hier IS TABLE OF cur_invoice_client_hier%ROWTYPE
    INDEX BY BINARY_INTEGER;

    TYPE t_igd IS TABLE OF invoice_groups_daily%ROWTYPE;

    v_ptr               BINARY_INTEGER;

    v_data              typ_invoice_client_hier;
    v_inv_parent        NUMBER := 0;
    v_flagged_level     NUMBER := 0;
    v_client_grouping   NUMBER := 0;
    v_trunc_string      VARCHAR2(100);
    v_limit             PLS_INTEGER := 1000;
    outrec              t_igd := t_igd();

  BEGIN

    BEGIN

      vRecordsRead := 0;
      vRecordsUpd  := 0;
      vRecordsIns  := 0;
      vRecordsErr  := 0;

      v_trunc_string := 'Truncate table invoice_groups_daily reuse storage';
      EXECUTE IMMEDIATE v_trunc_string;

      OPEN cur_invoice_client_hier;

      LOOP

        FETCH cur_invoice_client_hier
        BULK COLLECT
        INTO v_data
        LIMIT v_limit;

        IF v_data.COUNT = 0 THEN
          EXIT;
        END IF;

        v_ptr := v_data.FIRST;

        WHILE v_ptr IS NOT NULL LOOP
          -- Default to top level client (note that parent_id is the root (top level) id)
          IF v_data(v_ptr).hier_level = 1 THEN
            v_flagged_level := 0;
            v_inv_parent := v_data(v_ptr).parent_id;
            IF v_data(v_ptr).root_settlement_method = 'T' THEN
              v_client_grouping := NULL;
            ELSE
              v_client_grouping := v_data(v_ptr).client_to_bill;
            END IF;
          END IF;

          -- if in an invoice flag hierarchy check for end if level and default back to the root level
          IF v_inv_parent <> v_data(v_ptr).parent_id AND v_data(v_ptr).hier_level <= v_flagged_level AND v_client_grouping IS NULL THEN
            v_flagged_level := 0;
            v_inv_parent := v_data(v_ptr).parent_id;
            IF v_data(v_ptr).root_settlement_method = 'T' THEN
              v_client_grouping := NULL;
            ELSE
              v_client_grouping := v_data(v_ptr).client_to_bill;
            END IF;
          END IF;

          -- if the invoice flag is set, keep the client id and record the level
          IF v_data(v_ptr).inv_flag <> 'X' AND v_client_grouping IS NULL THEN
            v_inv_parent := v_data(v_ptr).cli_id;
            IF v_data(v_ptr).root_settlement_method = 'T' THEN
              v_client_grouping := NULL;
            ELSE
              v_client_grouping := v_data(v_ptr).client_to_bill;
            END IF;
            v_flagged_level := v_data(v_ptr).hier_level;
          END IF;

          outrec.EXTEND;
          outrec(outrec.COUNT).invoice_group    := NVL(v_client_grouping,v_inv_parent);
          outrec(outrec.COUNT).client_id        := v_data(v_ptr).cli_id;
          outrec(outrec.COUNT).parent_client_id := v_data(v_ptr).parent_id;
          outrec(outrec.COUNT).client_name      := v_data(v_ptr).business_name;

          v_ptr := v_data.NEXT(v_ptr);

        END LOOP;

      END LOOP;

      CLOSE cur_invoice_client_hier;

      vRecordsRead := outrec.COUNT;

    EXCEPTION
      WHEN OTHERS THEN
        vEMessage := 'INVOICE_GROUPS_DAILY - ' || SQLERRM;
        ins_txn_err('Read',vEMessage);
        vRecordsErr := GREATEST(outrec.count,1);

    END;

    BEGIN
      FORALL i IN 1..outrec.COUNT
        INSERT INTO invoice_groups_daily
        VALUES outrec(i);

      COMMIT;

      vRecordsIns := outrec.COUNT;

    EXCEPTION
      WHEN OTHERS THEN
        ROLLBACK;
        vEMessage := 'INVOICE_GROUPS_DAILY - ' || SQLERRM;
        ins_txn_err('Write',vEMessage);
        vRecordsErr := GREATEST(outrec.count,1);

    END;

    ins_extract_recon('INVOICE_GROUPS_DAILY',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.create_invoice_groups_daily',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  END create_invoice_groups_daily;

  --Added new procedure CHANNEL_TYPEETL as part of APWAS-14632

  PROCEDURE CHANNEL_TYPEETL
  IS

  CURSOR acur IS
   SELECT
    src.channel_type_id            as channel_type_id
   ,nvl(trg.code,'X')              as target_channel_type_id
   ,trg.rowid                      as target_rowid
   ,src.channel_name               as channel_name
   ,src.last_updated               as last_updated  --SCP-MCP merger PI Feature P57
   ,src.last_updated_by            as last_updated_by --SCP-MCP merger PI Feature P57
  FROM
  (
    select distinct channel_name,channel_type_id,
    last_updated, last_updated_by   --SCP-MCP merger PI Feature P57
    FROM   ra_channel_type
   )src
   ,channel  trg
  WHERE src.channel_type_id = trg.code(+)
  AND   NOT EXISTS
  (
    SELECT  1
    FROM    channel
    WHERE   code                = src.channel_type_id
    AND     nvl(name,' ')    = nvl(src.channel_name,' ')
    AND     last_updated     = src.last_updated  --SCP-MCP merger PI Feature P57
    AND     last_updated_by  = src.last_updated_by  --SCP-MCP merger PI Feature P57
  );

  fdata          acur%ROWTYPE;
  BEGIN

    vRecordsRead := 0;
    vRecordsUpd  := 0;
    vRecordsIns  := 0;
    vRecordsErr  := 0;

  OPEN acur;
  FETCH acur INTO fdata;
  WHILE acur%FOUND
  LOOP

    vRecordsRead := vRecordsRead + 1;

    IF fdata.target_Channel_type_id <> 'X'
    THEN

      BEGIN

        UPDATE
          channel t
        SET
           t.code    = to_char(fdata.channel_type_id)
          ,t.name     = fdata.channel_name
          ,t.last_updated = fdata.last_updated   --SCP-MCP merger PI Feature P57
          ,t.last_updated_by = fdata.last_updated_by  --SCP-MCP merger PI Feature P57
        WHERE
          rowid = fdata.target_rowid;

          vRecordsUpd := vRecordsUpd + 1;

      EXCEPTION
        WHEN others THEN
          vEMessage := 'CHANNEL_TYPE - ' || sqlerrm;
          ins_txn_err(fdata.CHANNEL_TYPE_ID,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      END;

    ELSE

      BEGIN


        INSERT INTO channel
          (code,name, last_updated, last_updated_by)  ----SCP-MCP merger PI Feature P57
        VALUES
          (to_char(fdata.CHANNEL_TYPE_ID),fdata.channel_name,fdata.last_updated, fdata.last_updated_by);  ----SCP-MCP merger PI Feature P57


          vRecordsIns := vRecordsIns + 1;

      EXCEPTION
        WHEN others THEN
          vEMessage := 'CHANNEL_TYPE - ' || sqlerrm;
          ins_txn_err(fdata.CHANNEL_TYPE_ID,vEMessage);
          vRecordsErr := vRecordsErr + 1;
      END;

    END IF;

    COMMIT;

    FETCH acur INTO fdata;
  END LOOP;
  CLOSE acur;
  ins_extract_recon('CHANNEL_TYPE',vRecordsRead,vRecordsUpd,vRecordsIns,vRecordsErr);
  COMMIT;

  EXCEPTION
    WHEN others THEN
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.CHANNEL_TYPE',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  END CHANNEL_TYPEETL;

 ----------------------------------------------------------------------------------------------------------------------------------

  procedure ins_txn_err (pINKey       IN varchar2
                        ,pINMessage   IN varchar2)
  is
    pragma autonomous_transaction;

  begin

    insert into transaction_errors
      (checknum,error_type,txn_sequence,ErrorMessage,datestamp)
    values
      (0,'SD',substr(pINKey,1,100),substr(pINMessage,1,200),vETL_DATE);

    commit;

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.ins_txn_err',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end ins_txn_err;

  procedure ins_extract_recon (pINTabName     IN varchar2
                              ,pINRecsRead    IN number
                              ,pINRecsUpd     IN number
                              ,pINRecsIns     IN number
                              ,pINRecsErr     IN number)
  is

    lv_amount   number(14,2);

  begin

    lv_amount := to_number(to_char(systimestamp,'YYMMDDHH24MISSFF2'))/100;

    vLastAmount := lv_amount;

    if lv_amount <= vLastAmount+vOffset then
      vOffset := vOffset + 0.01;
      lv_amount := lv_amount + vOffset;
    else
      vOffset := 0;
    end if;

    insert into extract_recon
      (recon_type, input_data_id, bin, stage
      ,description, records, amount, datestamp)
    values
      ('SD',0,0,1
      ,pINTabName || ' records',pINRecsRead,lv_amount,vETL_DATE);

    insert into extract_recon
      (recon_type, input_data_id, bin, stage
      ,description, records, amount, datestamp)
    values
      ('SD',0,0,2
      ,pINTabName || ' updated',pINRecsUpd,lv_amount,vETL_DATE);

    insert into extract_recon
      (recon_type, input_data_id, bin, stage
      ,description, records, amount, datestamp)
    values
      ('SD',0,0,3
      ,pINTabName || ' inserted',pINRecsIns,lv_amount,vETL_DATE);

    insert into extract_recon
      (recon_type, input_data_id, bin, stage
      ,description, records, amount, datestamp)
    values
      ('SD',0,0,4
      ,pINTabName || ' errors',pINRecsErr,lv_amount,vETL_DATE);

  exception
    when others then
      standard_error.when_others_exception
      (
        p_program         => 'client_data_etl.ins_extract_recon',
        p_additional_info => 'vETL_DATE: '||vETL_DATE,
        p_raise_exception => FALSE
      );

  end ins_extract_recon;

END client_data_etl;
/
