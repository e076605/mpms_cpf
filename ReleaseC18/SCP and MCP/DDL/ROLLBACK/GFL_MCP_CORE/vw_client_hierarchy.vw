DROP VIEW GFL_MCP_CORE.VW_CLIENT_HIERARCHY;

/* Formatted on 2/8/2018 11:11:50 AM (QP5 v5.227.12220.39724) */
CREATE OR REPLACE FORCE VIEW GFL_MCP_CORE.VW_CLIENT_HIERARCHY
(
   KEY_INDENTED,
   CLIENT_LEVEL,
   ID,
   NAME,
   CLIENT_TYPE_CODE,
   L2_CLIENT_ID,
   L2_CLIENT_NAME,
   TOP_CLIENT_ID,
   TOP_CLIENT_NAME,
   CLIENT_MCP_FLAG,
   CLIENT_GROUP_NAME,
   ORG_REGION_NAME,
   COUNTRY_NAME,
   COUNTRY_ISO_CODE,
   COUNTRY_ISO_NUM_CODE,
   PROCESSOR_CLIENT_CODE,
   GEO_REGION_ID,
   GEO_SUBREGION_ID,
   GEO_REGION_NAME,
   GEO_SUBREGION_NAME,
   SUB_COUNTRY_ID,
   SUB_COUNTRY_NAME
)
AS
   SELECT KEY_INDENTED,
          CLIENT_LEVEL,
          CLIENT_HRCHY.ID,
          CLIENT_HRCHY.name,
          CLIENT_HRCHY.CLIENT_TYPE_CODE,
          CASE
             WHEN CLIENT_HRCHY.CLIENT_TYPE_CODE = 'CL' THEN -1
             WHEN CLIENT_HRCHY.CLIENT_TYPE_CODE = 'L2' THEN CLIENT_HRCHY.id
             ELSE CLIENT_HRCHY.PARENT_CLIENT_ID
          END
             L2_CLIENT_ID,
          CASE
             WHEN CLIENT_HRCHY.CLIENT_TYPE_CODE = 'CL' THEN 'NA'
             WHEN CLIENT_HRCHY.CLIENT_TYPE_CODE = 'L2' THEN CLIENT_HRCHY.name
             ELSE client.name
          END
             L2_client_name,
          CLIENT_HRCHY.TOP_CLIENT_ID,
          CLIENT_HRCHY.TOP_CLIENT_NAME,
          'Y' CLIENT_MCP_FLAG,
          CLIENT_GROUP.NAME CLIENT_GROUP_NAME,
          ORG_REGION.NAME ORG_REGION_NAME,
          RA_COUNTRY.COUNTRY_NAME COUNTRY_NAME,
          RA_COUNTRY.COUNTRY_ISO_CODE AS COUNTRY_ISO_CODE,
          TO_CHAR (RA_COUNTRY.COUNTRY_ISO_NUM) AS COUNTRY_ISO_NUM_CODE,
          CLIENT_CODING.PROCESSOR_CLIENT_CODE,
          RA_GEO_REGION.GEO_REGION_ID AS GEO_REGION_ID,
          RA_GEO_SUBREGION.GEO_SUB_REGION_ID GEO_SUBREGION_ID,
          RA_GEO_REGION.GEO_REGION_NAME AS GEO_REGION_NAME,
          RA_GEO_SUBREGION.SUB_REGION_NAME AS GEO_SUBREGION_NAME,
          RA_ORG_SUB_COUNTRY.SUB_COUNTRY_ID AS SUB_COUNTRY_ID,
          RA_ORG_SUB_COUNTRY.SUB_COUNTRY_NAME AS SUB_COUNTRY_NAME
     FROM (    SELECT LPAD (' ', LEVEL - 1) || ID AS KEY_INDENTED,
                      LEVEL client_level,
                      id,
                      name,
                      PARENT_CLIENT_ID,
                      client_type_code,
                      CONNECT_BY_ROOT id AS TOP_CLIENT_ID,
                      CONNECT_BY_ROOT name AS TOP_CLIENT_NAME,
                      CONNECT_BY_ROOT CLIENT_GROUP_ID AS CLIENT_GROUP_ID,
                      CLIENT_MCP_FLAG
                 FROM client
           START WITH id = PARENT_CLIENT_ID AND CLIENT_MCP_FLAG = 'Y'
           CONNECT BY NOCYCLE PARENT_CLIENT_ID = PRIOR id) CLIENT_HRCHY,
          CLIENT_GROUP,
          ORG_REGION,
          RA_COUNTRY,
          RA_GEO_REGION,
          RA_GEO_SUBREGION,
          RA_ORG_SUB_COUNTRY,
          RA_ORG_GROUP,
          CLIENT_CODING,
          CLIENT
    WHERE     CLIENT_HRCHY.CLIENT_GROUP_ID = CLIENT_GROUP.ID(+)
          AND CLIENT_GROUP.ORG_REGION_CODE = ORG_REGION.CODE(+)
          AND ORG_REGION.COUNTRY_CODE = RA_COUNTRY.COUNTRY_ISO_CODE(+)
          AND RA_COUNTRY.GEO_SUB_REGION_ID =
                 RA_GEO_SUBREGION.GEO_SUB_REGION_ID(+)
          AND RA_GEO_SUBREGION.GEO_REGION_ID = RA_GEO_REGION.GEO_REGION_ID(+)
          AND CLIENT_HRCHY.id = CLIENT_CODING.CLIENT_ID(+)
          AND CLIENT.ID = CLIENT_HRCHY.PARENT_CLIENT_ID
          AND CLIENT_GROUP.ID = RA_ORG_GROUP.GROUP_ID(+)
          AND RA_ORG_GROUP.SUB_COUNTRY_ID =
                 RA_ORG_SUB_COUNTRY.SUB_COUNTRY_ID(+)
   UNION ALL
   SELECT '-1' AS KEY_INDENTED,
          -1 AS CLIENT_LEVEL,
          -1 AS ID,
          'N/A' AS name,
          'N/A' AS CLIENT_TYPE_CODE,
          -1 AS L2_CLIENT_ID,
          'N/A' AS L2_client_name,
          -1 AS TOP_CLIENT_ID,
          'N/A' AS TOP_CLIENT_NAME,
          'Y' AS CLIENT_MCP_FLAG,
          'N/A' AS CLIENT_GROUP_NAME,
          'N/A' AS ORG_REGION_NAME,
          'N/A' AS COUNTRY_NAME,
          '-1' AS COUNTRY_ISO_CODE,
          '000' AS COUNTRY_ISO_NUM_CODE,
          -1 AS PROCESSOR_CLIENT_CODE,
          -1 AS GEO_REGION_ID,
          -1 AS GEO_SUBREGION_ID,
          'N/A' AS GEO_REGION_NAME,
          'N/A' AS GEO_SUBREGION_NAME,
          -1 AS SUB_COUNTRY_ID,
          'N/A' AS SUB_COUNTRY_NAME
     FROM DUAL;


CREATE OR REPLACE SYNONYM RAL_MCP_DATA.VW_CLIENT_HIERARCHY FOR GFL_MCP_CORE.VW_CLIENT_HIERARCHY;

GRANT SELECT ON GFL_MCP_CORE.VW_CLIENT_HIERARCHY TO GFL_GENERAL_LEDGER WITH GRANT OPTION;

GRANT SELECT ON GFL_MCP_CORE.VW_CLIENT_HIERARCHY TO GFL_MCP_CORE_READ_RL;

GRANT SELECT ON GFL_MCP_CORE.VW_CLIENT_HIERARCHY TO GFL_MCP_CORE_USR_RL;

GRANT SELECT ON GFL_MCP_CORE.VW_CLIENT_HIERARCHY TO MCP_ISSUER_REP WITH GRANT OPTION;

GRANT SELECT ON GFL_MCP_CORE.VW_CLIENT_HIERARCHY TO RAL_MCP_DATA WITH GRANT OPTION;



CREATE OR REPLACE SYNONYM RAL_MCP_DATA.VW_CLIENT_HIERARCHY FOR GFL_MCP_CORE.VW_CLIENT_HIERARCHY;


GRANT SELECT ON GFL_MCP_CORE.VW_CLIENT_HIERARCHY TO GFL_GENERAL_LEDGER WITH GRANT OPTION;

GRANT SELECT ON GFL_MCP_CORE.VW_CLIENT_HIERARCHY TO GFL_MCP_CORE_READ_RL;

GRANT SELECT ON GFL_MCP_CORE.VW_CLIENT_HIERARCHY TO GFL_MCP_CORE_USR_RL;

GRANT SELECT ON GFL_MCP_CORE.VW_CLIENT_HIERARCHY TO MCP_ISSUER_REP WITH GRANT OPTION;

GRANT SELECT ON GFL_MCP_CORE.VW_CLIENT_HIERARCHY TO RAL_MCP_DATA WITH GRANT OPTION;
