  INSERT INTO fs_mcp_eod_neg_bal
    (
      purse_ccy,
      d_etl_dt_id,
      d_bin_id,
      d_transaction_type,
      available_balance,
      etl_date,
      business_date
    )
Select columns
from cpradmin.IF_TJF_RT08_BALANCES where current_balance < 0 and ica_number =&ica;

commit;