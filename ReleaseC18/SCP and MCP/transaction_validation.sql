CREATE OR REPLACE PACKAGE BODY GLOBAL_STAGING_AREA.transaction_validation AS

/*****************************************************************************************************
   PURPOSE:    To run a process

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        19/12/2012  Tim Berry        Initial version.
   2.0        03/01/2013  Tim Berry        CMPCPF-1081 changes.
   3.0        19/03/2013  Tim Berry        Gemini CR623 Changes.
   4.0        18/11/2013  JT               Issue Reporting CR657 Changes.
   5.0        19/02/2014  Veeraiah Mannem  MCP MI change.
   6.0        19/05/2014  JT               No data found error when nearing bulk collect limit.
   7.0        05/05/2014  Ramarao Nandigam Jira 10810 MCP Payment Method not being populated, this data is  required for AML.
   8.0        03/01/2015  JT               Incorrect transaction type recoding when mixture of initial loads and reloads.
   9.0        15/07/2015  JT               Added recycling report.
  10.0       11/02/2015  Srini             Fetch out of run issue- added exit when notfound
  11.0       06/05/2016 Ramarao            Added 96,97 transaction types part of MRQ process
  12.0       12/09/2016  Herald            APWAS-17657  Insert generic error message into transasction_errors table if there was no validation error
  13.0       04/02/2018  Srini             SCP and MCP merge
*****************************************************************************************************/

-----------------------------------------------------------------------
-- Author: Tim Berry
--
-- Date: 19th December, 2012
--
-- Related to: CMPCPF-1075
--
-- Description
-- -----------
-- The change being to restrict the insert of error records to just
-- those new cases from the transactions received for this days
-- processing only.
--
-- The change can be found in the where clause of the insert statement
-- to the TJF_ERROR_DATA table. There is also a no parallel hint as
-- the UAT table is parallel 4 and this causes problems with partitions
-- as the parallel supercedes the index and causes a bad plan.
-----------------------------------------------------------------------
-- Author: Tim Berry
--
-- Date: 3rd January, 2013
--
-- Related to: CMPCPF-1081
--
-- Description
-- -----------
-- Changed the transaction group to now use a function so that the
-- resolved order reference uses the suffix numbers to make the purse
-- to purse transfers unique by each transfer rather all being grouped
-- into 1 order. This allows more through when 1 may not match
-----------------------------------------------------------------------
-- Author: Tim Berry
--
-- Date: 19th March, 2013
--
-- Related to: Gemini CR623 Changes
--
-- Description
-- -----------
-- Added and removed columns from the TJF as required for Gemini
-- using the new view V_STG_TJF_TYPE2
-----------------------------------------------------------------------
-- Author: Jeff Tilby
--
-- Date: 18th November, 2013
--
-- Related to: Issue Reporting CR657 Changes
--
-- Description
-- -----------
-- Added columns for Issue Reporting
-----------------------------------------------------------------------

-- Author: Veeraiah Mannem
--
-- Date: 19th Feb, 2014
--
-- Related to: MCP MI change
--
-- Description
-- -----------
-- Added missing columns from TJF RT02 to  processor_transaction

-----------------------------------------------------------------------
-- Author: Jeff Tilby
--
-- Date: 19th May, 2014
--
-- Related to: No data found error when nearing bulk collect limit.
--
-- Description
-- -----------
-- Restructured to get avoid no data found error when nearing bulk collect limit.
------------------------------------------------------------------------
--Author :Ramarao Nandigam
   --
   --Date:05th May, 2014
   --
   --Related to :Jira 10810 MCP Payment Method not being populated, this data is  required for AML
   --
   --Removed hard code ('0') and mapped respective column
-----------------------------------------------------------------------
-- Author: Jeff Tilby
--
-- Date: 13th Jan, 2015
--
-- Related to: Incorrect transaction type recoding when mixture of initial loads and reloads.
--
-- Description
-- -----------
-- Changed the recoding code to handle mixture of initial loads and reloads.
-------------------------------------------------------------------------------------------------

  initial_load  CONSTANT    VARCHAR2(5)  := '1';

  g_etl_date    DATE;

------------------------------------------------------------------------------------------------------------------------------------

  FUNCTION boolean_to_string(p_boolean BOOLEAN) RETURN VARCHAR2 IS

  BEGIN
    IF p_boolean IS NULL THEN
      RETURN 'NULL';
    ELSIF p_boolean THEN
      RETURN 'TRUE';
    ELSE
      RETURN 'FALSE';
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      standard_error.when_others_exception
      (
        p_program         => 'transaction_validation.boolean_to_string'
      );

  END boolean_to_string;

------------------------------------------------------------------------------------------------------------------------------------

  PROCEDURE insert_error
  (
    p_check_number  NUMBER,
    p_txn_sequence  NUMBER,
    p_message       VARCHAR2
  )  IS
  BEGIN

    INSERT INTO transaction_errors
    (
      checknum,
      error_type,
      txn_sequence,
      ErrorMessage,
      datestamp
    )
    VALUES
    (
      p_check_number,
      'TJF',
      p_txn_sequence,
      p_message,
      g_etl_date
    );

  EXCEPTION
    WHEN OTHERS THEN
      standard_error.when_others_exception
      (
        p_program         => 'transaction_validation.insert_error',
        p_additional_info => 'p_txn_sequence: '||p_txn_sequence
      );

  END insert_error;

------------------------------------------------------------------------------------------------------------------------------------

  PROCEDURE update_first_load
  (
    p_input_data_id NUMBER
  ) IS

    v_transaction_recycle   transaction_recycle%ROWTYPE;

  BEGIN
    INSERT INTO mcp_account_first_load(ac_nbr,datestamp)
    SELECT  ac_nbr,
            MIN(TO_DATE(SUBSTR(cust_posting_datetime,1,19),'YYYY-MM-DD HH24:MI:SS'))
    FROM    v_stg_tjf_non_settlements_mcp
    WHERE   input_data_id = p_input_data_id
    AND     txn_cde <> '03043'
    AND     ac_nbr NOT IN (SELECT ac_nbr FROM mcp_account_first_load)
    GROUP BY ac_nbr;

  EXCEPTION
    WHEN OTHERS THEN
      standard_error.when_others_exception
      (
        p_program         => 'transaction_validation.update_first_load'
      );

  END update_first_load;

------------------------------------------------------------------------------------------------------------------------------------

  PROCEDURE process_tjf(p_input_data_id NUMBER) IS

    v_error_flag                    BOOLEAN;
    v_recode_required               BOOLEAN;
    v_source_count                  NUMBER := 0;
    v_processed_count               NUMBER := 0;
    v_recycle_new_count             NUMBER := 0;
    v_recycle_old_count             NUMBER := 0;
    v_client_bank_account_id        NUMBER;
    v_legal_entity_bank_account_id  NUMBER;
    v_current_order_reference       VARCHAR2(4000) := NULL;
    v_processor_transaction         processor_transaction%ROWTYPE;
    v_ptr                           BINARY_INTEGER;
    v_limit                         BINARY_INTEGER;
    v_in_out_indicator              NUMBER;
    v_amount                        NUMBER;
    v_exclude_matching              VARCHAR2(1);

    CURSOR cur_tjf_rt02 IS
    WITH dto_table_a AS
    (
      SELECT
        table_id,
        source_rowid,
        txn_sequence,
        fee_channel,
        program_ac_type,
        program_portfolio_cde,
        fn_processor_client_id
        (
          initiating_ol_1,
          initiating_ol_2,
          initiating_ol_3,
          ol_1,
          ol_2,
          ol_3
        ) as processor_client_id,
        fn_processor_client_id
        (
          ol_1,
          ol_2,
          ol_3,
          NULL,
          NULL,
          NULL
        ) as orig_sell_processor_client_id,
        adj_reason_cde,
        TO_NUMBER(txn_cde)                                                      AS txn_cde,
        txn_desc,
        TO_NUMBER(fee_channel)                                                  AS coda_channel,
        dispute_id,
        txn_dbcr_ind                                                            AS dbcr,
        TO_DATE(SUBSTR(txn.cust_posting_datetime,1,19),'YYYY-MM-DD-HH24:MI:SS') AS proc_date_time,
        to_date(txn.txn_effective_date,'YYYYMMDD')                              AS effective_date,
        txn.posted_amt / POWER(10,txn.ccy_exp_for_posting_amt)                  AS posted_amount,
        to_date(txn.posting_date,'YYYYMMDD')                                    AS posting_date,
        substr(txn.txn_dbcr_ind,1,1)                                            AS debit_credit,
        txn.purse1_txn_amt / POWER(10,txn.purse1_ccy_exp)                       AS txn_amount,
        TO_NUMBER(txn.cardno)                                                   AS card_nbr,
        txn.purse1_ccy                                                          AS txn_currency_code,
        txn.srce_cde                                                            AS source_code,
        txn.category_cde                                                        AS category_code,
        txn.create_user_id                                                      AS cashier_name,
        fnc_resolve_reference(sysgen_ref_nbr)                                   AS order_reference,
        txn.sysgen_ref_nbr                                                      AS sysgen_ref_nbr,
        txn.ac_nbr                                                              AS account_number,
        transaction_reason_code,
        transaction_reference_number,
        foreign_exchange_rate,
        foreign_exchange_exponent,
        foreign_exchange_margin_rate,
        foreign_exch_mgn_exponent,
        alternate_id1,
        cis_branch,
        loyalty_program_id1,
        loyalty_reg_award_amount1,
        loyalty_bns_award_amount1,
        loyalty_indicator1,
        loyalty_dbcr1,
        loyalty_program_id2,
        loyalty_reg_award_amount2,
        loyalty_bns_award_amount2,
        loyalty_indicator2,
        loyalty_dbcr2,
        loyalty_program_id3,
        loyalty_reg_award_amount3,
        loyalty_bns_award_amount3,
        loyalty_indicator3,
        loyalty_dbcr3,
        loyalty_program_id4,
        loyalty_reg_award_amount4,
        loyalty_bns_award_amount4,
        loyalty_indicator4,
        loyalty_dbcr4,
        loyalty_program_id5,
        loyalty_reg_award_amount5,
        loyalty_bns_award_amount5,
        loyalty_indicator5,
        loyalty_dbcr5,
        prepaid_indicator,
        source_product_code,
        source_rt,
        source_account_number,
        filler,
        bin,
        ccy_cde_for_posting_amt,
        card_brand_ind,
        company_id,
        fin_parent_bus_unit_no,
        fin_parent_child_unit_no,
        ica_on_bin,
        issuer_cntry_cde_on_bin,
        legal_entity_on_ica,
        level_3_cde,
        level_4_cde,
        level_5_cde,
        multi_purse_flag,
        non_fin_plastic_type,
        payment_method_of_load_reload,
        product_cde_on_bin,
        secnd_lgl_entity_on_ica,
        TO_DATE(txn.sett_service_value_date,'YYMMDD')                           AS sett_service_value_date,
        txn_comment,
        TO_DATE(SUBSTR(txn_ref_datetime,1,19),'YYYY-MM-DD-HH24:MI:SS')          AS txn_ref_datetime,
        input_data_id
      FROM
      (
        SELECT
          ROWIDTOCHAR(v_stg_tjf_type2.rowid)  AS source_rowid,
          'TJF' table_id,
          company_id,
          program_ac_type,
          program_portfolio_cde,
          ac_nbr,
          ol_1,
          ol_2,
          ol_3,
          txn_effective_date,
          cardno,
          posting_date,
          srce_cde,
          category_cde,
          level_3_cde,
          level_4_cde,
          level_5_cde,
          posted_amt,
          txn_dbcr_ind,
          txn_cde,
          create_user_id,
          cust_posting_datetime,
          txn_desc,
          sysgen_ref_nbr,
          adj_reason_cde,
          fee_channel,
          ccy_cde_for_posting_amt,
          ccy_exp_for_posting_amt,
          card_brand_ind,
          sett_service_value_date,
          issuer_cntry_cde_on_bin,
          product_cde_on_bin,
          ica_on_bin,
          legal_entity_on_ica,
          secnd_lgl_entity_on_ica,
          dispute_id,
          initiating_ol_1,
          initiating_ol_2,
          initiating_ol_3,
          payment_method_of_load_reload,
          fin_parent_bus_unit_no,
          fin_parent_child_unit_no,
          non_fin_plastic_type,
          txn_comment,
          multi_purse_flag,
          purse1_id,
          purse1_ccy,
          purse1_txn_amt,
          purse1_cr_db_ind,
          purse1_ccy_exp,
          transaction_reason_code,
          transaction_reference_number,
          foreign_exchange_rate,
          foreign_exchange_exponent,
          foreign_exchange_margin_rate,
          foreign_exch_mgn_exponent,
          alternate_id1,
          cis_branch,
          loyalty_program_id1,
          loyalty_reg_award_amount1,
          loyalty_bns_award_amount1,
          loyalty_indicator1,
          loyalty_dbcr1,
          loyalty_program_id2,
          loyalty_reg_award_amount2,
          loyalty_bns_award_amount2,
          loyalty_indicator2,
          loyalty_dbcr2,
          loyalty_program_id3,
          loyalty_reg_award_amount3,
          loyalty_bns_award_amount3,
          loyalty_indicator3,
          loyalty_dbcr3,
          loyalty_program_id4,
          loyalty_reg_award_amount4,
          loyalty_bns_award_amount4,
          loyalty_indicator4,
          loyalty_dbcr4,
          loyalty_program_id5,
          loyalty_reg_award_amount5,
          loyalty_bns_award_amount5,
          loyalty_indicator5,
          loyalty_dbcr5,
          prepaid_indicator,
          source_product_code,
          source_rt,
          source_account_number,
          filler,
          txn_ref_datetime,
          run_date,
          run_id,
          bin,
          input_data_id,
          txn_sequence
        FROM v_stg_tjf_type2
        WHERE input_data_id = p_input_data_id
        UNION ALL
        SELECT
          'N/A' source_rowid,
          'RECYCLE' table_id,
          company_id,
          program_ac_type,
          program_portfolio_cde,
          ac_nbr,
          ol_1,
          ol_2,
          ol_3,
          txn_effective_date,
          cardno,
          posting_date,
          srce_cde,
          category_cde,
          level_3_cde,
          level_4_cde,
          level_5_cde,
          posted_amt,
          txn_dbcr_ind,
          txn_cde,
          create_user_id,
          cust_posting_datetime,
          txn_desc,
          sysgen_ref_nbr,
          adj_reason_cde,
          fee_channel,
          ccy_cde_for_posting_amt,
          ccy_exp_for_posting_amt,
          card_brand_ind,
          sett_service_value_date,
          issuer_cntry_cde_on_bin,
          product_cde_on_bin,
          ica_on_bin,
          legal_entity_on_ica,
          secnd_lgl_entity_on_ica,
          dispute_id,
          initiating_ol_1,
          initiating_ol_2,
          initiating_ol_3,
          payment_method_of_load_reload,
          fin_parent_bus_unit_no,
          fin_parent_child_unit_no,
          non_fin_plastic_type,
          txn_comment,
          multi_purse_flag,
          purse1_id,
          purse1_ccy,
          purse1_txn_amt,
          purse1_cr_db_ind,
          purse1_ccy_exp,
          transaction_reason_code,
          transaction_reference_number,
          foreign_exchange_rate,
          foreign_exchange_exponent,
          foreign_exchange_margin_rate,
          foreign_exch_mgn_exponent,
          alternate_id1,
          cis_branch,
          loyalty_program_id1,
          loyalty_reg_award_amount1,
          loyalty_bns_award_amount1,
          loyalty_indicator1,
          loyalty_dbcr1,
          loyalty_program_id2,
          loyalty_reg_award_amount2,
          loyalty_bns_award_amount2,
          loyalty_indicator2,
          loyalty_dbcr2,
          loyalty_program_id3,
          loyalty_reg_award_amount3,
          loyalty_bns_award_amount3,
          loyalty_indicator3,
          loyalty_dbcr3,
          loyalty_program_id4,
          loyalty_reg_award_amount4,
          loyalty_bns_award_amount4,
          loyalty_indicator4,
          loyalty_dbcr4,
          loyalty_program_id5,
          loyalty_reg_award_amount5,
          loyalty_bns_award_amount5,
          loyalty_indicator5,
          loyalty_dbcr5,
          prepaid_indicator,
          source_product_code,
          source_rt,
          source_account_number,
          filler,
          txn_ref_datetime,
          run_date,
          run_id,
          bin,
          input_data_id,
          tjf.txn_sequence
        FROM tjf_error_data tjf
        WHERE NOT EXISTS
        (
          SELECT 1
          FROM transaction_recycle
          WHERE transaction_recycle.txn_sequence = tjf.txn_sequence
          AND releasedate IS NOT NULL
        )
      ) txn
    ),
    dto_table_b AS
    (
      SELECT
        table_id,
        source_rowid,
        dto_table_a.txn_sequence                                              AS txn_sequence,
        CASE
          WHEN nvl(cod.channel_code,'1') = '1' THEN 'B'
          WHEN nvl(cod.channel_code,'1') = '2' THEN 'T'
          WHEN nvl(cod.channel_code,'1') = '3' THEN 'E'
          WHEN nvl(cod.channel_code,'1') = '4' THEN 'W'
          WHEN nvl(cod.channel_code,'1') = '5' THEN 'S'
          ELSE 'B'
        END                                                                   AS decode_channel_code,
        dto_table_a.fee_channel                                               AS fee_channel,
        program_ac_type,
        program_portfolio_cde,
        dto_table_a.processor_client_id                                       AS processor_client_id,
        SUBSTR(LPAD(dto_table_a.processor_client_id,15,'0'),1,5)              AS owner_level_1,
        SUBSTR(LPAD(dto_table_a.processor_client_id,15,'0'),6,5)              AS owner_level_2,
        SUBSTR(LPAD(dto_table_a.processor_client_id,15,'0'),11,5)             AS owner_level_3,
        dto_table_a.orig_sell_processor_client_id,
        cod.client_id                                                         AS client_id,
        cod.channel_code                                                      AS channel_code,
        selling_cod.client_id                                                 AS original_selling_client_id,
        nvl(brn.front_end_sales_data_flag,'N')                                AS gpf_flag,
        dto_table_a.adj_reason_cde                                            AS adj_reason_cde,
        dto_table_a.txn_cde                                                   AS txn_cde,
        dto_table_a.txn_desc,
        dto_table_a.coda_channel,
        dto_table_a.dispute_id,
        dto_table_a.dbcr                                                      AS dbcr,
        dto_table_a.proc_date_time,
        dto_table_a.effective_date,
        dto_table_a.posted_amount,
        dto_table_a.posting_date,
        dto_table_a.debit_credit,
        dto_table_a.txn_amount,
        dto_table_a.card_nbr,
        dto_table_a.txn_currency_code,
        dto_table_a.source_code,
        dto_table_a.category_code,
        dto_table_a.cashier_name,
        dto_table_a.order_reference,
        dto_table_a.account_number,
        dto_table_a.sysgen_ref_nbr,
        dto_table_a.bin,
        dto_table_a.ccy_cde_for_posting_amt,
        NVL(inv.invoice_group,0)                                              AS invoice_client,
        NVL(cli.settlement_method,'I')                                        AS invoice_type_flag,
        dto_table_a.transaction_reason_code,
        dto_table_a.transaction_reference_number,
        dto_table_a.foreign_exchange_rate,
        dto_table_a.foreign_exchange_exponent,
        dto_table_a.foreign_exchange_margin_rate,
        dto_table_a.foreign_exch_mgn_exponent,
        dto_table_a.alternate_id1,
        dto_table_a.cis_branch,
        dto_table_a.loyalty_program_id1,
        dto_table_a.loyalty_reg_award_amount1,
        dto_table_a.loyalty_bns_award_amount1,
        dto_table_a.loyalty_indicator1,
        dto_table_a.loyalty_dbcr1,
        dto_table_a.loyalty_program_id2,
        dto_table_a.loyalty_reg_award_amount2,
        dto_table_a.loyalty_bns_award_amount2,
        dto_table_a.loyalty_indicator2,
        dto_table_a.loyalty_dbcr2,
        dto_table_a.loyalty_program_id3,
        dto_table_a.loyalty_reg_award_amount3,
        dto_table_a.loyalty_bns_award_amount3,
        dto_table_a.loyalty_indicator3,
        dto_table_a.loyalty_dbcr3,
        dto_table_a.loyalty_program_id4,
        dto_table_a.loyalty_reg_award_amount4,
        dto_table_a.loyalty_bns_award_amount4,
        dto_table_a.loyalty_indicator4,
        dto_table_a.loyalty_dbcr4,
        dto_table_a.loyalty_program_id5,
        dto_table_a.loyalty_reg_award_amount5,
        dto_table_a.loyalty_bns_award_amount5,
        dto_table_a.loyalty_indicator5,
        dto_table_a.loyalty_dbcr5,
        dto_table_a.prepaid_indicator,
        dto_table_a.source_product_code,
        dto_table_a.source_rt,
        dto_table_a.source_account_number,
        dto_table_a.filler,
        dto_table_a.card_brand_ind,
        dto_table_a.company_id,
        dto_table_a.fin_parent_bus_unit_no,
        dto_table_a.fin_parent_child_unit_no,
        dto_table_a.ica_on_bin,
        dto_table_a.issuer_cntry_cde_on_bin,
        dto_table_a.legal_entity_on_ica,
        dto_table_a.level_3_cde,
        dto_table_a.level_4_cde,
        dto_table_a.level_5_cde,
        dto_table_a.multi_purse_flag,
        dto_table_a.non_fin_plastic_type,
        dto_table_a.payment_method_of_load_reload,
        dto_table_a.product_cde_on_bin,
        dto_table_a.secnd_lgl_entity_on_ica,
        dto_table_a.sett_service_value_date,
        dto_table_a.txn_comment,
        dto_table_a.txn_ref_datetime,
        dto_table_a.input_data_id
      FROM
           dto_table_a,
            client_coding                      cod,
            client_coding                      selling_cod,
            client                             cli,
            invoice_groups_daily               inv,
            client                             brn
      WHERE dto_table_a.processor_client_id           = cod.processor_client_code(+)
      AND   dto_table_a.orig_sell_processor_client_id = selling_cod.processor_client_code(+)
      AND   cod.client_id                             = inv.client_id(+)
      AND   cod.client_id                             = brn.id(+)
      AND nvl(inv.invoice_group,0)                    = cli.id(+)
    ),
    dto_table_c AS
    (
      SELECT
        dto_table_b.table_id,
        dto_table_b.source_rowid,
        dto_table_b.txn_sequence,
        dto_table_b.program_ac_type,
        dto_table_b.program_portfolio_cde,
        CASE
          WHEN cls.class = 'NST' THEN
            dto_table_b.decode_channel_code || dto_table_b.fee_channel
          WHEN cls.class = 'FEE' THEN
            CASE
              WHEN dto_table_b.txn_cde in (1333,1334,1337,1338,5001,5002) THEN
                'BMF'
              ELSE
                'CHF'
              END
          WHEN cls.class = 'ADJ'
          AND dto_table_b.txn_cde = 3011
          AND dto_table_b.channel_code = '90'
          AND dto_table_b.decode_channel_code = 'B'
          AND dto_table_b.adj_reason_cde = 'X04' THEN
            'PCO'
          WHEN cls.class = 'ADJ'
          AND dto_table_b.txn_cde = 3011
          AND dto_table_b.channel_code = '93'
          AND dto_table_b.decode_channel_code = 'B'
          AND (dto_table_b.adj_reason_cde = 'AJ ' OR dto_table_b.adj_reason_cde = 'AJ') THEN
            'LCO'
          WHEN cls.class = 'ADJ'
          AND (dto_table_b.adj_reason_cde = 'AJ ' or dto_table_b.adj_reason_cde = 'AJ') THEN
            'ADJ'
          WHEN cls.class = 'ADJ'
          AND dto_table_b.adj_reason_cde IS NOT NULL THEN
            dto_table_b.adj_reason_cde
          ELSE
            'XXX'
        END                                                         AS txn_cde_prefix,
        dto_table_b.processor_client_id,
        dto_table_b.orig_sell_processor_client_id,
        dto_table_b.owner_level_1,
        dto_table_b.owner_level_2,
        dto_table_b.owner_level_3,
        dto_table_b.client_id,
        dto_table_b.original_selling_client_id,
        dto_table_b.gpf_flag,
        dto_table_b.adj_reason_cde,
        dto_table_b.txn_cde,
        dto_table_b.txn_desc,
        dto_table_b.coda_channel,
        dto_table_b.dispute_id,
        dto_table_b.dbcr,
        dto_table_b.proc_date_time,
        dto_table_b.effective_date,
        dto_table_b.posted_amount,
        dto_table_b.posting_date,
        dto_table_b.debit_credit,
        dto_table_b.txn_amount,
        nvl(crd.card_nbr,0)                                       AS card_nbr,
        dto_table_b.card_nbr                                      AS original_card_nbr,
        dto_table_b.txn_currency_code,
        dto_table_b.source_code,
        dto_table_b.category_code,
        dto_table_b.cashier_name,
        dto_table_b.order_reference,
        dto_table_b.account_number,
        dto_table_b.sysgen_ref_nbr,
        dto_table_b.channel_code,
        dto_table_b.bin,
        dto_table_b.invoice_client,
        dto_table_b.invoice_type_flag,
        NVL(cur.iso_code,'GBP')                                   AS coda_currency,
        dto_table_b.transaction_reason_code,
        dto_table_b.transaction_reference_number,
        dto_table_b.foreign_exchange_rate,
        dto_table_b.foreign_exchange_exponent,
        dto_table_b.foreign_exchange_margin_rate,
        dto_table_b.foreign_exch_mgn_exponent,
        dto_table_b.alternate_id1,
        dto_table_b.cis_branch,
        dto_table_b.loyalty_program_id1,
        dto_table_b.loyalty_reg_award_amount1,
        dto_table_b.loyalty_bns_award_amount1,
        dto_table_b.loyalty_indicator1,
        dto_table_b.loyalty_dbcr1,
        dto_table_b.loyalty_program_id2,
        dto_table_b.loyalty_reg_award_amount2,
        dto_table_b.loyalty_bns_award_amount2,
        dto_table_b.loyalty_indicator2,
        dto_table_b.loyalty_dbcr2,
        dto_table_b.loyalty_program_id3,
        dto_table_b.loyalty_reg_award_amount3,
        dto_table_b.loyalty_bns_award_amount3,
        dto_table_b.loyalty_indicator3,
        dto_table_b.loyalty_dbcr3,
        dto_table_b.loyalty_program_id4,
        dto_table_b.loyalty_reg_award_amount4,
        dto_table_b.loyalty_bns_award_amount4,
        dto_table_b.loyalty_indicator4,
        dto_table_b.loyalty_dbcr4,
        dto_table_b.loyalty_program_id5,
        dto_table_b.loyalty_reg_award_amount5,
        dto_table_b.loyalty_bns_award_amount5,
        dto_table_b.loyalty_indicator5,
        dto_table_b.loyalty_dbcr5,
        dto_table_b.prepaid_indicator,
        dto_table_b.source_product_code,
        dto_table_b.source_rt,
        dto_table_b.source_account_number,
        dto_table_b.filler,
        dto_table_b.card_brand_ind,
        dto_table_b.company_id,
        dto_table_b.fin_parent_bus_unit_no,
        dto_table_b.fin_parent_child_unit_no,
        dto_table_b.ica_on_bin,
        dto_table_b.issuer_cntry_cde_on_bin,
        dto_table_b.legal_entity_on_ica,
        dto_table_b.level_3_cde,
        dto_table_b.level_4_cde,
        dto_table_b.level_5_cde,
        dto_table_b.multi_purse_flag,
        dto_table_b.non_fin_plastic_type,
        dto_table_b.payment_method_of_load_reload,
        dto_table_b.product_cde_on_bin,
        dto_table_b.secnd_lgl_entity_on_ica,
        dto_table_b.sett_service_value_date,
        dto_table_b.txn_comment,
        dto_table_b.txn_ref_datetime,
        dto_table_b.input_data_id
      FROM
        dto_table_b,
        mcp_transaction_class        cls,
        currency                     cur,
        card                         crd
      WHERE dto_table_b.txn_cde = cls.txn_cde(+)
      AND NVL(cls.record_type(+),2)           = 2
      AND dto_table_b.ccy_cde_for_posting_amt = cur.iso_num_code(+)
      AND dto_table_b.card_nbr                = crd.card_nbr(+)
    )
    SELECT /*+ PARALLEL */
      dto_table_c.table_id,
      dto_table_c.source_rowid,
      dto_table_c.txn_sequence,
      dto_table_c.program_ac_type,
      dto_table_c.program_portfolio_cde,
      dto_table_c.processor_client_id,
      dto_table_c.orig_sell_processor_client_id,
      dto_table_c.proc_date_time,
      dto_table_c.effective_date,
      dto_table_c.posted_amount,
      dto_table_c.posting_date,
      dto_table_c.debit_credit,
      dto_table_c.txn_amount,
      dto_table_c.card_nbr,
      dto_table_c.original_card_nbr,
      dto_table_c.channel_code,
      dto_table_c.txn_currency_code,
      dto_table_c.source_code,
      dto_table_c.category_code,
      dto_table_c.client_id,
      dto_table_c.original_selling_client_id,
      dto_table_c.gpf_flag,
      dto_table_c.cashier_name,
      ptt.name                                    AS txn_description,
      nvl(typ.txn_num_code,0)                     AS type_code,
      dto_table_c.txn_desc,
      dto_table_c.coda_channel                    AS fee_channel,
      dto_table_c.dispute_id,
      dto_table_c.order_reference,
      dto_table_c.account_number,
      dto_table_c.sysgen_ref_nbr,
      dto_table_c.bin,
      dto_table_c.coda_currency,
      dto_table_c.dbcr,
      dto_table_c.txn_cde,
      dto_table_c.txn_cde_prefix,
      dto_table_c.adj_reason_cde,
      dto_table_c.owner_level_1,
      dto_table_c.owner_level_2,
      dto_table_c.owner_level_3,
      dto_table_c.invoice_client,
      dto_table_c.transaction_reason_code,
      dto_table_c.transaction_reference_number,
      dto_table_c.foreign_exchange_rate,
      dto_table_c.foreign_exchange_exponent,
      dto_table_c.foreign_exchange_margin_rate,
      dto_table_c.foreign_exch_mgn_exponent,
      dto_table_c.alternate_id1,
      dto_table_c.cis_branch,
      dto_table_c.loyalty_program_id1,
      dto_table_c.loyalty_reg_award_amount1,
      dto_table_c.loyalty_bns_award_amount1,
      dto_table_c.loyalty_indicator1,
      dto_table_c.loyalty_dbcr1,
      dto_table_c.loyalty_program_id2,
      dto_table_c.loyalty_reg_award_amount2,
      dto_table_c.loyalty_bns_award_amount2,
      dto_table_c.loyalty_indicator2,
      dto_table_c.loyalty_dbcr2,
      dto_table_c.loyalty_program_id3,
      dto_table_c.loyalty_reg_award_amount3,
      dto_table_c.loyalty_bns_award_amount3,
      dto_table_c.loyalty_indicator3,
      dto_table_c.loyalty_dbcr3,
      dto_table_c.loyalty_program_id4,
      dto_table_c.loyalty_reg_award_amount4,
      dto_table_c.loyalty_bns_award_amount4,
      dto_table_c.loyalty_indicator4,
      dto_table_c.loyalty_dbcr4,
      dto_table_c.loyalty_program_id5,
      dto_table_c.loyalty_reg_award_amount5,
      dto_table_c.loyalty_bns_award_amount5,
      dto_table_c.loyalty_indicator5,
      dto_table_c.loyalty_dbcr5,
      dto_table_c.prepaid_indicator,
      dto_table_c.source_product_code,
      dto_table_c.source_rt,
      dto_table_c.source_account_number,
      dto_table_c.filler,
      stl.matching,
      dto_table_c.invoice_type_flag,
      NVL(fld.ac_nbr,0)                           AS first_load_account,
      dto_table_c.card_brand_ind,
      dto_table_c.company_id,
      dto_table_c.fin_parent_bus_unit_no,
      dto_table_c.fin_parent_child_unit_no,
      dto_table_c.ica_on_bin,
      dto_table_c.issuer_cntry_cde_on_bin,
      dto_table_c.legal_entity_on_ica,
      dto_table_c.level_3_cde,
      dto_table_c.level_4_cde,
      dto_table_c.level_5_cde,
      dto_table_c.multi_purse_flag,
      dto_table_c.non_fin_plastic_type,
      dto_table_c.payment_method_of_load_reload,
      dto_table_c.product_cde_on_bin,
      dto_table_c.secnd_lgl_entity_on_ica,
      dto_table_c.sett_service_value_date,
      dto_table_c.txn_comment,
      dto_table_c.txn_ref_datetime,
      dto_table_c.input_data_id
    FROM  dto_table_c,
          settlement_matching              stl,
          mcp_transaction_map              typ,
          processor_transaction_type       ptt,
          mcp_account_first_load           fld
    WHERE dto_table_c.txn_cde_prefix      = typ.prefix(+)
    AND   dto_table_c.txn_cde             = typ.txn_cde(+)
    AND   dto_table_c.dbcr                = NVL(typ.dbcr(+),'X')
    AND   nvl(typ.txn_num_code,0)         = ptt.code(+)
    AND   dto_table_c.account_number      = fld.ac_nbr(+)
    AND   dto_table_c.proc_date_time      > fld.datestamp(+)
    AND   dto_table_c.invoice_type_flag   = stl.settlement_method
    ORDER BY dto_table_c.order_reference,dto_table_c.txn_sequence;

    TYPE typ_tjf_rt02 IS TABLE OF cur_tjf_rt02%ROWTYPE INDEX BY BINARY_INTEGER;

    buf_tjf_rt02 typ_tjf_rt02;

    TYPE typ_processor_transaction_str IS TABLE OF processor_transaction%ROWTYPE
    INDEX BY BINARY_INTEGER;

    processor_transaction_store typ_processor_transaction_str;

    TYPE typ_transaction_recycle_store IS TABLE OF transaction_recycle%ROWTYPE
    INDEX BY BINARY_INTEGER;

    transaction_recycle_store   typ_transaction_recycle_store;

    TYPE typ_recycle_record IS RECORD
    (
      txn_sequence  NUMBER,
      rowid_string  VARCHAR2(18),
      error_flag    BOOLEAN
    );

    TYPE typ_rowid_store IS TABLE OF typ_recycle_record
    INDEX BY BINARY_INTEGER;

    rowid_store typ_rowid_store;

  ----------------------------------------------------------------------------------------------------------------------------------

    PROCEDURE insert_recon IS

    BEGIN
      INSERT INTO extract_recon
      (
        recon_type,
        input_data_id,
        bin,
        stage,
        description,
        records,
        amount,
        datestamp
      )
      VALUES
      (
        'TJF',
        NULL,
        NULL,
        1,
        'Source Transactions',
        v_source_count,
        NULL,
        g_etl_date
      );

      INSERT INTO extract_recon
      (
        recon_type,
        input_data_id,
        bin,
        stage,
        description,
        records,
        amount,
        datestamp
      )
      VALUES
      (
        'TJF',
        NULL,
        NULL,
        2,
        'Recycle Transactions (New)',
        v_recycle_new_count,
        NULL,
        g_etl_date
      );

      INSERT INTO extract_recon
      (
        recon_type,
        input_data_id,
        bin,
        stage,
        description,
        records,
        amount,
        datestamp
      )
      VALUES
      (
        'TJF',
        NULL,
        NULL,
        3,
        'Recycle Transactions (unchanged)',
        v_recycle_old_count,
        NULL,
        g_etl_date
      );

      INSERT INTO extract_recon
      (
        recon_type,
        input_data_id,
        bin,
        stage,
        description,
        records,
        amount,
        datestamp
      )
      VALUES
      (
        'TJF',
        NULL,
        NULL,
        4,
        'Written Transactions',
        v_processed_count,
        NULL,
        g_etl_date
      );

      COMMIT;

    EXCEPTION
      WHEN OTHERS THEN
        standard_error.when_others_exception
        (
          p_program         => 'transaction_validation.insert_recon'
        );

    END insert_recon;

  ----------------------------------------------------------------------------------------------------------------------------------

    PROCEDURE map_tjf_rt02_processor_trans
    (
      p_tjf_rt02              IN OUT NOCOPY cur_tjf_rt02%ROWTYPE,
      p_processor_transaction IN OUT NOCOPY processor_transaction%ROWTYPE
    ) IS

    BEGIN
      p_processor_transaction.id                             := p_tjf_rt02.txn_sequence;
      p_processor_transaction.proc_date_time                 := p_tjf_rt02.proc_date_time;
      p_processor_transaction.proc_ref_nbr                   := 1;
      p_processor_transaction.effective_date                 := p_tjf_rt02.effective_date;
      p_processor_transaction.posted_amount                  := p_tjf_rt02.posted_amount;
      p_processor_transaction.posting_date                   := p_tjf_rt02.posting_date;
      p_processor_transaction.debit_credit                   := p_tjf_rt02.debit_credit;
      p_processor_transaction.txn_amount                     := p_tjf_rt02.txn_amount;
      p_processor_transaction.txn_local_datetime             := p_tjf_rt02.proc_date_time;
      p_processor_transaction.card_nbr                       := p_tjf_rt02.card_nbr;
      p_processor_transaction.channel_code                   := p_tjf_rt02.channel_code;
      p_processor_transaction.txn_currency_code              := p_tjf_rt02.txn_currency_code;
      p_processor_transaction.source_code                    := p_tjf_rt02.source_code;
      p_processor_transaction.category_code                  := p_tjf_rt02.category_code;
      p_processor_transaction.client_id                      := p_tjf_rt02.client_id;
      p_processor_transaction.bussiness_date                 := g_etl_date;
      p_processor_transaction.cashier_name                   := p_tjf_rt02.cashier_name;
      p_processor_transaction.reconciliation_amount          := 0;
      p_processor_transaction.txn_description                := p_tjf_rt02.txn_description;
      p_processor_transaction.merchant_id                    := 0;
      p_processor_transaction.sys_trace_audit_nbr            := p_tjf_rt02.sysgen_ref_nbr;
      p_processor_transaction.load_pay_method_code           := p_tjf_rt02.payment_method_of_load_reload;
      p_processor_transaction.recon_currency_code            := '0';
      p_processor_transaction.type_code                      := p_tjf_rt02.type_code;
      p_processor_transaction.source_bin                     := p_tjf_rt02.bin;
      p_processor_transaction.account_number                 := p_tjf_rt02.account_number;
      p_processor_transaction.posted_amt_ccy                 := p_tjf_rt02.coda_currency;
      p_processor_transaction.purse_1_txn_amt                := p_tjf_rt02.txn_amount;
      p_processor_transaction.purse_1_dr_cr                  := p_tjf_rt02.dbcr;
      p_processor_transaction.tjf_txn_cde                    := p_tjf_rt02.txn_cde;
      p_processor_transaction.tjf_txn_desc                   := p_tjf_rt02.txn_desc;
      p_processor_transaction.tjf_adj_code                   := p_tjf_rt02.adj_reason_cde;
      p_processor_transaction.tjf_fee_channel                := p_tjf_rt02.fee_channel;
      p_processor_transaction.owner_level_1                  := p_tjf_rt02.owner_level_1;
      p_processor_transaction.owner_level_2                  := p_tjf_rt02.owner_level_2;
      p_processor_transaction.owner_level_3                  := p_tjf_rt02.owner_level_3;
      p_processor_transaction.dispute_id                     := p_tjf_rt02.dispute_id;
      p_processor_transaction.record_type                    := 2;
      p_processor_transaction.etl_date                       := g_etl_date;
      p_processor_transaction.status                         :='XXX';
      p_processor_transaction.invoice_client_id              := p_tjf_rt02.invoice_client;
      p_processor_transaction.resolved_order_reference       := p_tjf_rt02.order_reference;
      p_processor_transaction.transaction_reason_code        := p_tjf_rt02.transaction_reason_code;
      p_processor_transaction.transaction_reference_number   := p_tjf_rt02.transaction_reference_number;
      p_processor_transaction.foreign_exchange_rate          := p_tjf_rt02.foreign_exchange_rate;
      p_processor_transaction.foreign_exchange_exponent      := p_tjf_rt02.foreign_exchange_exponent;
      p_processor_transaction.foreign_exchange_margin_rate   := p_tjf_rt02.foreign_exchange_margin_rate;
      p_processor_transaction.foreign_exch_mgn_exponent      := p_tjf_rt02.foreign_exch_mgn_exponent;
      p_processor_transaction.alternate_id1                  := p_tjf_rt02.alternate_id1;
      p_processor_transaction.cis_branch                     := p_tjf_rt02.cis_branch;
      p_processor_transaction.loyalty_program_id1            := p_tjf_rt02.loyalty_program_id1;
      p_processor_transaction.loyalty_reg_award_amount1      := p_tjf_rt02.loyalty_reg_award_amount1;
      p_processor_transaction.loyalty_bns_award_amount1      := p_tjf_rt02.loyalty_bns_award_amount1;
      p_processor_transaction.loyalty_indicator1             := p_tjf_rt02.loyalty_indicator1;
      p_processor_transaction.loyalty_dbcr1                  := p_tjf_rt02.loyalty_dbcr1;
      p_processor_transaction.loyalty_program_id2            := p_tjf_rt02.loyalty_program_id2;
      p_processor_transaction.loyalty_reg_award_amount2      := p_tjf_rt02.loyalty_reg_award_amount2;
      p_processor_transaction.loyalty_bns_award_amount2      := p_tjf_rt02.loyalty_bns_award_amount2;
      p_processor_transaction.loyalty_indicator2             := p_tjf_rt02.loyalty_indicator2;
      p_processor_transaction.loyalty_dbcr2                  := p_tjf_rt02.loyalty_dbcr2;
      p_processor_transaction.loyalty_program_id3            := p_tjf_rt02.loyalty_program_id3;
      p_processor_transaction.loyalty_reg_award_amount3      := p_tjf_rt02.loyalty_reg_award_amount3;
      p_processor_transaction.loyalty_bns_award_amount3      := p_tjf_rt02.loyalty_bns_award_amount3;
      p_processor_transaction.loyalty_indicator3             := p_tjf_rt02.loyalty_indicator3;
      p_processor_transaction.loyalty_dbcr3                  := p_tjf_rt02.loyalty_dbcr3;
      p_processor_transaction.loyalty_program_id4            := p_tjf_rt02.loyalty_program_id4;
      p_processor_transaction.loyalty_reg_award_amount4      := p_tjf_rt02.loyalty_reg_award_amount4;
      p_processor_transaction.loyalty_bns_award_amount4      := p_tjf_rt02.loyalty_bns_award_amount4;
      p_processor_transaction.loyalty_indicator4             := p_tjf_rt02.loyalty_indicator4;
      p_processor_transaction.loyalty_dbcr4                  := p_tjf_rt02.loyalty_dbcr4;
      p_processor_transaction.loyalty_program_id5            := p_tjf_rt02.loyalty_program_id5;
      p_processor_transaction.loyalty_reg_award_amount5      := p_tjf_rt02.loyalty_reg_award_amount5;
      p_processor_transaction.loyalty_bns_award_amount5      := p_tjf_rt02.loyalty_bns_award_amount5;
      p_processor_transaction.loyalty_indicator5             := p_tjf_rt02.loyalty_indicator5;
      p_processor_transaction.loyalty_dbcr5                  := p_tjf_rt02.loyalty_dbcr5;
      p_processor_transaction.prepaid_indicator              := p_tjf_rt02.prepaid_indicator;
      p_processor_transaction.source_product_code            := p_tjf_rt02.source_product_code;
      p_processor_transaction.source_rt                      := p_tjf_rt02.source_rt;
      p_processor_transaction.source_account_number          := p_tjf_rt02.source_account_number;
      p_processor_transaction.filler                         := p_tjf_rt02.filler;
      p_processor_transaction.program_ac_type                := p_tjf_rt02.program_ac_type;
      p_processor_transaction.program_portfolio_cde          := p_tjf_rt02.program_portfolio_cde;
      p_processor_transaction.orig_sell_processor_client_id  := p_tjf_rt02.orig_sell_processor_client_id;
      p_processor_transaction.original_selling_client_id     := p_tjf_rt02.original_selling_client_id;
      p_processor_transaction.card_brand_ind                 := p_tjf_rt02.card_brand_ind;
      p_processor_transaction.company_id                     := p_tjf_rt02.company_id;
      p_processor_transaction.fin_parent_bus_unit_no         := p_tjf_rt02.fin_parent_bus_unit_no;
      p_processor_transaction.fin_parent_child_unit_no       := p_tjf_rt02.fin_parent_child_unit_no;
      p_processor_transaction.ica_on_bin                     := p_tjf_rt02.ica_on_bin;
      p_processor_transaction.issuer_cntry_cde_on_bin        := p_tjf_rt02.issuer_cntry_cde_on_bin;
      p_processor_transaction.legal_entity_on_ica            := p_tjf_rt02.legal_entity_on_ica;
      p_processor_transaction.level_3_cde                    := p_tjf_rt02.level_3_cde;
      p_processor_transaction.level_4_cde                    := p_tjf_rt02.level_4_cde;
      p_processor_transaction.level_5_cde                    := p_tjf_rt02.level_5_cde;
      p_processor_transaction.multi_purse_flag               := p_tjf_rt02.multi_purse_flag;
      p_processor_transaction.non_fin_plastic_type           := p_tjf_rt02.non_fin_plastic_type;
      p_processor_transaction.payment_method_of_load_reload  := p_tjf_rt02.payment_method_of_load_reload;
      p_processor_transaction.product_cde_on_bin             := p_tjf_rt02.product_cde_on_bin;
      p_processor_transaction.secnd_lgl_entity_on_ica        := p_tjf_rt02.secnd_lgl_entity_on_ica;
      p_processor_transaction.sett_service_value_date        := p_tjf_rt02.sett_service_value_date;
      p_processor_transaction.txn_comment                    := p_tjf_rt02.txn_comment;
      p_processor_transaction.txn_ref_datetime               := p_tjf_rt02.txn_ref_datetime;
      p_processor_transaction.input_data_id                  := p_tjf_rt02.input_data_id;

    EXCEPTION
      WHEN OTHERS THEN
        standard_error.when_others_exception
        (
          p_program         => 'transaction_validation.map_tjf_rt02_processor_trans',
          p_additional_info => 'txn_sequence: '||p_tjf_rt02.txn_sequence
        );

    END map_tjf_rt02_processor_trans;

  ----------------------------------------------------------------------------------------------------------------------------------

    PROCEDURE save_processor_transaction(p_processor_transaction IN OUT NOCOPY processor_transaction%ROWTYPE) IS

    BEGIN
      processor_transaction_store(processor_transaction_store.COUNT+1) := p_processor_transaction;

    EXCEPTION
      WHEN OTHERS THEN
        standard_error.when_others_exception
        (
          p_program         => 'transaction_validation.save_processor_transaction',
          p_additional_info => 'txn_sequence: '||p_processor_transaction.id
        );

    END save_processor_transaction;

  ----------------------------------------------------------------------------------------------------------------------------------

    PROCEDURE flush_buffers IS

      v_ptr       BINARY_INTEGER;

    BEGIN
      standard_audit.log_entry
      (
        p_program     => 'transaction_validation.flush_buffers',
        p_step_name   => 'Flushing buffers',
        p_log_entry   => 'Updating processor transactions '||processor_transaction_store.COUNT,
        p_level       => 8
      );

      FOR i IN 1..processor_transaction_store.COUNT LOOP
        INSERT INTO processor_transaction VALUES processor_transaction_store(i);

        IF v_recode_required THEN
          IF processor_transaction_store(i).type_code = initial_load THEN
            INSERT INTO recode_txns
            (
              ac_nbr,
              old_txn_cde,
              old_txn_desc,
              new_txn_cde,
              new_txn_desc,
              txn_sequence
            )
            VALUES
            (
              processor_transaction_store(i).account_number,
              1,
              'InitialLoad',
              2,
              'Reload',
              processor_transaction_store(i).id
            );
          END IF;
        END IF;

      END LOOP;

      -- Can't do forall due to trigger.
      FOR i IN 1..transaction_recycle_store.COUNT LOOP
        INSERT INTO transaction_recycle VALUES transaction_recycle_store(i);
      END LOOP;
/*
      IF v_recode_required THEN
        v_ptr := processor_transaction_store.FIRST;

        WHILE v_ptr IS NOT NULL LOOP
          IF processor_transaction_store(v_ptr).type_code = initial_load THEN
            INSERT INTO recode_txns
            (
              ac_nbr,
              old_txn_cde,
              old_txn_desc,
              new_txn_cde,
              new_txn_desc,
              txn_sequence
            )
            VALUES
            (
              processor_transaction_store(v_ptr).account_number,
              1,
              'InitialLoad',
              2,
              'Reload',
              processor_transaction_store(v_ptr).id
            );
          END IF;
          v_ptr := processor_transaction_store.NEXT(v_ptr);
        END LOOP;
      END IF;*/

      recycling_report.process_buffer
      (
        p_exiting_recycling    => TRUE
      );

      COMMIT;

    EXCEPTION
      WHEN OTHERS THEN
        --ROLLBACK;
        standard_audit.log_entry
        (
          p_program     => 'transaction_validation.flush_buffers',
          p_step_name   => 'Flushing buffer error',
          p_log_entry   => 'Error while flushing buffer '||SQLERRM,
          p_level       => 8
        );
        standard_error.when_others_exception
        (
          p_program         => 'transaction_validation.flush_buffers'
        );

    END flush_buffers;

  ----------------------------------------------------------------------------------------------------------------------------------

    PROCEDURE save_transaction_recycle
    (
      p_txn_sequence NUMBER,
      p_status       VARCHAR2
    ) IS

      v_transaction_recycle   transaction_recycle%ROWTYPE;

    BEGIN
      v_transaction_recycle.txn_sequence := p_txn_sequence;
      v_transaction_recycle.datestamp    := g_etl_date;
      v_transaction_recycle.releasedate  := g_etl_date;
      v_transaction_recycle.status       := p_status;

      transaction_recycle_store(transaction_recycle_store.COUNT+1) := v_transaction_recycle;

    EXCEPTION
      WHEN OTHERS THEN
        standard_error.when_others_exception
        (
          p_program         => 'transaction_validation.save_transaction_recycle',
          p_additional_info => 'p_sequence: '||p_txn_sequence
        );

    END save_transaction_recycle;

  ----------------------------------------------------------------------------------------------------------------------------------

    PROCEDURE save_order_recycle
    (
      p_txn_sequence NUMBER,
      p_rowid_string VARCHAR2,
      p_error_flag   BOOLEAN
    ) IS

      v_rowid   typ_recycle_record;

    BEGIN

      v_rowid.txn_sequence := p_txn_sequence;
      v_rowid.rowid_string := p_rowid_string;
      v_rowid.error_flag   := p_error_flag;
      rowid_store(rowid_store.COUNT+1) := v_rowid;

    EXCEPTION
      WHEN OTHERS THEN
        standard_error.when_others_exception
        (
          p_program         => 'transaction_validation.save_order_recycle',
          p_additional_info => 'p_sequence: '||p_txn_sequence
        );

    END save_order_recycle;

  ----------------------------------------------------------------------------------------------------------------------------------

    PROCEDURE clear_stores IS

    BEGIN
      processor_transaction_store.DELETE;
      transaction_recycle_store.DELETE;
      rowid_store.DELETE;

    EXCEPTION
      WHEN OTHERS THEN
        standard_error.when_others_exception
        (
          p_program         => 'transaction_validation.clear_stores'
        );

    END clear_stores;

  ----------------------------------------------------------------------------------------------------------------------------------

    PROCEDURE insert_transaction_recycle
    (
      p_txn_sequence  NUMBER
    ) IS

    BEGIN
      INSERT INTO transaction_recycle
      (
        txn_sequence,
        datestamp,
        releasedate,
        status
      )
      VALUES
      (
        p_txn_sequence,
        g_etl_date,
        NULL,
        'REC'
      );

    EXCEPTION
      WHEN OTHERS THEN
        standard_error.when_others_exception
        (
          p_program         => 'transaction_validation.insert_transaction_recycle',
          p_additional_info => 'p_txn_sequence: '||p_txn_sequence
        );

    END insert_transaction_recycle;

  ----------------------------------------------------------------------------------------------------------------------------------

    PROCEDURE recycle_order IS

      v_store_ptr        BINARY_INTEGER;
      v_location         VARCHAR2(100);
      v_additional_info  VARCHAR2(100);

    BEGIN

      v_store_ptr := rowid_store.FIRST;
      v_location        := 'Before Loop';
      v_additional_info := '';

      standard_audit.log_entry
      (
        p_program     => 'transaction_validation.recycle_order',
        p_step_name   => 'Recycle order',
        p_log_entry   => 'Recycling order',
        p_level       => 8
      );

      WHILE v_store_ptr IS NOT NULL LOOP
        v_additional_info := 'TXN Sequence: '||rowid_store(v_store_ptr).txn_sequence||' ROWID: '||rowid_store(v_store_ptr).rowid_string;

        standard_audit.log_entry
        (
          p_program     => 'transaction_validation.recycle_order',
          p_step_name   => 'Recycle transaction',
          p_log_entry   => 'TXN Sequence: '||rowid_store(v_store_ptr).txn_sequence||' ROWID: '||rowid_store(v_store_ptr).rowid_string,
          p_level       => 9
        );

        IF rowid_store(v_store_ptr).rowid_string = 'N/A' THEN
          v_location          := 'Already in recycling';
          v_recycle_old_count := v_recycle_old_count + 1;
        ELSE
          v_location        := 'Inserting into TJF error data';
          INSERT INTO tjf_error_data
          (
            company_id,
            program_ac_type,
            program_portfolio_cde,
            ac_nbr,
            ol_1,
            ol_2,
            ol_3,
            txn_effective_date,
            cardno,
            posting_date,
            srce_cde,
            category_cde,
            level_3_cde,
            level_4_cde,
            level_5_cde,
            posted_amt,
            txn_dbcr_ind,
            txn_cde,
            create_user_id,
            cust_posting_datetime,
            txn_desc,
            sysgen_ref_nbr,
            adj_reason_cde,
            fee_channel,
            ccy_cde_for_posting_amt,
            ccy_exp_for_posting_amt,
            card_brand_ind,
            sett_service_value_date,
            issuer_cntry_cde_on_bin,
            product_cde_on_bin,
            ica_on_bin,
            legal_entity_on_ica,
            secnd_lgl_entity_on_ica,
            dispute_id,
            initiating_ol_1,
            initiating_ol_2,
            initiating_ol_3,
            payment_method_of_load_reload,
            fin_parent_bus_unit_no,
            fin_parent_child_unit_no,
            non_fin_plastic_type,
            txn_comment,
            multi_purse_flag,
            purse1_id,
            purse1_ccy,
            purse1_txn_amt,
            purse1_cr_db_ind,
            purse1_ccy_exp,
            txn_ref_datetime,
            transaction_reason_code,
            transaction_reference_number,
            foreign_exchange_rate,
            foreign_exchange_exponent,
            foreign_exchange_margin_rate,
            foreign_exch_mgn_exponent,
            alternate_id1,
            cis_branch,
            loyalty_program_id1,
            loyalty_reg_award_amount1,
            loyalty_bns_award_amount1,
            loyalty_indicator1,
            loyalty_dbcr1,
            loyalty_program_id2,
            loyalty_reg_award_amount2,
            loyalty_bns_award_amount2,
            loyalty_indicator2,
            loyalty_dbcr2,
            loyalty_program_id3,
            loyalty_reg_award_amount3,
            loyalty_bns_award_amount3,
            loyalty_indicator3,
            loyalty_dbcr3,
            loyalty_program_id4,
            loyalty_reg_award_amount4,
            loyalty_bns_award_amount4,
            loyalty_indicator4,
            loyalty_dbcr4,
            loyalty_program_id5,
            loyalty_reg_award_amount5,
            loyalty_bns_award_amount5,
            loyalty_indicator5,
            loyalty_dbcr5,
            prepaid_indicator,
            source_product_code,
            source_rt,
            source_account_number,
            filler,
            run_date,
            run_id,
            bin,
            input_data_id,
            txn_sequence
          )
          SELECT /*+ noparallel (tjf) */
            company_id,
            program_ac_type,
            program_portfolio_cde,
            ac_nbr,
            ol_1,
            ol_2,
            ol_3,
            txn_effective_date,
            cardno,
            posting_date,
            srce_cde,
            category_cde,
            level_3_cde,
            level_4_cde,
            level_5_cde,
            posted_amt,
            txn_dbcr_ind,
            txn_cde,
            create_user_id,
            cust_posting_datetime,
            txn_desc,
            sysgen_ref_nbr,
            adj_reason_cde,
            fee_channel,
            ccy_cde_for_posting_amt,
            ccy_exp_for_posting_amt,
            card_brand_ind,
            sett_service_value_date,
            issuer_cntry_cde_on_bin,
            product_cde_on_bin,
            ica_on_bin,
            legal_entity_on_ica,
            secnd_lgl_entity_on_ica,
            dispute_id,
            initiating_ol_1,
            initiating_ol_2,
            initiating_ol_3,
            payment_method_of_load_reload,
            fin_parent_bus_unit_no,
            fin_parent_child_unit_no,
            non_fin_plastic_type,
            txn_comment,
            multi_purse_flag,
            purse1_id,
            purse1_ccy,
            purse1_txn_amt,
            purse1_cr_db_ind,
            purse1_ccy_exp,
            txn_ref_datetime,
            transaction_reason_code,
            transaction_reference_number,
            foreign_exchange_rate,
            foreign_exchange_exponent,
            foreign_exchange_margin_rate,
            foreign_exch_mgn_exponent,
            alternate_id1,
            cis_branch,
            loyalty_program_id1,
            loyalty_reg_award_amount1,
            loyalty_bns_award_amount1,
            loyalty_indicator1,
            loyalty_dbcr1,
            loyalty_program_id2,
            loyalty_reg_award_amount2,
            loyalty_bns_award_amount2,
            loyalty_indicator2,
            loyalty_dbcr2,
            loyalty_program_id3,
            loyalty_reg_award_amount3,
            loyalty_bns_award_amount3,
            loyalty_indicator3,
            loyalty_dbcr3,
            loyalty_program_id4,
            loyalty_reg_award_amount4,
            loyalty_bns_award_amount4,
            loyalty_indicator4,
            loyalty_dbcr4,
            loyalty_program_id5,
            loyalty_reg_award_amount5,
            loyalty_bns_award_amount5,
            loyalty_indicator5,
            loyalty_dbcr5,
            prepaid_indicator,
            source_product_code,
            source_rt,
            source_account_number,
            filler,
            run_date,
            run_id,
            bin,
            input_data_id,
            txn_sequence
          FROM
            v_stg_tjf_type2    tjf
          WHERE ROWID = CHARTOROWID(rowid_store(v_store_ptr).rowid_string);

          v_recycle_new_count := v_recycle_new_count + 1;

          --APWAS-17657  Insert generic error message into transasction_errors table if there was no validation error for this record
          IF (NOT rowid_store(v_store_ptr).error_flag) THEN
            insert_error(10, rowid_store(v_store_ptr).txn_sequence, 'One or more reocrds failed transaction validation with the same order reference number');
          END IF;

        END IF;

        v_location := 'Inserting into recycling table';
        insert_transaction_recycle(rowid_store(v_store_ptr).txn_sequence);

        v_location  := 'Before next recycle';
        v_store_ptr := rowid_store.NEXT(v_store_ptr);

      END LOOP;

      v_additional_info := '';
      v_location := 'After Loop';

      recycling_report.process_buffer
      (
        p_exiting_recycling => FALSE
      );

      COMMIT;

    EXCEPTION
      WHEN OTHERS THEN
        ROLLBACK;
        standard_error.when_others_exception
        (
          p_program         => 'transaction_validation.recycle_order',
          p_location        => v_location,
          p_additional_info => v_additional_info
        );

    END recycle_order;

  ----------------------------------------------------------------------------------------------------------------------------------

    FUNCTION check_validation_error(p_tjf_rt02 IN OUT NOCOPY cur_tjf_rt02%ROWTYPE) RETURN BOOLEAN IS

      v_validation_error_flag  BOOLEAN;

    BEGIN

      v_validation_error_flag := FALSE;

      --------------------------------------------------------
      -- Missing Client error
      --------------------------------------------------------
      IF p_tjf_rt02.client_id is NULL THEN
        v_validation_error_flag := TRUE;
        standard_error.log_validation_error
        (
          p_program         => 'transaction_validation.check_validation_error',
          p_location        => 'Client check',
          p_message         => 'Client ID not found for processor client id '||p_tjf_rt02.processor_client_id,
          p_additional_info => 'Transaction sequence: '||p_tjf_rt02.txn_sequence
        );
        insert_error(1,p_tjf_rt02.txn_sequence,'Client ID not found for processor client id '||p_tjf_rt02.processor_client_id);
      END IF;

      IF p_tjf_rt02.original_selling_client_id IS NULL THEN
        v_validation_error_flag := TRUE;
        standard_error.log_validation_error
        (
          p_program         => 'transaction_validation.check_validation_error',
          p_location        => 'Selling client check',
          p_message         => 'Selling client ID not found for selling processor client id '||p_tjf_rt02.orig_sell_processor_client_id,
          p_additional_info => 'Transaction sequence: '||p_tjf_rt02.txn_sequence
        );
        insert_error(1,p_tjf_rt02.txn_sequence,'Client ID not found for selling processor client id '||p_tjf_rt02.orig_sell_processor_client_id);
      END IF;

      --------------------------------------------------------
      -- Invalid transaction type error
      --------------------------------------------------------
      IF p_tjf_rt02.type_code = 0 THEN
        v_validation_error_flag := TRUE;
        standard_error.log_validation_error
        (
          p_program         => 'transaction_validation.check_validation_error',
          p_location        => 'Type code check',
          p_message         => 'Type code not found for prefix '||p_tjf_rt02.txn_cde_prefix||' code '||p_tjf_rt02.txn_cde||' DB/CR '||p_tjf_rt02.dbcr,
          p_additional_info => 'Transaction sequence: '||p_tjf_rt02.txn_sequence
        );

        insert_error(3,p_tjf_rt02.txn_sequence,'Type code not found for prefix '||p_tjf_rt02.txn_cde_prefix||' code '||p_tjf_rt02.txn_cde||' DB/CR '||p_tjf_rt02.dbcr);
      END IF;

     --------------------------------------------------------
      -- Missing invoice client
      --------------------------------------------------------
      IF p_tjf_rt02.INVOICE_CLIENT = 0 and p_tjf_rt02.TYPE_CODE in (1,2,3) THEN
        v_validation_error_flag := TRUE;
        standard_error.log_validation_error
        (
          p_program         => 'transaction_validation.check_validation_error',
          p_location        => 'Invoice client check',
          p_message         => 'Invoice Client ID not found for Client ID '||p_tjf_rt02.client_id,
          p_additional_info => 'Transaction sequence: '||p_tjf_rt02.txn_sequence
        );
        insert_error(5,p_tjf_rt02.txn_sequence,'Invoice Client ID not found for Client ID '||p_tjf_rt02.client_id);
      END IF;

      --------------------------------------------------------
      -- Missing Settlement and Bank
      --------------------------------------------------------
      IF p_tjf_rt02.TYPE_CODE in (1,2,3)
      AND p_tjf_rt02.INVOICE_CLIENT > 0 and p_tjf_rt02.INVOICE_TYPE_FLAG <> 'N' THEN
        BEGIN
          SELECT
            nvl(prt.client_bank_account_id,0),
            nvl(prt.legal_entity_bank_account_id,0)
          INTO
            v_client_bank_account_id,
            v_legal_entity_bank_account_id
          FROM
            client_contract_part   prt,
            currency               cur
          WHERE
                prt.client_contract_id = p_tjf_rt02.invoice_client
            AND cur.iso_num_code       = p_tjf_rt02.txn_currency_code
            AND prt.currency_code      = cur.iso_code;

        EXCEPTION
          WHEN OTHERS THEN
            v_validation_error_flag := TRUE;
            standard_error.log_validation_error
            (
              p_program         => 'transaction_validation.check_validation_error',
              p_location        => 'Client contract part check',
              p_message         => 'Client contract part not found for client '||p_tjf_rt02.invoice_client||' Currency: '||p_tjf_rt02.txn_currency_code,
              p_additional_info => 'Transaction sequence: '||p_tjf_rt02.txn_sequence
            );
            insert_error(7,p_tjf_rt02.txn_sequence,'Client contract part not found for client '||p_tjf_rt02.invoice_client||' Currency: '||p_tjf_rt02.txn_currency_code);
        END;
      END IF;

      --------------------------------------------------------
      -- Missing card
      --------------------------------------------------------
      IF LENGTH(p_tjf_rt02.card_nbr) < 10 THEN
        v_validation_error_flag := TRUE;
        standard_error.log_validation_error
        (
          p_program         => 'transaction_validation.check_validation_error',
          p_location        => 'Card number check',
          p_message         => 'Invalid or missing card number: '||p_tjf_rt02.original_card_nbr,
          p_additional_info => 'Transaction sequence: '||p_tjf_rt02.txn_sequence
        );
        insert_error(6,p_tjf_rt02.txn_sequence,'Invalid card number: '||p_tjf_rt02.original_card_nbr);
      END IF;

      RETURN v_validation_error_flag;

    EXCEPTION
      WHEN OTHERS THEN
        standard_error.when_others_exception
        (
          p_program         => 'transaction_validation.check_validation_error',
          p_additional_info => 'Transaction sequence: '||p_tjf_rt02.txn_sequence,
          p_raise_exception => FALSE
        );
        insert_error(0,p_tjf_rt02.txn_sequence,'Invalid card number: '||p_tjf_rt02.original_card_nbr);

        RETURN TRUE;

    END check_validation_error;

  ----------------------------------------------------------------------------------------------------------------------------------

    PROCEDURE process_order IS

    BEGIN
      IF v_current_order_reference IS NOT NULL THEN
        IF NOT v_error_flag THEN
          BEGIN
            flush_buffers;
          EXCEPTION
            WHEN OTHERS THEN
              v_error_flag := TRUE;
          END;
        END IF;

        IF v_error_flag THEN
          recycle_order;
        ELSE
          v_processed_count := v_processed_count + processor_transaction_store.COUNT;
        END IF;
      END IF;

      clear_stores;

      -- Last process order will be after last record so v_ptr will be null
      IF v_ptr IS NOT NULL THEN
        v_recode_required := TRUE;
      END IF;

    EXCEPTION
      WHEN OTHERS THEN
        standard_error.when_others_exception
        (
          p_program         => 'transaction_validation.process_order'
        );

    END process_order;

  ----------------------------------------------------------------------------------------------------------------------------------

  BEGIN

    standard_audit.open_audit_trail
    (
      p_namespace   => 'TRANSACTION_VALIDATION',
      p_program     => 'transaction_validation.process_tjf',
      p_override    => FALSE
    );

    standard_audit.log_entry
    (
      p_program     => 'transaction_validation.process_tjf',
      p_step_name   => 'Initialising',
      p_log_entry   => 'p_input_data_id: '||p_input_data_id,
      p_level       => 2
    );

    v_source_count            := 0;
    v_processed_count         := 0;
    v_recycle_new_count       := 0;
    v_recycle_old_count       := 0;

    BEGIN
      g_etl_date := utl_etl_control.get_current_etl_date;
    EXCEPTION
      WHEN OTHERS THEN
        g_etl_date := NULL;
    END;

    -- If ETL date is not set then try business date off of the header record.
    IF g_etl_date IS NULL THEN
      SELECT  TO_DATE(TRIM(SUBSTR(the_record,5,6)),'YYMMDD')
      INTO    g_etl_date
      FROM    tjf_records_stg
      WHERE   input_data_id = p_input_data_id
      AND     record_type = '00';
    END IF;

    standard_audit.log_entry
    (
      p_program     => 'transaction_validation.process_tjf',
      p_step_name   => 'ETL date set',
      p_log_entry   => 'ETL date: '||g_etl_date,
      p_level       => 2
    );

    recycling_report.initialise
    (
      p_business_date     => g_etl_date,
      p_input_data_id     => p_input_data_id,
      p_record_type       => '02'
    );

    update_first_load(p_input_data_id);

    v_limit := NVL(stored_parameter.get_parameter('TRANSACTION_VALIDATION','BULK_COLLECT_LIMIT'),1000);

    standard_audit.log_entry
    (
      p_program     => 'transaction_validation.process_tjf',
      p_step_name   => 'Before process TJF Loop',
      p_log_entry   => 'Start of process TJF loop, limit: '||v_limit,
      p_level       => 2
    );

    OPEN cur_tjf_rt02;

    LOOP

      FETCH cur_tjf_rt02
      BULK COLLECT
      INTO buf_tjf_rt02
      LIMIT v_limit;

      IF buf_tjf_rt02.COUNT = 0 THEN
        EXIT;
      END IF;

      standard_audit.log_entry
      (
        p_program     => 'transaction_validation.process_tjf',
        p_step_name   => 'Process Buffer',
        p_log_entry   => 'Processing records '||buf_tjf_rt02.COUNT,
        p_level       => 4
      );

      v_source_count := v_source_count + buf_tjf_rt02.COUNT;

      v_ptr := buf_tjf_rt02.FIRST;

      WHILE v_ptr IS NOT NULL LOOP
        IF buf_tjf_rt02(v_ptr).order_reference <> NVL(v_current_order_reference,'X') THEN
          standard_audit.log_entry
          (
            p_program     => 'transaction_validation.process_tjf',
            p_step_name   => 'Before process order (loop)',
            p_log_entry   => 'Order reference.' || v_current_order_reference || ' error flag '||boolean_to_string(v_error_flag),
            p_level       => 8
          );

          process_order;
          v_error_flag              := FALSE;
          v_current_order_reference := buf_tjf_rt02(v_ptr).order_reference;
        END IF;

        IF buf_tjf_rt02(v_ptr).source_rowid = 'N/A' THEN
          v_in_out_indicator  := 0;
        ELSE
          v_in_out_indicator  := 1;
        END IF;

        v_amount := buf_tjf_rt02(v_ptr).txn_amount;

        IF v_amount <>0 AND buf_tjf_rt02(v_ptr).debit_credit = 'D' THEN
          v_amount := -v_amount;
        END IF;

        recycling_report.add_record
        (
          p_txn_sequence      => buf_tjf_rt02(v_ptr).txn_sequence,
          p_purse_txn_seq     => 0,
          p_business_date     => g_etl_date,
          p_record_type       => '02',
          p_in_out_indicator  => v_in_out_indicator,
          p_bin               => buf_tjf_rt02(v_ptr).bin,
          p_ica               => buf_tjf_rt02(v_ptr).ica_on_bin,
          p_txn_cde           => buf_tjf_rt02(v_ptr).txn_cde,
          p_ssv_date          => NULL,
          p_amount_type       => recycling_report.amount_type_purse,
          p_currency_code     => buf_tjf_rt02(v_ptr).txn_currency_code,
          p_amount            => v_amount,
          p_input_data_id     => p_input_data_id                                    -- Has to be input_data_id being processed NOT the original for batching.
        );

        BEGIN
          IF check_validation_error(buf_tjf_rt02(v_ptr)) THEN
            v_error_flag    := TRUE;
            save_order_recycle(buf_tjf_rt02(v_ptr).txn_sequence,buf_tjf_rt02(v_ptr).source_rowid, TRUE);
          ELSE
            save_order_recycle(buf_tjf_rt02(v_ptr).txn_sequence,buf_tjf_rt02(v_ptr).source_rowid, FALSE);
          END IF;

          IF buf_tjf_rt02(v_ptr).first_load_account = 0 THEN
            v_recode_required := FALSE;
          END IF;

          map_tjf_rt02_processor_trans(buf_tjf_rt02(v_ptr),v_processor_transaction);
          save_processor_transaction(v_processor_transaction);
          
          --Added below for SCP and MCP merge
          SELECT NVL(MAX(exclude_matching),'N')
          INTO   v_exclude_matching
          FROM   client
          WHERE  id = buf_tjf_rt02(v_ptr).invoice_client;


          IF (v_exclude_matching = 'Y' AND buf_tjf_rt02(v_ptr).type_code in (1,2,3,51,52,98,99,96,97)) THEN
            save_transaction_recycle(buf_tjf_rt02(v_ptr).txn_sequence,'NMR');
          ELSIF ((buf_tjf_rt02(v_ptr).matching = 'Y' AND buf_tjf_rt02(v_ptr).type_code in (1,2,3))
          OR (buf_tjf_rt02(v_ptr).type_code in (51,52,98,99,96,97))) AND NVL(v_exclude_matching,'N') = 'N' THEN--Added 96,97 as part of CMP1181 CR
            save_transaction_recycle(buf_tjf_rt02(v_ptr).txn_sequence,'MRQ');            
          ELSE
            save_transaction_recycle(buf_tjf_rt02(v_ptr).txn_sequence,'NMA');
          END IF;

        EXCEPTION
          WHEN OTHERS THEN
            v_error_flag := TRUE;

            standard_error.log_validation_error
            (
              p_program         => 'transaction_validation.process_tjf',
              p_location        => 'Transaction Processing Error',
              p_message         => 'Error while processing txn_sequence '||buf_tjf_rt02(v_ptr).txn_sequence||'. '||SQLERRM,
              p_additional_info => 'Transaction sequence: '||buf_tjf_rt02(v_ptr).txn_sequence
            );
        END;

        standard_audit.log_entry
        (
          p_program     => 'transaction_validation.process_tjf',
          p_step_name   => 'Process TJF loop',
          p_log_entry   =>
            'Processing: '||buf_tjf_rt02(v_ptr).table_id||' '||buf_tjf_rt02(v_ptr).order_reference||' '||buf_tjf_rt02(v_ptr).txn_sequence||' '||boolean_to_string(v_error_flag),
          p_level       => 9
        );

        v_ptr := buf_tjf_rt02.NEXT(v_ptr);
      END LOOP;
    END LOOP;

    standard_audit.log_entry
    (
      p_program     => 'transaction_validation.process_tjf',
      p_step_name   => 'Before process order (end)',
      p_log_entry   => 'Order reference.' || v_current_order_reference || ' error flag '||boolean_to_string(v_error_flag),
      p_level       => 8
    );

    process_order;

    CLOSE cur_tjf_rt02;

    standard_audit.log_entry
    (
      p_program     => 'transaction_validation.process_tjf',
      p_step_name   => 'Process TJF End',
      p_log_entry   =>
        'End of process TJF loop.'
        ||' Source Count: '                 ||v_source_count
        ||' New into Recycling: '           ||v_recycle_new_count
        ||' Remaining in Recycling: '       ||v_recycle_old_count
        ||' Processed: '                    ||v_processed_count,
      p_level       => 2
    );

    standard_audit.log_entry
    (
      p_program     => 'transaction_validation.process_tjf',
      p_step_name   => 'insert recon',
      p_log_entry   => 'Inserting into extract_recon',
      p_level       => 2
    );

    insert_recon;

    recycling_report.finalise;

    standard_audit.close_audit_trail
    (
      p_program => 'transaction_validation.process_tjf'
    );

  EXCEPTION
    WHEN OTHERS THEN
      IF cur_tjf_rt02%ISOPEN THEN
        CLOSE cur_tjf_rt02;
      END IF;

      standard_audit.log_entry
      (
        p_program     => 'transaction_validation.process_tjf',
        p_step_name   => 'Process TJF error',
        p_log_entry   => 'p_input_data_id: '||p_input_data_id||' error '||SQLERRM,
        p_level       => 1
      );

      standard_audit.close_audit_trail
      (
        p_program => 'transaction_validation.process_tjf'
      );

      standard_error.when_others_exception
      (
        p_program         => 'transaction_validation.process_tjf',
        p_additional_info => 'p_input_data_id: '||p_input_data_id
      );

  END process_tjf;

------------------------------------------------------------------------------------------------------------------------------------

  PROCEDURE control(pINEnv IN VARCHAR2) IS

    v_file_seq1             NUMBER;
    v_file_seq2             NUMBER;

    ----------------------------------------------------------------------------------------------------------------------------------

    FUNCTION get_file_seq(p_env IN VARCHAR2) RETURN NUMBER IS

      v_ret_val NUMBER;

    BEGIN
      IF p_env = 'PROD' THEN
        SELECT  file_seq
        INTO    v_ret_val
        FROM    mcp_etl_control
        WHERE   file_type = 'CRDTXN'
        AND     file_name LIKE '%TVXSP%'
        AND     processed = 'N';
      ELSE
        SELECT  file_seq
        INTO    v_ret_val
        FROM    mcp_etl_control
        WHERE   file_type = 'CRDTXN'
        AND     file_name LIKE '%TVXST%'
        AND     processed = 'N';
      END IF;

      RETURN v_ret_val;

    EXCEPTION
      WHEN OTHERS THEN
        standard_error.when_others_exception
        (
          p_program         => 'transaction_validation.get_file_seq',
          p_additional_info => 'p_env: '||p_env
        );

    END get_file_seq;

  ----------------------------------------------------------------------------------------------------------------------------------

  BEGIN

    standard_audit.open_audit_trail
    (
      p_namespace   => 'TRANSACTION_VALIDATION',
      p_program     => 'transaction_validation.control'
    );

    IF pINEnv IN ('BOTH', 'PROD', 'TEST') THEN
      IF pINEnv = 'BOTH' THEN
        v_file_seq1 := get_file_seq('PROD');
        v_file_seq2 := get_file_seq('TEST');
      ELSE
        v_file_seq1 := get_file_seq(pINEnv);
        v_file_seq2 := -1;
      END IF;
    ELSE
      standard_error.raise_program_error
      (
        p_program          => 'transaction_validation.control',
        p_message          => 'Invalid Environment Parameter '|| pINEnv
      );
    END IF;

    IF v_file_seq1 = -99 OR v_file_seq2 = -99 THEN
      standard_error.raise_program_error
      (
        p_program          => 'transaction_validation.control',
        p_message          => 'Invalid TJF file'
      );
    END IF;

    process_tjf(v_file_seq1);

    IF v_file_seq2 > 0 THEN
      process_tjf(v_file_seq2);
    END IF;

    standard_audit.close_audit_trail
    (
      p_program => 'transaction_validation.control'
    );

  EXCEPTION
    WHEN OTHERS THEN
      standard_audit.close_audit_trail
      (
        p_program => 'transaction_validation.control'
      );
      standard_error.when_others_exception
      (
        p_program         => 'transaction_validation.control',
        p_additional_info => 'pINEnv: '||pINEnv
      );

  END control;

------------------------------------------------------------------------------------------------------------------------------------

END transaction_validation;
/
