CREATE OR REPLACE PACKAGE BODY CPI_OWNER.caefxt_processing AS

/*****************************************************************************************************
   PURPOSE:    To process incoming CAEF files

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        17/08/2015  JT               Initial version.
  2.0        29/04/2016  Srini             Removed the sequence and populating it by a trigger in CPF.
  3.0        31/07/2016  Srini             Added autonoumous transaction for hide card number
  4.0        07/08/2018  Srini             DE44219-added constraints and new columns to package
  5.0       19/02/2018  Srini             Add exception section to consider the missing cards
*****************************************************************************************************/

  ----------------------------------------------------------------------------------------------------------------------------------

  -- Retrieve Changed Pan record (Creating new if required)

  v_check_flag    VARCHAR2(1) := 'N'; --CMP0630

  FUNCTION hide_card_no(p_pan IN VARCHAR2) RETURN VARCHAR2 IS

    v_seq_no        VARCHAR2(12);
    v_last_four     VARCHAR2(4);
    v_changed_pan   VARCHAR2(16);
    v_pan           VARCHAR2(23);
    PRAGMA          AUTONOMOUS_TRANSACTION;

  BEGIN
    v_check_flag := 'N';
    v_pan := LTRIM(TRIM(p_pan),'0');

    SELECT  MIN(changed_pan)
    INTO    v_changed_pan
    FROM    pan_xref
    WHERE   pan = v_pan;

    IF v_changed_pan IS NULL THEN
      v_seq_no      := TO_CHAR(seq_pan_xref.NEXTVAL);
      v_last_four   := SUBSTR(v_pan,-4,4);
      v_changed_pan := v_seq_no|| v_last_four;
      BEGIN
      INSERT INTO pan_xref
      (
        pan,
        changed_pan,
        bin,
        last_four,
        pan_xref_id,
        source,
        creation_datetime
      )
      VALUES
      (
        v_pan,
        v_changed_pan,
        SUBSTR(v_pan,1,6),
        v_last_four,
        v_seq_no,
        'CAEF',
        SYSTIMESTAMP
      );
        EXCEPTION
        WHEN OTHERS THEN
        v_changed_pan := NULL;
                    
        SELECT  MIN(changed_pan)
        INTO    v_changed_pan
        FROM    pan_xref
        WHERE   pan = v_pan;
                
        standard_error.when_others_exception
        (
          p_program         => 'caefxt_processing.retrieve_card_no',
          p_additional_info => 'p_pan: '||v_pan|| ' v_changed_pan: '||v_changed_pan,
          p_raise_exception => FALSE
        );
        END;
     COMMIT;

      v_check_flag := 'Y';

    END IF;

    RETURN v_changed_pan;

  EXCEPTION
    WHEN OTHERS THEN
      standard_error.when_others_exception
      (
        p_program         => 'caefxt_processing.hide_card_no',
        p_additional_info => 'p_pan: '||p_pan
      );

  END hide_card_no;

  ----------------------------------------------------------------------------------------------------------------------------------

  PROCEDURE update_acof_records_stg
  (
    p_input_data_id   NUMBER,
    p_run_id          NUMBER,
    p_start_time      DATE
  ) IS

  BEGIN
    acof_all_processing.update_acof_records_stg
    (
      p_input_data_id   => p_input_data_id,
      p_run_id          => p_run_id,
      p_start_time      => p_start_time
    );

  EXCEPTION
    WHEN OTHERS THEN
      standard_error.when_others_exception
      (
        p_program         => 'caefxt_processing.update_acof_records_stg',
        p_additional_info => 'p_input_data_id: '||p_input_data_id||' p_run_id: '||p_run_id||' p_start_time: '||p_start_time
      );

  END update_acof_records_stg;

  ----------------------------------------------------------------------------------------------------------------------------------

  PROCEDURE process_file_import_data
  (
    p_input_data_id       NUMBER,
    p_update_stage_table  VARCHAR2 DEFAULT 'Y'
  ) IS

    v_location              VARCHAR2(100);
    v_ptr                   BINARY_INTEGER;
    v_run_id                NUMBER;
    v_start_time            DATE;
    v_inventory_indicator   VARCHAR2(1 CHAR);
    v_changed_card_no       VARCHAR2(16 CHAR);
    v_string_ptr            VARCHAR2(1 CHAR);
    v_mapping_script        VARCHAR2(32767);
    v_the_record            acof_records_stg.the_record%TYPE;
    v_proc_client_id        VARCHAR2(50 CHAR);
    v_ips_header_date       DATE;
    v_bulk_limit            PLS_INTEGER := 10000;
    v_string_table          string_table;
    v_read_ptr              BINARY_INTEGER;
    v_messages              VARCHAR2(32767);

    CURSOR  cur_file_import_data IS
    SELECT  file_data
    FROM    file_import_data
    WHERE   input_data_id = p_input_data_id;

    buf_file_import_data    cur_file_import_data%ROWTYPE;

    TYPE typ_tmp_acof_all IS TABLE OF tmp_acof_all%ROWTYPE
    INDEX BY BINARY_INTEGER;

    buf_tmp_acof_all  typ_tmp_acof_all;

    TYPE typ_record_type_count IS TABLE OF NUMBER
    INDEX BY VARCHAR2(1 CHAR);

    v_record_type_count     typ_record_type_count;

    TYPE typ_client_tab IS TABLE OF NUMBER
    INDEX BY BINARY_INTEGER;

    v_typ_client_tab         typ_client_tab;

    v_typ_card_dist_info     typ_card_distribution_tab := typ_card_distribution_tab();
    v_typ_card_dist_info_mty typ_card_distribution_tab := typ_card_distribution_tab();

    v_cnt           NUMBER        := 1;
    v_insert_flag   VARCHAR2(1);

    --------------------------------------------------------------------------------------------------------------------------------

    PROCEDURE process_line(p_line VARCHAR2) IS

      v_record_type           tmp_acof_all.record_type%TYPE;
      v_the_record_in         tmp_acof_all.the_record%TYPE;
      v_the_record_remapped   tmp_acof_all.the_record%TYPE;
      v_write_ptr             BINARY_INTEGER := 0;

    BEGIN
      v_record_type   := SUBSTR(p_line,1,1);
      v_the_record_in := RTRIM(SUBSTR(p_line,2));

      v_write_ptr := buf_tmp_acof_all.COUNT;

      IF v_record_type = 'H' OR v_record_type = 'T' THEN
        IF v_record_type = 'H' THEN
          v_ips_header_date := TO_DATE(SUBSTR(v_the_record_in,8,6),'YYMMDD');
        END IF;

        buf_tmp_acof_all(v_write_ptr).the_record          := v_the_record_in;
        buf_tmp_acof_all(v_write_ptr).status              := 'C';
        buf_tmp_acof_all(v_write_ptr).inventory_indicator := 'N';
        buf_tmp_acof_all(v_write_ptr).start_time          := v_start_time;
        buf_tmp_acof_all(v_write_ptr).run_id              := v_run_id;
        buf_tmp_acof_all(v_write_ptr).record_type         := v_record_type;
        buf_tmp_acof_all(v_write_ptr).bin                 := NULL;
        buf_tmp_acof_all(v_write_ptr).input_data_id       := p_input_data_id;
        buf_tmp_acof_all(v_write_ptr).txn_sequence        := seq_txn_sequence.NEXTVAL;
        buf_tmp_acof_all(v_write_ptr).first_appearance    := 'N';
        buf_tmp_acof_all(v_write_ptr).ips_header_date     := v_ips_header_date;

      ELSIF v_record_type = 'D' THEN

        v_location    := 'Set Card Number';
        v_changed_card_no := hide_card_no(p_pan => SUBSTR(v_the_record_in, 2, 16));

        v_location    := 'Set Inventory indicator';
        IF TRIM(REPLACE(SUBSTR(v_the_record_in,1224,81),',','')) IS NULL THEN
          v_inventory_indicator := 'I';  -- Inventory
        ELSE
          v_inventory_indicator := 'S';  -- Sold
        END IF;

        BEGIN
          v_location := 'Inserting to Card distribution info Table type';
          IF v_check_flag = 'Y' and v_typ_client_tab.count > 0 THEN

            v_insert_flag :=  'N';

            FOR i IN 1..v_typ_client_tab.count LOOP
              IF TO_CHAR(v_typ_client_tab(i)) =  LPAD(SUBSTR(v_the_record_in, 1426 , 5),5,'0')
              OR TO_CHAR(v_typ_client_tab(i)) =  NVL(LPAD(RTRIM(TRIM(SUBSTR(v_the_record_in, 1432 , 5)),'*'),5,'0'),'00000') THEN
                v_insert_flag := 'Y';
              END IF;
            END LOOP;

            IF v_insert_flag = 'Y' THEN
              v_proc_client_id :=
                LPAD(SUBSTR(v_the_record_in, 1426 , 5),5,'0')
                || NVL(LPAD(RTRIM(TRIM(SUBSTR(v_the_record_in, 1432 , 5)),'*'),5,'0'),'00000')
                || NVL(LPAD(RTRIM(TRIM(SUBSTR(v_the_record_in, 1438 , 5)),'*'),5,'0'),'00000');

              v_typ_card_dist_info.extend;

              v_typ_card_dist_info(v_cnt):=
                typ_card_distribution_obj
                (
                  v_proc_client_id,
                  v_inventory_indicator,
                  v_record_type,
                  SUBSTR(v_the_record_in,2,6)
                );
              v_cnt :=  v_cnt + 1;
            END IF;
          END IF;

        EXCEPTION
          WHEN OTHERS THEN
          standard_error.when_others_exception
          (
            p_program         => 'caefxt_processing.process_file_import_data - insert card distro',
            p_location        => v_location,
            p_additional_info => 'Card distribution info ',
            p_raise_exception => FALSE
          );
        END;

        v_location    := 'UPDATE acof_all';
        string_mapper.set_variable('CARDNO', v_changed_card_no);
        string_mapper.map_string(v_the_record_in,v_the_record_remapped);

        v_location    := 'INSERT acof_all '||TO_CHAR(v_ptr);

        buf_tmp_acof_all(v_write_ptr).the_record          := v_the_record_remapped;
        buf_tmp_acof_all(v_write_ptr).status              := 'C';
        buf_tmp_acof_all(v_write_ptr).inventory_indicator := v_inventory_indicator;
        buf_tmp_acof_all(v_write_ptr).start_time          := v_start_time;
        buf_tmp_acof_all(v_write_ptr).run_id              := v_run_id;
        buf_tmp_acof_all(v_write_ptr).record_type         := v_record_type;
        buf_tmp_acof_all(v_write_ptr).bin                 := SUBSTR(v_the_record_in,2,6);
        buf_tmp_acof_all(v_write_ptr).input_data_id       := p_input_data_id;
       -- buf_tmp_acof_all(v_write_ptr).txn_sequence        := seq_txn_sequence.NEXTVAL;
        buf_tmp_acof_all(v_write_ptr).first_appearance    := 'Y';
        buf_tmp_acof_all(v_write_ptr).ips_header_date     := v_ips_header_date;
      END IF;

      IF v_record_type_count.EXISTS(v_record_type) THEN
        v_record_type_count(v_record_type) := v_record_type_count(v_record_type) + 1;
      ELSE
        v_record_type_count(v_record_type) := 1;
      END IF;

    EXCEPTION
      WHEN OTHERS THEN
        standard_error.when_others_exception
        (
          p_program         => 'caefxt_processing.process_line'
        );

    END process_line;

    --------------------------------------------------------------------------------------------------------------------------------

    PROCEDURE temp_to_staging IS

    BEGIN
      standard_audit.log_entry
      (
        p_program     => 'caefxt_processing.temp_to_staging',
        p_step_name   => 'Update acof_all H and T',
        p_log_entry   => 'Update global staging table acof_all header and trailer records',
        p_level       => 2
      );

      UPDATE  acof_all
      SET     status = 'H'
      WHERE   status = 'C'
      AND     (record_type = 'H' OR record_type = 'T');

      standard_audit.log_entry
      (
        p_program     => 'caefxt_processing.temp_to_staging',
        p_step_name   => 'Update acof_all',
        p_log_entry   => 'Update global staging table acof_all',
        p_level       => 2
      );

      INSERT/*+ APPEND */
      INTO acof_all
      (
        the_record,
        status,
        inventory_indicator,
        start_time,
        run_id,
        record_type,
        bin,
        input_data_id,
     --   txn_sequence,
        ips_header_date,
        first_appearance
      )
      SELECT
        the_record,
        status,
        inventory_indicator,
        start_time,
        run_id,
        record_type,
        bin,
        input_data_id,
     --   txn_sequence,
        ips_header_date,
        first_appearance
      FROM tmp_acof_all;

      COMMIT;

      standard_audit.log_entry
      (
        p_program     => 'caefxt_processing.temp_to_staging',
        p_step_name   => 'Update acof_all statuses',
        p_log_entry   => 'Update global staging table acof_all',
        p_level       => 2
      );

      UPDATE  acof_all
      SET     status = 'H'
      WHERE   status = 'C'
      AND     input_data_id <> p_input_data_id
      AND     record_type = 'D'
      AND EXISTS
      (
        SELECT  1
        FROM    acof_all new_acof_all
        WHERE   new_acof_all.input_data_id = p_input_data_id
        AND     new_acof_all.record_type = 'D'
        AND     SUBSTR(new_acof_all.the_record,1,16) = SUBSTR(acof_all.the_record,1,16)
      );

      COMMIT;

      BEGIN
        v_location    := 'Insert data to card distribution info';

        standard_audit.log_entry
        (
          p_program     => 'caefxt_processing.temp_to_staging',
          p_step_name   => 'Update tmp_card_distribution_info',
          p_log_entry   => 'Update table tmp_card_distribution_info',
          p_level       => 2
        );

        INSERT INTO tmp_card_distribution_info
        (
          total_cnt,
          processor_client_id,
          inventory_indicator,
          start_time,
          run_id,
          record_type,
          bin,
          input_data_id,
          ips_header_date,
          create_date,
          create_user
        )
        SELECT
          count(processor_client_id),
          NVL(processor_client_id,'0'),
          inventory_indicator,
          v_start_time,
          v_run_id,
          record_type,
          bin,
          p_input_data_id,
          v_ips_header_date,
          TRUNC(SYSDATE),
          USER
        FROM TABLE(v_typ_card_dist_info)
        GROUP BY
          NVL(processor_client_id,'0'),
          inventory_indicator,
          v_start_time,
          v_run_id,
          record_type,
          bin,
          p_input_data_id,
          v_ips_header_date,
          TRUNC(SYSDATE),
          USER;

        standard_audit.log_entry
        (
          p_program     => 'caefxt_processing.temp_to_staging',
          p_step_name   => 'Update tmp_card_distribution_info',
          p_log_entry   => 'Update global staging table tmp_card_distribution_info',
          p_level       => 2
        );


        INSERT INTO card_distribution_info
        (
          total_cnt,
          processor_client_id,
          inventory_indicator,
          start_time,
          run_id,
          record_type,
          bin,
          input_data_id,
          ips_header_date,
          create_date,
          create_user
        )
        SELECT
          total_cnt,
          processor_client_id,
          inventory_indicator,
          start_time,
          run_id,
          record_type,
          bin,
          input_data_id,
          ips_header_date,
          create_date,
          create_user
        FROM tmp_card_distribution_info;

        COMMIT;

        standard_audit.log_entry
        (
          p_program     => 'caefxt_processing.temp_to_staging',
          p_step_name   => 'Update v_typ_card_dist_info',
          p_log_entry   => 'Update v_typ_card_dist_info',
          p_level       => 2
        );

        v_typ_card_dist_info := v_typ_card_dist_info_mty;
        v_cnt := 1;

      EXCEPTION
        WHEN OTHERS THEN
          standard_error.when_others_exception
          (
            p_program         => 'caefxt_processing.temp_to_staging',
            p_location        => v_location,
            p_additional_info => 'inserting into card_distribution_info: ',
            p_raise_exception => FALSE
          );
      END;

   EXCEPTION
      WHEN OTHERS THEN
        standard_error.when_others_exception
        (
          p_program         => 'caefxt_processing.temp_to_staging'
        );

    END temp_to_staging;

    --------------------------------------------------------------------------------------------------------------------------------

  BEGIN
    standard_audit.open_audit_trail
    (
      p_namespace   => 'CAEFXT_PROCESSING',
      p_program     => 'caefxt_processing.process_file_import_data'
    );

    v_location    := 'Start';
    v_run_id      := seq_ips_ext_log_run_id.NEXTVAL;
    v_start_time  := SYSDATE;

    standard_audit.log_entry
    (
      p_program     => 'caefxt_processing.process_file_import_data',
      p_step_name   => v_location,
      p_log_entry   => 'Processing input_data_id: '||p_input_data_id,
      p_level       => 2
    );

    v_location    := 'Truncate Tables';
    standard_audit.log_entry
    (
      p_program     => 'caefxt_processing.process_file_import_data',
      p_step_name   => v_location,
      p_log_entry   => 'TRUNCATE tmp_acof_all',
      p_level       => 2
    );

    EXECUTE IMMEDIATE 'TRUNCATE TABLE tmp_acof_all';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE tmp_card_distribution_info';

    v_location    := 'Get mapping script';
    string_mapper.clear_variable_store;
    IF NOT stored_parameter.parameter_exists('CAEFXT_PROCESSING','MAPPING_SCRIPT') THEN
      standard_error.raise_program_error
      (
        p_program          => 'caefxt_processing.process_file_import_data',
        p_message          => 'Mapping script (Stored Parameter namespace: "CAEFXT_PROCESSING" name: "MAPPING_SCRIPT") is not found'
      );
    END IF;

    v_mapping_script := stored_parameter.get_parameter('CAEFXT_PROCESSING','MAPPING_SCRIPT');

    v_location    := 'Load mapping script';
    string_mapper.load_script(v_mapping_script);

    v_location    := 'Load client ids';
    BEGIN
      SELECT DISTINCT client_id
      BULK COLLECT
      INTO  v_typ_client_tab
      FROM  invoice_client_fee
      WHERE fee_type_id = 21; ---- Card distribution fee

    EXCEPTION
      WHEN OTHERS THEN
        standard_error.when_others_exception
        (
          p_program         => 'Load client ids',
          p_location        => v_location,
          p_additional_info => 'Bilk collect client ids ',
          p_raise_exception => FALSE
        );
    END;

    v_location    := 'Open cur_external_caef';

    OPEN cur_file_import_data;
    FETCH cur_file_import_data
    INTO buf_file_import_data;
    IF cur_file_import_data%NOTFOUND THEN
      CLOSE cur_file_import_data;
      standard_error.raise_program_error
      (
        p_program         => 'caefxt_processing.process_file_import_data',
        p_message         => 'File Import record not found for file import ID '||p_input_data_id
      );
    END IF;

    CLOSE cur_file_import_data;

    blob_utilities.open_blob(buf_file_import_data.file_data);

    v_location    := 'Start fetch loop';
    standard_audit.log_entry
    (
      p_program     => 'caefxt_processing.process_file_import_data',
      p_step_name   => v_location,
      p_log_entry   => 'Start data fetch loop. Bulk Limit '||v_bulk_limit,
      p_level       => 2
    );

    LOOP
      blob_utilities.fetch_blob_to_string_table(v_string_table,v_bulk_limit);

      IF v_string_table.count = 0 THEN
        EXIT;
      END IF;

      v_read_ptr := v_string_table.FIRST;

      WHILE v_read_ptr IS NOT NULL LOOP
        process_line(v_string_table(v_read_ptr));
        v_location    := 'Next line ptr';
        v_read_ptr := v_string_table.NEXT(v_read_ptr);
      END LOOP;

      v_location    := 'Bulk update tmp_acof_all';

      FORALL i IN buf_tmp_acof_all.FIRST .. buf_tmp_acof_all.LAST
        INSERT INTO tmp_acof_all VALUES buf_tmp_acof_all(i);

      COMMIT;

      buf_tmp_acof_all.DELETE;
      v_location    := 'END OUTER LOOP';
    END LOOP;

    v_location    := 'Close BLOB';

    standard_audit.log_entry
    (
      p_program     => 'caefxt_processing.process_file_import_data',
      p_step_name   => v_location,
      p_log_entry   => 'Close BLOB',
      p_level       => 3
    );

    blob_utilities.close_blob;

    v_location    := 'Update global staging';

    temp_to_staging;

   v_location    := 'Call update_acof_records_stg';
    standard_audit.log_entry
    (
      p_program     => 'caefxt_processing.process_file_import_data',
      p_step_name   => v_location,
      p_log_entry   => 'Update acof_records_stg',
      p_level       => 2
    );

    IF p_update_stage_table = 'Y' THEN
      update_acof_records_stg
      (
        p_input_data_id => p_input_data_id,
        p_run_id        => v_run_id,
        p_start_time    => v_start_time
      );
    END IF;

    v_location    := 'Write Log';

    v_string_ptr :=  v_record_type_count.FIRST;

    WHILE v_string_ptr IS NOT NULL LOOP

      pkg_ips_ext_common.g_ips_ext_log.messages :=
        pkg_ips_ext_common.g_ips_ext_log.messages || CHR(10)
        || 'Record Type '||v_string_ptr||': '||v_record_type_count(v_string_ptr)||' records';

      v_string_ptr :=  v_record_type_count.NEXT(v_string_ptr);

    END LOOP;

    v_string_ptr  :=  v_record_type_count.FIRST;
    v_messages    := 'Records updated '||CHR(10)||CHR(10);

    WHILE v_string_ptr IS NOT NULL LOOP
      v_messages :=
        v_messages || CHR(10)
        || 'Record Type '||v_string_ptr||': '||v_record_type_count(v_string_ptr)||' records';

      v_string_ptr :=  v_record_type_count.NEXT(v_string_ptr);
    END LOOP;

    standard_audit.log_entry
    (
      p_program     => 'caefxt_processing.process_file_import_data',
      p_step_name   => v_location,
      p_log_entry   => v_messages,
      p_level       => 2
    );

    string_mapper.clear_variable_store;

    standard_audit.close_audit_trail
    (
      p_program => 'caefxt_processing.process_file_import_data'
    );

  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      standard_audit.close_audit_trail
      (
        p_program => 'caefxt_processing.process_file_import_data'
      );
      standard_error.when_others_exception
      (
        p_program         => 'caefxt_processing.process_file_import_data',
        p_location        => v_location,
        p_additional_info => 'p_input_data_id: '||p_input_data_id
      );

  END process_file_import_data;

  ----------------------------------------------------------------------------------------------------------------------------------

END caefxt_processing;
/
