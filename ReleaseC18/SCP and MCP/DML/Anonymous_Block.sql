DECLARE 

    v_ica_to_process          VARCHAR2(100) := &ENTER_ICA_NUMBER;
    v_etl_date                DATE := TO_DATE('&ETL_DATE','DD-MM-YYYY');
    v_error_count_account     PLS_INTEGER;
    v_error_count_cardholder  PLS_INTEGER;
    v_error_count_card        PLS_INTEGER;
    v_ptr                     BINARY_INTEGER;
    v_record_count            PLS_INTEGER;
    v_insert_count            PLS_INTEGER;
    v_update_count            PLS_INTEGER;
    v_ch_count                PLS_INTEGER;
    

    CURSOR cur_account IS
    SELECT
      DISTINCT
      --pan,
      accountcd,
      accopendt,
      accounttp,
      --cardholder_id,
      statuschgdt,
      plreason,
      acclangcd,
      authdeclcnt,
      cashauthcnt,
      purauthcnt,
      cashauthamt,
      purauthamt,
      balance,
      cdind,
      availcredit,
      lasttxdt,
      lasttxamt,
      lasttxcd,
      lastauthappdt,
      lastauthreqdt,
      lastauthrefdt,
      mcp_accountcd,
      mcp_rowid,
      accountrs,
      acountst,
      alternative_id1,
      bin,
      icanr,
      ol1,
      ol2,
      ol3,
      procs_cl_id,
      rds_client_id
    FROM
    (
      SELECT
        /*+parallel(8)*/
        v_acof_detail_delta_mcp.pan,
        v_acof_detail_delta_mcp.accountcd,
        TO_DATE(v_acof_detail_delta_mcp.accopendt,'YYYYMMDD')     AS accopendt,
        NVL(v_acof_detail_delta_mcp.accounttp,'MCA')              AS accounttp,
        --acof_xref.cardholder_id,
        --TO_DATE(v_acof_detail_delta_mcp.statuschgdt,'YYYYMMDD')   AS statuschgdt,
        MAX(TO_DATE(v_acof_detail_delta_mcp.statuschgdt,'YYYYMMDD')) OVER ()   AS statuschgdt,
        NVL(v_acof_detail_delta_mcp.plreason,'OPN')               AS plreason,
        LOWER(NVL(v_acof_detail_delta_mcp.langcd,'en'))           AS acclangcd,
        NVL(TO_NUMBER(v_acof_detail_delta_mcp.authdeclcnt),0)     AS authdeclcnt,
        NVL(TO_NUMBER(v_acof_detail_delta_mcp.cashauthcnt),0)     AS cashauthcnt,
        NVL(TO_NUMBER(v_acof_detail_delta_mcp.purauthcnt),0)      AS purauthcnt,
        NVL(TO_NUMBER(v_acof_detail_delta_mcp.cashauthamt),0)/100 AS cashauthamt,
        NVL(TO_NUMBER(v_acof_detail_delta_mcp.purauthamt),0)/100  AS purauthamt,
        NVL(TO_NUMBER(v_acof_detail_delta_mcp.balance),0)/100     AS balance,
        SUBSTR(CDIND,1,1)                                         AS cdind,
        NVL(TO_NUMBER(v_acof_detail_delta_mcp.availcredit),0)/100 AS availcredit,
        TO_DATE(v_acof_detail_delta_mcp.lasttxdt,'YYYYMMDD')      AS lasttxdt,
        NVL(TO_NUMBER(v_acof_detail_delta_mcp.lasttxamt),0)/100   AS lasttxamt,
        TO_CHAR(TO_NUMBER(lasttxcd))                              AS lasttxcd,        -- Was wrong before so maintains backwards compatibility
        TO_DATE(v_acof_detail_delta_mcp.lastauthappdt,'YYYYMMDD') AS lastauthappdt,
        TO_DATE(v_acof_detail_delta_mcp.lastauthreqdt,'YYYYMMDD') AS lastauthreqdt,
        TO_DATE(v_acof_detail_delta_mcp.lastauthrefdt,'YYYYMMDD') AS lastauthrefdt,
        NVL(customer_account.account_nbr,0)                       AS mcp_accountcd,
        customer_account.rowid                                    AS mcp_rowid,
        NVL(v_acof_detail_delta_mcp.accountrs,'OPN')              AS accountrs,     --Card holder reporting fix
        NVL(v_acof_detail_delta_mcp.acountst,'01')                AS acountst,       --Card holder reporting fix
        v_acof_detail_delta_mcp.alternative_id1                   AS alternative_id1,
        v_acof_detail_delta_mcp.bin,
        v_acof_detail_delta_mcp.icanr,
        NVL(v_acof_detail_delta_mcp.ol1,'00000') ol1,
        NVL(v_acof_detail_delta_mcp.ol2,'00000') ol2,
        NVL(v_acof_detail_delta_mcp.ol3,'00000') ol3,
        GLOBAL_STAGING_AREA.fn_processor_client_id(v_acof_detail_delta_mcp.ol1, v_acof_detail_delta_mcp.ol2, v_acof_detail_delta_mcp.ol3,null,null,null) procs_cl_id,
        rds_client_id
      FROM  GLOBAL_STAGING_AREA.v_acof_detail_delta_mcp,
            --acof_xref,
            GFL_MCP_CORE.customer_account,
            CPRADMIN.ra_client
      WHERE 
      /* Modified for the feature F57475
      ((p_input_data_id IS NOT NULL AND v_acof_detail_delta_mcp.input_data_id = p_input_data_id AND STATUS = 'C') OR (p_input_data_id IS NULL AND STATUS = 'C'))*/
        ltrim(icanr,'0') in (v_ica_to_process) AND STATUS = 'C'
      --AND   v_acof_detail_delta_mcp.pan           = acof_xref.pan
      --AND   acof_xref.cardholder_type             ='P'
      AND v_acof_detail_delta_mcp.cardholder_type = 'PRI'
      AND v_acof_detail_delta_mcp.accountcd     = customer_account.account_nbr(+)
      AND GLOBAL_STAGING_AREA.fn_processor_client_id(v_acof_detail_delta_mcp.ol1, v_acof_detail_delta_mcp.ol2, v_acof_detail_delta_mcp.ol3,null,null,null)
          = ra_client.processor_client_id(+)
      AND NVL(ra_client.client_level,'BRANCH') = 'BRANCH'
    ) source
    WHERE mcp_accountcd = 0
    OR    NOT EXISTS
    (
      SELECT 1
      FROM  GFL_MCP_CORE.customer_account
      WHERE customer_account.ROWID                                                        = source.mcp_rowid
      AND   NVL(customer_account.open_date,TO_DATE('19010101','YYYYMMDD'))                = NVL(source.accopendt,TO_DATE('19010101','YYYYMMDD'))
      AND   customer_account.type_code                                                    = source.accounttp
      --AND   customer_account.cardholder_id                                                = source.cardholder_id
      AND   NVL(customer_account.last_status_change_date,TO_DATE('19010101','YYYYMMDD'))  = NVL(source.statuschgdt,TO_DATE('19010101','YYYYMMDD'))
      AND   NVL(customer_account.status_reason_code,' ')                                  = NVL(source.accountrs,' ')
      AND   NVL(customer_account.language_code,' ')                                       = NVL(source.acclangcd,' ')
      AND   NVL(customer_account.auth_decline_count,0)                                    = NVL(source.authdeclcnt,0)
      AND   NVL(customer_account.cash_auth_count,0)                                       = NVL(source.cashauthcnt,0)
      AND   NVL(customer_account.purchase_auth_count,0)                                   = NVL(source.purauthcnt,0)
      AND   NVL(customer_account.cash_auth_amount,0.00)                                   = NVL(source.cashauthamt,0.00)
      AND   NVL(customer_account.purchase_auth_amount,0.00)                               = NVL(source.purauthamt,0.00)
      AND   NVL(customer_account.balance,0.00)                                            = NVL(source.balance,0.00)
      AND   NVL(customer_account.balance_debit_credit,' ')                                = NVL(source.cdind,' ')
      AND   NVL(customer_account.available_credit_line,0.00)                              = NVL(source.availcredit,0.00)
      AND   NVL(customer_account.last_txn_date,TO_DATE('19010101','YYYYMMDD'))            = NVL(source.lasttxdt,TO_DATE('19010101','YYYYMMDD'))
      AND   NVL(customer_account.last_txn_amount,0.00)                                    = NVL(source.lasttxamt,0.00)
      AND   NVL(customer_account.last_txn_code,' ')                                       = NVL(source.lasttxcd,' ')
      AND   NVL(customer_account.last_auth_approve_date,TO_DATE('19010101','YYYYMMDD'))   = NVL(source.lastauthappdt,TO_DATE('19010101','YYYYMMDD'))
      AND   NVL(customer_account.last_auth_request_date,TO_DATE('19010101','YYYYMMDD'))   = NVL(source.lastauthreqdt,TO_DATE('19010101','YYYYMMDD'))
      AND   NVL(customer_account.last_auth_refer_date,TO_DATE('19010101','YYYYMMDD'))     = NVL(source.lastauthrefdt,TO_DATE('19010101','YYYYMMDD'))
      AND   NVL(customer_account.account_status_code,' ')                                 = NVL(source.acountst,' ')
      AND   NVL(customer_account.bin,0)                                                   = NVL(source.bin,0)
      AND   NVL(customer_account.ica,0)                                                   = NVL(source.icanr,0)
      AND   NVL(customer_account.ol1,' ')                                                 = NVL(source.ol1,' ')
      AND   NVL(customer_account.ol2,' ')                                                 = NVL(source.ol2,' ')
      AND   NVL(customer_account.ol3,' ')                                                 = NVL(source.ol3,' ')
      AND   NVL(customer_account.client_id,0)                                             = NVL(source.rds_client_id,0)
    );
    
    
        TYPE typ_account IS TABLE OF cur_account%ROWTYPE
    INDEX BY BINARY_INTEGER;

    buf_account typ_account;
    
    CURSOR cur_cardholder IS
    SELECT DISTINCT
      cardholder_id,
      nameline1,
      birthdate,
      pan,
      addr0,
      addr1,
      addr2,
      addr3,
      addr4,
      cc,
      zip,
      langcd,
      homefone,
      busyfone,
      cellfone,
      empid,
      emailaddress,
      taxid,
      mcp_chid,
      mcp_rowid,
      custnumber, --added as part of Qantas Reporting Fix
      accountcd,
      cardholder_type
    FROM
    (
      SELECT
        /*+parallel(8)*/
        --acof_xref.cardholder_id,
        cardholder.id   AS cardholder_id,
        v_acof_detail_delta_mcp.nameline1,
        TO_DATE(NVL(v_acof_detail_delta_mcp.birthdate,'19000101'),'YYYYMMDD') birthdate,
        v_acof_detail_delta_mcp.pan,
        v_acof_detail_delta_mcp.addr0,
        v_acof_detail_delta_mcp.addr1,
        v_acof_detail_delta_mcp.addr2,
        v_acof_detail_delta_mcp.addr3,
        v_acof_detail_delta_mcp.addr4,
        v_acof_detail_delta_mcp.cc,
        v_acof_detail_delta_mcp.zip,
        LOWER(NVL(v_acof_detail_delta_mcp.langcd,'en')) AS langcd,
        v_acof_detail_delta_mcp.homefone,
        v_acof_detail_delta_mcp.busyfone,
        v_acof_detail_delta_mcp.cellfone,
        v_acof_detail_delta_mcp.empid,
        v_acof_detail_delta_mcp.emailaddress,
        v_acof_detail_delta_mcp.taxid,
        NVL(cardholder.id,0)                            AS mcp_chid,
        cardholder.rowid                                AS mcp_rowid,
        v_acof_detail_delta_mcp.custnumber,
        v_acof_detail_delta_mcp.accountcd,
        DECODE (v_acof_detail_delta_mcp.cardholder_type, 'PRI', 'P', 'SEC', 'S', 'ADI', 'A',NULL) AS cardholder_type
      FROM
        GLOBAL_STAGING_AREA.v_acof_detail_delta_mcp,
        --acof_xref,
        GFL_MCP_CORE.cardholder
      WHERE 
      /*Modified for feature F57475
      ((p_input_data_id IS NOT NULL AND v_acof_detail_delta_mcp.input_data_id = p_input_data_id AND STATUS = 'C') OR (p_input_data_id IS NULL AND STATUS = 'C'))*/
     ltrim(icanr,'0') in (v_ica_to_process) AND STATUS = 'C'
     --AND   v_acof_detail_delta_mcp.pan = acof_xref.pan
      --AND   acof_xref.cardholder_type IN ('P','A')
      --AND   acof_xref.cardholder_id = cardholder.id(+)
      AND   v_acof_detail_delta_mcp.custnumber = cardholder.ips_cardholder_id(+)

    ) source
    WHERE source.mcp_chid = 0
    OR NOT EXISTS
    (
      SELECT  1
      FROM    GFL_MCP_CORE.cardholder
      WHERE   cardholder.ROWID                                          = source.mcp_rowid
      AND     NVL(cardholder.name,' ')                                  = NVL(source.nameline1,' ')
      AND     NVL(cardholder.birth_date,TO_DATE('19010101','YYYYMMDD')) = NVL(source.birthdate,TO_DATE('19010101','YYYYMMDD'))
      AND     NVL(cardholder.address_line_1,' ')                        = NVL(source.addr0,' ')
      AND     NVL(cardholder.address_line_2,' ')                        = NVL(source.addr1,' ')
      AND     NVL(cardholder.address_line_3,' ')                        = NVL(source.addr2,' ')
      AND     NVL(cardholder.city,' ')                                  = NVL(source.addr3,' ')
      AND     NVL(cardholder.sub_region_code,' ')                       = NVL(source.addr4,' ')
      AND     NVL(cardholder.country_code,' ')                          = NVL(source.cc,' ')
      AND     NVL(cardholder.zip_post_code,' ')                         = NVL(source.zip,' ')
      AND     NVL(cardholder.language_code,' ')                         = NVL(source.langcd,' ')
      AND     NVL(cardholder.home_phone_nbr,' ')                        = NVL(source.homefone,' ')
      AND     NVL(cardholder.business_phone_nbr,' ')                    = NVL(source.busyfone,' ')
      AND     NVL(cardholder.mobile_phone_nbr,' ')                      = NVL(source.cellfone,' ')
      AND     NVL(cardholder.employee_flag,' ')                         = NVL(source.empid,' ')
      AND     NVL(cardholder.email_address,' ')                         = NVL(source.emailaddress,' ')
      AND     NVL(cardholder.tax_id_nbr,' ')                            = NVL(source.taxid,' ')
      AND     NVL(cardholder.ips_cardholder_id,' ')                     = NVL(source.custnumber,' ')
      AND     NVL(cardholder.account_nbr,0)                             = NVL(source.accountcd,0)
    );

    TYPE typ_cardholder IS TABLE OF cur_cardholder%ROWTYPE
    INDEX BY BINARY_INTEGER;

    buf_cardholder  typ_cardholder;
    
    CURSOR cur_card IS
    SELECT  pan,
            expdt,
            logexpdt,
            prevexpdt,
            accountcd,
            bin,
            cardholder_id,
            --pltype,
            plreason,  ----for APWAS-12179
            statuschgdt,
            plcompind,
            plcompcd,
            clcompenddt,
            add_date,
            accopendt,
            client_id,
            mcp_card,
            mcp_rowid,
            ips_proxy_id,--Added for CMP0925
            activation_date,-- CMP1278 -Card Activation report
            prev_activation_date,-- CMP1278 -Card Activation report
            card_status_code
    FROM
    (
      SELECT  /*+parallel(8)*/
              v_acof_detail_delta_mcp.pan,
              TO_DATE(v_acof_detail_delta_mcp.expdt,'YYYYMMDD')                               AS expdt,
              TO_DATE(v_acof_detail_delta_mcp.logexpdt,'YYYYMMDD')                            AS logexpdt,
              TO_DATE(v_acof_detail_delta_mcp.prevexpdt,'YYYYMMDD')                           AS prevexpdt,
              v_acof_detail_delta_mcp.accountcd,
              v_acof_detail_delta_mcp.bin,
              ch.id                                                                           AS cardholder_id,
              --acof_xref.bin,
              --acof_xref.cardholder_id,
              --decode(acof_xref.cardholder_type,'X','P',cardholder_type)                       AS pltype,
              NVL(v_acof_detail_delta_mcp.plreason ,'OPN')                                    AS plreason,    ----for APWAS-12179
              TO_DATE(v_acof_detail_delta_mcp.statuschgdt,'YYYYMMDD')                         AS statuschgdt,
              v_acof_detail_delta_mcp.plcompind,
              v_acof_detail_delta_mcp.plcompcd,
              TO_DATE(v_acof_detail_delta_mcp.clcompenddt,'YYYYMMDD')                         AS clcompenddt,
              TO_DATE(v_acof_detail_delta_mcp.accopendt,'YYYYMMDD')                           AS add_date,
              TO_DATE(v_acof_detail_delta_mcp.accopendt,'YYYYMMDD')                           AS accopendt,
              ra_client.rds_client_id                                                         AS client_id,
              NVL(card.card_nbr,0)                                                            AS mcp_card,
              card.rowid                                                                      AS mcp_rowid,
              v_acof_detail_delta_mcp.ipsproxyid                                              AS ips_proxy_id,    --Added for CMP0925
              TO_DATE(v_acof_detail_delta_mcp.activation_date,'YYYYMMDD')                     AS activation_date,-- CMP1278 -Card Activation report
              (CASE WHEN v_acof_detail_delta_mcp.prev_activation_date='0 0 0' then null
                    WHEN LENGTH(LTRIM(RTRIM(v_acof_detail_delta_mcp.prev_activation_date)))<8 THEN NULL
              ELSE TO_DATE(v_acof_detail_delta_mcp.prev_activation_date,'YYYYMMDD')
              END) AS prev_activation_date, -- CMP1278 -Card Activation report
              v_acof_detail_delta_mcp.plstatus card_status_code
      FROM    GLOBAL_STAGING_AREA.v_acof_detail_delta_mcp,
              GFL_MCP_CORE.cardholder ch,
              --acof_xref,
              GFL_MCP_CORE.card,
              CPRADMIN.ra_client
      WHERE   
      /* Modified for F57475
      ((p_input_data_id IS NOT NULL AND v_acof_detail_delta_mcp.input_data_id = p_input_data_id AND STATUS = 'C') OR (p_input_data_id IS NULL AND STATUS = 'C'))*/
      ltrim(icanr,'0') in (v_ica_to_process) AND STATUS = 'C'
      AND     v_acof_detail_delta_mcp.custnumber = ch.ips_cardholder_id
      --AND     v_acof_detail_delta_mcp.pan = acof_xref.pan
      AND     v_acof_detail_delta_mcp.pan = card.card_nbr(+)
      AND     fn_processor_client_id(v_acof_detail_delta_mcp.OL1, v_acof_detail_delta_mcp.OL2, v_acof_detail_delta_mcp.OL3,null,null,null)
 = ra_client.processor_client_id(+)
      AND     NVL(ra_client.client_level,'BRANCH') = 'BRANCH'
    ) source
    WHERE mcp_card = 0
    OR NOT EXISTS
    (
      SELECT  1
      FROM    card
      WHERE   card.rowid                                                        = source.mcp_rowid
      AND     card.expiry_date                                                  = source.expdt
      AND     NVL(card.logical_expiry_date,TO_DATE('19010101','YYYYMMDD'))      = NVL(source.logexpdt,TO_DATE('19010101','YYYYMMDD'))
      AND     NVL(card.previous_expiry_date,TO_DATE('19010101','YYYYMMDD'))     = NVL(source.prevexpdt,TO_DATE('19010101','YYYYMMDD'))
      AND     NVL(card.account_nbr,0)                                           = NVL(source.accountcd,0)
      AND     NVL(card.cardholder_id,0)                                         = NVL(source.cardholder_id,0)
      --AND     NVL(card.type_flag,' ')                                           = NVL(source.pltype,' ')
      AND     card.status_reason_code                                           = source.plreason              -- APWAS-12179
      AND     NVL(card.last_status_change_date,TO_DATE('19010101','YYYYMMDD'))  = NVL(source.statuschgdt,TO_DATE('19010101','YYYYMMDD'))
      AND     NVL(card.compromised_severity,' ')                                = NVL(source.plcompind,' ')
      AND     NVL(card.compromised_purge_date,TO_DATE('19010101','YYYYMMDD'))   = NVL(source.clcompenddt,TO_DATE('19010101','YYYYMMDD'))
      AND     NVL(card.add_date,TO_DATE('19010101','YYYYMMDD'))                 = NVL(source.add_date,TO_DATE('19010101','YYYYMMDD'))
      AND     NVL(card.first_txn_date,TO_DATE('19010101','YYYYMMDD'))           = NVL(source.accopendt,TO_DATE('19010101','YYYYMMDD'))
      AND     NVL(card.client_id,0)                                             = NVL(source.client_id,0)
      AND     NVL(card.source_bin,0)                                            = NVL(source.bin,0)
      AND     NVL(card.ips_proxy_id,0)                                          = NVL(source.ips_proxy_id,0)    --Added for CMP0925
      AND     NVL(card.activation_date,TO_DATE('19010101','YYYYMMDD'))          = NVL(source.activation_date,TO_DATE('19010101','YYYYMMDD'))-- CMP1278 -Card Activation report
      AND     NVL(card.previous_activation_date,TO_DATE('19010101','YYYYMMDD')) = NVL(source.prev_activation_date,TO_DATE('19010101','YYYYMMDD'))-- CMP1278 -Card Activation report
      AND     NVL(card.card_status_code,'X')                                    = NVL(source.card_status_code,'X')
    );

    TYPE typ_card IS TABLE OF cur_card%ROWTYPE
    INDEX BY BINARY_INTEGER;

    buf_card  typ_card;
    
   --Opening and  Closing balance--
    CURSOR OPN_CAL IS 
    SELECT 
     AGG_BIN.CURRENCY  PURSE_CURRENCY,
     RA_BIN.ICA_NUMBER ICA,
     RA_BIN.BIN,
     DECODE(D_SCHEME_TRANS_ID,'88','0001','89','0004') TRANSACTION_DESCRIPTION_CODE,
     TRANSACTION_VAL   TRANSACTION_AMOUNT,
     'RECON_REP'   FLOAT_TYPE,
     RA_BIN.BIN_SPONSOR_ID,
     SYSDATE LAST_UPDATED
   FROM
  (
   SELECT B.BIN,
     A.CURRENCY,
     SUM(A.TRANSACTION_VALUE) TRANSACTION_VAL,
     D_SCHEME_TRANS_ID 
     FROM CPRADMIN.FD_IPS_POOL_BALANCE A,
    (SELECT BIN,PORTFOLIO_CODE  FROM CPRADMIN.RA_BIN
        UNION 
     SELECT BIN,PORTFOLIO_CODE FROM CPRADMIN.RA_BIN_CURRENCY) B
     WHERE A.PORTFOLIO_CODE = B.PORTFOLIO_CODE
     AND REPORT_DATE = V_ETL_DATE
     GROUP BY B.BIN,A.CURRENCY,D_SCHEME_TRANS_ID
      )   AGG_BIN,
       CPRADMIN.RA_BIN
     WHERE AGG_BIN.BIN = RA_BIN.BIN
     AND RA_BIN.ICA_NUMBER = v_ica_to_process;

    --Negative balance--
    CURSOR NEG_BAL IS 
    SELECT 
    PURSE_CCY,
    D_ETL_DT_ID,
    D_BIN_ID,
    D_TRANSACTION_TYPE,
    SUM(AVAILABLE_BALANCE) AVAILABLE_BAL,
    ETL_DATE,
    BUSINESS_DATE
    FROM
    (
    SELECT 
    CURRENCY_CODE PURSE_CCY,
    (SELECT TIME_DIMENSION_KEY FROM CPRADMIN.D_TIME WHERE DATE_NAME = V_ETL_DATE) D_ETL_DT_ID, 
    BIN D_BIN_ID ,
    '675' D_TRANSACTION_TYPE,
    AVAILABLE_BALANCE,
    V_ETL_DATE ETL_DATE,
    BUSINESS_DATE
    FROM CPRADMIN.IF_TJF_RT08_BALANCES
    WHERE BIN IN (SELECT BIN FROM CPRADMIN.RA_BIN WHERE ICA_NUMBER = v_ica_to_process)
    AND AVAILABLE_BALANCE < 0
    )
    GROUP BY PURSE_CCY,
    D_ETL_DT_ID,
    D_BIN_ID,
    D_TRANSACTION_TYPE,
    ETL_DATE,
    BUSINESS_DATE;

    
    PROCEDURE insert_error
  (
    p_check_number  IN NUMBER,
    p_sequence      IN NUMBER,
    p_message       IN VARCHAR2
  ) IS

    PRAGMA AUTONOMOUS_TRANSACTION;

  BEGIN
    INSERT INTO GLOBAL_STAGING_AREA.transaction_errors
    (
      checknum,
      error_type,
      txn_sequence,
      ErrorMessage,
      datestamp
    )
    VALUES
    (
      p_check_number,
      'ACOF',
      p_sequence,
      p_message,
      v_etl_date
    );

    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      standard_error.when_others_exception
      (
        p_program         => 'card_extraction.insert_error',
        p_additional_info => 'p_check_number: '||p_check_number||' p_sequence: '||p_sequence||' p_message: '||p_message
      );

  END insert_error;
    
BEGIN
    

/************START CARDHOLDER*********************/

   BEGIN

      v_record_count        := 0;
      v_insert_count        := 0;
      v_update_count        := 0;
      v_error_count_account := 0;

    OPEN cur_account;
    LOOP

      FETCH cur_account
      BULK COLLECT
      INTO  buf_account
      LIMIT 1000;

      IF buf_account.COUNT=0 THEN
        EXIT;
      END IF;

      v_ptr := buf_account.FIRST;

      WHILE v_ptr IS NOT NULL LOOP

        v_record_count := v_record_count + 1;

        IF buf_account(v_ptr).mcp_accountcd > 0 THEN

          BEGIN

            UPDATE GFL_MCP_CORE.customer_account acc
            SET
              acc.open_date               = buf_account(v_ptr).accopendt,
              acc.type_code               = buf_account(v_ptr).accounttp,
              --acc.cardholder_id           = buf_account(v_ptr).cardholder_id,
              acc.last_status_change_date = buf_account(v_ptr).statuschgdt,
              acc.status_reason_code      = buf_account(v_ptr).accountrs,
              acc.language_code           = buf_account(v_ptr).acclangcd,
              acc.auth_decline_count      = buf_account(v_ptr).authdeclcnt,
              acc.cash_auth_count         = buf_account(v_ptr).cashauthcnt,
              acc.purchase_auth_count     = buf_account(v_ptr).purauthcnt,
              acc.cash_auth_amount        = buf_account(v_ptr).cashauthamt,
              acc.purchase_auth_amount    = buf_account(v_ptr).purauthamt,
              acc.balance                 = buf_account(v_ptr).balance,
              acc.balance_debit_credit    = buf_account(v_ptr).cdind,
              acc.available_credit_line   = buf_account(v_ptr).availcredit,
              acc.last_txn_date           = buf_account(v_ptr).lasttxdt,
              acc.last_txn_amount         = buf_account(v_ptr).lasttxamt,
              acc.last_txn_code           = buf_account(v_ptr).lasttxcd,
              acc.last_auth_approve_date  = buf_account(v_ptr).lastauthappdt,
              acc.last_auth_request_date  = buf_account(v_ptr).lastauthreqdt,
              acc.last_auth_refer_date    = buf_account(v_ptr).lastauthrefdt,
              acc.account_status_code     = buf_account(v_ptr).acountst,
              acc.last_updated            = sysdate,
              acc.last_updated_by         = user,
              acc.alternative_id1         = buf_account(v_ptr).alternative_id1,
              acc.bin                     = buf_account(v_ptr).bin,
              acc.ica                     = buf_account(v_ptr).icanr,
              acc.ol1                     = buf_account(v_ptr).ol1,
              acc.ol2                     = buf_account(v_ptr).ol2,
              acc.ol3                     = buf_account(v_ptr).ol3,
              acc.processor_client_id     = buf_account(v_ptr).procs_cl_id,
              acc.client_id               = buf_account(v_ptr).rds_client_id
              WHERE acc.rowid = buf_account(v_ptr).mcp_rowid;

            v_update_count := v_update_count + 1;

          EXCEPTION
            WHEN OTHERS THEN
              v_error_count_account := v_error_count_account + 1;
              insert_error(0,buf_account(v_ptr).accountcd,SQLERRM);
                standard_error.log_validation_error
              (
                p_program          => 'card_extraction.merge_account',
                p_location         => 'merge account update error',
                p_message          => SQLERRM,
                p_additional_info  => 'account: '||buf_account(v_ptr).accountcd
              );
          END;
        ELSE
          BEGIN
            INSERT INTO GFL_MCP_CORE.customer_account
            (
              account_nbr,
              open_date,
              type_code,
              --cardholder_id,
              last_status_change_date,
              status_reason_code,
              language_code,
              auth_decline_count,
              cash_auth_count,
              purchase_auth_count,
              cash_auth_amount,
              purchase_auth_amount,
              balance,
              balance_debit_credit,
              available_credit_line,
              last_txn_date,
              last_txn_amount,
              last_txn_code,
              last_auth_approve_date,
              last_auth_request_date,
              last_auth_refer_date,
              account_status_code,
              alternative_id1,
              bin,
              ica,
              ol1,
              ol2,
              ol3,
              processor_client_id,
              client_id
            )
            VALUES
            (
              buf_account(v_ptr).accountcd,
              buf_account(v_ptr).accopendt,
              buf_account(v_ptr).accounttp,
              --buf_account(v_ptr).cardholder_id,
              buf_account(v_ptr).statuschgdt,
              buf_account(v_ptr).accountrs,
              buf_account(v_ptr).acclangcd,
              buf_account(v_ptr).authdeclcnt,
              buf_account(v_ptr).cashauthcnt,
              buf_account(v_ptr).purauthcnt,
              buf_account(v_ptr).cashauthamt,
              buf_account(v_ptr).purauthamt,
              buf_account(v_ptr).balance,
              buf_account(v_ptr).cdind,
              buf_account(v_ptr).availcredit,
              buf_account(v_ptr).lasttxdt,
              buf_account(v_ptr).lasttxamt,
              buf_account(v_ptr).lasttxcd,
              buf_account(v_ptr).lastauthappdt,
              buf_account(v_ptr).lastauthreqdt,
              buf_account(v_ptr).lastauthrefdt,
              buf_account(v_ptr).acountst,
              buf_account(v_ptr).alternative_id1,
              TO_NUMBER(buf_account(v_ptr).bin),
              TO_NUMBER(buf_account(v_ptr).icanr),
              buf_account(v_ptr).ol1,
              buf_account(v_ptr).ol2,
              buf_account(v_ptr).ol3,
              buf_account(v_ptr).procs_cl_id,
              buf_account(v_ptr).rds_client_id
            );

            v_insert_count := v_insert_count + 1;

          EXCEPTION
            WHEN OTHERS THEN
              v_error_count_account := v_error_count_account + 1;
              insert_error(0,buf_account(v_ptr).accountcd,SQLERRM);
             standard_audit.log_entry
              (
                p_program     => 'card_extraction.merge_account',
                p_step_name   => 'merge account insert error',
                p_log_entry   => 'account : '||buf_account(v_ptr).accountcd||' Error: '||SQLERRM,
                p_level       => 9
              );
              standard_error.log_validation_error
              (
                p_program          => 'card_extraction.merge_account',
                p_location         => 'merge account insert error',
                p_message          => SQLERRM,
                p_additional_info  => 'account : '||buf_account(v_ptr).accountcd
              );
          END;
        END IF;

        v_ptr := buf_account.NEXT(v_ptr);
      END LOOP;
      COMMIT;
    END LOOP;
    CLOSE cur_account;

   end;
   
  
   BEGIN

    v_record_count            := 0;
    v_insert_count            := 0;
    v_update_count            := 0;
    v_error_count_cardholder  := 0;

    OPEN cur_cardholder;

    LOOP
      FETCH cur_cardholder
      BULK COLLECT
      INTO  buf_cardholder
      LIMIT 1000;

      IF buf_cardholder.COUNT = 0 THEN
        EXIT;
      END IF;

      v_ptr := buf_cardholder.FIRST;
      WHILE v_ptr IS NOT NULL LOOP

        v_record_count := v_record_count + 1;

        IF buf_cardholder(v_ptr).mcp_chid > 0 THEN

          BEGIN

            UPDATE GFL_MCP_CORE.cardholder
            SET name                = buf_cardholder(v_ptr).nameline1,
                birth_date          = buf_cardholder(v_ptr).birthdate,
                address_line_1      = buf_cardholder(v_ptr).addr0,
                address_line_2      = buf_cardholder(v_ptr).addr1,
                address_line_3      = buf_cardholder(v_ptr).addr2,
                city                = buf_cardholder(v_ptr).addr3,
                sub_region_code     = buf_cardholder(v_ptr).addr4,
                country_code        = buf_cardholder(v_ptr).cc,
                zip_post_code       = buf_cardholder(v_ptr).zip,
                language_code       = buf_cardholder(v_ptr).langcd,
                home_phone_nbr      = buf_cardholder(v_ptr).homefone,
                business_phone_nbr  = buf_cardholder(v_ptr).busyfone,
                mobile_phone_nbr    = buf_cardholder(v_ptr).cellfone,
                employee_flag       = buf_cardholder(v_ptr).empid,
                email_address       = buf_cardholder(v_ptr).emailaddress,
                tax_id_nbr          = buf_cardholder(v_ptr).taxid,
                account_nbr         = buf_cardholder(v_ptr).accountcd,
                cardholder_type     = buf_cardholder(v_ptr).cardholder_type,
                last_updated        = sysdate,
                last_updated_by     = user
            WHERE ROWID = buf_cardholder(v_ptr).mcp_rowid;

            v_update_count := v_update_count + 1;

          EXCEPTION
            WHEN OTHERS THEN
              v_error_count_cardholder := v_error_count_cardholder + 1;
              insert_error(0,buf_cardholder(v_ptr).pan,SQLERRM);
              standard_audit.log_entry
              (
                p_program     => 'card_extraction.merge_cardholder',
                p_step_name   => 'merge cardholder update error',
                p_log_entry   => 'Pan: '||buf_cardholder(v_ptr).pan||' Error: '||SQLERRM,
                p_level       => 9
              );
              standard_error.log_validation_error
              (
                p_program          => 'card_extraction.merge_cardholder',
                p_location         => 'merge cardholder update error',
                p_message          => SQLERRM,
                p_additional_info  => 'Pan: '||buf_cardholder(v_ptr).pan
              );
          END;

        ELSE

          BEGIN

            -- in order to ensure no duplicate records are inserted for a cardholder if the cardholder is issued
            -- primary and additional card on the same day
            SELECT COUNT(*) INTO v_ch_count
            FROM cardholder
            WHERE ips_cardholder_id = buf_cardholder(v_ptr).custnumber;

             IF v_ch_count = 0 THEN

                INSERT INTO GFL_MCP_CORE.cardholder
                (
                  id,
                  name,
                  birth_date,
                  address_line_1,
                  address_line_2,
                  address_line_3,
                  city,
                  sub_region_code,
                  country_code,
                  zip_post_code,
                  language_code,
                  home_phone_nbr,
                  business_phone_nbr,
                  mobile_phone_nbr,
                  employee_flag,
                  tax_id_nbr,
                  email_address,
                  ips_cardholder_id, --added as part of Qantas Reporting Fix
                  account_nbr,
                  cardholder_type
                )
                VALUES
                (
                  GLOBAL_STAGING_AREA.acof_xref_seq.NEXTVAL,    --buf_cardholder(v_ptr).cardholder_id,
                  buf_cardholder(v_ptr).nameline1,
                  buf_cardholder(v_ptr).birthdate,
                  buf_cardholder(v_ptr).addr0,
                  buf_cardholder(v_ptr).addr1,
                  buf_cardholder(v_ptr).addr2,
                  buf_cardholder(v_ptr).addr3,
                  buf_cardholder(v_ptr).addr4,
                  buf_cardholder(v_ptr).cc,
                  buf_cardholder(v_ptr).zip,
                  buf_cardholder(v_ptr).langcd,
                  buf_cardholder(v_ptr).homefone,
                  buf_cardholder(v_ptr).busyfone,
                  buf_cardholder(v_ptr).cellfone,
                  buf_cardholder(v_ptr).empid,
                  buf_cardholder(v_ptr).taxid,
                  buf_cardholder(v_ptr).emailaddress,
                  buf_cardholder(v_ptr).custnumber,
                  buf_cardholder(v_ptr).accountcd,
                  buf_cardholder(v_ptr).cardholder_type
                );

                v_insert_count := v_insert_count + 1;
             END IF;

          EXCEPTION
            WHEN OTHERS THEN
              v_error_count_cardholder := v_error_count_cardholder + 1;
              insert_error(0,buf_cardholder(v_ptr).pan,SQLERRM);
              standard_audit.log_entry
              (
                p_program     => 'card_extraction.merge_cardholder',
                p_step_name   => 'merge cardholder insert error',
                p_log_entry   => 'Pan: '||buf_cardholder(v_ptr).pan||' Error: '||SQLERRM,
                p_level       => 9
              );
              standard_error.log_validation_error
              (
                p_program          => 'card_extraction.merge_cardholder',
                p_location         => 'merge cardholder insert error',
                p_message          => SQLERRM,
                p_additional_info  => 'Pan: '||buf_cardholder(v_ptr).pan
              );
          END;
        END IF;

        v_ptr := buf_cardholder.NEXT(v_ptr);
      END LOOP;
      COMMIT;
    END LOOP;
    CLOSE cur_cardholder;

end;

   
   BEGIN

    v_record_count      := 0;
    v_insert_count      := 0;
    v_update_count      := 0;
    v_error_count_card  := 0;

    OPEN cur_card;

    LOOP
      FETCH cur_card
      BULK COLLECT
      INTO  buf_card
      LIMIT 1000;

      IF buf_card.COUNT = 0 THEN
        EXIT;
      END IF;

      v_ptr := buf_card.FIRST;
      WHILE v_ptr IS NOT NULL LOOP

        v_record_count := v_record_count + 1;

        IF buf_card(v_ptr).mcp_card > 0 THEN
          BEGIN
            UPDATE GFL_MCP_CORE.card
            SET expiry_date              = buf_card(v_ptr).expdt,
                logical_expiry_date      = buf_card(v_ptr).logexpdt,
                previous_expiry_date     = buf_card(v_ptr).prevexpdt,
                account_nbr              = buf_card(v_ptr).accountcd,
                cardholder_id            = buf_card(v_ptr).cardholder_id,
                --type_flag                = buf_card(v_ptr).pltype,
                status_reason_code       = buf_card(v_ptr).plreason,                   ---- for APWAS-12179
                last_status_change_date  = buf_card(v_ptr).statuschgdt,
                compromised_severity     = buf_card(v_ptr).plcompind,
                compromised_purge_date   = buf_card(v_ptr).clcompenddt,
                add_date                 = buf_card(v_ptr).add_date,
                first_txn_date           = buf_card(v_ptr).accopendt,
                client_id                = buf_card(v_ptr).client_id,
                source_bin               = buf_card(v_ptr).bin,
                ips_proxy_id             = buf_card(v_ptr).ips_proxy_id,    --Added for CMP0925
                activation_date          = buf_card(v_ptr).activation_date,-- CMP1278 -Card Activation report
                previous_activation_date = buf_card(v_ptr).prev_activation_date,-- CMP1278 -Card Activation report
                card_status_code         = buf_card(v_ptr).card_status_code,
                last_updated             = sysdate,
                last_updated_by          = user
            WHERE
              rowid = buf_card(v_ptr).mcp_rowid;

            v_update_count := v_update_count + 1;

          EXCEPTION
            WHEN OTHERS THEN
              v_error_count_card := v_error_count_card + 1;
              insert_error(0,buf_card(v_ptr).pan,SQLERRM);
              standard_audit.log_entry
              (
                p_program     => 'card_extraction.merge_card',
                p_step_name   => 'merge card update error',
                p_log_entry   => 'Pan: '||buf_card(v_ptr).pan||' Error: '||SQLERRM,
                p_level       => 9
              );
              standard_error.log_validation_error
              (
                p_program          => 'card_extraction.merge_card',
                p_location         => 'merge card update error',
                p_message          => SQLERRM,
                p_additional_info  => 'Pan: '||buf_card(v_ptr).pan
              );
          END;
        ELSE
          BEGIN
            INSERT INTO GFL_MCP_CORE.card
            (
              card_nbr,
              expiry_date,
              logical_expiry_date,
              previous_expiry_date,
              account_nbr,
              cardholder_id,
              --type_flag,
              status_reason_code,
              last_status_change_date,
              compromised_severity,
              compromised_purge_date,
              add_date,
              first_txn_date,
              client_id,
              source_bin,
              ips_proxy_id,    --Added for CMP0925
              activation_date,-- CMP1278 -Card Activation report
              previous_activation_date,-- CMP1278 -Card Activation report
              card_status_code
            )
            VALUES
            (
              buf_card(v_ptr).pan,
              buf_card(v_ptr).expdt,
              buf_card(v_ptr).logexpdt,
              buf_card(v_ptr).prevexpdt,
              buf_card(v_ptr).accountcd,
              buf_card(v_ptr).cardholder_id,
              --buf_card(v_ptr).pltype,
              buf_card(v_ptr).plreason,      ----Addded for APWAS-12179
              buf_card(v_ptr).statuschgdt,
              buf_card(v_ptr).plcompcd,
              buf_card(v_ptr).clcompenddt,
              buf_card(v_ptr).add_date,
              buf_card(v_ptr).accopendt,
              buf_card(v_ptr).client_id,
              buf_card(v_ptr).bin,
              buf_card(v_ptr).ips_proxy_id,    --Added for CMP0925
              buf_card(v_ptr).activation_date,-- CMP1278 -Card Activation report
              buf_card(v_ptr).prev_activation_date,-- CMP1278 -Card Activation report
              buf_card(v_ptr).card_status_code
            );

            v_insert_count := v_insert_count + 1;

          EXCEPTION
            WHEN OTHERS THEN
              v_error_count_card := v_error_count_card + 1;
              insert_error(0,buf_card(v_ptr).pan,SQLERRM);
              standard_audit.log_entry
              (
                p_program     => 'card_extraction.merge_card',
                p_step_name   => 'merge card insert error',
                p_log_entry   => 'Pan: '||buf_card(v_ptr).pan||' Error: '||SQLERRM,
                p_level       => 9
              );
              standard_error.log_validation_error
              (
                p_program          => 'card_extraction.merge_card',
                p_location         => 'merge card insert error',
                p_message          => SQLERRM,
                p_additional_info  => 'Pan: '||buf_card(v_ptr).pan
              );
          END;
        END IF;

        v_ptr := buf_card.NEXT(v_ptr);
      END LOOP;

      COMMIT;

    END LOOP;

    CLOSE cur_card;
END;


/************END CARDHOLDER*********************/

/************START open and close balance*********************/
   
   BEGIN
   FOR I IN OPN_CAL LOOP
   begin
   ---POPULATE THE OPENING AND CALCULATED CLOSING BALANCE  
     
    INSERT INTO gfl_mcp_data.self_issue_float_details
        (
        purse_currency,
        ica,
        bin,
        business_date,
        transaction_description_code,
        transaction_amount,
        float_type,
        bin_sponsor_id,
        etl_date,
        last_updated
        )
    VALUES
        (
        i.purse_currency,
        i.ica,
        i.bin,
        v_etl_date,        
        i.transaction_description_code,
        i.transaction_amount,
        i.float_type,
        i.bin_sponsor_id,
        v_etl_date,
        i.last_updated
        );
    COMMIT;
    EXCEPTION
            WHEN OTHERS THEN
             standard_audit.log_entry
              (
                p_program     => 'SELF_ISSUE_FLOAT_DETAILS',
                p_step_name   => 'SELF_ISSUE_FLOAT_DETAILS',
                p_log_entry   =>  v_ica_to_process||' Error: '||SQLERRM,
                p_level       => 9
              );
              standard_error.log_validation_error
              (
                p_program          => 'SELF_ISSUE_FLOAT_DETAILS',
                p_location         => 'SELF_ISSUE_FLOAT_DETAILS',
                p_message          => SQLERRM,
                p_additional_info  => 'ICA : '||v_ica_to_process
              );
              END;
              
    
     END LOOP;

    --POPULATE THE CLOSING BALANCE
    INSERT INTO GFL_MCP_DATA.SELF_ISSUE_FLOAT_DETAILS
    (
    PURSE_CURRENCY,
    ICA,
    BIN,
    BUSINESS_DATE,
    TRANSACTION_DESCRIPTION_CODE,
    TRANSACTION_AMOUNT,
    FLOAT_TYPE,
    BIN_SPONSOR_ID,
    ETL_DATE,
    LAST_UPDATED
    )
    SELECT
    PURSE_CURRENCY,
    ICA,
    BIN,
    V_ETL_DATE,
    '0002',
    TRANSACTION_AMOUNT,
    FLOAT_TYPE,
    BIN_SPONSOR_ID,
    V_ETL_DATE,
    SYSDATE LAST_UPDATED
    FROM GFL_MCP_DATA.SELF_ISSUE_FLOAT_DETAILS
    WHERE ICA = v_ica_to_process
    AND   BUSINESS_DATE = V_ETL_DATE
    AND   TRANSACTION_DESCRIPTION_CODE = '0004';
COMMIT;

    EXCEPTION
            WHEN OTHERS THEN
             standard_audit.log_entry
              (
                p_program     => 'SELF_ISSUE_FLOAT_DETAILS_END',
                p_step_name   => 'SELF_ISSUE_FLOAT_DETAILS_END',
                p_log_entry   => v_ica_to_process||' Error: '||SQLERRM,
                p_level       => 9
              );
              standard_error.log_validation_error
              (
                p_program          => 'SELF_ISSUE_FLOAT_DETAILS_END',
                p_location         => 'SELF_ISSUE_FLOAT_DETAILS_END',
                p_message          => SQLERRM,
                p_additional_info  => 'ICA : '||v_ica_to_process
              );
end;

/************END open and close balance*********************/


/************START Negative balancee*********************/

   BEGIN
        FOR I IN NEG_BAL LOOP
        BEGIN
        INSERT INTO MCP_ISSUER_REP.FS_MCP_EOD_NEG_BAL
        (
        PURSE_CCY,
        D_ETL_DT_ID,
        D_BIN_ID,
        D_TRANSACTION_TYPE,
        AVAILABLE_BALANCE,
        ETL_DATE,
        BUSINESS_DATE
        )
        VALUES
        (
        I.PURSE_CCY,
        I.D_ETL_DT_ID,
        I.D_BIN_ID,
        I.D_TRANSACTION_TYPE,
        I.AVAILABLE_BAL,
        I.ETL_DATE,
        I.BUSINESS_DATE
        );
        commit;
        EXCEPTION
                    WHEN OTHERS THEN
                     standard_audit.log_entry
                      (
                        p_program     => 'Negative balance',
                        p_step_name   => 'Negative balance',
                        p_log_entry   => 'Bin : '||I.D_BIN_ID||' Error: '||SQLERRM,
                        p_level       => 9
                      );
                      standard_error.log_validation_error
                      (
                        p_program          => 'Negative balance',
                        p_location         => 'Negative balance',
                        p_message          => SQLERRM,
                        p_additional_info  => 'Bin : '||I.D_BIN_ID
                      );
                      END;
        END LOOP

        COMMIT;

    END;


/************END Negative balance*********************/
EXCEPTION
    WHEN OTHERS THEN
     standard_audit.log_entry
      (
        p_program     => 'Anonymous block',
        p_step_name   => 'Anonymous block',
        p_log_entry   => ' Error: '||SQLERRM,
        p_level       => 9
      );
      standard_error.log_validation_error
      (
        p_program          => 'Anonymous block',
        p_location         => 'Anonymous block',
        p_message          => SQLERRM,
        p_additional_info  => SQLERRM
      );

end;
/
