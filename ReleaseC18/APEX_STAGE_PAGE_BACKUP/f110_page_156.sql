set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_050100 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2016.08.24'
,p_release=>'5.1.1.00.08'
,p_default_workspace_id=>2094526905262116
,p_default_application_id=>110
,p_default_owner=>'CPRADMIN'
);
end;
/
prompt --application/set_environment
 
prompt APPLICATION 110 - Mastercard Prod
--
-- Application Export:
--   Application:     110
--   Name:            Mastercard Prod
--   Date and Time:   11:38 Friday February 9, 2018
--   Exported By:     ADMIN
--   Flashback:       0
--   Export Type:     Page Export
--   Version:         5.1.1.00.08
--   Instance ID:     69309211345420
--

prompt --application/pages/delete_00156
begin
wwv_flow_api.remove_page (p_flow_id=>wwv_flow.g_flow_id, p_page_id=>156);
end;
/
prompt --application/pages/page_00156
begin
wwv_flow_api.create_page(
 p_id=>156
,p_user_interface_id=>wwv_flow_api.id(51051178144194287)
,p_tab_set=>'T_EXTRA'
,p_name=>'FRM_CLIENT_EXTRA_ATTR'
,p_page_mode=>'NORMAL'
,p_step_title=>'Form On Ra Client Extra Attr'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title=>'Form On Ra Client Extra Attr'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script language="JavaScript" type="text/javascript">',
'<!--',
'',
' htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';',
'',
'//-->',
'</script>'))
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'ADMIN'
,p_last_upd_yyyymmddhh24miss=>'20160307065202'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(66700265861936517)
,p_plug_name=>'Client Extra Attr'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(144961623842324671)
,p_plug_display_sequence=>2
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>1
,p_plug_column_width=>'STYLE="height:1000px; width:1000px; background-color:aqua;"'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(67920675893604411)
,p_plug_name=>'<a style="color:#DF0101">RECORD LOCKED'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(144963741644324672)
,p_plug_display_sequence=>1
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a style="color:#DF0101">This record cannot be amended currently as it is ',
'Pending a change approval. Once the change is Approved/Rejected, this record ',
'will be unlocked and made available for amendments.',
''))
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXISTS'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select 1 from dual',
'where :P156_ID is not null',
'and static_data_auth_utils.get_locked_status(''RA_CLIENT_EXTRA_ATTR_VW'',:P156_ID)=''Y'''))
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'Y'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(67070489886491286)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_api.id(66700265861936517)
,p_button_name=>'CREATE'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(144960334777324670)
,p_button_image_alt=>'Manage Client Fees'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:240:&SESSION.::&DEBUG.:RP,240:P240_RA_CLIENT_ID:&P156_RA_CLIENT_ID.'
,p_button_condition=>'P156_ID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(68817261585007348)
,p_button_sequence=>60
,p_button_plug_id=>wwv_flow_api.id(66700265861936517)
,p_button_name=>'CREATE'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(144960334777324670)
,p_button_image_alt=>'Manage Front End Revenue Share'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:331:&SESSION.::&DEBUG.:RP,331:P331_RA_CLIENT_ID:&P156_RA_CLIENT_ID.'
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P156_ID is not null',
'and :P156_CLIENT_SETTLEMENT_FORMAT in(''I'',''S'',''R'',''T'')'))
,p_button_condition_type=>'SQL_EXPRESSION'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(60235080949533061)
,p_button_sequence=>70
,p_button_plug_id=>wwv_flow_api.id(66700265861936517)
,p_button_name=>'MANAGE_CLIENT_FEES'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(144960334777324670)
,p_button_image_alt=>'Manage Client Fees -  New Format'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:446:&SESSION.::&DEBUG.:RP,446:P446_CLIENT_ID:&P156_RA_CLIENT_ID.'
,p_button_condition=>'P156_ID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(61103985553010154)
,p_button_sequence=>80
,p_button_plug_id=>wwv_flow_api.id(66700265861936517)
,p_button_name=>'MANAGE_FRONT_END_REVENUE_SHARE'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(144960334777324670)
,p_button_image_alt=>'Manage Fees Revenue Share -  New Format'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:427:&SESSION.::&DEBUG.:427:P427_CLIENT_ID:&P156_RA_CLIENT_ID.'
,p_button_condition=>'P156_ID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(66701088877936518)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(66700265861936517)
,p_button_name=>'SAVE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(144960334777324670)
,p_button_image_alt=>'Apply Changes'
,p_button_position=>'REGION_TEMPLATE_CHANGE'
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select 1 from dual',
'where :P156_ID is not null',
'and static_data_auth_utils.get_locked_status(''RA_CLIENT_EXTRA_ATTR_VW'',:P156_ID  )=''N'''))
,p_button_condition_type=>'EXISTS'
,p_grid_new_grid=>false
,p_security_scheme=>wwv_flow_api.id(145183515750747381)
,p_database_action=>'UPDATE'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(66700671533936518)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(66700265861936517)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(144960334777324670)
,p_button_image_alt=>'Cancel'
,p_button_position=>'REGION_TEMPLATE_CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:183:&SESSION.::&DEBUG.:156::'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(66701284626936518)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(66700265861936517)
,p_button_name=>'CREATE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(144960334777324670)
,p_button_image_alt=>'Create'
,p_button_position=>'REGION_TEMPLATE_CREATE'
,p_button_condition=>'P156_ID'
,p_button_condition_type=>'ITEM_IS_NULL'
,p_grid_new_grid=>false
,p_security_scheme=>wwv_flow_api.id(145183515750747381)
,p_database_action=>'INSERT'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(66700880327936518)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(66700265861936517)
,p_button_name=>'DELETE'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(144960334777324670)
,p_button_image_alt=>'Delete'
,p_button_position=>'REGION_TEMPLATE_DELETE'
,p_button_redirect_url=>'javascript:confirmDelete(htmldb_delete_message,''DELETE'');'
,p_button_condition=>'P156_ID'
,p_button_condition_type=>'NEVER'
,p_grid_new_grid=>false
,p_database_action=>'DELETE'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(66700478463936518)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(66700265861936517)
,p_button_name=>'CREATE'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(144960334777324670)
,p_button_image_alt=>'Create'
,p_button_position=>'RIGHT_OF_IR_SEARCH_BAR'
,p_button_redirect_url=>'f?p=&APP_ID.:156:&SESSION.::&DEBUG.:149::'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(66703765260936522)
,p_branch_action=>'f?p=&APP_ID.:183:&SESSION.::&DEBUG.:RP,156::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1
,p_branch_condition_type=>'REQUEST_IN_CONDITION'
,p_branch_condition=>'CREATE, SAVE'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(60235389112533063)
,p_branch_action=>'f?p=&FLOW_ID.:430:&SESSION.::&DEBUG.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_api.id(60235080949533061)
,p_branch_sequence=>10
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(61104284356010154)
,p_branch_action=>'f?p=&FLOW_ID.:427:&SESSION.::&DEBUG.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_api.id(61103985553010154)
,p_branch_sequence=>10
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(67070780738491287)
,p_branch_action=>'f?p=&FLOW_ID.:223:&SESSION.::&DEBUG.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_api.id(67070489886491286)
,p_branch_sequence=>10
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(68817562759007348)
,p_branch_action=>'f?p=&FLOW_ID.:331:&SESSION.::&DEBUG.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_api.id(68817261585007348)
,p_branch_sequence=>10
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(61459292738580991)
,p_branch_action=>'f?p=&APP_ID.:446:&SESSION.::&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_api.id(60235080949533061)
,p_branch_sequence=>20
,p_branch_comment=>'Created 05-DEC-2013 14:24 by GUPTAD'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(37734645663930661)
,p_name=>'P156_CARDNUMBER_REQ'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_api.id(66700265861936517)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Card Number Required? '
,p_source=>'CARDNUMBER_REQ'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC: Y;Y,N'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(144974824100324677)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(37734852121942887)
,p_name=>'P156_PROXYNBR_REQ'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_api.id(66700265861936517)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Proxy Card Number Required?'
,p_source=>'PROXY_CARDNBR_REQ'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC: Y;Y,N'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(144974824100324677)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(38821335856353470)
,p_name=>'P156_TRANSACTION_TIMESTAMP_REQUIRED'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_api.id(66700265861936517)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Transaction Timestamp Required?'
,p_source=>'TRANSACTION_TIMESTAMP_REQ'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC:Y,N'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(144974824100324677)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(57423989221918367)
,p_name=>'P156_ALTERNATE_ID'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_api.id(66700265861936517)
,p_prompt=>'Alternate ID (Self Settlement only)'
,p_source=>'ALTERNATE_ID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC2:None;None,Account Number;ACCOUNT_NUMBER,Cardholder Id;CARDHOLDER_ID,Proxy Id;PROXY_CARD_NUMBER'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(144974824100324677)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(60879775355251352)
,p_name=>'P156_FEES_COMM_INV_TEMPLATE'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(66700265861936517)
,p_use_cache_before_default=>'NO'
,p_item_default=>'Fees_comm_inv_template'
,p_prompt=>'CMP0630 Invoice Format'
,p_source=>'FEES_COMM_INV_TEMPLATE'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC2:No;N,Yes;Y'
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(144974824100324677)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(66701485923936519)
,p_name=>'P156_ID'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_api.id(66700265861936517)
,p_prompt=>'Id'
,p_source=>'ID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(144974824100324677)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(66701682134936519)
,p_name=>'P156_RA_CLIENT_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(66700265861936517)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Client'
,p_source=>'RA_CLIENT_ID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'   select CLIENT_NAME||''-''||',
'CLIENT_LEVEL||'' - Processor CL ID=''||PROCESSOR_CLIENT_ID display_value, RDS_CLIENT_ID return_value ',
'from RA_CLIENT',
'where PROCESSOR_ID=2',
'and CLIENT_LEVEL in (''CLIENT'',''LEVEL2'')',
'order by 1'))
,p_lov_display_null=>'YES'
,p_cSize=>64
,p_cMaxlength=>255
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(144974824100324677)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'NOT_ENTERABLE'
,p_attribute_02=>'FIRST_ROWSET'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(66702078356936520)
,p_name=>'P156_CLIENT_MCP_FLAG'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(66700265861936517)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Client Mcp Flag'
,p_source=>'CLIENT_MCP_FLAG'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC2:Y;Y,N;N'
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(144974824100324677)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(66702263350936520)
,p_name=>'P156_CLIENT_EXCEL_INDICATOR'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(66700265861936517)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Client Excel Indicator'
,p_source=>'CLIENT_EXCEL_INDICATOR'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC2:N;N,Y;Y'
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(144974824100324677)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(66702480050936520)
,p_name=>'P156_CLIENT_SETTLEMENT_FORMAT'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(66700265861936517)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Client Settlement Format'
,p_source=>'CLIENT_SETTLEMENT_FORMAT'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC2:Invoice;I,Statement;S,Self Settlement;N,Invoice with Revenue Share;R,Net Settlement;T'
,p_lov_display_null=>'YES'
,p_lov_null_text=>'Select Settlement Type'
,p_cSize=>30
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(144974824100324677)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(69321590942349721)
,p_name=>'P156_INCLUDE_REV_NET_SETTLE'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(66700265861936517)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Include Rev Net Settle'
,p_source=>'INCLUDE_REV_NET_SETTLE'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC2:Y;Y,N;N'
,p_lov_display_null=>'YES'
,p_lov_null_text=>'%'
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(144974824100324677)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_computation(
 p_id=>wwv_flow_api.id(37780460117709632)
,p_computation_sequence=>10
,p_computation_item=>'P156_CARDNUMBER_REQ'
,p_computation_type=>'ITEM_VALUE'
,p_computation=>'null'
,p_compute_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'IF :P156_CLIENT_SETTLEMENT_FORMAT NOT IN (''S'',''N'') AND  :P156_FEES_COMM_INV_TEMPLATE=''N'' THEN',
'    RETURN TRUE;',
'ELSIF',
':P156_CLIENT_SETTLEMENT_FORMAT  IN (''S'',''N'') THEN',
'    RETURN TRUE;',
'ELSE',
'RETURN FALSE;',
'END IF;',
'END;'))
,p_compute_when_type=>'FUNCTION_BODY'
);
wwv_flow_api.create_page_computation(
 p_id=>wwv_flow_api.id(37780865662783010)
,p_computation_sequence=>20
,p_computation_item=>'P156_PROXYNBR_REQ'
,p_computation_type=>'ITEM_VALUE'
,p_computation=>'NULL'
,p_compute_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'IF :P156_CLIENT_SETTLEMENT_FORMAT NOT IN (''S'',''N'') AND  :P156_FEES_COMM_INV_TEMPLATE=''N'' THEN',
'    RETURN TRUE;',
'ELSIF',
':P156_CLIENT_SETTLEMENT_FORMAT  IN (''S'',''N'') THEN',
'    RETURN TRUE;',
'ELSE',
'RETURN FALSE;',
'END IF;',
'END;'))
,p_compute_when_type=>'FUNCTION_BODY'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(69524392819030361)
,p_validation_name=>'ValidateNetSettle'
,p_validation_sequence=>10
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'  vTopClient          number(11);',
'  vTopSettleFormat    varchar2(1);',
'  vClientType         varchar2(3);',
'begin',
'',
'select',
'  parent_id',
'  ,nvl(root_settlement_method,''X'')',
'  ,client_type_code',
'into',
'  vTopClient',
' ,vTopSettleFormat',
' ,vClientType',
'from',
'(',
'select ',
'  rownum rowno',
' ,cli_id',
' ,connect_by_root cli_id as parent_id',
' ,connect_by_root client_settlement_format as root_settlement_method',
' ,level hier_level',
' ,client_name as cli_name',
' ,client_type_code',
' ,client_name as business_name',
' ,invoice_type_flag',
'from',
'  (select',
'     cli.rds_client_id as cli_id',
'     ,cli.parent_client_id',
'     ,ext.client_settlement_format ',
'     ,cli.client_name',
'     ,decode(cli.client_level,''CLIENT'',''CL'',''LEVEL2'',''L2'',''BRANCH'',''BR'',''XX'') as client_type_code',
'     ,nvl(substr(cli.ra_invoice_type,1,1),decode(cli.invoice_flag,''Y'',''S'')) as invoice_type_flag',
'   from',
'     ra_client               cli',
'     ,ra_client_extra_attr   ext',
'   where',
'     cli.rds_client_id = ext.ra_client_id(+)',
'  )',
'start with decode(client_type_code,''CL'',1,''L2'',2,''BR'',3,1) = 1',
'connect by nocycle prior cli_id = parent_client_id',
'order siblings by cli_id, parent_client_id',
')',
'where',
'  cli_id = :P156_RA_CLIENT_ID;',
'',
'-- select',
'--     parent_id',
'--     ,nvl(root_settlement_method,''X'')',
'--     ,client_type_code',
'--  into',
'--     vTopClient',
'--    ,vTopSettleFormat',
'--    ,vClientType',
'--  from',
'--    gfl_mcp_core.inv_hier',
'--  where',
'--    cli_id = :P156_RA_CLIENT_ID;',
'',
'  if :P156_INCLUDE_REV_NET_SETTLE in (''Y'',''N'') and vClientType = ''CL'' ',
'  then ',
'   return ''Client level clients cannot have the include in rev net settle set''; ',
'  end if;',
'',
'  if :P156_INCLUDE_REV_NET_SETTLE in (''Y'',''N'') and vClientType = ''BR'' ',
'  then ',
'   return ''Branch level clients cannot have the include in rev net settle set''; ',
'  end if;',
'',
'  if :P156_CLIENT_SETTLEMENT_FORMAT = ''T'' and vClientType <> ''CL'' ',
'  then ',
'  return ''You may only set the net settlement format on a client level client''; ',
'  end if;',
'',
'  if :P156_INCLUDE_REV_NET_SETTLE = ''Y'' and vTopSettleFormat <> ''T'' ',
'  then ',
'  return ''You may only set the inclusion of clients that are within a client with net settlement set''; ',
'  end if;',
'',
'  if nvl(:P156_INCLUDE_REV_NET_SETTLE,''X'') not in (''N'',''Y'') and vClientType = ''L2''',
'    and vTopSettleFormat = ''T'' ',
'  then ',
'    return ''Include rev net settlement must be set as this level 2 client is within a net settlement heirarchy'' ; ',
'  end if;',
'',
'  if :P156_INCLUDE_REV_NET_SETTLE = ''Y'' and vClientType = ''L2''',
'    and :P156_CLIENT_SETTLEMENT_FORMAT = ''R''',
'  then ',
'    return ''Level 2 client can only be include in net settlement or invoice with revenue share not both'' ; ',
'  end if;',
'',
'exception',
'  when others then return ''No Hierarchy entry'';',
'',
'end;'))
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_always_execute=>'N'
,p_when_button_pressed=>wwv_flow_api.id(66701088877936518)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(60847872496188942)
,p_name=>'Enable Item'
,p_event_sequence=>10
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P156_CLIENT_SETTLEMENT_FORMAT'
,p_condition_element=>'P156_CLIENT_SETTLEMENT_FORMAT'
,p_triggering_condition_type=>'NOT_IN_LIST'
,p_triggering_expression=>'N,'' '''
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(60848169673188949)
,p_event_id=>wwv_flow_api.id(60847872496188942)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P156_ALTERNATE_ID'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(60851087361536669)
,p_event_id=>wwv_flow_api.id(60847872496188942)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P156_ALTERNATE_ID'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(37735866289104577)
,p_name=>'TC_VAL'
,p_event_sequence=>20
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P156_CLIENT_SETTLEMENT_FORMAT,P156_FEES_COMM_INV_TEMPLATE'
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'($v(''P156_FEES_COMM_INV_TEMPLATE'') == ''Y'') && ($v(''P156_CLIENT_SETTLEMENT_FORMAT'') != ''N'' && $v(''P156_CLIENT_SETTLEMENT_FORMAT'') !=''S'' && $v(''P156_CLIENT_SETTLEMENT_FORMAT'') !='''' ) '
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(37736166381104583)
,p_event_id=>wwv_flow_api.id(37735866289104577)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P156_CARDNUMBER_REQ,P156_PROXYNBR_REQ,P156_TRANSACTION_TIMESTAMP_REQUIRED'
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(37736346350104592)
,p_event_id=>wwv_flow_api.id(37735866289104577)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P156_CARDNUMBER_REQ,P156_PROXYNBR_REQ,P156_TRANSACTION_TIMESTAMP_REQUIRED'
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(37759956743139108)
,p_name=>'load'
,p_event_sequence=>30
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P156_CLIENT_SETTLEMENT_FORMAT,P156_FEES_COMM_INV_TEMPLATE'
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'($v(''P156_FEES_COMM_INV_TEMPLATE'') != ''Y'') && ($v(''P156_CLIENT_SETTLEMENT_FORMAT'') = ''N'' || $v(''P156_CLIENT_SETTLEMENT_FORMAT'') =''S'' || $v(''P156_CLIENT_SETTLEMENT_FORMAT'') ='''' ) '
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
,p_display_when_type=>'NEVER'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(37760236371139125)
,p_event_id=>wwv_flow_api.id(37759956743139108)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P156_CARDNUMBER_REQ,P156_PROXYNBR_REQ'
,p_attribute_01=>''''''
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(37760441767153649)
,p_name=>'unload'
,p_event_sequence=>40
,p_bind_type=>'bind'
,p_bind_event_type=>'unload'
,p_display_when_type=>'NEVER'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(37760733967153652)
,p_event_id=>wwv_flow_api.id(37760441767153649)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'UPDATE RA_CLIENT_EXTRA_ATTR_a SET CARDNUMBER_REQ=NULL,PROXY_CARDNBR_REQ=NULL WHERE fees_comm_inv_template=''N'' and ra_client_id=:P156_Ra_CLIENT_ID;',
'UPDATE RA_CLIENT_EXTRA_ATTR_a SET CARDNUMBER_REQ=NULL,PROXY_CARDNBR_REQ=NULL WHERE client_settlement_format in (''S'',''N'') and ra_client_id=:P156_Ra_CLIENT_ID;',
'commit;',
'END;'))
,p_stop_execution_on_error=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(66703272547936521)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_FORM_FETCH'
,p_process_name=>'Fetch Row from RA_CLIENT_EXTRA_ATTR'
,p_attribute_02=>'RA_CLIENT_EXTRA_ATTR'
,p_attribute_03=>'P156_ID'
,p_attribute_04=>'ID'
,p_process_error_message=>'Unable to fetch row.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(66703471515936521)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Set ID Value'
,p_process_sql_clob=>':P156_ID := RA_CLIENT_EXTRA_ATTR_seq.nextval;'
,p_process_error_message=>'Unable to get primary key item value.'
,p_process_when_button_id=>wwv_flow_api.id(66700478463936518)
,p_security_scheme=>wwv_flow_api.id(145183515750747381)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(66702872478936520)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_FORM_PROCESS'
,p_process_name=>'Process Row of RA_CLIENT_EXTRA_ATTR'
,p_attribute_02=>'RA_CLIENT_EXTRA_ATTR_VW'
,p_attribute_03=>'P156_ID'
,p_attribute_04=>'ID'
,p_attribute_11=>'I:U'
,p_process_error_message=>'Action failed.'
,p_process_success_message=>'Submitted for approval'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(66703079932936521)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'reset page'
,p_attribute_01=>'CLEAR_CACHE_CURRENT_PAGE'
,p_process_when=>'SAVE, CANCEL'
,p_process_when_type=>'REQUEST_IN_CONDITION'
);
end;
/
begin
wwv_flow_api.import_end(p_auto_install_sup_obj => nvl(wwv_flow_application_install.get_auto_install_sup_obj, false));
commit;
end;
/
set verify on feedback on define on
prompt  ...done
