set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_050100 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2016.08.24'
,p_release=>'5.1.1.00.08'
,p_default_workspace_id=>2094526905262116
,p_default_application_id=>110
,p_default_owner=>'CPRADMIN'
);
end;
/
prompt --application/set_environment
 
prompt APPLICATION 110 - Mastercard Prod
--
-- Application Export:
--   Application:     110
--   Name:            Mastercard Prod
--   Date and Time:   09:29 Friday February 9, 2018
--   Exported By:     ADMIN
--   Flashback:       0
--   Export Type:     Page Export
--   Version:         5.1.1.00.08
--   Instance ID:     69309211345420
--

prompt --application/pages/delete_00647
begin
wwv_flow_api.remove_page (p_flow_id=>wwv_flow.g_flow_id, p_page_id=>647);
end;
/
prompt --application/pages/page_00647
begin
wwv_flow_api.create_page(
 p_id=>647
,p_user_interface_id=>wwv_flow_api.id(51051178144194287)
,p_tab_set=>'T_EXTRA'
,p_name=>'MCP ICA Extra'
,p_page_mode=>'NORMAL'
,p_step_title=>'MCP ICA Extra'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title=>'MCP ICA Extra'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'NO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'ADMIN'
,p_last_upd_yyyymmddhh24miss=>'20150402131428'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(52772182107889502)
,p_plug_name=>'ICA Extra'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(144963741644324672)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY_3'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT A.id,',
'       A.bin_sponsor_id,',
'       B.bin_sponsor_name,',
'       A.ica_nbr,',
'       A.is_primary_ica,',
'       A.country_code,',
'       C.country_name,',
'       A.created_date,',
'       A.last_updated_date,',
'       A.last_updated_by',
'  FROM ra_mcp_ica_extra_vw A,',
'       ra_bin_sponsor B,',
'       ra_country C',
' WHERE A.bin_sponsor_id = B.rds_bin_sponsor_id',
'   AND A.country_code = C.country_iso_code'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_row_template=>1
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(52772271833889502)
,p_name=>'ICA Extra'
,p_max_row_count=>'1000000'
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_show_detail_link=>'C'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML:EMAIL'
,p_detail_link=>'f?p=&APP_ID.:651:&SESSION.::&DEBUG.::P651_ID:#ID#'
,p_detail_link_text=>'<img src="#IMAGE_PREFIX#menu/pencil2_16x16.gif" alt="">'
,p_detail_link_auth_scheme=>wwv_flow_api.id(145163727938580583)
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'ADMIN'
,p_internal_uid=>4789111306118436
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(52772486502889550)
,p_db_column_name=>'ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'LEFT'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'ID'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(52772564957889560)
,p_db_column_name=>'BIN_SPONSOR_ID'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Bin Sponsor Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'BIN_SPONSOR_ID'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(52772688028889560)
,p_db_column_name=>'ICA_NBR'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'ICA Number'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'ICA_NBR'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(52772885973889560)
,p_db_column_name=>'COUNTRY_CODE'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Country Code'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
,p_tz_dependent=>'N'
,p_static_id=>'COUNTRY_CODE'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(52772973858889560)
,p_db_column_name=>'CREATED_DATE'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'Created Date'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'CREATED_DATE'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(52773092123889560)
,p_db_column_name=>'LAST_UPDATED_DATE'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Last Updated Date'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'LAST_UPDATED_DATE'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(52773192682889561)
,p_db_column_name=>'LAST_UPDATED_BY'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Last Updated By'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'LAST_UPDATED_BY'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(52822776163790601)
,p_db_column_name=>'IS_PRIMARY_ICA'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Primary ICA?'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'IS_PRIMARY_ICA'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(52832372063387400)
,p_db_column_name=>'BIN_SPONSOR_NAME'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Bin Sponsor Name'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'BIN_SPONSOR_NAME'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(52832491383387404)
,p_db_column_name=>'COUNTRY_NAME'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Country'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'COUNTRY_NAME'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(52773289901890640)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'47902'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'ID:ICA_NBR:BIN_SPONSOR_NAME:IS_PRIMARY_ICA:COUNTRY_NAME:CREATED_DATE:LAST_UPDATED_DATE:LAST_UPDATED_BY:'
,p_flashback_enabled=>'N'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(52775288440984727)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(52772182107889502)
,p_button_name=>'CREATE'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(144960334777324670)
,p_button_image_alt=>'Create'
,p_button_position=>'RIGHT_OF_IR_SEARCH_BAR'
,p_button_redirect_url=>'f?p=&APP_ID.:651:&SESSION.::&DEBUG.:651::'
,p_grid_new_grid=>false
,p_security_scheme=>wwv_flow_api.id(145163727938580583)
);
end;
/
begin
wwv_flow_api.import_end(p_auto_install_sup_obj => nvl(wwv_flow_application_install.get_auto_install_sup_obj, false));
commit;
end;
/
set verify on feedback on define on
prompt  ...done
