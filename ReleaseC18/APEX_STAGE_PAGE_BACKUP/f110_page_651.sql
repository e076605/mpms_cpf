set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_050100 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2016.08.24'
,p_release=>'5.1.1.00.08'
,p_default_workspace_id=>2094526905262116
,p_default_application_id=>110
,p_default_owner=>'CPRADMIN'
);
end;
/
prompt --application/set_environment
 
prompt APPLICATION 110 - Mastercard Prod
--
-- Application Export:
--   Application:     110
--   Name:            Mastercard Prod
--   Date and Time:   09:30 Friday February 9, 2018
--   Exported By:     ADMIN
--   Flashback:       0
--   Export Type:     Page Export
--   Version:         5.1.1.00.08
--   Instance ID:     69309211345420
--

prompt --application/pages/delete_00651
begin
wwv_flow_api.remove_page (p_flow_id=>wwv_flow.g_flow_id, p_page_id=>651);
end;
/
prompt --application/pages/page_00651
begin
wwv_flow_api.create_page(
 p_id=>651
,p_user_interface_id=>wwv_flow_api.id(51051178144194287)
,p_tab_set=>'T_EXTRA'
,p_name=>'MCP ICA Extra'
,p_page_mode=>'NORMAL'
,p_step_title=>'MCP ICA Extra'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_javascript_code=>'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';'
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_cache_timeout_seconds=>21600
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'ADMIN'
,p_last_upd_yyyymmddhh24miss=>'20150908070937'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(52775972395103538)
,p_plug_name=>'ICA Extra'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(144961623842324671)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>1
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(52831970842315210)
,p_plug_name=>'<a style="color:#DF0101">RECORD LOCKED'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(144963741644324672)
,p_plug_display_sequence=>5
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY_3'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a style="color:#DF0101">This record cannot be amended currently as it is ',
'Pending a change approval. Once the change is Approved/Rejected, this record ',
'will be unlocked and made available for amendments.',
''))
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXISTS'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select 1 from dual',
'where :P651_ID is not null',
'and static_data_auth_utils.get_locked_status(''RA_MCP_ICA_EXTRA_VW'',:P651_ID)=''Y'''))
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'Y'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(52776279974103542)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(52775972395103538)
,p_button_name=>'SAVE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(144960334777324670)
,p_button_image_alt=>'Apply Changes'
,p_button_position=>'REGION_TEMPLATE_CHANGE'
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select 1 from dual',
'where :P651_ID is not null',
'and static_data_auth_utils.get_locked_status(''RA_MCP_ICA_EXTRA_VW'',:P651_ID)=''N'''))
,p_button_condition_type=>'EXISTS'
,p_grid_new_grid=>false
,p_database_action=>'UPDATE'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(52776591373103542)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(52775972395103538)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(144960334777324670)
,p_button_image_alt=>'Cancel'
,p_button_position=>'REGION_TEMPLATE_CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:647:&SESSION.::&DEBUG.:::'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(52776187024103542)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(52775972395103538)
,p_button_name=>'CREATE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(144960334777324670)
,p_button_image_alt=>'Create'
,p_button_position=>'REGION_TEMPLATE_CREATE'
,p_button_condition=>'P651_ID'
,p_button_condition_type=>'ITEM_IS_NULL'
,p_grid_new_grid=>false
,p_database_action=>'INSERT'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(52776376632103542)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(52775972395103538)
,p_button_name=>'DELETE'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(144960334777324670)
,p_button_image_alt=>'Delete'
,p_button_position=>'REGION_TEMPLATE_DELETE'
,p_button_redirect_url=>'javascript:apex.confirm(htmldb_delete_message,''DELETE'');'
,p_button_execute_validations=>'N'
,p_button_condition_type=>'NEVER'
,p_grid_new_grid=>false
,p_database_action=>'DELETE'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(52824573378034919)
,p_branch_name=>'Go to page'
,p_branch_action=>'f?p=&APP_ID.:647:&SESSION.::&DEBUG.:RP,651::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>5
,p_branch_condition_type=>'REQUEST_IN_CONDITION'
,p_branch_condition=>'CREATE, SAVE'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(52777179894103546)
,p_branch_action=>'f?p=&APP_ID.:651:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(52777377302103565)
,p_name=>'P651_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(52775972395103538)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Id'
,p_source=>'ID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(144974824100324677)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(52777590243103598)
,p_name=>'P651_BIN_SPONSOR_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(52775972395103538)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Bin Sponsor Id'
,p_source=>'BIN_SPONSOR_ID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>32
,p_cMaxlength=>255
,p_cHeight=>1
,p_display_when=>'P651_BIN_SPONSOR_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_read_only_when=>'P651_BIN_SPONSOR_ID'
,p_read_only_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_api.id(144975116336324677)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(52777762020103616)
,p_name=>'P651_ICA_NBR'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(52775972395103538)
,p_use_cache_before_default=>'NO'
,p_prompt=>'ICA Number'
,p_source=>'ICA_NBR'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'ICA_BS_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT DISTINCT',
'       B.ica_number || ''-'' || BS.bin_sponsor_name d, ',
'       B.ica_number || ''-'' || BS.rds_bin_sponsor_id r',
'  FROM ra_bin B,',
'       ra_bin_sponsor BS',
' WHERE b.processor_id=2',
'   AND b.bin_sponsor_id= bs.rds_bin_sponsor_id',
'   AND bs.is_mcp=''Y''',
' ORDER BY 1'))
,p_cSize=>32
,p_cMaxlength=>255
,p_cHeight=>1
,p_read_only_when=>'P651_ICA_NBR'
,p_read_only_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_api.id(144975116336324677)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'NOT_ENTERABLE'
,p_attribute_02=>'FIRST_ROWSET'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(52777968024103616)
,p_name=>'P651_IS_PRIMARY_ICA'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(52775972395103538)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Primary ICA?'
,p_source=>'IS_PRIMARY_ICA'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC2:Yes;Y,No;N'
,p_cSize=>32
,p_cMaxlength=>255
,p_cHeight=>1
,p_field_template=>wwv_flow_api.id(144975116336324677)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(52778174588103616)
,p_name=>'P651_COUNTRY_CODE'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(52775972395103538)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Country'
,p_source=>'COUNTRY_CODE'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LST_COUNTRY_NAME'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select country_name d, country_iso_code r',
'from   RA_COUNTRY',
'order by 1'))
,p_cSize=>32
,p_cMaxlength=>2
,p_cHeight=>1
,p_field_template=>wwv_flow_api.id(144975116336324677)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(52778376186103617)
,p_name=>'P651_CREATED_DATE'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(52775972395103538)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Created Date'
,p_source=>'CREATED_DATE'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>32
,p_cMaxlength=>255
,p_cHeight=>1
,p_display_when=>'P651_CREATED_DATE'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_api.id(144974824100324677)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(52778586617103617)
,p_name=>'P651_LAST_UPDATED_DATE'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(52775972395103538)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Last Updated Date'
,p_source=>'LAST_UPDATED_DATE'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>32
,p_cMaxlength=>255
,p_cHeight=>1
,p_display_when=>'P651_LAST_UPDATED_DATE'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_api.id(144974824100324677)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(52778785035103619)
,p_name=>'P651_LAST_UPDATED_BY'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(52775972395103538)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Last Updated By'
,p_source=>'LAST_UPDATED_BY'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>32
,p_cMaxlength=>30
,p_cHeight=>1
,p_display_when=>'P651_LAST_UPDATED_BY'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_api.id(144974824100324677)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(54486882980681747)
,p_name=>'P651_BIN_SPONSOR_NAME'
,p_item_sequence=>25
,p_item_plug_id=>wwv_flow_api.id(52775972395103538)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Bin Sponsor Name'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT bin_sponsor_name',
'FROM ra_bin_sponsor',
'WHERE rds_bin_sponsor_id = :P651_BIN_SPONSOR_ID'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'P651_BIN_SPONSOR_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_read_only_when=>'P651_BIN_SPONSOR_ID'
,p_read_only_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_api.id(144975116336324677)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_api.create_page_computation(
 p_id=>wwv_flow_api.id(54487578160775101)
,p_computation_sequence=>10
,p_computation_item=>'P651_BIN_SPONSOR_NAME'
,p_computation_type=>'QUERY'
,p_computation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT bin_sponsor_name',
'FROM ra_bin_sponsor',
'WHERE rds_bin_sponsor_id = :P651_BIN_SPONSOR_ID'))
,p_compute_when=>'P651_BIN_SPONSOR_ID'
,p_compute_when_type=>'ITEM_IS_NOT_NULL'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(52794792016793519)
,p_validation_name=>'ICA Number not null'
,p_validation_sequence=>10
,p_validation=>'P651_ICA_NBR'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'ICA Number is a mandatory field'
,p_always_execute=>'N'
,p_only_for_changed_rows=>'Y'
,p_associated_item=>wwv_flow_api.id(52777762020103616)
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(52794976057800847)
,p_validation_name=>'Primary ICA flag not null'
,p_validation_sequence=>20
,p_validation=>'P651_IS_PRIMARY_ICA'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Primary ICA flag is a mandatory field'
,p_always_execute=>'N'
,p_only_for_changed_rows=>'Y'
,p_associated_item=>wwv_flow_api.id(52777968024103616)
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(52795260529808036)
,p_validation_name=>'Country not null'
,p_validation_sequence=>30
,p_validation=>'P651_COUNTRY_CODE'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Country is a mandatory field'
,p_always_execute=>'N'
,p_only_for_changed_rows=>'Y'
,p_associated_item=>wwv_flow_api.id(52778174588103616)
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(52795766481911671)
,p_validation_name=>'Primary ICA already exists'
,p_validation_sequence=>40
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT 1',
'FROM ra_mcp_ica_extra',
'WHERE bin_sponsor_id = :P651_BIN_SPONSOR_ID',
'AND is_primary_ica = ''Y''',
'AND id != NVL(:P651_ID, -1)',
'UNION',
'SELECT 1',
'FROM ra_mcp_ica_extra_A',
'WHERE bin_sponsor_id = :P651_BIN_SPONSOR_ID',
'AND is_primary_ica = ''Y''',
'AND id != NVL(:P651_ID, -1)',
'AND auth_status = ''PENDING'';'))
,p_validation_type=>'NOT_EXISTS'
,p_error_message=>'Either primary ICA already exists or is pending for approval for this bin sponsor'
,p_always_execute=>'N'
,p_validation_condition=>'P651_IS_PRIMARY_ICA'
,p_validation_condition2=>'Y'
,p_validation_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_only_for_changed_rows=>'Y'
,p_associated_item=>wwv_flow_api.id(52777968024103616)
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(52796371528334617)
,p_validation_name=>'ICA and BS already configured'
,p_validation_sequence=>50
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT 1',
'  FROM ra_mcp_ica_extra',
' WHERE ica_nbr = :P651_ICA_NBR',
'UNION',
'SELECT 1',
'  FROM ra_mcp_ica_extra_a',
' WHERE ica_nbr = :P651_ICA_NBR',
'   AND auth_status = ''PENDING'''))
,p_validation_type=>'NOT_EXISTS'
,p_error_message=>'This ICA and Bin Sponsor either has already been configured or is pending for approval.'
,p_always_execute=>'N'
,p_when_button_pressed=>wwv_flow_api.id(52776187024103542)
,p_only_for_changed_rows=>'Y'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(52792468638455322)
,p_name=>'Split ICA and BS'
,p_event_sequence=>10
,p_bind_type=>'bind'
,p_bind_event_type=>'apexbeforepagesubmit'
,p_display_when_type=>'ITEM_IS_NULL'
,p_display_when_cond=>'P651_BIN_SPONSOR_ID'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(52792761949455336)
,p_event_id=>wwv_flow_api.id(52792468638455322)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P651_BIN_SPONSOR_ID := SUBSTR(:P651_ICA_NBR, INSTR(:P651_ICA_NBR, ''-'')+1, LENGTH(:P651_ICA_NBR)-INSTR(:P651_ICA_NBR, ''-''));',
':P651_ICA_NBR := SUBSTR(:P651_ICA_NBR, 1, INSTR(:P651_ICA_NBR, ''-'')-1);'))
,p_attribute_02=>'P651_ICA_NBR,P651_BIN_SPONSOR_ID'
,p_attribute_03=>'P651_ICA_NBR,P651_BIN_SPONSOR_ID'
,p_attribute_04=>'N'
,p_stop_execution_on_error=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(52779168790103623)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_FORM_FETCH'
,p_process_name=>'Fetch Row from RA_MCP_ICA_EXTRA_VW'
,p_attribute_02=>'RA_MCP_ICA_EXTRA_VW'
,p_attribute_03=>'P651_ID'
,p_attribute_04=>'ID'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(52779362161103638)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Get PK'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin ',
'    if :P651_ID is null then',
'        select "RA_MCP_ICA_EXTRA_SEQ".nextval',
'          into :P651_ID',
'          from sys.dual;',
'    end if;',
'end;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(52776187024103542)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(52779561248103638)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_FORM_PROCESS'
,p_process_name=>'Process Row of RA_MCP_ICA_EXTRA_VW'
,p_attribute_02=>'RA_MCP_ICA_EXTRA_VW'
,p_attribute_03=>'P651_ID'
,p_attribute_04=>'ID'
,p_attribute_11=>'I:U:D'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_success_message=>'Action Processed.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(52779770795103639)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'reset page'
,p_attribute_01=>'CLEAR_CACHE_CURRENT_PAGE'
,p_process_when=>'CANCEL'
,p_process_when_type=>'REQUEST_IN_CONDITION'
);
end;
/
begin
wwv_flow_api.import_end(p_auto_install_sup_obj => nvl(wwv_flow_application_install.get_auto_install_sup_obj, false));
commit;
end;
/
set verify on feedback on define on
prompt  ...done
