set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_050100 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2016.08.24'
,p_release=>'5.1.1.00.08'
,p_default_workspace_id=>2094526905262116
,p_default_application_id=>110
,p_default_owner=>'CPRADMIN'
);
end;
/
prompt --application/set_environment
 
prompt APPLICATION 110 - Mastercard Prod
--
-- Application Export:
--   Application:     110
--   Name:            Mastercard Prod
--   Date and Time:   11:35 Friday February 9, 2018
--   Exported By:     ADMIN
--   Flashback:       0
--   Export Type:     Page Export
--   Version:         5.1.1.00.08
--   Instance ID:     69309211345420
--

prompt --application/pages/delete_00183
begin
wwv_flow_api.remove_page (p_flow_id=>wwv_flow.g_flow_id, p_page_id=>183);
end;
/
prompt --application/pages/page_00183
begin
wwv_flow_api.create_page(
 p_id=>183
,p_user_interface_id=>wwv_flow_api.id(51051178144194287)
,p_tab_set=>'T_EXTRA'
,p_name=>'RPT_CLIENT_EXTRA_ATTR'
,p_page_mode=>'NORMAL'
,p_step_title=>'Report On Ra Client Extra Attr'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title=>'Report On Ra Client Extra Attr'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'ADMIN'
,p_last_upd_yyyymmddhh24miss=>'20160310051605'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(66706685441939675)
,p_plug_name=>'Client Extra Attr'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(144963741644324672)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_point=>'BODY_3'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select EX.ID,EX.RA_CLIENT_ID',
',EX.CLIENT_MCP_FLAG,EX.CLIENT_EXCEL_INDICATOR,EX.CLIENT_SETTLEMENT_FORMAT,',
'CL.CLIENT_NAME,CL.CLIENT_LEVEL,CL.PROCESSOR_CLIENT_ID,',
'DECODE(EX.Fees_comm_inv_template,''Y'',''Yes'',''N'',''No'',''No'') Fees_comm_inv_template, ',
'DECODE(ex.alternate_id,''ACCOUNT_NUMBER'',''Account Number'',''CARDHOLDER_ID'',''Cardholder Id'',''PROXY_CARD_NUMBER'',''Proxy Id'',''None'',''None'',''None'') Alternate_id,',
'DECODE(EX.CARDNUMBER_REQ,''Y'',''Yes'',''N'',''No'',''NA'') Card_Number_Required,',
'DECODE(EX.proxy_cardnbr_REQ,''Y'',''Yes'',''N'',''No'',''NA'') Proxy_Card_Number_Required,',
'DECODE(EX.TRANSACTION_TIMESTAMP_REQ,''Y'',''Yes'',''N'',''No'',''NA'') Transaction_Timestamp_Required',
'from ra_client_extra_attr ex,',
'ra_client cl',
'where EX.RA_CLIENT_ID=CL.RDS_CLIENT_ID(+)'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_row_template=>1
,p_prn_output_show_link=>'Y'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_document_header=>'APEX'
,p_prn_units=>'MILLIMETERS'
,p_prn_paper_size=>'A4'
,p_prn_width=>297
,p_prn_height=>210
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#ffffff'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(66706889607939675)
,p_name=>'Report on RA_CLIENT_EXTRA_ATTR'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than 10,000 rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_show_display_row_count=>'Y'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_show_detail_link=>'C'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV'
,p_detail_link=>'f?p=&APP_ID.:156:&SESSION.::&DEBUG.::P156_ID:#ID#'
,p_detail_link_text=>'<img src="#IMAGE_PREFIX#e2.gif"  border="0">'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'ADMIN'
,p_internal_uid=>18723729080168609
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(66706981959939675)
,p_db_column_name=>'ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_static_id=>'ID'
,p_help_text=>'Primary Key'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(66707066149939675)
,p_db_column_name=>'RA_CLIENT_ID'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Ra Client Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_static_id=>'RA_CLIENT_ID'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(66707287306939675)
,p_db_column_name=>'CLIENT_MCP_FLAG'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Client Mcp Flag'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'CLIENT_MCP_FLAG'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(66707390704939675)
,p_db_column_name=>'CLIENT_EXCEL_INDICATOR'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Client Excel Indicator'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'CLIENT_EXCEL_INDICATOR'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(66707481672939675)
,p_db_column_name=>'CLIENT_SETTLEMENT_FORMAT'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'Client Settlement Format'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'CLIENT_SETTLEMENT_FORMAT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(66707565385939675)
,p_db_column_name=>'CLIENT_NAME'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Client Name'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'CLIENT_NAME'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(66707692253939675)
,p_db_column_name=>'CLIENT_LEVEL'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Client Level'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'CLIENT_LEVEL'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(66707772201939676)
,p_db_column_name=>'PROCESSOR_CLIENT_ID'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Processor Client Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'PROCESSOR_CLIENT_ID'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(60880265228267428)
,p_db_column_name=>'FEES_COMM_INV_TEMPLATE'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'CMP0630 Invoice Format'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_heading_alignment=>'LEFT'
,p_static_id=>'FEES_COMM_INV_TEMPLATE'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(59447874133714970)
,p_db_column_name=>'ALTERNATE_ID'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Alternate Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'ALTERNATE_ID'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(27861838114913825)
,p_db_column_name=>'CARD_NUMBER_REQUIRED'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'Card Number Required'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CARD_NUMBER_REQUIRED'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(27862364615931948)
,p_db_column_name=>'PROXY_CARD_NUMBER_REQUIRED'
,p_display_order=>13
,p_column_identifier=>'M'
,p_column_label=>'Proxy Card Number Required'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PROXY_CARD_NUMBER_REQUIRED'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(38860354130879534)
,p_db_column_name=>'TRANSACTION_TIMESTAMP_REQUIRED'
,p_display_order=>14
,p_column_identifier=>'N'
,p_column_label=>'Transaction Timestamp Required'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'TRANSACTION_TIMESTAMP_REQUIRED'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(66707965894939676)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'31175'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'CLIENT_NAME:CLIENT_LEVEL:PROCESSOR_CLIENT_ID:CLIENT_MCP_FLAG:CLIENT_EXCEL_INDICATOR:CLIENT_SETTLEMENT_FORMAT:FEES_COMM_INV_TEMPLATE:ALTERNATE_ID:CARD_NUMBER_REQUIRED:PROXY_CARD_NUMBER_REQUIRED:TRANSACTION_TIMESTAMP_REQUIRED:'
,p_flashback_enabled=>'N'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(66708081324939676)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(66706685441939675)
,p_button_name=>'CREATE'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(144960334777324670)
,p_button_image_alt=>'Create'
,p_button_position=>'RIGHT_OF_IR_SEARCH_BAR'
,p_button_redirect_url=>'f?p=&APP_ID.:156:&SESSION.::&DEBUG.:156::'
,p_grid_new_grid=>false
,p_security_scheme=>wwv_flow_api.id(145183515750747381)
);
end;
/
begin
wwv_flow_api.import_end(p_auto_install_sup_obj => nvl(wwv_flow_application_install.get_auto_install_sup_obj, false));
commit;
end;
/
set verify on feedback on define on
prompt  ...done
