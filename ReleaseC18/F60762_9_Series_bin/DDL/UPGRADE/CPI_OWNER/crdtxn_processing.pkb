/* Formatted on 2/19/2018 9:59:43 PM (QP5 v5.227.12220.39724) */
CREATE OR REPLACE PACKAGE BODY CPI_OWNER.crdtxn_processing
AS
   /*****************************************************************************************************
      PURPOSE:    To process incoming TJF files

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        17/08/2015  JT               Initial version.
      2.0        22/2/2016   Lavanya R        Fix for APWAS-17447 2 Series Bin Changes in CPF
      3.0        22/2/2016   Srini            Fix for APWAS-17498  PROD - crdtxn_processing.process_line fails for blank lines and |A character in file
      4.0       07/09/2016   Herald           APWAS-19575 Added parallel hint and txn_sequence column to tjf_records_stg insert statement
      5.0        07/08/2018  Srini             DE44219- added constraints and new columns to package
      6.0       19/02/2018  Srini             Add exception section to consider the missing cards
      7.0        19/02/2018  Swati            F60762 : Added code for 9 series BIN Support and made it configurable
     *****************************************************************************************************/

   ----------------------------------------------------------------------------------------------------------------------------------

   FUNCTION luhn_check (p_pan VARCHAR2)
      RETURN BOOLEAN
   IS
      v_multiplier   SIMPLE_INTEGER := 0;
      v_number       SIMPLE_INTEGER := 0;
      v_sum          SIMPLE_INTEGER := 0;
      v_pos          SIMPLE_INTEGER := 0;
   BEGIN
      v_multiplier := 1;
      v_sum := 0;

      FOR v_pos IN REVERSE 1 .. LENGTH (p_pan)
      LOOP
         v_number := TO_NUMBER (SUBSTR (p_pan, v_pos, 1)) * v_multiplier;
         v_sum := v_sum + FLOOR (v_number / 10) + MOD (v_number, 10);
         v_multiplier := 3 - v_multiplier;
      END LOOP;

      RETURN (MOD (v_sum, 10) = 0);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN FALSE;
   END luhn_check;

   ----------------------------------------------------------------------------------------------------------------------------------

   FUNCTION validate_card_no (p_pan VARCHAR2)
      RETURN BOOLEAN
   IS
      v_number   NUMBER;
      v_cnt      NUMBER;
   BEGIN
      SELECT COUNT (1)
        INTO v_cnt
        FROM STORED_PARAMETER_VALUE
       WHERE     NAMESPACE = 'CARD_AND_BIN_CHECK'
             AND VALUE = SUBSTR (p_pan, 1, 1); --------------------Added for bin configuration

      IF                            /*SUBSTR(p_pan,1,1) NOT IN ('2','4','5')*/
           v_cnt = 0
         OR LENGTH (p_pan) <> 16
         OR REGEXP_INSTR (p_pan, '[^[:digit:]]+') > 0
      THEN
         RETURN FALSE;
      END IF;                                 

      RETURN luhn_check (p_pan);
   EXCEPTION
      WHEN OTHERS
      THEN
         standard_error.when_others_exception (
            p_program           => 'crdtxn_processing.validate_card_no',
            p_additional_info   => 'p_pan: ' || p_pan);
   END validate_card_no;

   ----------------------------------------------------------------------------------------------------------------------------------

   -- Retrieve Changed Pan record (Creating new if required)

   PROCEDURE retrieve_card_no (p_pan                  IN     VARCHAR2,
                               p_cardno_is_optional   IN     VARCHAR2,
                               p_changed_pan             OUT VARCHAR2,
                               p_bin                     OUT VARCHAR2)
   IS
      v_seq_no      VARCHAR2 (12);
      v_last_four   VARCHAR2 (4);
      v_pan         VARCHAR2 (32767);
   BEGIN
      v_pan := TRIM (LTRIM (p_pan, '0'));
      p_changed_pan := NULL;

      IF v_pan IS NOT NULL
      THEN
         SELECT MIN (changed_pan), MIN (bin)
           INTO p_changed_pan, p_bin
           FROM pan_xref
          WHERE pan = v_pan;

         IF p_changed_pan IS NULL
         THEN
            IF validate_card_no (v_pan)
            THEN
               v_seq_no := TO_CHAR (seq_pan_xref.NEXTVAL);
               v_last_four := SUBSTR (v_pan, -4, 4);
               p_changed_pan := v_seq_no || v_last_four;
               p_bin := SUBSTR (v_pan, 1, 6);

               BEGIN
                  INSERT INTO pan_xref (pan,
                                        changed_pan,
                                        bin,
                                        last_four,
                                        pan_xref_id,
                                        source,
                                        creation_datetime)
                       VALUES (v_pan,
                               p_changed_pan,
                               p_bin,
                               v_last_four,
                               v_seq_no,
                               'TJF',
                               SYSTIMESTAMP);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     p_changed_pan := NULL;

                     SELECT MIN (changed_pan), MIN (bin)
                       INTO p_changed_pan, p_bin
                       FROM pan_xref
                      WHERE pan = v_pan;

                     standard_error.when_others_exception (
                        p_program           => 'crdtxn_processing.retrieve_card_no',
                        p_additional_info   =>    'p_pan: '
                                               || v_pan
                                               || ' p_changed_pan: '
                                               || p_changed_pan,
                        p_raise_exception   => FALSE);
               END;

               COMMIT;

               p_bin := SUBSTR (v_pan, 1, 6);
            ELSE
               IF p_cardno_is_optional = 'Y'
               THEN
                  p_changed_pan := NULL;
                  p_bin := NULL;
               ELSE
                  p_changed_pan := '2000008944370000';
                  p_bin := SUBSTR (v_pan, 1, 6);
               END IF;
            END IF;
         END IF;
      ELSE
         IF p_cardno_is_optional = 'Y'
         THEN
            p_changed_pan := NULL;
            p_bin := NULL;
         ELSE
            p_changed_pan := '2000008944370000';
            p_bin := NULL;
         END IF;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         standard_error.when_others_exception (
            p_program           => 'crdtxn_processing.retrieve_card_no',
            p_additional_info   => 'p_pan: ' || p_pan);
   END retrieve_card_no;

   ----------------------------------------------------------------------------------------------------------------------------------

   -- Obfuscate any card numbers found within a string

   FUNCTION obfuscate_pan_numbers (p_string VARCHAR2)
      RETURN VARCHAR2
   IS
      v_return_string   VARCHAR2 (32767);
      v_search_pos      PLS_INTEGER;
      v_number_pos      PLS_INTEGER;
      v_change_pan      VARCHAR2 (16);
      v_bin             VARCHAR2 (6 CHAR);
   BEGIN
      v_search_pos := 1;
      v_return_string := '';

      WHILE v_search_pos > 0
      LOOP
         v_number_pos :=
            REGEXP_INSTR (p_string, '[[:digit:]]{16}', v_search_pos);

         IF v_number_pos = 0
         THEN
            v_return_string :=
               v_return_string || SUBSTR (p_string, v_search_pos);
            v_search_pos := 0;
         ELSE
            v_return_string :=
                  v_return_string
               || SUBSTR (p_string,
                          v_search_pos,
                          (v_number_pos - v_search_pos));
            retrieve_card_no (p_pan                  => SUBSTR (p_string, v_number_pos, 16),
                              p_cardno_is_optional   => 'Y',
                              p_changed_pan          => v_change_pan,
                              p_bin                  => v_bin);

            IF v_change_pan IS NULL
            THEN
               v_return_string :=
                  v_return_string || SUBSTR (p_string, v_number_pos, 1);
               v_search_pos := v_number_pos + 1;
            ELSE
               v_return_string := v_return_string || v_change_pan;
               v_search_pos := v_number_pos + 16;
            END IF;
         END IF;
      END LOOP;

      RETURN v_return_string;
   EXCEPTION
      WHEN OTHERS
      THEN
         standard_error.when_others_exception (
            p_program           => 'crdtxn_processing.obfuscate_pan_numbers',
            p_additional_info   => 'p_string: ' || p_string);
   END obfuscate_pan_numbers;

   ----------------------------------------------------------------------------------------------------------------------------------

   -- Remotely update TJF_RECORDS_STG with 08 Balance records

   PROCEDURE update_rt08_all_records_stg (p_input_data_id     IN NUMBER,
                                          p_run_id            IN NUMBER,
                                          p_start_time        IN DATE,
                                          p_business_date     IN DATE,
                                          p_08_record_count   IN NUMBER)
   IS
   BEGIN
      tjf_rt08_all_processing.update_tjf_records_stg (
         p_input_data_id     => p_input_data_id,
         p_run_id            => p_run_id,
         p_start_time        => p_start_time,
         p_business_date     => p_business_date,
         p_08_record_count   => p_08_record_count);
   EXCEPTION
      WHEN OTHERS
      THEN
         standard_error.when_others_exception (
            p_program           => 'crdtxn_processing.update_rt08_all_records_stg',
            p_additional_info   =>    'p_input_data_id: '
                                   || p_input_data_id
                                   || ' p_run_id: '
                                   || p_run_id
                                   || ' p_start_time: '
                                   || p_start_time);
   END update_rt08_all_records_stg;

   ----------------------------------------------------------------------------------------------------------------------------------

   -- Process the TJF file

   PROCEDURE process_file_import_data (p_input_data_id NUMBER)
   IS
      v_location                 VARCHAR2 (100);
      v_read_ptr                 BINARY_INTEGER := 0;
      v_run_id                   NUMBER;
      v_start_time               DATE;
      v_string_ptr               VARCHAR2 (2);
      v_messages                 VARCHAR2 (32767);
      v_header_business_date     DATE;
      v_bin                      VARCHAR2 (6 CHAR);
      v_account_number           VARCHAR2 (23 CHAR);
      v_business_date            DATE;
      v_bulk_limit               PLS_INTEGER := 10000;
      v_string_table             string_table;

      CURSOR cur_file_import_data
      IS
         SELECT file_data
           FROM file_import_data
          WHERE input_data_id = p_input_data_id;

      buf_file_import_data       cur_file_import_data%ROWTYPE;

      TYPE typ_record_type_count IS TABLE OF NUMBER
         INDEX BY VARCHAR2 (2);

      v_record_type_count        typ_record_type_count;

      TYPE typ_remap_element IS RECORD
      (
         start_pos            card_number_mapping.start_pos%TYPE,
         cardno_length        card_number_mapping.cardno_length%TYPE,
         primary_cardno       card_number_mapping.primary_cardno%TYPE,
         cardno_is_optional   card_number_mapping.cardno_is_optional%TYPE,
         window_length        card_number_mapping.window_length%TYPE
      );

      TYPE typ_remap_table IS TABLE OF typ_remap_element
         INDEX BY BINARY_INTEGER;

      TYPE typ_remap_store IS TABLE OF typ_remap_table
         INDEX BY VARCHAR2 (2);

      v_remap_store              typ_remap_store;

      TYPE typ_tmp_tjf_records_rest IS TABLE OF tmp_tjf_records_rest%ROWTYPE
         INDEX BY BINARY_INTEGER;

      buf_tmp_tjf_records_rest   typ_tmp_tjf_records_rest;

      TYPE typ_tmp_tjf_records_rt08 IS TABLE OF tmp_tjf_records_rt08%ROWTYPE
         INDEX BY BINARY_INTEGER;

      buf_tmp_tjf_records_rt08   typ_tmp_tjf_records_rt08;

      --------------------------------------------------------------------------------------------------------------------------------

      -- Replace any card numbers with their obfuscated version

      PROCEDURE obfuscate_card_numbers (
         p_record_type      IN            VARCHAR2,
         p_the_record_in    IN OUT NOCOPY VARCHAR2,
         p_the_record_out      OUT        VARCHAR2,
         p_bin                 OUT        VARCHAR2)
      IS
         v_ptr              BINARY_INTEGER := 0;
         v_pos              SIMPLE_INTEGER := 0;
         v_start_pos        SIMPLE_INTEGER := 0;
         v_pan              VARCHAR2 (32767);
         v_change_pan       VARCHAR2 (16);
         v_changed_string   VARCHAR2 (32767);
         v_bin              VARCHAR2 (6 CHAR);
         v_remap_table      typ_remap_table;
      BEGIN
         p_the_record_out := '';
         v_pos := 1;

         -- Is remapping card numbers required
         IF v_remap_store.EXISTS (p_record_type)
         THEN
            v_remap_table := v_remap_store (p_record_type);

            v_ptr := v_remap_table.FIRST;

            WHILE v_ptr IS NOT NULL
            LOOP
               v_start_pos := v_remap_table (v_ptr).start_pos;

               IF v_start_pos > v_pos
               THEN
                  p_the_record_out :=
                        p_the_record_out
                     || SUBSTR (p_the_record_in, v_pos, v_start_pos - v_pos);
                  v_pos := v_start_pos;
               END IF;

               IF v_pos <= v_start_pos
               THEN
                  IF v_remap_table (v_ptr).window_length > 0
                  THEN
                     v_changed_string :=
                        obfuscate_pan_numbers (
                           SUBSTR (p_the_record_in,
                                   v_start_pos,
                                   v_remap_table (v_ptr).window_length));
                     p_the_record_out := p_the_record_out || v_changed_string;
                     v_pos := v_pos + v_remap_table (v_ptr).window_length;
                  ELSE
                     v_pan :=
                        SUBSTR (p_the_record_in,
                                v_start_pos,
                                v_remap_table (v_ptr).cardno_length);
                     retrieve_card_no (
                        p_pan                  => v_pan,
                        p_cardno_is_optional   => CASE
                                                    WHEN v_remap_table (
                                                            v_ptr).primary_cardno =
                                                            'Y'
                                                    THEN
                                                       'N'
                                                    ELSE
                                                       v_remap_table (v_ptr).cardno_is_optional
                                                 END,
                        p_changed_pan          => v_change_pan,
                        p_bin                  => v_bin);

                     IF v_remap_table (v_ptr).primary_cardno = 'Y'
                     THEN
                        p_bin := v_bin;
                     END IF;

                     -- If a cardno was returned then update record else add next 16 chars.
                     IF v_change_pan IS NOT NULL
                     THEN
                        p_the_record_out := p_the_record_out || v_change_pan;
                     ELSE
                        p_the_record_out :=
                              p_the_record_out
                           || SUBSTR (p_the_record_in, v_pos, 16);
                     END IF;

                     v_pos := v_pos + v_remap_table (v_ptr).cardno_length;
                  END IF;
               ELSE
                  standard_error.log_warning_error (
                     p_program           => 'crdtxn_processing.obfuscate_card_numbers',
                     p_message           => 'Invalid configuration detected. Ignoring configuration entry.',
                     p_additional_info   =>    'p_record_type: '
                                            || p_record_type
                                            || ' v_start_pos '
                                            || v_start_pos
                                            || ' v_pos '
                                            || v_pos);
               END IF;

               v_ptr := v_remap_table.NEXT (v_ptr);
            END LOOP;
         END IF;

         p_the_record_out :=
            p_the_record_out || SUBSTR (p_the_record_in, v_pos);
      EXCEPTION
         WHEN OTHERS
         THEN
            standard_error.when_others_exception (
               p_program           => 'crdtxn_processing.obfuscate_card_numbers',
               p_additional_info   => 'p_record_type: ' || p_record_type);
      END obfuscate_card_numbers;

      --------------------------------------------------------------------------------------------------------------------------------

      PROCEDURE load_card_number_mapping
      IS
         CURSOR cur_card_number_mapping
         IS
              SELECT *
                FROM card_number_mapping
            ORDER BY record_type, start_pos;

         TYPE typ_card_number_mapping
            IS TABLE OF cur_card_number_mapping%ROWTYPE
            INDEX BY BINARY_INTEGER;

         buf_card_number_mapping   typ_card_number_mapping;
         v_record_type             card_number_mapping.record_type%TYPE;
         v_card_ptr                card_number_mapping.start_pos%TYPE;
         v_ptr                     BINARY_INTEGER;
      BEGIN
         OPEN cur_card_number_mapping;

         FETCH cur_card_number_mapping
            BULK COLLECT INTO buf_card_number_mapping;

         CLOSE cur_card_number_mapping;

         v_ptr := buf_card_number_mapping.FIRST;

         WHILE v_ptr IS NOT NULL
         LOOP
            v_record_type := buf_card_number_mapping (v_ptr).record_type;

            IF v_remap_store.EXISTS (v_record_type)
            THEN
               v_card_ptr := v_remap_store (v_record_type).COUNT + 1;
            ELSE
               v_card_ptr := 1;
            END IF;

            v_remap_store (v_record_type) (v_card_ptr).start_pos :=
               buf_card_number_mapping (v_ptr).start_pos;
            v_remap_store (v_record_type) (v_card_ptr).cardno_length :=
               buf_card_number_mapping (v_ptr).cardno_length;
            v_remap_store (v_record_type) (v_card_ptr).primary_cardno :=
               buf_card_number_mapping (v_ptr).primary_cardno;
            v_remap_store (v_record_type) (v_card_ptr).cardno_is_optional :=
               buf_card_number_mapping (v_ptr).cardno_is_optional;
            v_remap_store (v_record_type) (v_card_ptr).window_length :=
               buf_card_number_mapping (v_ptr).window_length;
            v_ptr := buf_card_number_mapping.NEXT (v_ptr);
         END LOOP;
      EXCEPTION
         WHEN OTHERS
         THEN
            standard_error.when_others_exception (
               p_program => 'crdtxn_processing.load_card_number_mapping');
      END load_card_number_mapping;

      --------------------------------------------------------------------------------------------------------------------------------

      PROCEDURE process_line (p_line VARCHAR2)
      IS
         v_record_type           tmp_tjf_records_rest.record_type%TYPE;
         v_the_record_in         tmp_tjf_records_rest.the_record%TYPE;
         v_the_record_remapped   tmp_tjf_records_rest.the_record%TYPE;
         v_write_ptr             BINARY_INTEGER := 0;
      BEGIN
         v_record_type := SUBSTR (p_line, 1, 2);
         v_the_record_in := RTRIM (SUBSTR (p_line, 3));

         IF v_record_type IS NOT NULL
         THEN
            IF v_record_type = '00'
            THEN
               v_write_ptr := buf_tmp_tjf_records_rest.COUNT;

               buf_tmp_tjf_records_rest (v_write_ptr).the_record :=
                  SUBSTR (v_the_record_in, 51);
               buf_tmp_tjf_records_rest (v_write_ptr).start_time :=
                  v_start_time;
               buf_tmp_tjf_records_rest (v_write_ptr).run_id := v_run_id;
               buf_tmp_tjf_records_rest (v_write_ptr).record_type :=
                  v_record_type;
               buf_tmp_tjf_records_rest (v_write_ptr).bin := NULL;
               buf_tmp_tjf_records_rest (v_write_ptr).input_data_id :=
                  p_input_data_id;

               v_header_business_date :=
                  TO_DATE (SUBSTR (v_the_record_in, 55, 6), 'YYMMDD');
            ELSIF v_record_type = '99'
            THEN
               v_write_ptr := buf_tmp_tjf_records_rest.COUNT;

               buf_tmp_tjf_records_rest (v_write_ptr).the_record :=
                  SUBSTR (v_the_record_in, 51);
               buf_tmp_tjf_records_rest (v_write_ptr).start_time :=
                  v_start_time;
               buf_tmp_tjf_records_rest (v_write_ptr).run_id := v_run_id;
               buf_tmp_tjf_records_rest (v_write_ptr).record_type :=
                  v_record_type;
               buf_tmp_tjf_records_rest (v_write_ptr).bin := NULL;
               buf_tmp_tjf_records_rest (v_write_ptr).input_data_id :=
                  p_input_data_id;
            ELSE
               obfuscate_card_numbers (
                  p_record_type      => v_record_type,
                  p_the_record_in    => v_the_record_in,
                  p_the_record_out   => v_the_record_remapped,
                  p_bin              => v_bin);

               IF v_record_type = '08'
               THEN
                  v_account_number := TRIM (SUBSTR (v_the_record_in, 13, 23));
                  v_business_date :=
                     TO_DATE (TRIM (SUBSTR (v_the_record_in, 123, 8)),
                              'YYYYMMDD');

                  v_write_ptr := buf_tmp_tjf_records_rt08.COUNT;

                  buf_tmp_tjf_records_rt08 (v_write_ptr).account_number :=
                     v_account_number;
                  buf_tmp_tjf_records_rt08 (v_write_ptr).balance_from_date :=
                     v_business_date;
                  buf_tmp_tjf_records_rt08 (v_write_ptr).balance_to_date :=
                     TO_DATE ('99991231', 'YYYYMMDD');
                  buf_tmp_tjf_records_rt08 (v_write_ptr).the_record :=
                     v_the_record_remapped;
                  buf_tmp_tjf_records_rt08 (v_write_ptr).start_time :=
                     v_start_time;
                  buf_tmp_tjf_records_rt08 (v_write_ptr).run_id := v_run_id;
                  buf_tmp_tjf_records_rt08 (v_write_ptr).record_type :=
                     v_record_type;
                  buf_tmp_tjf_records_rt08 (v_write_ptr).bin := v_bin;
                  buf_tmp_tjf_records_rt08 (v_write_ptr).input_data_id :=
                     p_input_data_id;
               ELSE
                  v_write_ptr := buf_tmp_tjf_records_rest.COUNT;

                  buf_tmp_tjf_records_rest (v_write_ptr).the_record :=
                     v_the_record_remapped;
                  buf_tmp_tjf_records_rest (v_write_ptr).start_time :=
                     v_start_time;
                  buf_tmp_tjf_records_rest (v_write_ptr).run_id := v_run_id;
                  buf_tmp_tjf_records_rest (v_write_ptr).record_type :=
                     v_record_type;
                  buf_tmp_tjf_records_rest (v_write_ptr).bin := v_bin;
                  buf_tmp_tjf_records_rest (v_write_ptr).input_data_id :=
                     p_input_data_id;
               END IF;
            END IF;

            IF v_record_type_count.EXISTS (v_record_type)
            THEN
               v_record_type_count (v_record_type) :=
                  v_record_type_count (v_record_type) + 1;
            ELSE
               standard_audit.log_entry (
                  p_program     => 'crdtxn_processing.process_line',
                  p_step_name   => 'v_record_type',
                  p_log_entry   => 'record ' || SUBSTR (p_line, 1, 77),
                  p_level       => 2);
               v_record_type_count (v_record_type) := 1;
            END IF;
         END IF;
      EXCEPTION
         WHEN OTHERS
         THEN
            standard_error.when_others_exception (
               p_program => 'crdtxn_processing.process_line');
      END process_line;

      --------------------------------------------------------------------------------------------------------------------------------

      PROCEDURE temp_to_staging
      IS
      BEGIN
         standard_audit.log_entry (
            p_program     => 'crdtxn_processing.temp_to_staging',
            p_step_name   => 'Update tjf_records_stg',
            p_log_entry   => 'Update global staging table tjf_records_stg',
            p_level       => 2);

         INSERT /*+ APPEND PARALLEL(tjf_records_stg) */
               INTO  tjf_records_stg (the_record,
                                      start_time,
                                      run_id,
                                      record_type,
                                      bin,
                                      input_data_id,
                                      txn_sequence)
            SELECT the_record,
                   v_start_time,
                   v_run_id,
                   record_type,
                   bin,
                   p_input_data_id,
                   tjf_txn_sequence.NEXTVAL
              FROM tmp_tjf_records_rest;

         COMMIT;

         standard_audit.log_entry (
            p_program     => 'crdtxn_processing.temp_to_staging',
            p_step_name   => 'Update tjf_rt08_all',
            p_log_entry   => 'Update global staging table tjf_rt08_all',
            p_level       => 2);

         INSERT INTO tjf_rt08_all (account_number,
                                   balance_from_date,
                                   balance_to_date,
                                   the_record,
                                   start_time,
                                   run_id,
                                   record_type,
                                   bin,
                                   input_data_id)
            SELECT account_number,
                   balance_from_date,
                   balance_to_date,
                   the_record,
                   v_start_time,
                   v_run_id,
                   record_type,
                   bin,
                   p_input_data_id
              FROM tmp_tjf_records_rt08;

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            standard_error.when_others_exception (
               p_program => 'crdtxn_processing.temp_to_staging');
      END temp_to_staging;

   --------------------------------------------------------------------------------------------------------------------------------

   BEGIN
      standard_audit.open_audit_trail (
         p_namespace   => 'CRDTXN_PROCESSING',
         p_program     => 'crdtxn_processing.process_file_import_data');

      v_location := 'Start';
      v_run_id := seq_ips_ext_log_run_id.NEXTVAL;
      v_start_time := SYSDATE;

      v_location := 'Create External tjf';
      standard_audit.log_entry (
         p_program     => 'crdtxn_processing.process_file_import_data',
         p_step_name   => v_location,
         p_log_entry   => 'Processing input_data_id: ' || p_input_data_id,
         p_level       => 2);

      v_location := 'Truncate Tables';
      standard_audit.log_entry (
         p_program     => 'crdtxn_processing.process_file_import_data',
         p_step_name   => v_location,
         p_log_entry   => 'TRUNCATE tmp_tjf_records_rest and tmp_tjf_records_rt08',
         p_level       => 2);

      EXECUTE IMMEDIATE 'TRUNCATE TABLE tmp_tjf_records_rest';

      EXECUTE IMMEDIATE 'TRUNCATE TABLE tmp_tjf_records_rt08';

      v_location := 'Load card mapping';
      standard_audit.log_entry (
         p_program     => 'crdtxn_processing.process_file_import_data',
         p_step_name   => v_location,
         p_log_entry   => 'Load card mapping',
         p_level       => 2);

      load_card_number_mapping;

      v_location := 'Open cur_file_import_data';
      standard_audit.log_entry (
         p_program     => 'crdtxn_processing.process_file_import_data',
         p_step_name   => v_location,
         p_log_entry   => 'Open cur_file_import_data',
         p_level       => 2);

      OPEN cur_file_import_data;

      FETCH cur_file_import_data INTO buf_file_import_data;

      IF cur_file_import_data%NOTFOUND
      THEN
         standard_error.raise_program_error (
            p_program   => 'crdtxn_processing.process_file_import_data',
            p_message   =>    'File Import record not found for file import ID '
                           || p_input_data_id);
      END IF;

      CLOSE cur_file_import_data;

      blob_utilities.open_blob (buf_file_import_data.file_data);

      v_location := 'Start fetch loop';
      standard_audit.log_entry (
         p_program     => 'crdtxn_processing.process_file_import_data',
         p_step_name   => v_location,
         p_log_entry   => 'Start data fetch loop. Bulk Limit ' || v_bulk_limit,
         p_level       => 2);

      LOOP
         blob_utilities.fetch_blob_to_string_table (v_string_table,
                                                    v_bulk_limit);

         IF v_string_table.COUNT = 0
         THEN
            EXIT;
         END IF;

         v_read_ptr := v_string_table.FIRST;

         WHILE v_read_ptr IS NOT NULL
         LOOP
            process_line (v_string_table (v_read_ptr));
            v_location := 'Next line ptr';
            v_read_ptr := v_string_table.NEXT (v_read_ptr);
         END LOOP;

         v_location := 'Bulk update tmp_tjf_records_rt08';

         FORALL i
             IN buf_tmp_tjf_records_rt08.FIRST ..
                buf_tmp_tjf_records_rt08.LAST
            INSERT INTO tmp_tjf_records_rt08
                 VALUES buf_tmp_tjf_records_rt08 (i);

         COMMIT;

         v_location := 'Bulk update tmp_tjf_records_rest';

         FORALL i
             IN buf_tmp_tjf_records_rest.FIRST ..
                buf_tmp_tjf_records_rest.LAST
            INSERT INTO tmp_tjf_records_rest
                 VALUES buf_tmp_tjf_records_rest (i);

         COMMIT;

         buf_tmp_tjf_records_rt08.DELETE;
         buf_tmp_tjf_records_rest.DELETE;

         v_location := 'END OUTER LOOP';
      END LOOP;

      v_location := 'Close BLOB';

      standard_audit.log_entry (
         p_program     => 'crdtxn_processing.process_file_import_data',
         p_step_name   => v_location,
         p_log_entry   => 'Close BLOB',
         p_level       => 3);

      blob_utilities.close_blob;

      v_location := 'Update global staging';

      standard_audit.log_entry (
         p_program     => 'crdtxn_processing.process_file_import_data',
         p_step_name   => v_location,
         p_log_entry   => 'Update tjf_records_stg',
         p_level       => 2);

      temp_to_staging;

      v_location := 'Call update_rt08_all_records_stg';

      standard_audit.log_entry (
         p_program     => 'crdtxn_processing.process_file_import_data',
         p_step_name   => v_location,
         p_log_entry   => 'Calling update_rt08_all_records_stg',
         p_level       => 2);

      IF v_record_type_count.EXISTS ('08')
      THEN
         update_rt08_all_records_stg (
            p_input_data_id     => p_input_data_id,
            p_run_id            => v_run_id,
            p_start_time        => v_start_time,
            p_business_date     => v_header_business_date,
            p_08_record_count   => v_record_type_count ('08'));
      END IF;

      v_location := 'Write Log';

      v_string_ptr := v_record_type_count.FIRST;
      v_messages := 'Records updated ' || CHR (10) || CHR (10);

      WHILE v_string_ptr IS NOT NULL
      LOOP
         v_messages :=
               v_messages
            || CHR (10)
            || 'Record Type '
            || v_string_ptr
            || ': '
            || v_record_type_count (v_string_ptr)
            || ' records';

         v_string_ptr := v_record_type_count.NEXT (v_string_ptr);
      END LOOP;

      standard_audit.log_entry (
         p_program     => 'crdtxn_processing.process_file_import_data',
         p_step_name   => v_location,
         p_log_entry   => v_messages,
         p_level       => 2);

      standard_audit.close_audit_trail (
         p_program => 'crdtxn_processing.process_file_import_data');
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         standard_audit.close_audit_trail (
            p_program => 'crdtxn_processing.process_file_import_data');
         standard_error.when_others_exception (
            p_program           => 'crdtxn_processing.process_file_import_data',
            p_location          => v_location,
            p_additional_info   => 'p_input_data_id: ' || p_input_data_id);
   END process_file_import_data;
----------------------------------------------------------------------------------------------------------------------------------

END crdtxn_processing;
/