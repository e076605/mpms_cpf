delete from CPI_OWNER.stored_parameter_value where namespace='CARD_AND_BIN_CHECK';
delete from CPI_OWNER.stored_parameter_namespace where namespace='CARD_AND_BIN_CHECK';


INSERT INTO CPI_OWNER.STORED_PARAMETER_NAMESPACE (NAMESPACE, DESCRIPTION)
     VALUES ('CARD_AND_BIN_CHECK', 'Starting digit of CardNumbers to be processed by CPF');

INSERT INTO CPI_OWNER.stored_parameter_value  (NAMESPACE,
                                     NAME,
                                     DESCRIPTION,
                                     VALUE,
                                     LAST_UPDATED,
                                     LAST_UPDATED_BY)
     VALUES ('CARD_AND_BIN_CHECK',
                   'BIN1',
                   'Starting digit of CardNumbers to be processed by CPF',
                   '2',
                   SYSDATE,
                   'CPI_OWNER');

INSERT INTO CPI_OWNER.stored_parameter_value (NAMESPACE,
                                     NAME,
                                     DESCRIPTION,
                                     VALUE,
                                     LAST_UPDATED,
                                     LAST_UPDATED_BY)
    VALUES ('CARD_AND_BIN_CHECK',
                   'BIN2',
                   'Starting digit of CardNumbers to be processed by CPF',
                   '4',
                   SYSDATE,
                   'CPI_OWNER');
     
 INSERT INTO CPI_OWNER.stored_parameter_value  (NAMESPACE,
                                     NAME,
                                     DESCRIPTION,
                                     VALUE,
                                     LAST_UPDATED,
                                     LAST_UPDATED_BY)
    VALUES ('CARD_AND_BIN_CHECK',
                   'BIN3',
                   'Starting digit of CardNumbers to be processed by CPF',
                   '5',
                   SYSDATE,
                   'CPI_OWNER');
     
  INSERT INTO CPI_OWNER.stored_parameter_value (NAMESPACE,
                                     NAME,
                                     DESCRIPTION,
                                     VALUE,
                                     LAST_UPDATED,
                                     LAST_UPDATED_BY)
     VALUES ('CARD_AND_BIN_CHECK',
                   'BIN4',
                   'Starting digit of CardNumbers to be processed by CPF',
                   '9',
                   SYSDATE,
                   'CPI_OWNER');
 
 COMMIT ;
 