CREATE OR REPLACE PACKAGE BODY GLOBAL_STAGING_AREA.pkg_ins_issuer_fees
is

/*****************************************************************************************************
   PURPOSE:    Processing required to store scheme fees transactions (TJF type 4)

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        09/04/2013  Tim Berry        Initial version.
   1.1       27/11/2015   Srini            Changed the action date logic
*****************************************************************************************************/

  vETL_DATE             date;
  InvalidEnvironment    exception;
  InvalidFile           exception;
  ProcessingProblem     exception;
  vFILE_SEQ1            number;
  vFILE_SEQ2            number;
  ErrorMessage          varchar2(250);

  procedure control (pINEnv IN VARCHAR2)
  is
    cursor fetchTJFType4 (aFileSeq number, bFileSeq number) is
    select
      fees.TXN_SEQUENCE
      ,fees.INPUT_DATA_ID
      ,case when fees.TXN_AMT_DBCR_IND = 'DB'
         then (fees.RECON_AMT / rpad(1,(1 * fees.RECON_CCY_CDE_EXP) + 1, '0')) * -1
         else fees.RECON_AMT / rpad(1,(1 * fees.RECON_CCY_CDE_EXP) + 1, '0')
       end as recon_reporting_amt
      ,fees.company_id
      ,fees.program_account_code
      ,fees.program_portfolio_code
      ,fees.account_number
      ,fees.ol_1
      ,fees.ol_2
      ,fees.ol_3
      ,fees.card_number
      ,fees.GCMS_PROCESSING_CDE
      ,to_date(fees.sett_date,'YYMMDD') as  sett_date
      ,fees.txn_amt
      ,fees.txn_amt_dbcr_ind
      ,fees.txn_ccy_cde
      ,fees.TXN_CCY_CDE_EXP
      ,fees.RECON_AMT
      ,fees.RECON_CCY_CDE
      ,fees.RECON_CCY_CDE_EXP
      ,fees.FUNCTION_CDE
      ,fees.message_reason_cde
      ,fees.APPROVAL_CDE
      ,fees.ca_terminal_id
      ,fees.RECEIVING_INST
      ,fees.FORWARDING_INST_ID_CDE
      ,fees.RETRIEVAL_REF_NBR
      ,fees.CONV_RATE_RECON_EXP
      ,fees.CONV_RATE_RECON_FACTOR
      ,fees.ORIGINAL_TXN_AMT
      ,fees.ACQUIRER_REF_DATA
      ,fees.FEE_COLL_CONTROL_NBR
      --,ACTION_DATE JIRA-9648
      ,to_date(DECODE(fees.ACTION_DATE,'000000','',fees.ACTION_DATE),'YYMMDD') as action_date
      ,fees.REVERSAL_IND
      ,fees.MEMBER_RECON_IND_1
      ,fees.SETT_SERVICE_XFER_AGENT_ID_CDE
      ,fees.SETT_SERVICE_XFER_AGENT_ACCNT
      ,fees.SETT_SERVICE_ID_CDE
      ,fees.IPS_PROCESS_CDE
      ,fees.IPS_TXN_CDE
      ,fees.IPS_TXN_SUFFIX
      ,fees.IPS_SOURCE
      ,fees.NETWORK_ID
      ,to_date(fees.ACQUIRER_SETT_DATE,'YYMMDD') as ACQUIRER_SETT_DATE
      ,fees.CROSS_BORDER_IND
      ,fees.SUSPENDED_IND
      ,fees."1740_DIRECTION" as "1740_DIRECTION"
      ,fees.CARD_BRAND_IND
      ,to_date(fees.SETT_SERVICE_VALUE_DATE,'YYMMDD') as SETT_SERVICE_VALUE_DATE
      ,fees.ISSUER_CNTRY_CDE_ON_ICA
      ,fees.PROD_CDE_ASSD_TO_BIN
      ,fees.LEGAL_ENTITY_ON_ICA
      ,fees.SECND_LGL_ENTITY_ASSIGN_ICA
      ,fees.FIN_PARENT_BUS_UNIT_NO
      ,fees.FIN_PARENT_CHILD_UNIT_NO
      ,cur.iso_code as currency
      ,fees.ICA_ON_BIN as ica
      ,icg.bin_sponsor_id
      ,bsp.name as bin_sponsor_name
      ,icg.parent_business_unit_name
      ,bin.child_business_unit_name
      ,fees.MESSAGE_TYPE_IDENTIFIER
      ,fees.TXN_DESC
      ,to_date(fees.sett_date,'YYMMDD')     as business_date
      ,case when fees.TXN_AMT_DBCR_IND = 'DB'
         then (fees.TXN_AMT / rpad(1,(1 * fees.TXN_CCY_CDE_EXP) + 1, '0')) * -1
         else fees.TXN_AMT / rpad(1,(1 * fees.TXN_CCY_CDE_EXP) + 1, '0')
       end as txn_reporting_amt
      ,map.txn_num_code as txn_type_id
    from
      v_stg_tjf_issuer_fees_mcp        fees
      ,ica_agreement      icg
      ,bin_sponsor        bsp
      ,mcp_transaction_map map
      ,currency           cur
      ,bin                bin
      ,rt04_mcp_prefix_map lkp  --Added as part of CMP-1005
    WHERE fees.input_data_id in (aFileSeq,bFileSeq)
      AND fees.ica_on_bin                     = icg.ica_nbr
      AND icg.bin_sponsor_id                  = bsp.id
      AND fees.recon_ccy_cde                  = cur.iso_num_code
      AND fees.message_type_identifier        = map.txn_cde(+)
      AND fees.txn_amt_dbcr_ind               = map.dbcr(+)
      AND fees.message_reason_cde             = lkp.reason_cde(+) --Added as part of CMP-1005
      AND map.prefix                          = NVL(lkp.prefix, 'SCC') --Added as part of CMP-1005
      AND (INSTR(fees.txn_desc, lkp.txn_desc) = 1 OR lkp.txn_desc IS NULL) --Added as part of CMP-1005
      AND bin.bin_sponsor_id(+)               = icg.bin_sponsor_id
      AND bin.ica_nbr(+)                      = icg.ica_nbr
      AND NVL(bin.is_primary_bin, 'N')        = 'Y';


    fdata fetchTJFType4%rowtype;



  begin
    -- return; -- to be removed
    if pINEnv in ('BOTH','PROD','TEST') then
      if pINEnv = 'BOTH' then
        vFILE_SEQ1 := GetFileSeq('PROD');
        vFILE_SEQ2 := GetFileSeq('TEST');
      else
        vFILE_SEQ1 := GetFileSeq(pINEnv);
        vFILE_SEQ2 := -1;
      end if;
    else
      standard_error.raise_program_error
      (p_program => 'pkg_ins_issuer_fees.control',
       p_message => 'InvalidEnvironment');
    end if;

    if vFILE_SEQ1 = -99 or vFILE_SEQ2 = -99
    then
      standard_error.raise_program_error
      (p_program => 'pkg_ins_issuer_fees.control',
       p_message => 'InvalidFile');
    end if;

    vETL_DATE := utl_etl_control.get_current_etl_date;

    open fetchTJFType4 (vFILE_SEQ1,vFILE_SEQ2);


    fetch fetchTJFType4 into fdata;


    while fetchTJFType4%found
            loop
      begin

          insert into MCP_ISSUER_FEES
          (TXN_SEQUENCE
          ,INPUT_DATA_ID
          ,PROGRAM_PORTFOLIO_CODE
          ,ACCOUNT_NUMBER
          ,OL_1
          ,OL_2
          ,OL_3
          ,GCMS_PROCESSING_CDE
          ,SETT_DATE
          ,TXN_AMT
          ,TXN_AMT_DBCR_IND
          ,TXN_CCY_CDE
          ,RECON_AMT
          ,RECON_CCY_CDE_EXP
          ,SECND_LGL_ENTITY_ASSIGN_ICA
          ,LEGAL_ENTITY_ON_ICA
          ,PROD_CDE_ASSD_TO_BIN
          ,ISSUER_CNTRY_CDE_ON_ICA
          ,SETT_SERVICE_VALUE_DATE
          ,CARD_BRAND_IND
         ,"1740_DIRECTION"
          ,SUSPENDED_IND
          ,CROSS_BORDER_IND
          ,ACQUIRER_SETT_DATE
          ,NETWORK_ID
          ,IPS_SOURCE
          ,IPS_TXN_SUFFIX
          ,IPS_TXN_CDE
          ,IPS_PROCESS_CDE
          ,SETT_SERVICE_ID_CDE
          ,SETT_SERVICE_XFER_AGENT_ACCNT
          ,MEMBER_RECON_IND_1
          ,ACTION_DATE
          ,REVERSAL_IND
          ,FEE_COLL_CONTROL_NBR
          ,ACQUIRER_REF_DATA
          ,ORIGINAL_TXN_AMT
          ,CONV_RATE_RECON_FACTOR
          ,CONV_RATE_RECON_EXP
          ,RETRIEVAL_REF_NBR
          ,FORWARDING_INST_ID_CDE
          ,RECEIVING_INST
          ,CA_TERMINAL_ID
          ,APPROVAL_CDE
          ,MESSAGE_REASON_CDE
          ,FUNCTION_CDE
          ,TXN_CCY_CDE_EXP
          ,COMPANY_ID
          ,PROGRAM_ACCOUNT_CODE
          ,RECON_REPORTING_AMT
          ,CURRENCY
          ,ICA
          ,BIN_SPONSOR_ID
          ,BIN_SPONSOR_NAME
          ,PARENT_BUSINESS_UNIT_NAME
          ,CHILD_BUSINESS_UNIT_NAME
          ,MESSAGE_TYPE_IDENTIFIER
          ,TXN_DESC
          ,BUSINESS_DATE
          ,TXN_REPORTING_AMT
          ,TXN_TYPE_ID
          ,ETL_DATE)
        values
          (fdata.TXN_SEQUENCE
          ,fdata.INPUT_DATA_ID
          ,fdata.PROGRAM_PORTFOLIO_CODE
          ,fdata.ACCOUNT_NUMBER
          ,fdata.OL_1
          ,fdata.OL_2
          ,fdata.OL_3
          ,fdata.GCMS_PROCESSING_CDE
          ,fdata.SETT_DATE
          ,fdata.TXN_AMT
          ,fdata.TXN_AMT_DBCR_IND
          ,fdata.TXN_CCY_CDE
          ,fdata.RECON_AMT
          ,fdata.RECON_CCY_CDE_EXP
          ,fdata.SECND_LGL_ENTITY_ASSIGN_ICA
          ,fdata.LEGAL_ENTITY_ON_ICA
          ,fdata.PROD_CDE_ASSD_TO_BIN
          ,fdata.ISSUER_CNTRY_CDE_ON_ICA
          ,fdata.SETT_SERVICE_VALUE_DATE
          ,fdata.CARD_BRAND_IND
          ,fdata."1740_DIRECTION"
          ,fdata.SUSPENDED_IND
          ,fdata.CROSS_BORDER_IND
          ,fdata.ACQUIRER_SETT_DATE
          ,fdata.NETWORK_ID
          ,fdata.IPS_SOURCE
          ,fdata.IPS_TXN_SUFFIX
          ,fdata.IPS_TXN_CDE
          ,fdata.IPS_PROCESS_CDE
          ,fdata.SETT_SERVICE_ID_CDE
          ,fdata.SETT_SERVICE_XFER_AGENT_ACCNT
          ,fdata.MEMBER_RECON_IND_1
          ,fdata.ACTION_DATE
          ,fdata.REVERSAL_IND
          ,fdata.FEE_COLL_CONTROL_NBR
          ,fdata.ACQUIRER_REF_DATA
          ,fdata.ORIGINAL_TXN_AMT
          ,fdata.CONV_RATE_RECON_FACTOR
          ,fdata.CONV_RATE_RECON_EXP
          ,fdata.RETRIEVAL_REF_NBR
          ,fdata.FORWARDING_INST_ID_CDE
          ,fdata.RECEIVING_INST
          ,fdata.CA_TERMINAL_ID
          ,fdata.APPROVAL_CDE
          ,fdata.MESSAGE_REASON_CDE
          ,fdata.FUNCTION_CDE
          ,fdata.TXN_CCY_CDE_EXP
          ,fdata.COMPANY_ID
          ,fdata.PROGRAM_ACCOUNT_CODE
          ,fdata.recon_reporting_amt
          ,fdata.currency
          ,fdata.ica
          ,fdata.bin_sponsor_id
          ,fdata.bin_sponsor_name
          ,fdata.parent_business_unit_name
          ,fdata.child_business_unit_name
          ,fdata.MESSAGE_TYPE_IDENTIFIER
          ,fdata.TXN_DESC
          ,fdata.business_date
          ,fdata.txn_reporting_amt
          ,fdata.txn_type_id
          ,vETL_DATE);


      exception
        when others then
          ErrorMessage := sqlerrm;
          insert into transaction_errors
            (CHECKNUM
            ,ERROR_TYPE
            ,TXN_SEQUENCE
            ,ERRORMESSAGE
            ,DATESTAMP
            ,SEQORDER)
          values
            (0
            ,'TYP4'
            ,fdata.TXN_SEQUENCE
            ,ErrorMessage
            ,vETL_DATE
            ,0);

      end;

      fetch fetchTJFType4 into fdata;
    end loop;
    commit;
    close fetchTJFType4;

  exception
    WHEN OTHERS THEN
      standard_error.when_others_exception
      (p_program         => 'pkg_ins_issuer_fees.control',
       p_additional_info => sqlerrm);

  end control;

  function GetFileSeq (pINEnv IN VARCHAR2) return number
  is
    vRetVal       number(9);
  begin

    if pINEnv = 'PROD' then

      select
        file_seq
      into
        vRetVal
      from
        mcp_etl_control
      where
            file_type = 'CRDTXN'
        and file_name like '%TVXSP%'
        and processed = 'N';

    else

      select
        file_seq
      into
        vRetVal
      from
        mcp_etl_control
      where
            file_type = 'CRDTXN'
        and file_name like '%TVXST%'
        and processed = 'N';

    end if;

    Return (VRetVal);

  exception
    when others then
      Return (-99);

  end GetFileSeq;

end pkg_ins_issuer_fees;
/
