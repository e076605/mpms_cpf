CREATE OR REPLACE PACKAGE BODY GLOBAL_STAGING_AREA.settlement_validation AS

/*****************************************************************************************************
   PURPOSE:    Processing required to store settlement transactions (TJF type 1)
               and their related purse and loyalty data (TJF type 20).

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        09/04/2013  Tim Berry        Initial version.
   1.1        24/04/2013  Unknown          Removed +1 from sett_ca_amt exponent to reflect IPS fix.
   1.2        12/11/2014  JT               Corrected interchange type lookup.
   1.3        06/01/2015  JT               Corrected interchange exponent
   1.4        19/01/2015  JT               Corrected interchange exponent again due to false information from IPS
   1.5        11/02/2015  JT               Corrected debit credit indicator on error message
   1.6        07/07/2015  JT               Corrected purse sequence.
   1.7        08/07/2015  Renu             Changed Logic of source_code and  txn_cde_prefix for INTERCHANGE TRANSACTION SPINOFF
   1.8        08/07/2015  Lavanya          Added txn_suffix parameter for ge_processor_transaction_type as part of CMP0921 Auto Recon
   1.9        22/07/2015  JT               Added recycling report
   2.0        10/09/2015  Srini            APWAS-15415 - Removed AND buf_rt01.sett_cca_amt <> buf_rt01.cca_amt condition to get sett_cca_amt
   2.1        09/10/2015  JT               APWAS-15865 - txn_cde should not trim down to NULL
   2.2        14/7/2016   Mahendra         CMP1268 - Project Mahi - MI Loyalty
   2.3        04/08/2016  Herald C         APWAS-19084 MCP Recycling
   2.4        30/01/2017  Mahendra         Japan - Ecomm
   2.5       10/09/2015   Dillip           F32524 chnages NAB/ISRAELPOST
   2.6       31/01/2018   Srinivas         F57475 Phase (Change) 1 – Enable MCP processing to receive SCP data  
*****************************************************************************************************/

  ----------------------------------------------------------------------------------------------------------------------------------

  PROCEDURE process_tjf(p_input_data_id NUMBER) IS

    v_etl_date              DATE;
    v_etl_date_string       VARCHAR2(6);

    --------------------------------------------------------------------------------------------------------------------------------

    PROCEDURE insert_error
    (
      p_check_num NUMBER,
      p_type      VARCHAR2,
      p_sequence  NUMBER,
      p_message   VARCHAR2
    ) IS

      PRAGMA AUTONOMOUS_TRANSACTION;

    BEGIN
      IF p_sequence IS NULL THEN
        standard_error.raise_program_error
        (
          p_program         => 'settlement_validation.insert_error',
          p_message         => 'Transaction Sequence is null',
          p_additional_info => 'p_check_num: '||p_check_num||' p_type: '||p_type||' p_message: '||p_message
        );
      ELSE
        INSERT INTO transaction_errors
        (
          checknum,
          error_type,
          txn_sequence,
          ErrorMessage,
          datestamp
        )
        VALUES
        (
          p_check_num,
          p_type,
          p_sequence,
          SUBSTR(p_message,1,200),
          v_etl_date
        );
      END IF;
      COMMIT;

    EXCEPTION
      WHEN OTHERS THEN
        standard_error.when_others_exception
        (
          p_program         => 'settlement_validation.insert_error',
          p_additional_info => 'p_check_num: '||p_check_num||' p_type: '||p_type||' p_sequence: '||p_sequence||' p_message: '||p_message,
          p_raise_exception => FALSE          -- Don't want an error here to stop the run
        );

    END insert_error;

    --------------------------------------------------------------------------------------------------------------------------------

    PROCEDURE insert_tran_recycle
    (
      p_sequence IN NUMBER,
      p_txn_type IN VARCHAR2,
      p_rel_date IN DATE,
      p_status   IN VARCHAR2
    ) IS

    BEGIN
      INSERT INTO spend_purse_tjf_recycle
      (
        txn_sequence,
        txn_type,
        datestamp,
        releasedate,
        status,
        input_data_id
      )
      VALUES
      (
        p_sequence,
        p_txn_type,
        v_etl_date,
        p_rel_date,
        p_status,
        p_input_data_id
      );

    EXCEPTION
      WHEN OTHERS THEN
        standard_error.when_others_exception
        (
          p_program         => 'settlement_validation.insert_tran_recycle',
          p_additional_info => 'p_sequence: '||p_sequence ||' p_txn_type: '||p_txn_type||' p_rel_date: '||p_rel_date||' p_status: '||p_status
        );

    END insert_tran_recycle;

    --------------------------------------------------------------------------------------------------------------------------------

    PROCEDURE insert_error_data
    (
      p_txn_sequence  NUMBER,
      p_rowid         ROWID
    ) IS

    BEGIN

      IF p_rowid IS NOT NULL THEN

        INSERT INTO typ1_tjf_error_data
        (
          cmpy_id,
          pat,
          pfc,
          acc_nbr,
          ol1,
          ol2,
          ol3,
          txn_effective_date,
          card_nbr,
          posting_date,
          source_cde,
          category_cde,
          level_3_cde,
          level_4_cde,
          level_5_cde,
          posted_amt,
          txn_crdb_1,
          txn_cde,
          cuid,
          cust_posting_datetime,
          txn_desc,
          sysgenrefno,
          adj_reason_cde,
          channel_cde,
          message_type_id,
          card_nbr_2,
          processing_cde,
          transmission_datetime,
          local_datetime,
          sys_trace_audit_nbr,
          txn_amt,
          txn_ccy_cde,
          txn_ccy_cde_exp,
          recon_amt,
          recon_ccy_cde,
          recon_ccy_cde_exp,
          ch_bill_amt,
          issuer_country_cde,
          ch_bill_ccy_cde,
          ch_bill_ccy_cde_exp,
          ic_amt,
          cca_amt,
          card_seq_nbr,
          card_expiration_date,
          function_cde,
          message_reason_cde,
          approval_cde,
          ca_terminal_id,
          receiving_inst_id,
          forwarding_inst_id_cde,
          retrieval_ref_nbr,
          ca_name,
          ca_business_code,
          ca_id_code,
          ca_addr,
          ca_postcode,
          ca_region,
          ca_country_code,
          cashback_amt,
          total_ch_fee_amt,
          fx_ccy_conv_fee,
          conv_rate_recon_exp,
          conv_rate_recon_factor,
          conv_rate_ch_bill_exp,
          conv_rate_ch_bill_factor,
          orig_txn_amt,
          orig_recon_amt,
          acquiring_country_cde,
          acquirer_ref_data,
          acquirer_inst_id_cde,
          ss_transfer_agent_id_cde,
          ss_transfer_agent_accnt,
          ss_level_cde,
          ss_id_cde,
          ips_process_cde,
          ips_txn_cde,
          ips_txn_suffix,
          ips_source,
          network_id,
          aqs_date,
          cross_border_ind,
          suspended_ind,
          txn_crdb_2,
          ic_amt_crdb,
          cca_amt_crdb,
          ic_ccy_cde,
          sett_cca_amt,
          card_prog_id_brand,
          business_service_type_cde,
          business_service_type_id,
          ic_rate_designator,
          business_date,
          business_cycle,
          card_brand_ind,
          ssv_date,
          issuer_country_cde_on_bin,
          prod_cde_on_bin,
          ica_on_bin,
          legal_entity_on_ica,
          second_legal_entity_on_ica,
          fx_ccy_conv_fee_ind,
          fin_parent_business_unit_nbr,
          fin_parent_child_unit_nbr,
          plastic_type,
          financial_trust_ind,
          dispute_id,
          chip_ind,
          pos_entry_mode,
          ic_extended_decimal,
          ic_extended_decimal_exp,
          emv_terminal_capability,
          sett_wh_key,
          cvc2_validation,
          chargeback_eligibility,
          ucaf_indicator,
          multi_purse_flag,
          txn_ref_datetime,
          carddata_input_capability,
          cardhldr_auth_capability,
          cardhldr_present,
          carddata_input_mode,
          cardhldr_auth_method,
          bin,
          txn_sequence,
          input_data_id
        )
        SELECT
          cmpy_id,
          pat,
          pfc,
          acc_nbr,
          ol1,
          ol2,
          ol3,
          txn_effective_date,
          card_nbr,
          posting_date,
          source_cde,
          category_cde,
          level_3_cde,
          level_4_cde,
          level_5_cde,
          posted_amt,
          txn_crdb_1,
          txn_cde,
          cuid,
          cust_posting_datetime,
          txn_desc,
          sysgenrefno,
          adj_reason_cde,
          channel_cde,
          message_type_id,
          card_nbr_2,
          processing_cde,
          transmission_datetime,
          local_datetime,
          sys_trace_audit_nbr,
          txn_amt,
          txn_ccy_cde,
          txn_ccy_cde_exp,
          recon_amt,
          recon_ccy_cde,
          recon_ccy_cde_exp,
          ch_bill_amt,
          issuer_country_cde,
          ch_bill_ccy_cde,
          ch_bill_ccy_cde_exp,
          ic_amt,
          cca_amt,
          card_seq_nbr,
          card_expiration_date,
          function_cde,
          message_reason_cde,
          approval_cde,
          ca_terminal_id,
          receiving_inst_id,
          forwarding_inst_id_cde,
          retrieval_ref_nbr,
          ca_name,
          ca_business_code,
          ca_id_code,
          ca_addr,
          ca_postcode,
          ca_region,
          ca_country_code,
          cashback_amt,
          total_ch_fee_amt,
          fx_ccy_conv_fee,
          conv_rate_recon_exp,
          conv_rate_recon_factor,
          conv_rate_ch_bill_exp,
          conv_rate_ch_bill_factor,
          orig_txn_amt,
          orig_recon_amt,
          acquiring_country_cde,
          acquirer_ref_data,
          acquirer_inst_id_cde,
          ss_transfer_agent_id_cde,
          ss_transfer_agent_accnt,
          ss_level_cde,
          ss_id_cde,
          ips_process_cde,
          ips_txn_cde,
          ips_txn_suffix,
          ips_source,
          network_id,
          aqs_date,
          cross_border_ind,
          suspended_ind,
          txn_crdb_2,
          ic_amt_crdb,
          cca_amt_crdb,
          ic_ccy_cde,
          sett_cca_amt,
          card_prog_id_brand,
          business_service_type_cde,
          business_service_type_id,
          ic_rate_designator,
          business_date,
         business_cycle,
          card_brand_ind,
          ssv_date,
          issuer_country_cde_on_bin,
          prod_cde_on_bin,
          ica_on_bin,
          legal_entity_on_ica,
          second_legal_entity_on_ica,
          fx_ccy_conv_fee_ind,
          fin_parent_business_unit_nbr,
          fin_parent_child_unit_nbr,
          plastic_type,
          financial_trust_ind,
          dispute_id,
          chip_ind,
          pos_entry_mode,
          ic_extended_decimal,
          ic_extended_decimal_exp,
          emv_terminal_capability,
          sett_wh_key,
          cvc2_validation,
          chargeback_eligibility,
          ucaf_indicator,
          multi_purse_flag,
          txn_ref_datetime,
          carddata_input_capability,
          cardhldr_auth_capability,
          cardhldr_present,
          carddata_input_mode,
          cardhldr_auth_method,
          bin,
          txn_sequence,
          input_data_id
        FROM  v_stg_tjf_type1
        WHERE ROWID = p_rowid;

      END IF;

      insert_tran_recycle
      (
        p_sequence  => p_txn_sequence,
        p_txn_type  => 'TYP1',
        p_rel_date  => NULL,
        p_status    => 'REC'
      );

      recycling_report.process_buffer
      (
        p_exiting_recycling    => FALSE
      );

    EXCEPTION
      WHEN OTHERS THEN
        insert_error
        (
          p_check_num   => 0,
          p_type        => 'TYP1',
          p_sequence    => p_txn_sequence,
          p_message     => SQLERRM
        );

        standard_error.when_others_exception
        (
          p_program         => 'settlement_validation.insert_error_data',
          p_additional_info => 'p_txn_sequence: '||p_txn_sequence
        );

    END insert_error_data;

    --------------------------------------------------------------------------------------------------------------------------------

    -- Returns the txn_num_code

    FUNCTION get_processor_transaction_type
    (
      p_txn_cde                     VARCHAR2,
      p_prefix                      VARCHAR2,
      p_dbcr                        VARCHAR2,
      p_suffix                      VARCHAR2
    ) RETURN mcp_transaction_map.txn_num_code%TYPE
      RESULT_CACHE
      RELIES_ON (mcp_transaction_map) IS

      v_txn_num_code   mcp_transaction_map.txn_num_code%TYPE;

    BEGIN
      SELECT  MAX(txn_num_code)
      INTO    v_txn_num_code
      FROM    mcp_transaction_map
      WHERE   txn_cde    = TO_NUMBER(p_txn_cde)
      AND     prefix     = p_prefix
      AND     dbcr       = NVL(p_dbcr,'XX')
      AND     (txn_suffix = p_suffix OR txn_suffix IS NULL);

      RETURN v_txn_num_code;

    EXCEPTION
      WHEN OTHERS THEN
        standard_error.when_others_exception
        (
          p_program         => 'settlement_validation.get_processor_transaction_type',
          p_additional_info => 'p_txn_cde: '||p_txn_cde||' p_prefix: '||p_prefix||' p_dbcr: '||p_dbcr
        );

    END get_processor_transaction_type;

    --------------------------------------------------------------------------------------------------------------------------------

    -- Converts text amount details into a numeric amount of the correct size and sign

    FUNCTION calculate_amount
    (
      ip_text_amount  VARCHAR2,
      ip_exp          VARCHAR2,
      ip_dbcr_ind     VARCHAR2 DEFAULT 'CR'
    ) RETURN NUMBER IS

      v_amount      NUMBER;
      v_exp         NUMBER;

    BEGIN
      v_exp     := NVL(TO_NUMBER(TRIM(ip_exp)),0);
      v_amount  := NVL(TO_NUMBER(ip_text_amount),0);                 -- 2 lines to help with error tracing
      v_amount  := v_amount / POWER(10,v_exp);

      IF ip_dbcr_ind = 'DB' OR ip_dbcr_ind = '-' THEN
          v_amount := -v_amount;
      END IF;

      RETURN v_amount;

    EXCEPTION
      WHEN OTHERS THEN
        standard_error.when_others_exception
        (
          p_program         => 'settlement_validation.calculate_amount',
          p_additional_info => 'ip_text_amount: '||ip_text_amount||' ip_exp: '||ip_exp||' ip_dbcr_ind: '||ip_dbcr_ind
        );

    END calculate_amount;

    --------------------------------------------------------------------------------------------------------------------------------

    -- Converts numeric currency codes to alpha currency codes

    FUNCTION get_currency_alpha
    (
      p_iso_num_code     currency.iso_num_code%TYPE
    ) RETURN currency.iso_code%TYPE
      RESULT_CACHE
      RELIES_ON (currency) IS

      v_iso_code      currency.iso_code%TYPE;

    BEGIN
      IF NVL(p_iso_num_code,'000') = '000' THEN
        v_iso_code := NULL;
      ELSE
        SELECT  MAX(iso_code)
        INTO    v_iso_code
        FROM    currency
        WHERE   iso_num_code = p_iso_num_code;


        IF v_iso_code IS NULL THEN
          standard_error.raise_program_error
          (
            p_program         => 'settlement_validation.get_currency_alpha',
            p_message         => 'Currency code not found '|| p_iso_num_code,
            p_additional_info => 'p_iso_num_code: '||p_iso_num_code
          );
        END IF;
      END IF;

      RETURN v_iso_code;

    EXCEPTION
      WHEN OTHERS THEN
        standard_error.when_others_exception
        (
          p_program         => 'settlement_validation.get_currency_alpha',
          p_additional_info => 'p_iso_num_code: '||p_iso_num_code
        );

    END get_currency_alpha;

    --------------------------------------------------------------------------------------------------------------------------------

    -- Check if currency is offered

    FUNCTION is_currency_offered
    (
      p_account_code          account_type.code%TYPE,
      p_currency_alpha_code   copy_mcp_purse_v.purse_currency_alpha_code%TYPE
    ) RETURN VARCHAR2
      RESULT_CACHE
      RELIES_ON (account_type) IS

      v_result  VARCHAR2(1);

    BEGIN
      SELECT  NVL(MAX('Y'),'N')
      INTO    v_result
      FROM    account_type,
              copy_mcp_purse_v
      WHERE   account_type.code = p_account_code
      AND     copy_mcp_purse_v.psc_code                   = account_type.program_code
      AND     copy_mcp_purse_v.purse_currency_alpha_code  = p_currency_alpha_code;

      RETURN v_result;

    EXCEPTION
      WHEN OTHERS THEN
        standard_error.when_others_exception
        (
          p_program         => 'settlement_validation.is_currency_offered',
          p_additional_info => 'p_account_code: '||p_account_code||' p_currency_alpha_code: '||p_currency_alpha_code
        );

    END is_currency_offered;

    --------------------------------------------------------------------------------------------------------------------------------

    -- Returns ICA number if valid ICA number else NULL

    FUNCTION get_ica_number
    (
      p_ica_nbr   ica_agreement.ica_nbr%TYPE
    ) RETURN ica_agreement.ica_nbr%TYPE
      RESULT_CACHE
      RELIES_ON (ica_agreement) IS

      v_ica_nbr   ica_agreement.ica_nbr%TYPE;

    BEGIN
      SELECT  MAX(ica_nbr)
      INTO    v_ica_nbr
      FROM    ica_agreement
      WHERE   ica_nbr = p_ica_nbr;

      RETURN v_ica_nbr;

    EXCEPTION
      WHEN OTHERS THEN
        standard_error.when_others_exception
        (
          p_program         => 'settlement_validation.get_ica_number',
          p_additional_info => 'p_ica_nbr: '||p_ica_nbr
        );

    END get_ica_number;

    --------------------------------------------------------------------------------------------------------------------------------

    -- Returns account type code if valid account type code else NULL
    FUNCTION get_account_type
    (
      p_code   account_type.code%TYPE
    ) RETURN account_type.code%TYPE
      RESULT_CACHE
      RELIES_ON (account_type) IS

      v_code   account_type.code%TYPE;

    BEGIN
      SELECT  MAX(code)
      INTO    v_code
      FROM    account_type
      WHERE   code = p_code;

      RETURN v_code;

    EXCEPTION
      WHEN OTHERS THEN
        standard_error.when_others_exception
        (
          p_program         => 'settlement_validation.get_account_type',
          p_additional_info => 'p_code: '||p_code
        );

    END get_account_type;

    --------------------------------------------------------------------------------------------------------------------------------

    -- Returns the prefix

    FUNCTION get_rt01_prefix
    (
      p_txn_cde                     VARCHAR2,
      p_source_cde                  VARCHAR2,
      p_process_cde                 VARCHAR2,
      p_ips_transaction_cde         VARCHAR2,
     p_message_type_id             VARCHAR2,
      p_function_cde                VARCHAR2,
      p_txn_crdb_1                  VARCHAR2,
      p_txn_crdb_2                  VARCHAR2
    ) RETURN rt01_mcp_prefix_map.prefix%TYPE
      RESULT_CACHE
      RELIES_ON (rt01_mcp_prefix_map) IS

      v_prefix   rt01_mcp_prefix_map.prefix%TYPE;

    BEGIN
      SELECT  NVL(MAX(prefix),'XXX')  -- XXX is a dummy value which should put the transaction into recycling
      INTO    v_prefix
      FROM    rt01_mcp_prefix_map
      WHERE   txn_cde             = TO_NUMBER(p_txn_cde)
      AND     source_cde          = p_source_cde
      AND     process_cde         = NVL(LTRIM(p_process_cde,'0'),'0')
      AND     ips_transaction_cde = p_ips_transaction_cde
      AND     message_type_id     = TO_NUMBER(p_message_type_id)
      AND     function_cde        = p_function_cde
      AND     txn_crdb_1          = NVL(p_txn_crdb_1,'XX')
      AND     txn_crdb_2          = p_txn_crdb_2;

      RETURN v_prefix;

    EXCEPTION
      WHEN OTHERS THEN
        standard_error.when_others_exception
        (
          p_program => 'settlement_validation.get_rt01_prefix',
          p_additional_info =>
            'p_txn_cde: '               ||p_txn_cde
            ||' p_source_cde: '         ||p_source_cde
            ||' p_process_cde: '        ||p_process_cde
            ||' p_ips_transaction_cde: '||p_ips_transaction_cde
            ||' p_message_type_id: '    ||p_message_type_id
            ||' p_function_cde: '       ||p_function_cde
            ||' p_txn_crdb_1: '         ||p_txn_crdb_1
            ||' p_txn_crdb_2: '         ||p_txn_crdb_2
        );

    END get_rt01_prefix;

    --------------------------------------------------------------------------------------------------------------------------------

    -- Returns account type code if valid account type code else NULL

    FUNCTION get_scheme_region_id
    (
      p_country_iso_code    ra_country.country_iso_code%TYPE
    ) RETURN ra_scheme_countries.ra_scheme_region_id%TYPE
      RESULT_CACHE
      RELIES_ON (ra_scheme_countries,ra_scheme_region) IS

      v_scheme_region_id_of_bin   ra_scheme_countries.ra_scheme_region_id%TYPE;

    BEGIN
      IF p_country_iso_code IS NOT NULL THEN
        SELECT  MAX(ra_scheme_countries.ra_scheme_region_id)
        INTO    v_scheme_region_id_of_bin
        FROM    ra_scheme_countries,
                ra_scheme_region
        WHERE   ra_scheme_countries.ra_scheme_region_id = ra_scheme_region.scheme_region_id
        AND     ra_scheme_region.scheme_id              = 2
        AND     ra_scheme_countries.ra_country_iso      = p_country_iso_code;

        IF v_scheme_region_id_of_bin IS NULL THEN
          standard_error.raise_program_error
          (
            p_program          => 'settlement_validation.get_scheme_region_id',
            p_message          => 'Region ID not found for country '|| p_country_iso_code
          );
        END IF;
      END IF;

      RETURN v_scheme_region_id_of_bin;

    EXCEPTION
      WHEN OTHERS THEN
        standard_error.when_others_exception
        (
          p_program         => 'settlement_validation.get_scheme_region_id',
          p_additional_info => 'p_country_iso_code: '||p_country_iso_code
        );

    END get_scheme_region_id;

    --------------------------------------------------------------------------------------------------------------------------------

    -- Returns the bin or null if bin hasn't been configured

    FUNCTION get_bin
    (
      p_bin   ra_bin.bin%TYPE
    ) RETURN ra_bin.bin%TYPE
      RESULT_CACHE
      RELIES_ON (ra_bin) IS

      v_bin   ra_bin.bin%TYPE;

    BEGIN
      SELECT  MAX(ra_bin.bin)
      INTO    v_bin
      FROM    ra_bin
      WHERE   ra_bin.bin = p_bin;

      RETURN v_bin;

    EXCEPTION
      WHEN OTHERS THEN
        standard_error.when_others_exception
        (
          p_program         => 'settlement_validation.get_bin',
          p_additional_info => 'p_bin: '||p_bin
        );

    END get_bin;

    --------------------------------------------------------------------------------------------------------------------------------

    -- Returns country code for the bin

    FUNCTION get_country_of_bin
    (
      p_bin   ra_bin.bin%TYPE
    ) RETURN ra_bin.country_iso%TYPE
      RESULT_CACHE
      RELIES_ON (ra_bin) IS

      v_country_iso     ra_bin.country_iso%TYPE;

    BEGIN
      SELECT  MAX(ra_bin.country_iso)
      INTO    v_country_iso
      FROM    ra_bin
      WHERE   ra_bin.bin = p_bin;

     RETURN v_country_iso;

    EXCEPTION
      WHEN OTHERS THEN
        standard_error.when_others_exception
        (
          p_program         => 'settlement_validation.get_country_of_bin',
          p_additional_info => 'p_bin: '||p_bin
        );

    END get_country_of_bin;

    --------------------------------------------------------------------------------------------------------------------------------

    -- Returns the 2 character country code whether a 2 character or 3 character country code is entered

    FUNCTION get_valid_country_code
    (
      p_country_code  VARCHAR2
    ) RETURN ra_country.country_iso_code%TYPE
      RESULT_CACHE
      RELIES_ON (ra_country) IS

      v_country_iso_code   ra_country.country_iso_code%TYPE;

    BEGIN
      IF p_country_code IS NOT NULL AND LENGTH(p_country_code) > 2 THEN
        SELECT  MAX(country_iso_code)
        INTO    v_country_iso_code
        FROM    ra_country
        WHERE   country_iso_code3 = p_country_code;

        IF v_country_iso_code IS NULL THEN
          standard_error.raise_program_error
          (
            p_program         => 'settlement_validation.get_valid_country_code',
            p_message         => 'ISO 3 Country code not found '|| p_country_code,
            p_additional_info => 'p_country_code: '||p_country_code
          );
        END IF;
      ELSE
        v_country_iso_code := p_country_code;
      END IF;

      RETURN v_country_iso_code;

    EXCEPTION
      WHEN OTHERS THEN
        standard_error.when_others_exception
        (
          p_program         => 'settlement_validation.get_valid_country_code',
          p_additional_info => 'p_country_code: '||p_country_code
        );

    END get_valid_country_code;

    --------------------------------------------------------------------------------------------------------------------------------

    PROCEDURE process_tjf_records IS

      v_field_in_error        VARCHAR2(50);
      v_txn_ref_datetime      VARCHAR2(26) ;
      v_txn_sequence          NUMBER;
      v_rt01_prefix           VARCHAR2(6) := NULL;
      v_ic_ccy_cde            currency.iso_code%TYPE;
      v_ch_bill_ccy_cde       currency.iso_code%TYPE;
      v_recon_ccy_cde         currency.iso_code%TYPE;
      v_txn_ccy_cde           currency.iso_code%TYPE;
      v_crdb                  VARCHAR2(2);
      v_processor_client_id   client_coding.processor_client_code%TYPE;
      v_processor_count       PLS_INTEGER;
      v_in_out_indicator      NUMBER;

      CURSOR cur_tjf IS
      SELECT /*+ PARALLEL(8) */
        rt01.acc_nbr                                      AS acc_nbr,
        rt01.bin                                          AS bin,
        rt01.card_nbr                                     AS card_nbr,
        rt01.card_nbr_2                                   AS card_nbr_2,
        rt01.multi_purse_flag                             AS multi_purse_flag,
        rt01.sys_trace_audit_nbr                          AS sys_trace_audit_nbr,
        rt01.approval_cde                                 AS approval_cde,
        rt01.sysgenrefno                                  AS sysgenrefno,
        rt01.txn_ref_datetime                             AS txn_ref_datetime,
        rt01.txn_effective_date                           AS txn_effective_date,
        rt01.posting_date                                 AS posting_date,
        rt01.cust_posting_datetime                        AS cust_posting_datetime,
        rt01.transmission_datetime                        AS transmission_datetime,
        rt01.local_datetime                               AS local_datetime,
        rt01.business_date                                AS business_date,
        rt01.AQS_DATE                                     AS aqs_date,
        rt01.ssv_date                                     AS ssv_date,
        rt01.cmpy_id                                      AS cmpy_id,
        rt01.pat                                          AS pat,
        rt01.pfc                                          AS pfc,
        rt01.ol1                                          AS ol1,
        rt01.ol2                                          AS ol2,
        rt01.ol3                                          AS ol3,
        rt01.ica_on_bin                                   AS ica_on_bin,
        rt01.legal_entity_on_ica                          AS legal_entity_on_ica,
        rt01.second_legal_entity_on_ica                   AS second_legal_entity_on_ica,
        rt01.source_cde                                   AS source_cde,
        rt01.category_cde                                 AS category_cde,
        rt01.level_3_cde                                  AS level_3_cde,
        rt01.level_4_cde                                  AS level_4_cde,
        rt01.level_5_cde                                  AS level_5_cde,
        rt01.txn_cde                                      AS txn_cde,
        rt01.txn_desc                                     AS txn_desc,
        rt01.adj_reason_cde                               AS adj_reason_cde,
        rt01.channel_cde                                  AS channel_cde,
        rt01.cuid                                         AS cuid,
        rt01.message_type_id                              AS message_type_id,
        rt01.processing_cde                               AS processing_cde,
        rt01.function_cde                                 AS function_cde,
        rt01.message_reason_cde                           AS message_reason_cde,
        rt01.ips_process_cde                              AS ips_process_cde,
        rt01.ips_txn_cde                                  AS ips_txn_cde,
        rt01.ips_txn_suffix                               AS ips_txn_suffix,
        rt01.ips_source                                   AS ips_source,
        rt01.txn_ccy_cde                                  AS typ1_curcode,
        rt01.txn_sequence                                 AS rt01_txn_sequence,
        rt01.ic_amt_crdb                                  AS ic_amt_crdb,
        rt01.ic_extended_decimal                          AS ic_extended_decimal,
        rt01.ic_extended_decimal_exp                      AS ic_extended_decimal_exp,
        rt01.posted_amt                                   AS posted_amt,
        rt01.ch_bill_ccy_cde_exp                          AS ch_bill_ccy_cde_exp,
        rt01.txn_amt                                      AS txn_amt,
        rt01.txn_ccy_cde_exp                              AS txn_ccy_cde_exp,
        rt01.recon_amt                                    AS recon_amt,
        rt01.recon_ccy_cde_exp                            AS recon_ccy_cde_exp,
        rt01.ch_bill_amt                                  AS ch_bill_amt,
        rt01.orig_txn_amt                                 AS orig_txn_amt,
        rt01.orig_recon_amt                               AS orig_recon_amt,
        rt01.txn_crdb_1                                   AS txn_crdb_1,
        rt01.txn_crdb_2                                   AS txn_crdb_2,
        rt01.fx_ccy_conv_fee_ind                          AS fx_ccy_conv_fee_ind,
        rt01.conv_rate_recon_factor                       AS conv_rate_recon_factor,
        rt01.conv_rate_recon_exp                          AS conv_rate_recon_exp,
        rt01.conv_rate_ch_bill_factor                     AS conv_rate_ch_bill_factor,
        rt01.conv_rate_ch_bill_exp                        AS conv_rate_ch_bill_exp,
        rt01.total_ch_fee_amt                             AS total_ch_fee_amt,
        rt01.issuer_country_cde                           AS issuer_country_cde,
        rt01.ca_terminal_id                               AS ca_terminal_id,
        rt01.receiving_inst_id                            AS receiving_inst_id,
        rt01.forwarding_inst_id_cde                       AS forwarding_inst_id_cde,
        rt01.retrieval_ref_nbr                            AS retrieval_ref_nbr,
        rt01.ca_name                                      AS ca_name,
        rt01.ca_business_code                             AS ca_business_code,
        rt01.ca_id_code                                   AS ca_id_code,
        rt01.ca_addr                                      AS ca_addr,
        rt01.ca_postcode                                  AS ca_postcode,
        rt01.ca_region                                    AS ca_region,
        rt01.ca_country_code                              AS ca_country_code,
        rt01.acquiring_country_cde                        AS acquiring_country_cde,
        rt01.acquirer_ref_data                            AS acquirer_ref_data,
        rt01.acquirer_inst_id_cde                         AS acquirer_inst_id_cde,
        rt01.ss_transfer_agent_id_cde                     AS ss_transfer_agent_id_cde,
        rt01.ss_transfer_agent_accnt                      AS ss_transfer_agent_accnt,
        rt01.ss_level_cde                                 AS ss_level_cde,
        rt01.ss_id_cde                                    AS ss_id_cde,
        rt01.network_id                                   AS network_id,
        rt01.cross_border_ind                             AS cross_border_ind,
        rt01.suspended_ind                                AS suspended_ind,
        rt01.card_prog_id_brand                           AS card_prog_id_brand,
        rt01.card_brand_ind                               AS card_brand_ind,
        rt01.issuer_country_cde_on_bin                    AS issuer_country_cde_on_bin,
        rt01.prod_cde_on_bin                              AS prod_cde_on_bin,
        rt01.card_seq_nbr                                 AS card_seq_nbr,
        rt01.card_expiration_date                         AS card_expiration_date,
        rt01.business_service_type_cde                    AS business_service_type_cde,
        rt01.business_service_type_id                     AS business_service_type_id,
        rt01.ic_rate_designator                           AS ic_rate_designator,
        rt01.business_cycle                               AS business_cycle,
        rt01.fin_parent_business_unit_nbr                 AS fin_parent_business_unit_nbr,
        rt01.fin_parent_child_unit_nbr                    AS fin_parent_child_unit_nbr,
        rt01.plastic_type                                 AS plastic_type,
        rt01.financial_trust_ind                          AS financial_trust_ind,
        rt01.dispute_id                                   AS dispute_id,
        rt01.chip_ind                                     AS chip_ind,
        rt01.pos_entry_mode                               AS pos_entry_mode,
        rt01.emv_terminal_capability                      AS emv_terminal_capability,
        rt01.sett_wh_key                                  AS sett_wh_key,
        rt01.cvc2_validation                              AS cvc2_validation,
        rt01.chargeback_eligibility                       AS chargeback_eligibility,
        rt01.ucaf_indicator                               AS ucaf_indicator,
        rt01.ic_amt                                       AS ic_amt,
        rt01.cca_amt_crdb                                 AS cca_amt_crdb,
        rt01.cca_amt                                      AS cca_amt,
        rt01.sett_cca_amt                                 AS sett_cca_amt,
        rt01.fx_ccy_conv_fee                              AS fx_ccy_conv_fee,
        rt01.cashback_amt                                 AS cashback_amt,
        rt01.ic_ccy_cde                                   AS ic_ccy_cde,
        rt01.ch_bill_ccy_cde                              AS ch_bill_ccy_cde,
        rt01.txn_ccy_cde                                  AS txn_ccy_cde,
        rt01.recon_ccy_cde                                AS recon_ccy_cde,
        rt01.carddata_input_capability                    AS carddata_input_capability,
        rt01.cardhldr_auth_capability                     AS cardhldr_auth_capability,
        rt01.cardhldr_present                             AS cardhldr_present,
        rt01.carddata_input_mode                          AS carddata_input_mode,
        rt01.cardhldr_auth_method                         AS cardhldr_auth_method,
        rt01.tjf_rowid                                    AS tjf_rowid,
        rt20.addendum_id                                  AS addendum_id,
        rt20.purse_id_1                                   AS purse_id_1,
        rt20.purse_ccy_1                                  AS purse_ccy_1,
        rt20.purse_amt_1                                  AS purse_amt_1,
        rt20.purse_amt_crdb_1                             AS purse_amt_crdb_1,
        rt20.purse_ccy_exp_1                              AS purse_ccy_exp_1,
        rt20.purse_id_2                                   AS purse_id_2,
        rt20.purse_ccy_2                                  AS purse_ccy_2,
        rt20.purse_amt_2                                  AS purse_amt_2,
        rt20.purse_amt_crdb_2                             AS purse_amt_crdb_2,
        rt20.purse_ccy_exp_2                              AS purse_ccy_exp_2,
        rt20.purse_id_3                                   AS purse_id_3,
        rt20.purse_ccy_3                                  AS purse_ccy_3,
        rt20.purse_amt_3                                  AS purse_amt_3,
        rt20.purse_amt_crdb_3                             AS purse_amt_crdb_3,
        rt20.purse_ccy_exp_3                              AS purse_ccy_exp_3,
        rt20.purse_id_4                                   AS purse_id_4,
        rt20.purse_ccy_4                                  AS purse_ccy_4,
        rt20.purse_amt_4                                  AS purse_amt_4,
        rt20.purse_amt_crdb_4                             AS purse_amt_crdb_4,
        rt20.purse_ccy_exp_4                              AS purse_ccy_exp_4,
        rt20.purse_id_5                                   AS purse_id_5,
        rt20.purse_ccy_5                                  AS purse_ccy_5,
        rt20.purse_amt_5                                  AS purse_amt_5,
        rt20.purse_amt_crdb_5                             AS purse_amt_crdb_5,
        rt20.purse_ccy_exp_5                              AS purse_ccy_exp_5,
        rt20.purse_id_6                                   AS purse_id_6,
        rt20.purse_ccy_6                                  AS purse_ccy_6,
        rt20.purse_amt_6                                  AS purse_amt_6,
        rt20.purse_amt_crdb_6                             AS purse_amt_crdb_6,
        rt20.purse_ccy_exp_6                              AS purse_ccy_exp_6,
        rt20.purse_id_7                                   AS purse_id_7,
        rt20.purse_ccy_7                                  AS purse_ccy_7,
        rt20.purse_amt_7                                  AS purse_amt_7,
        rt20.purse_amt_crdb_7                             AS purse_amt_crdb_7,
        rt20.purse_ccy_exp_7                              AS purse_ccy_exp_7,
        rt20.purse_id_8                                   AS purse_id_8,
        rt20.purse_ccy_8                                  AS purse_ccy_8,
        rt20.purse_amt_8                                  AS purse_amt_8,
        rt20.purse_amt_crdb_8                             AS purse_amt_crdb_8,
        rt20.purse_ccy_exp_8                              AS purse_ccy_exp_8,
        rt20.purse_id_9                                   AS purse_id_9,
        rt20.purse_ccy_9                                  AS purse_ccy_9,
        rt20.purse_amt_9                                  AS purse_amt_9,
        rt20.purse_amt_crdb_9                             AS purse_amt_crdb_9,
        rt20.purse_ccy_exp_9                              AS purse_ccy_exp_9,
        rt20.purse_id_10                                  AS purse_id_10,
        rt20.purse_ccy_10                                 AS purse_ccy_10,
        rt20.purse_amt_10                                 AS purse_amt_10,
        rt20.purse_amt_crdb_10                            AS purse_amt_crdb_10,
        rt20.purse_ccy_exp_10                             AS purse_ccy_exp_10,
        rt20.purse_id_11                                  AS purse_id_11,
        rt20.purse_ccy_11                                 AS purse_ccy_11,
        rt20.purse_amt_11                                 AS purse_amt_11,
        rt20.purse_amt_crdb_11                            AS purse_amt_crdb_11,
        rt20.purse_ccy_exp_11                             AS purse_ccy_exp_11,
        rt20.purse_id_12                                  AS purse_id_12,
        rt20.purse_ccy_12                                 AS purse_ccy_12,
        rt20.purse_amt_12                                 AS purse_amt_12,
        rt20.purse_amt_crdb_12                            AS purse_amt_crdb_12,
        rt20.purse_ccy_exp_12                             AS purse_ccy_exp_12,
        rt20.purse_id_13                                  AS purse_id_13,
        rt20.purse_ccy_13                                 AS purse_ccy_13,
        rt20.purse_amt_13                                 AS purse_amt_13,
        rt20.purse_amt_crdb_13                            AS purse_amt_crdb_13,
        rt20.purse_ccy_exp_13                             AS purse_ccy_exp_13,
        rt20.purse_id_14                                  AS purse_id_14,
        rt20.purse_ccy_14                                 AS purse_ccy_14,
        rt20.purse_amt_14                                 AS purse_amt_14,
        rt20.purse_amt_crdb_14                            AS purse_amt_crdb_14,
        rt20.purse_ccy_exp_14                             AS purse_ccy_exp_14,
        rt20.purse_id_15                                  AS purse_id_15,
        rt20.purse_ccy_15                                 AS purse_ccy_15,
        rt20.purse_amt_15                                 AS purse_amt_15,
        rt20.purse_amt_crdb_15                            AS purse_amt_crdb_15,
        rt20.purse_ccy_exp_15                             AS purse_ccy_exp_15,
        rt20.purse_id_16                                  AS purse_id_16,
        rt20.purse_ccy_16                                 AS purse_ccy_16,
        rt20.purse_amt_16                                 AS purse_amt_16,
        rt20.purse_amt_crdb_16                            AS purse_amt_crdb_16,
        rt20.purse_ccy_exp_16                             AS purse_ccy_exp_16,
        rt20.purse_id_17                                  AS purse_id_17,
        rt20.purse_ccy_17                                 AS purse_ccy_17,
        rt20.purse_amt_17                                 AS purse_amt_17,
        rt20.purse_amt_crdb_17                            AS purse_amt_crdb_17,
        rt20.purse_ccy_exp_17                             AS purse_ccy_exp_17,
        rt20.purse_id_18                                  AS purse_id_18,
        rt20.purse_ccy_18                                 AS purse_ccy_18,
        rt20.purse_amt_18                                 AS purse_amt_18,
        rt20.purse_amt_crdb_18                            AS purse_amt_crdb_18,
        rt20.purse_ccy_exp_18                             AS purse_ccy_exp_18,
        rt20.purse_id_19                                  AS purse_id_19,
        rt20.purse_ccy_19                                 AS purse_ccy_19,
        rt20.purse_amt_19                                 AS purse_amt_19,
        rt20.purse_amt_crdb_19                            AS purse_amt_crdb_19,
        rt20.purse_ccy_exp_19                             AS purse_ccy_exp_19,
        rt20.purse_id_20                                  AS purse_id_20,
        rt20.purse_ccy_20                                 AS purse_ccy_20,
        rt20.purse_amt_20                                 AS purse_amt_20,
        rt20.purse_amt_crdb_20                            AS purse_amt_crdb_20,
        rt20.purse_ccy_exp_20                             AS purse_ccy_exp_20,
        rt20.alternate_id_1                               AS alternate_id_1,
        rt20.branch                                       AS branch,
        rt20.lprog_id_1                                   AS lprog_id_1,
        rt20.regular_reward_amt_1                         AS regular_reward_amt_1,
        rt20.bonus_reward_amt_1                           AS bonus_reward_amt_1,
        rt20.monetary_ind_1                               AS monetary_ind_1,
        rt20.lprog_amt_crdb_1                             AS lprog_amt_crdb_1,
        rt20.lprog_id_2                                   AS lprog_id_2,
        rt20.regular_reward_amt_2                         AS regular_reward_amt_2,
        rt20.bonus_reward_amt_2                           AS bonus_reward_amt_2,
        rt20.monetary_ind_2                               AS monetary_ind_2,
        rt20.lprog_amt_crdb_2                             AS lprog_amt_crdb_2,
        rt20.lprog_id_3                                   AS lprog_id_3,
        rt20.regular_reward_amt_3                         AS regular_reward_amt_3,
        rt20.bonus_reward_amt_3                           AS bonus_reward_amt_3,
        rt20.monetary_ind_3                               AS monetary_ind_3,
        rt20.lprog_amt_crdb_3                             AS lprog_amt_crdb_3,
        rt20.lprog_id_4                                   AS lprog_id_4,
        rt20.regular_reward_amt_4                         AS regular_reward_amt_4,
        rt20.bonus_reward_amt_4                           AS bonus_reward_amt_4,
        rt20.monetary_ind_4                               AS monetary_ind_4,
        rt20.lprog_amt_crdb_4                             AS lprog_amt_crdb_4,
        rt20.lprog_id_5                                   AS lprog_id_5,
        rt20.regular_reward_amt_5                         AS regular_reward_amt_5,
        rt20.bonus_reward_amt_5                           AS bonus_reward_amt_5,
        rt20.monetary_ind_5                               AS monetary_ind_5,
        rt20.lprog_amt_crdb_5                             AS lprog_amt_crdb_5,
        rt20.txn_sequence                                 AS rt20_txn_sequence
      FROM
      (
        SELECT  cmpy_id,
                pat,
                pfc,
                acc_nbr,
                ol1,
                ol2,
                ol3,
                txn_effective_date,
                card_nbr,
                posting_date,
                source_cde,
                category_cde,
                level_3_cde,
                level_4_cde,
                level_5_cde,
                posted_amt,
                txn_crdb_1,
                txn_cde,
                cuid,
                cust_posting_datetime,
                txn_desc,
                sysgenrefno,
                adj_reason_cde,
                channel_cde,
                message_type_id,
                card_nbr_2,
                processing_cde,
                transmission_datetime,
                local_datetime,
                sys_trace_audit_nbr,
                txn_amt,
                txn_ccy_cde,
                txn_ccy_cde_exp,
                recon_amt,
                recon_ccy_cde,
                recon_ccy_cde_exp,
                ch_bill_amt,
                issuer_country_cde,
                ch_bill_ccy_cde,
                ch_bill_ccy_cde_exp,
                ic_amt,
                cca_amt,
                card_seq_nbr,
                card_expiration_date,
                function_cde,
                message_reason_cde,
                approval_cde,
                ca_terminal_id,
                receiving_inst_id,
                forwarding_inst_id_cde,
                retrieval_ref_nbr,
                ca_name,
                ca_business_code,
                ca_id_code,
                ca_addr,
                ca_postcode,
                ca_region,
                ca_country_code,
                cashback_amt,
                total_ch_fee_amt,
                fx_ccy_conv_fee,
                conv_rate_recon_exp,
                conv_rate_recon_factor,
                conv_rate_ch_bill_exp,
                conv_rate_ch_bill_factor,
                orig_txn_amt,
                orig_recon_amt,
                acquiring_country_cde,
                acquirer_ref_data,
                acquirer_inst_id_cde,
                ss_transfer_agent_id_cde,
                ss_transfer_agent_accnt,
                ss_level_cde,
                ss_id_cde,
                ips_process_cde,
                ips_txn_cde,
                ips_txn_suffix,
                ips_source,
                network_id,
                aqs_date,
                cross_border_ind,
                suspended_ind,
                txn_crdb_2,
                ic_amt_crdb,
                cca_amt_crdb,
                ic_ccy_cde,
                sett_cca_amt,
                card_prog_id_brand,
                business_service_type_cde,
                business_service_type_id,
                ic_rate_designator,
                business_date,
                business_cycle,
                card_brand_ind,
                ssv_date,
                issuer_country_cde_on_bin,
                prod_cde_on_bin,
                ica_on_bin,
                legal_entity_on_ica,
                second_legal_entity_on_ica,
                fx_ccy_conv_fee_ind,
                fin_parent_business_unit_nbr,
                fin_parent_child_unit_nbr,
                plastic_type,
                financial_trust_ind,
                dispute_id,
                chip_ind,
                pos_entry_mode,
                ic_extended_decimal,
                ic_extended_decimal_exp,
                emv_terminal_capability,
                sett_wh_key,
                cvc2_validation,
                chargeback_eligibility,
                ucaf_indicator,
                multi_purse_flag,
                txn_ref_datetime,
                bin,
                txn_sequence,
                input_data_id,
                carddata_input_capability,
                cardhldr_auth_capability,
                 cardhldr_present,
                 carddata_input_mode,
                 cardhldr_auth_method,
                tjf_rowid
        FROM    v_stg_tjf_type1
        WHERE   v_stg_tjf_type1.input_data_id = p_input_data_id
       UNION ALL
        SELECT  cmpy_id,
                pat,
                pfc,
                acc_nbr,
                ol1,
               ol2,
                ol3,
                txn_effective_date,
                card_nbr,
                posting_date,
                source_cde,
                category_cde,
                level_3_cde,
                level_4_cde,
                level_5_cde,
                posted_amt,
                txn_crdb_1,
                txn_cde,
                cuid,
                cust_posting_datetime,
                txn_desc,
                sysgenrefno,
                adj_reason_cde,
                channel_cde,
                message_type_id,
                card_nbr_2,
                processing_cde,
                transmission_datetime,
                local_datetime,
                sys_trace_audit_nbr,
                txn_amt,
                txn_ccy_cde,
                txn_ccy_cde_exp,
                recon_amt,
                recon_ccy_cde,
                recon_ccy_cde_exp,
                ch_bill_amt,
                issuer_country_cde,
                ch_bill_ccy_cde,
                ch_bill_ccy_cde_exp,
                ic_amt,
                cca_amt,
                card_seq_nbr,
                card_expiration_date,
                function_cde,
                message_reason_cde,
                approval_cde,
                ca_terminal_id,
                receiving_inst_id,
                forwarding_inst_id_cde,
                retrieval_ref_nbr,
                ca_name,
                ca_business_code,
                ca_id_code,
                ca_addr,
                ca_postcode,
                ca_region,
                ca_country_code,
                cashback_amt,
                total_ch_fee_amt,
                fx_ccy_conv_fee,
                conv_rate_recon_exp,
                conv_rate_recon_factor,
                conv_rate_ch_bill_exp,
                conv_rate_ch_bill_factor,
                orig_txn_amt,
                orig_recon_amt,
                acquiring_country_cde,
                acquirer_ref_data,
                acquirer_inst_id_cde,
               ss_transfer_agent_id_cde,
                ss_transfer_agent_accnt,
                ss_level_cde,
                ss_id_cde,
                ips_process_cde,
                ips_txn_cde,
                ips_txn_suffix,
                ips_source,
                network_id,
                aqs_date,
                cross_border_ind,
                suspended_ind,
                txn_crdb_2,
                ic_amt_crdb,
                cca_amt_crdb,
                ic_ccy_cde,
                sett_cca_amt,
                card_prog_id_brand,
                business_service_type_cde,
                business_service_type_id,
                ic_rate_designator,
                business_date,
                business_cycle,
                card_brand_ind,
                ssv_date,
                issuer_country_cde_on_bin,
                prod_cde_on_bin,
                ica_on_bin,
                legal_entity_on_ica,
                second_legal_entity_on_ica,
                fx_ccy_conv_fee_ind,
                fin_parent_business_unit_nbr,
                fin_parent_child_unit_nbr,
                plastic_type,
                financial_trust_ind,
                dispute_id,
                chip_ind,
                pos_entry_mode,
                ic_extended_decimal,
                ic_extended_decimal_exp,
                emv_terminal_capability,
                sett_wh_key,
                cvc2_validation,
                chargeback_eligibility,
                ucaf_indicator,
                multi_purse_flag,
                txn_ref_datetime,
                bin,
                txn_sequence,
                input_data_id,
                carddata_input_capability,
                cardhldr_auth_capability,
                cardhldr_present,
                 carddata_input_mode,
                 cardhldr_auth_method,
                NULL                                  AS tjf_rowid
        FROM  typ1_tjf_error_data
        WHERE NOT EXISTS
        (
          SELECT  1
          FROM    spend_purse_tjf_recycle
          WHERE   spend_purse_tjf_recycle.txn_sequence = typ1_tjf_error_data.txn_sequence
          AND     spend_purse_tjf_recycle.releasedate IS NOT NULL
        )
      ) rt01,
      (
        SELECT
          cmpy_id,
          pat,
          pfc,
          acc_nbr,
          ol1,
          ol2,
          ol3,
          txn_effective_date,
          card_nbr,
          addendum_id,
          purse_id_1,
          purse_ccy_1,
          purse_amt_1,
          purse_amt_crdb_1,
          purse_ccy_exp_1,
          purse_id_2,
          purse_ccy_2,
          purse_amt_2,
          purse_amt_crdb_2,
          purse_ccy_exp_2,
          purse_id_3,
          purse_ccy_3,
          purse_amt_3,
          purse_amt_crdb_3,
          purse_ccy_exp_3,
          purse_id_4,
          purse_ccy_4,
          purse_amt_4,
          purse_amt_crdb_4,
          purse_ccy_exp_4,
          purse_id_5,
          purse_ccy_5,
          purse_amt_5,
          purse_amt_crdb_5,
          purse_ccy_exp_5,
          purse_id_6,
          purse_ccy_6,
          purse_amt_6,
          purse_amt_crdb_6,
          purse_ccy_exp_6,
          purse_id_7,
          purse_ccy_7,
          purse_amt_7,
          purse_amt_crdb_7,
          purse_ccy_exp_7,
          purse_id_8,
          purse_ccy_8,
          purse_amt_8,
          purse_amt_crdb_8,
          purse_ccy_exp_8,
          purse_id_9,
          purse_ccy_9,
          purse_amt_9,
          purse_amt_crdb_9,
          purse_ccy_exp_9,
          purse_id_10,
          purse_ccy_10,
          purse_amt_10,
          purse_amt_crdb_10,
          purse_ccy_exp_10,
          purse_id_11,
          purse_ccy_11,
          purse_amt_11,
          purse_amt_crdb_11,
          purse_ccy_exp_11,
          purse_id_12,
          purse_ccy_12,
          purse_amt_12,
          purse_amt_crdb_12,
          purse_ccy_exp_12,
          purse_id_13,
          purse_ccy_13,
          purse_amt_13,
          purse_amt_crdb_13,
          purse_ccy_exp_13,
          purse_id_14,
          purse_ccy_14,
          purse_amt_14,
          purse_amt_crdb_14,
          purse_ccy_exp_14,
          purse_id_15,
          purse_ccy_15,
          purse_amt_15,
          purse_amt_crdb_15,
          purse_ccy_exp_15,
          purse_id_16,
          purse_ccy_16,
          purse_amt_16,
          purse_amt_crdb_16,
          purse_ccy_exp_16,
          purse_id_17,
          purse_ccy_17,
          purse_amt_17,
          purse_amt_crdb_17,
          purse_ccy_exp_17,
          purse_id_18,
          purse_ccy_18,
          purse_amt_18,
          purse_amt_crdb_18,
          purse_ccy_exp_18,
          purse_id_19,
          purse_ccy_19,
          purse_amt_19,
          purse_amt_crdb_19,
          purse_ccy_exp_19,
          purse_id_20,
          purse_ccy_20,
          purse_amt_20,
          purse_amt_crdb_20,
          purse_ccy_exp_20,
          alternate_id_1,
          branch,
          lprog_id_1,
          regular_reward_amt_1,
          bonus_reward_amt_1,
          monetary_ind_1,
          lprog_amt_crdb_1,
          lprog_id_2,
          regular_reward_amt_2,
          bonus_reward_amt_2,
          monetary_ind_2,
          lprog_amt_crdb_2,
          lprog_id_3,
          regular_reward_amt_3,
          bonus_reward_amt_3,
          monetary_ind_3,
          lprog_amt_crdb_3,
          lprog_id_4,
          regular_reward_amt_4,
          bonus_reward_amt_4,
          monetary_ind_4,
          lprog_amt_crdb_4,
          lprog_id_5,
          regular_reward_amt_5,
          bonus_reward_amt_5,
          monetary_ind_5,
          lprog_amt_crdb_5,
          txn_ref_datetime,
          txn_sequence,
          input_data_id
        FROM  v_stg_tjf_type20
        WHERE input_data_id = p_input_data_id
        UNION ALL
        SELECT
          cmpy_id,
          pat,
          pfc,
          acc_nbr,
          ol1,
          ol2,
          ol3,
          txn_effective_date,
          card_nbr,
          addendum_id,
          purse_id_1,
          purse_ccy_1,
          purse_amt_1,
          purse_amt_crdb_1,
          purse_ccy_exp_1,
          purse_id_2,
          purse_ccy_2,
          purse_amt_2,
          purse_amt_crdb_2,
          purse_ccy_exp_2,
          purse_id_3,
          purse_ccy_3,
          purse_amt_3,
          purse_amt_crdb_3,
          purse_ccy_exp_3,
          purse_id_4,
          purse_ccy_4,
          purse_amt_4,
          purse_amt_crdb_4,
          purse_ccy_exp_4,
          purse_id_5,
          purse_ccy_5,
          purse_amt_5,
          purse_amt_crdb_5,
          purse_ccy_exp_5,
          purse_id_6,
          purse_ccy_6,
          purse_amt_6,
          purse_amt_crdb_6,
          purse_ccy_exp_6,
          purse_id_7,
          purse_ccy_7,
          purse_amt_7,
          purse_amt_crdb_7,
          purse_ccy_exp_7,
          purse_id_8,
          purse_ccy_8,
          purse_amt_8,
          purse_amt_crdb_8,
          purse_ccy_exp_8,
          purse_id_9,
          purse_ccy_9,
          purse_amt_9,
          purse_amt_crdb_9,
          purse_ccy_exp_9,
          purse_id_10,
          purse_ccy_10,
          purse_amt_10,
          purse_amt_crdb_10,
          purse_ccy_exp_10,
          purse_id_11,
          purse_ccy_11,
          purse_amt_11,
          purse_amt_crdb_11,
          purse_ccy_exp_11,
          purse_id_12,
          purse_ccy_12,
          purse_amt_12,
          purse_amt_crdb_12,
          purse_ccy_exp_12,
          purse_id_13,
          purse_ccy_13,
          purse_amt_13,
          purse_amt_crdb_13,
          purse_ccy_exp_13,
          purse_id_14,
          purse_ccy_14,
          purse_amt_14,
          purse_amt_crdb_14,
          purse_ccy_exp_14,
          purse_id_15,
          purse_ccy_15,
          purse_amt_15,
          purse_amt_crdb_15,
          purse_ccy_exp_15,
          purse_id_16,
          purse_ccy_16,
          purse_amt_16,
          purse_amt_crdb_16,
          purse_ccy_exp_16,
          purse_id_17,
          purse_ccy_17,
          purse_amt_17,
          purse_amt_crdb_17,
          purse_ccy_exp_17,
          purse_id_18,
          purse_ccy_18,
          purse_amt_18,
          purse_amt_crdb_18,
          purse_ccy_exp_18,
          purse_id_19,
          purse_ccy_19,
          purse_amt_19,
          purse_amt_crdb_19,
          purse_ccy_exp_19,
          purse_id_20,
          purse_ccy_20,
          purse_amt_20,
          purse_amt_crdb_20,
          purse_ccy_exp_20,
          alternate_id_1,
          branch,
          lprog_id_1,
          regular_reward_amt_1,
          bonus_reward_amt_1,
          monetary_ind_1,
          lprog_amt_crdb_1,
          lprog_id_2,
          regular_reward_amt_2,
          bonus_reward_amt_2,
          monetary_ind_2,
          lprog_amt_crdb_2,
          lprog_id_3,
          regular_reward_amt_3,
          bonus_reward_amt_3,
          monetary_ind_3,
          lprog_amt_crdb_3,
          lprog_id_4,
          regular_reward_amt_4,
          bonus_reward_amt_4,
          monetary_ind_4,
          lprog_amt_crdb_4,
          lprog_id_5,
          regular_reward_amt_5,
          bonus_reward_amt_5,
          monetary_ind_5,
          lprog_amt_crdb_5,
          txn_ref_datetime,
          txn_sequence,
          input_data_id
        FROM typ20_tjf_error_data
        WHERE NOT EXISTS
        (
          SELECT  1
          FROM    spend_purse_tjf_recycle
          WHERE   spend_purse_tjf_recycle.txn_sequence = typ20_tjf_error_data.txn_sequence
          AND     spend_purse_tjf_recycle.releasedate IS NOT NULL
        )
      ) rt20
      WHERE rt01.acc_nbr          = rt20.acc_nbr(+)
      AND   rt01.txn_ref_datetime = rt20.txn_ref_datetime(+);

      TYPE typ_tjf IS TABLE OF cur_tjf%ROWTYPE
      INDEX BY BINARY_INTEGER;

      buf_tjf   typ_tjf;

      TYPE typ_purse_record IS RECORD
      (
        purse_id        VARCHAR2(6 CHAR),
        purse_currency  VARCHAR2(3 CHAR),
        purse_amount    NUMBER,
        purse_sequence  NUMBER
      );

      TYPE typ_purse IS TABLE OF typ_purse_record
      INDEX BY BINARY_INTEGER;

      buf_purse   typ_purse;

      TYPE typ_loyalty_record IS RECORD
      (
        prog_id               VARCHAR2(6),
        regular_reward_amt    NUMBER,
        bonus_reward_amt      NUMBER,
        monetary_ind          VARCHAR2(1)
      );

      TYPE typ_loyalty IS TABLE OF typ_loyalty_record
      INDEX BY BINARY_INTEGER;

      buf_loyalty   typ_loyalty;

     -------------------------------------------------------------------------------------------------------------------------------

      PROCEDURE process_tjf_record(p_tjf IN OUT NOCOPY cur_tjf%ROWTYPE) IS

        v_error                 BOOLEAN;

        buf_rt01          rt01%ROWTYPE;
        buf_rt20_purse    rt20_purse%ROWTYPE;
        buf_rt20_loyalty  rt20_loyalty%ROWTYPE;

        TYPE typ_store_rt20_purse IS TABLE OF rt20_purse%ROWTYPE
        INDEX BY BINARY_INTEGER;

        store_rt20_purse    typ_store_rt20_purse;

        TYPE typ_store_rt20_loyalty IS TABLE OF rt20_loyalty%ROWTYPE
        INDEX BY BINARY_INTEGER;

        store_rt20_loyalty  typ_store_rt20_loyalty;

        validation_exception    EXCEPTION;

        ----------------------------------------------------------------------------------------------------------------------------

        PROCEDURE write_rt01(p_rt01 IN OUT NOCOPY rt01%ROWTYPE) IS

        BEGIN
          INSERT INTO rt01
          VALUES p_rt01;

          insert_tran_recycle
          (
            p_sequence  => p_rt01.txn_seq_nbr,
            p_txn_type  => 'TYP1',
            p_rel_date  => v_etl_date,
            p_status    => 'VAL'
          );

        EXCEPTION
          WHEN OTHERS THEN
            insert_error
            (
              p_check_num   => 0,
              p_type        => 'TYP1',
              p_sequence    => p_tjf.rt01_txn_sequence,
              p_message     => SQLERRM
            );
            standard_error.when_others_exception
            (
              p_program         => 'settlement_validation.write_rt01',
              p_additional_info => 'TXN Sequence: '||p_rt01.txn_seq_nbr
            );
            RAISE validation_exception;

        END write_rt01;

        ----------------------------------------------------------------------------------------------------------------------------

        PROCEDURE write_rt20_purse(p_rt20_purse IN OUT NOCOPY rt20_purse%ROWTYPE) IS

        BEGIN
          INSERT INTO rt20_purse
          VALUES
            p_rt20_purse;

          insert_tran_recycle
          (
            p_sequence  => p_rt20_purse.txn_seq_nbr,
            p_txn_type  => 'TYP20',
            p_rel_date  => v_etl_date,
            p_status    => 'VAL'
          );

        EXCEPTION
          WHEN OTHERS THEN
            insert_error
            (
              p_check_num   => 0,
              p_type        => 'TYP1',
              p_sequence    => p_tjf.rt01_txn_sequence,
              p_message     => SQLERRM
            );
            standard_error.when_others_exception
            (
              p_program         => 'settlement_validation.write_rt20_purse',
              p_additional_info => 'TXN Sequence: '||p_rt20_purse.txn_seq_nbr||' purse_ccy: '||p_rt20_purse.purse_ccy
            );
            RAISE validation_exception;

        END write_rt20_purse;

        ----------------------------------------------------------------------------------------------------------------------------

        PROCEDURE write_rt20_loyalty(p_rt20_loyalty IN OUT NOCOPY rt20_loyalty%ROWTYPE) IS

        BEGIN
          INSERT INTO rt20_loyalty
          VALUES
            p_rt20_loyalty;

          insert_tran_recycle
          (
            p_sequence  => p_rt20_loyalty.txn_seq_nbr,
            p_txn_type  => 'TYP20',
            p_rel_date  => v_etl_date,
            p_status    => 'VAL'
          );

        EXCEPTION
          WHEN OTHERS THEN
            insert_error
            (
              p_check_num   => 0,
              p_type        => 'TYP1',
              p_sequence    => p_tjf.rt01_txn_sequence,
              p_message     => SQLERRM
            );
            standard_error.when_others_exception
            (
              p_program         => 'settlement_validation.write_rt20_loyalty',
              p_additional_info => 'TXN Sequence: '||p_rt20_loyalty.txn_seq_nbr
            );
            RAISE validation_exception;

        END write_rt20_loyalty;

        ----------------------------------------------------------------------------------------------------------------------------

        PROCEDURE spin_off_ic_amt(p_rt01 IN OUT NOCOPY rt01%ROWTYPE, p_ic_amt_crdb VARCHAR2) IS

          buf_rt01      rt01%ROWTYPE;

        BEGIN
          buf_rt01 := p_rt01;

          buf_rt01.txn_seq_nbr              := tjf_txn_sequence.NEXTVAL;
          buf_rt01.txn_seq_nbr_parent       := p_rt01.txn_seq_nbr;
          buf_rt01.txn_desc                 := 'INTERCHANGE TRANSACTION SPINOFF';
          IF SUBSTR(p_rt01.suspended_ind,1,1)='G' THEN
          buf_rt01.source_cde  := 'ICG';
          ELSIF SUBSTR(p_rt01.suspended_ind,1,1)='M' THEN
          buf_rt01.source_cde  := 'ICM';
          END IF;

          buf_rt01.posted_amt               := 0;
          buf_rt01.txn_amt                  := 0;
          buf_rt01.recon_amt                := 0;
          buf_rt01.ch_bill_amt              := 0;
          buf_rt01.orig_txn_amt             := 0;
          buf_rt01.orig_recon_amt           := 0;
          buf_rt01.cca_amt                  := 0;
          buf_rt01.sett_cca_amt             := 0;
          buf_rt01.fx_ccy_conv_fee          := 0;
          buf_rt01.cashback_amt             := 0;
          buf_rt01.ic_cde                   := SUBSTR (buf_rt01.txn_desc, 38, 2) ;
          buf_rt01.fx_event_txn             := 'N';
          buf_rt01.fx_event_txn_type        := 'N/A';
          buf_rt01.txn_ref_datetime         := NULL;

          IF SUBSTR(p_rt01.suspended_ind,1,1)='G' THEN
            buf_rt01.txn_cde_prefix := 'ICG';
          ELSIF SUBSTR(p_rt01.suspended_ind,1,1)='M' THEN
            buf_rt01.txn_cde_prefix := 'ICM';
          END IF;

          buf_rt01.processor_transaction_type  :=
            get_processor_transaction_type
            (
              p_txn_cde => buf_rt01.txn_cde,
              p_prefix  => buf_rt01.txn_cde_prefix,
              p_dbcr    => p_ic_amt_crdb,
              p_suffix  => buf_rt01.ips_txn_suffix
            );

          IF buf_rt01.processor_transaction_type IS NULL THEN
            insert_error
            (
              p_check_num   => 105,
              p_type        => 'TYP1',
              p_sequence    => p_tjf.rt01_txn_sequence,
              p_message     => 'Missing Internal Transaction Type'
            );
            standard_error.raise_program_error
            (
              p_program         => 'settlement_validation.spin_off_ic_amt',
              p_message         => 'Missing Internal Transaction Type',
              p_location        => 'Validation Error',
              p_additional_info =>
                'SP1:TXN Sequence: '    ||p_tjf.rt01_txn_sequence
                ||' txn cde: '          ||buf_rt01.txn_cde
                ||' prefix: '           ||buf_rt01.txn_cde_prefix
                ||' crdb: '             ||buf_rt01.txn_crdb_2
                ||' source_cde - '      ||buf_rt01.source_cde
                ||' ips_process_cde - ' ||buf_rt01.ips_process_cde
                ||' ips_txn_cde - '     ||buf_rt01.ips_txn_cde
                ||' message_type_id - ' ||buf_rt01.message_type_id
                ||' function_cde - '    ||buf_rt01.function_cde
                ||' txn_crdb_1 - '      ||buf_rt01.txn_crdb_1
                ||' txn_crdb_2 - '      ||buf_rt01.txn_crdb_2
                ||' IC crdb - '         ||p_ic_amt_crdb,
              p_raise_exception => FALSE
            );
            raise validation_exception;
          END IF;

          write_rt01(buf_rt01);

        EXCEPTION
          WHEN validation_exception THEN
            RAISE validation_exception;

          WHEN OTHERS THEN
            standard_error.when_others_exception
            (
              p_program         => 'settlement_validation.spin_off_ic_amt',
              p_additional_info => 'TXN Sequence: '||p_rt01.txn_seq_nbr
            );

        END spin_off_ic_amt;

        ----------------------------------------------------------------------------------------------------------------------------

        PROCEDURE spin_off_cca_amt(p_rt01 IN OUT NOCOPY rt01%ROWTYPE) IS

          buf_rt01      rt01%ROWTYPE;

        BEGIN
          buf_rt01 := p_rt01;

          buf_rt01.txn_seq_nbr              := tjf_txn_sequence.NEXTVAL;
          buf_rt01.txn_seq_nbr_parent       := p_rt01.txn_seq_nbr;

          IF buf_rt01.sett_cca_amt = 0 THEN
            buf_rt01.txn_cde_prefix := 'CC1';
          ELSIF buf_rt01.sett_cca_amt = buf_rt01.cca_amt THEN
            buf_rt01.txn_cde_prefix := 'CC2';
          ELSIF buf_rt01.sett_cca_amt <> buf_rt01.cca_amt AND buf_rt01.sett_cca_amt <> 0 AND buf_rt01.cca_amt <> 0  THEN
            buf_rt01.txn_cde_prefix := 'CC3';
          END IF;

          buf_rt01.txn_desc                 := 'SETT_CCA_AMOUNT';
          buf_rt01.source_cde               := 'CCA';
          buf_rt01.txn_amt                  := buf_rt01.cca_amt;
          buf_rt01.posted_amt               := 0;
          buf_rt01.recon_amt                := 0;
          buf_rt01.ch_bill_amt              := 0;
          buf_rt01.orig_txn_amt             := 0;
          buf_rt01.orig_recon_amt           := 0;
          buf_rt01.fx_ccy_conv_fee          := 0;
          buf_rt01.cashback_amt             := 0;
          buf_rt01.fx_event_txn             := 'N';
          buf_rt01.fx_event_txn_type        := 'N/A';
          buf_rt01.txn_ref_datetime         := NULL;

          buf_rt01.processor_transaction_type  :=
            get_processor_transaction_type
            (
              p_txn_cde => buf_rt01.txn_cde,
              p_prefix  => buf_rt01.txn_cde_prefix,
              p_dbcr    => buf_rt01.txn_crdb_2,
              p_suffix  => buf_rt01.ips_txn_suffix
            );

          IF buf_rt01.processor_transaction_type IS NULL THEN
            insert_error
            (
              p_check_num   => 105,
              p_type        => 'TYP1',
              p_sequence    => p_tjf.rt01_txn_sequence,
              p_message     => 'Missing Internal Transaction Type'
            );
            standard_error.raise_program_error
            (
              p_program         => 'settlement_validation.spin_off_cca_amt',
              p_message         => 'Missing Internal Transaction Type',
              p_location        => 'Validation Error',
              p_additional_info =>
                'SP2:TXN Sequence: '    ||p_tjf.rt01_txn_sequence
                ||' txn cde: '          ||buf_rt01.txn_cde
                ||' prefix: '           ||buf_rt01.txn_cde_prefix
                ||' crdb: '             ||buf_rt01.txn_crdb_2
                ||' source_cde - '      ||buf_rt01.source_cde
                ||' ips_process_cde - ' ||buf_rt01.ips_process_cde
                ||' ips_txn_cde - '     ||buf_rt01.ips_txn_cde
                ||' message_type_id - ' ||buf_rt01.message_type_id
                ||' function_cde - '    ||buf_rt01.function_cde
                ||' txn_crdb_1 - '      ||buf_rt01.txn_crdb_1
                ||' txn_crdb_2 - '      ||buf_rt01.txn_crdb_2,
              p_raise_exception => FALSE
            );
            raise validation_exception;
          END IF;

          write_rt01(buf_rt01);

        EXCEPTION
          WHEN validation_exception THEN
            RAISE validation_exception;

          WHEN OTHERS THEN
            standard_error.when_others_exception
            (
              p_program         => 'settlement_validation.spin_off_cca_amt',
              p_additional_info => 'TXN Sequence: '||p_rt01.txn_seq_nbr
            );

        END spin_off_cca_amt;

        ----------------------------------------------------------------------------------------------------------------------------

        PROCEDURE spin_off_sett_cca_amt(p_rt01 IN OUT NOCOPY rt01%ROWTYPE) IS

          buf_rt01      rt01%ROWTYPE;

        BEGIN
          buf_rt01 := p_rt01;

          buf_rt01.txn_seq_nbr              := tjf_txn_sequence.NEXTVAL;
          buf_rt01.txn_seq_nbr_parent       := p_rt01.txn_seq_nbr;

          IF buf_rt01.cca_amt <> 0 THEN
            buf_rt01.txn_cde_prefix := 'CC4';
          ELSE
            buf_rt01.txn_cde_prefix := 'CC5';
          END IF;

          buf_rt01.txn_desc                 := 'SETT_CCA_AMOUNT';
          buf_rt01.source_cde               := 'CCA';
          buf_rt01.txn_amt                  := buf_rt01.sett_cca_amt;
          buf_rt01.posted_amt               := 0;
          buf_rt01.recon_amt                := 0;
          buf_rt01.ch_bill_amt              := 0;
          buf_rt01.orig_txn_amt             := 0;
          buf_rt01.orig_recon_amt           := 0;
          buf_rt01.cca_amt                  := 0;
          buf_rt01.sett_cca_amt             := 0;
          buf_rt01.fx_ccy_conv_fee          := 0;
          buf_rt01.cashback_amt             := 0;
          buf_rt01.fx_event_txn             := 'N';
          buf_rt01.fx_event_txn_type        := 'N/A';
          buf_rt01.txn_ref_datetime         := NULL;

          buf_rt01.processor_transaction_type  :=
            get_processor_transaction_type
            (
              p_txn_cde => buf_rt01.txn_cde,
              p_prefix  => buf_rt01.txn_cde_prefix,
              p_dbcr    => buf_rt01.txn_crdb_2,
              p_suffix  => buf_rt01.ips_txn_suffix
            );

          IF buf_rt01.processor_transaction_type IS NULL THEN
            insert_error
            (
              p_check_num   => 105,
              p_type        => 'TYP1',
              p_sequence    => p_tjf.rt01_txn_sequence,
              p_message     => 'Missing Internal Transaction Type'
           );
            standard_error.raise_program_error
            (
              p_program         => 'settlement_validation.spin_off_sett_cca_amt',
              p_message         => 'Missing Internal Transaction Type',
              p_location        => 'Validation Error',
              p_additional_info =>
                'SP3:TXN Sequence: '    ||p_tjf.rt01_txn_sequence
                ||' txn cde: '          ||buf_rt01.txn_cde
                ||' prefix: '           ||buf_rt01.txn_cde_prefix
                ||' crdb: '             ||buf_rt01.txn_crdb_2
                ||' source_cde - '      ||buf_rt01.source_cde
                ||' ips_process_cde - ' ||buf_rt01.ips_process_cde
                ||' ips_txn_cde - '     ||buf_rt01.ips_txn_cde
                ||' message_type_id - ' ||buf_rt01.message_type_id
                ||' function_cde - '    ||buf_rt01.function_cde
                ||' txn_crdb_1 - '      ||buf_rt01.txn_crdb_1
                ||' txn_crdb_2 - '      ||buf_rt01.txn_crdb_2,
              p_raise_exception => FALSE
            );
            raise validation_exception;
          END IF;

          write_rt01(buf_rt01);

        EXCEPTION
          WHEN validation_exception THEN
            RAISE validation_exception;

          WHEN OTHERS THEN
            standard_error.when_others_exception
            (
              p_program         => 'settlement_validation.spin_off_sett_cca_amt',
              p_additional_info => 'TXN Sequence: '||p_rt01.txn_seq_nbr
            );

        END spin_off_sett_cca_amt;

       -----------------------------------------------------------------------------------------------------------------------------

        PROCEDURE spin_off_fx_ccy_conv_fee(p_rt01 IN OUT NOCOPY rt01%ROWTYPE) IS

          buf_rt01      rt01%ROWTYPE;

        BEGIN
          buf_rt01 := p_rt01;

          buf_rt01.txn_seq_nbr              := tjf_txn_sequence.NEXTVAL;
          buf_rt01.txn_seq_nbr_parent       := p_rt01.txn_seq_nbr;
          buf_rt01.txn_cde_prefix           := 'FCC';
          buf_rt01.txn_desc                 := 'FOREX_CCY_CONVERSION_FEE' ;
          buf_rt01.txn_amt                  := buf_rt01.fx_ccy_conv_fee;
          buf_rt01.posted_amt               := 0;
          buf_rt01.recon_amt                := 0;
          buf_rt01.ch_bill_amt              := 0;
          buf_rt01.orig_txn_amt             := 0;
          buf_rt01.orig_recon_amt           := 0;
          buf_rt01.fx_ccy_conv_fee          := 0;
          buf_rt01.cashback_amt             := 0;
          buf_rt01.cca_amt                  := 0;
          buf_rt01.sett_cca_amt             := 0;
          buf_rt01.fx_event_txn             := 'N';
          buf_rt01.fx_event_txn_type        := 'N/A';
          buf_rt01.txn_ref_datetime         := NULL;

          buf_rt01.processor_transaction_type  :=
            get_processor_transaction_type
            (
              p_txn_cde => buf_rt01.txn_cde,
              p_prefix  => buf_rt01.txn_cde_prefix,
              p_dbcr    => buf_rt01.txn_crdb_2,
              p_suffix  => buf_rt01.ips_txn_suffix
            );

          IF buf_rt01.processor_transaction_type IS NULL THEN
            insert_error
            (
              p_check_num   => 105,
              p_type        => 'TYP1',
              p_sequence    => p_tjf.rt01_txn_sequence,
              p_message     => 'Missing Internal Transaction Type'
            );
            standard_error.raise_program_error
            (
              p_program         => 'settlement_validation.spin_off_fx_ccy_conv_fee',
              p_message         => 'Missing Internal Transaction Type',
              p_location        => 'Validation Error',
              p_additional_info =>
                'SP4:TXN Sequence: '    ||p_tjf.rt01_txn_sequence
                ||' txn cde: '          ||buf_rt01.txn_cde
                ||' prefix: '           ||buf_rt01.txn_cde_prefix
                ||' crdb: '             ||buf_rt01.txn_crdb_2
                ||' source_cde - '      ||buf_rt01.source_cde
                ||' ips_process_cde - ' ||buf_rt01.ips_process_cde
                ||' ips_txn_cde - '     ||buf_rt01.ips_txn_cde
                ||' message_type_id - ' ||buf_rt01.message_type_id
                ||' function_cde - '    ||buf_rt01.function_cde
                ||' txn_crdb_1 - '      ||buf_rt01.txn_crdb_1
                ||' txn_crdb_2 - '      ||buf_rt01.txn_crdb_2,
              p_raise_exception => FALSE
            );
            raise validation_exception;
          END IF;

          write_rt01(buf_rt01);

        EXCEPTION
          WHEN validation_exception THEN
            RAISE validation_exception;

          WHEN OTHERS THEN
            standard_error.when_others_exception
            (
              p_program         => 'settlement_validation.spin_off_fx_ccy_conv_fee',
              p_additional_info => 'TXN Sequence: '||p_rt01.txn_seq_nbr
            );

        END spin_off_fx_ccy_conv_fee;



     PROCEDURE spin_off_cashback_rebate(p_rt01 IN OUT NOCOPY rt01%ROWTYPE) IS
        buf_rt01      rt01%ROWTYPE;
        BEGIN
          buf_rt01 := p_rt01;
          buf_rt01.txn_seq_nbr                := tjf_txn_sequence.NEXTVAL;
          buf_rt01.txn_seq_nbr_parent         := p_rt01.txn_seq_nbr;
          buf_rt01.txn_cde_prefix             := 'AQR';
          buf_rt01.posted_amt                 := -1 * buf_rt01.posted_amt;
          buf_rt01.recon_amt                  := -1 * buf_rt01.recon_amt;
          buf_rt01.ch_bill_amt                := -1 * buf_rt01.ch_bill_amt;
          buf_rt01.orig_txn_amt               := -1 * buf_rt01.orig_txn_amt;
          buf_rt01.ic_amt                     :=  -1 * buf_rt01.ic_amt;
          buf_rt01.orig_recon_amt             := -1 * buf_rt01.orig_recon_amt;
          buf_rt01.fx_ccy_conv_fee            := -1 * buf_rt01.fx_ccy_conv_fee;
          buf_rt01.cashback_amt               := -1 * buf_rt01.cca_amt;
          buf_rt01.cca_amt                    := -1 * buf_rt01.cca_amt;
          buf_rt01.sett_cca_amt               := -1 * buf_rt01.sett_cca_amt;
          buf_rt01.fx_event_txn               := 'N';
          buf_rt01.fx_event_txn_type          := 'N/A';
          buf_rt01.txn_amt                    := -1 * buf_rt01.txn_amt;
          buf_rt01.txn_ref_datetime           := NULL;
          buf_rt01.txn_desc                   := CASE
                                                   WHEN buf_rt01.processor_transaction_type = '428'
                                                 THEN 'Acquirer Rebate'
                                                 WHEN buf_rt01.processor_transaction_type = '429'
                                                 THEN 'Acquirer Rebate Reversal'
                                               END;


          buf_rt01.txn_crdb_1                := CASE
                                                   WHEN buf_rt01.processor_transaction_type = '428'
                                               THEN 'DB'
                                               WHEN buf_rt01.processor_transaction_type = '429'
                                               THEN 'CR'
                                             END;

          buf_rt01.txn_crdb_2                := CASE
                                                   WHEN buf_rt01.processor_transaction_type = '428'
                                               THEN 'DB'
                                               WHEN buf_rt01.processor_transaction_type = '429'
                                               THEN 'CR'
                                             END;

           buf_rt01.processor_transaction_type := CASE
                                                 WHEN buf_rt01.processor_transaction_type = '428'
                                                 THEN '430'
                                                 WHEN buf_rt01.processor_transaction_type = '429'
                                                 THEN  '431'
                                                 END;


          write_rt01(buf_rt01);

        EXCEPTION
          WHEN OTHERS THEN
            standard_error.when_others_exception
            (
              p_program         => 'settlement_validation.spin_off_cashback_rebate',
              p_additional_info => 'TXN Sequence: '||p_rt01.txn_seq_nbr
            );

        END spin_off_cashback_rebate;


       -----------------------------------------------------------------------------------------------------------------------------

        PROCEDURE spin_off_cashback_amt(p_rt01 IN OUT NOCOPY rt01%ROWTYPE) IS

          buf_rt01      rt01%ROWTYPE;

        BEGIN
          buf_rt01 := p_rt01;

          buf_rt01.txn_seq_nbr              := tjf_txn_sequence.NEXTVAL;
          buf_rt01.txn_seq_nbr_parent       := p_rt01.txn_seq_nbr;
          buf_rt01.txn_cde_prefix           := 'CB1';
          buf_rt01.txn_desc                 := 'CASHBACK';
          buf_rt01.txn_amt                  := buf_rt01.cashback_amt;
          buf_rt01.posted_amt               := 0;
          buf_rt01.recon_amt                := 0;
          buf_rt01.ch_bill_amt              := 0;
          buf_rt01.orig_txn_amt             := 0;
          buf_rt01.orig_recon_amt           := 0;
          buf_rt01.fx_ccy_conv_fee          := 0;
          buf_rt01.cashback_amt             := 0;
          buf_rt01.cca_amt                  := 0;
          buf_rt01.sett_cca_amt             := 0;
          buf_rt01.fx_event_txn             := 'N';
          buf_rt01.fx_event_txn_type        := 'N/A';
          buf_rt01.txn_ref_datetime         := NULL;

          write_rt01(buf_rt01);

        EXCEPTION
          WHEN OTHERS THEN
            standard_error.when_others_exception
            (
              p_program         => 'settlement_validation.spin_off_cashback_amt',
              p_additional_info => 'TXN Sequence: '||p_rt01.txn_seq_nbr
            );

        END spin_off_cashback_amt;

        ----------------------------------------------------------------------------------------------------------------------------

        -- Converts purse columns into a table

        PROCEDURE expand_purses
        (
          p_tjf  IN OUT NOCOPY cur_tjf%ROWTYPE
        ) IS

         ---------------------------------------------------------------------------------------------------------------------------

          PROCEDURE add_purse
          (
            p_purse_id                VARCHAR2,
            p_purse_ccy               VARCHAR2,
            p_purse_amt               VARCHAR2,
            p_purse_amt_crdb          VARCHAR2,
            p_purse_ccy_exp           VARCHAR2,
            p_purse_sequence          NUMBER,
            p_source_cde              VARCHAR2,
            p_rt20_txn_sequence       NUMBER
          ) IS

            v_field_in_error  varchar2(200);
            v_purse_record    typ_purse_record;
            v_purse_amount    NUMBER;

          BEGIN
            v_field_in_error                := 'v_purse_amount';
            v_purse_amount                  :=
              calculate_amount
              (
                ip_text_amount  => p_purse_amt,
                ip_exp          => p_purse_ccy_exp,
                ip_dbcr_ind     => p_purse_amt_crdb
              );

            --IF v_purse_amount <> 0 THEN   -- JIRA APWAS-19084 MCP Recycling (addded OR below)
            IF v_purse_amount <> 0  OR
                (p_source_cde = 'MON' AND TRIM(p_purse_ccy) IS NOT NULL AND TRIM(p_purse_amt) IS NOT NULL AND p_rt20_txn_sequence IS NOT NULL ) THEN
              v_field_in_error := 'purse_id';
              v_purse_record.purse_id       := p_purse_id;
              v_field_in_error := 'purse_currency';
              v_purse_record.purse_currency := p_purse_ccy;
              v_field_in_error := 'purse_amount';
              v_purse_record.purse_amount   := v_purse_amount;
              v_field_in_error := 'purse sequence';
              v_purse_record.purse_sequence :=p_purse_sequence;
              v_field_in_error := 'buf_purse';
              buf_purse(buf_purse.COUNT+1)  := v_purse_record;
            END IF;

          EXCEPTION
            WHEN OTHERS THEN
              insert_error
              (
                p_check_num   => 0,
                p_type        => 'TYP1',
                p_sequence    => p_tjf.rt01_txn_sequence,
                p_message     => 'Error processing '||v_field_in_error||'. '||SQLERRM
              );
              standard_error.when_others_exception
              (
                p_program         => 'settlement_validation.add_purse',
                p_location        => 'Validation Error',
                p_additional_info => 'TXN Sequence: '||p_tjf.rt20_txn_sequence||' Error processing '||v_field_in_error,
                p_raise_exception => FALSE
              );
              RAISE validation_exception;

          END add_purse;

          ----------------------------------------------------------------------------------------------------------------------------

        BEGIN
          buf_purse.DELETE;
          add_purse(p_tjf.purse_id_1,p_tjf.purse_ccy_1,p_tjf.purse_amt_1,p_tjf.purse_amt_crdb_1,p_tjf.purse_ccy_exp_1,1, p_tjf.source_cde, p_tjf.rt20_txn_sequence);
          add_purse(p_tjf.purse_id_2,p_tjf.purse_ccy_2,p_tjf.purse_amt_2,p_tjf.purse_amt_crdb_2,p_tjf.purse_ccy_exp_2,2, p_tjf.source_cde, p_tjf.rt20_txn_sequence);
          add_purse(p_tjf.purse_id_3,p_tjf.purse_ccy_3,p_tjf.purse_amt_3,p_tjf.purse_amt_crdb_3,p_tjf.purse_ccy_exp_3,3, p_tjf.source_cde, p_tjf.rt20_txn_sequence);
          add_purse(p_tjf.purse_id_4,p_tjf.purse_ccy_4,p_tjf.purse_amt_4,p_tjf.purse_amt_crdb_4,p_tjf.purse_ccy_exp_4,4, p_tjf.source_cde, p_tjf.rt20_txn_sequence);
          add_purse(p_tjf.purse_id_5,p_tjf.purse_ccy_5,p_tjf.purse_amt_5,p_tjf.purse_amt_crdb_5,p_tjf.purse_ccy_exp_5,5, p_tjf.source_cde, p_tjf.rt20_txn_sequence);
          add_purse(p_tjf.purse_id_6,p_tjf.purse_ccy_6,p_tjf.purse_amt_6,p_tjf.purse_amt_crdb_6,p_tjf.purse_ccy_exp_6,6, p_tjf.source_cde, p_tjf.rt20_txn_sequence);
          add_purse(p_tjf.purse_id_7,p_tjf.purse_ccy_7,p_tjf.purse_amt_7,p_tjf.purse_amt_crdb_7,p_tjf.purse_ccy_exp_7,7, p_tjf.source_cde, p_tjf.rt20_txn_sequence);
          add_purse(p_tjf.purse_id_8,p_tjf.purse_ccy_8,p_tjf.purse_amt_8,p_tjf.purse_amt_crdb_8,p_tjf.purse_ccy_exp_8,8, p_tjf.source_cde, p_tjf.rt20_txn_sequence);
          add_purse(p_tjf.purse_id_9,p_tjf.purse_ccy_9,p_tjf.purse_amt_9,p_tjf.purse_amt_crdb_9,p_tjf.purse_ccy_exp_9,9, p_tjf.source_cde, p_tjf.rt20_txn_sequence);
          add_purse(p_tjf.purse_id_10,p_tjf.purse_ccy_10,p_tjf.purse_amt_10,p_tjf.purse_amt_crdb_10,p_tjf.purse_ccy_exp_10,10, p_tjf.source_cde, p_tjf.rt20_txn_sequence);
          add_purse(p_tjf.purse_id_11,p_tjf.purse_ccy_11,p_tjf.purse_amt_11,p_tjf.purse_amt_crdb_11,p_tjf.purse_ccy_exp_11,11, p_tjf.source_cde, p_tjf.rt20_txn_sequence);
          add_purse(p_tjf.purse_id_12,p_tjf.purse_ccy_12,p_tjf.purse_amt_12,p_tjf.purse_amt_crdb_12,p_tjf.purse_ccy_exp_12,12, p_tjf.source_cde, p_tjf.rt20_txn_sequence);
          add_purse(p_tjf.purse_id_13,p_tjf.purse_ccy_13,p_tjf.purse_amt_13,p_tjf.purse_amt_crdb_13,p_tjf.purse_ccy_exp_13,13, p_tjf.source_cde, p_tjf.rt20_txn_sequence);
          add_purse(p_tjf.purse_id_14,p_tjf.purse_ccy_14,p_tjf.purse_amt_14,p_tjf.purse_amt_crdb_14,p_tjf.purse_ccy_exp_14,14, p_tjf.source_cde, p_tjf.rt20_txn_sequence);
          add_purse(p_tjf.purse_id_15,p_tjf.purse_ccy_15,p_tjf.purse_amt_15,p_tjf.purse_amt_crdb_15,p_tjf.purse_ccy_exp_15,15, p_tjf.source_cde, p_tjf.rt20_txn_sequence);
          add_purse(p_tjf.purse_id_16,p_tjf.purse_ccy_16,p_tjf.purse_amt_16,p_tjf.purse_amt_crdb_16,p_tjf.purse_ccy_exp_16,16, p_tjf.source_cde, p_tjf.rt20_txn_sequence);
          add_purse(p_tjf.purse_id_17,p_tjf.purse_ccy_17,p_tjf.purse_amt_17,p_tjf.purse_amt_crdb_17,p_tjf.purse_ccy_exp_17,17, p_tjf.source_cde, p_tjf.rt20_txn_sequence);
          add_purse(p_tjf.purse_id_18,p_tjf.purse_ccy_18,p_tjf.purse_amt_18,p_tjf.purse_amt_crdb_18,p_tjf.purse_ccy_exp_18,18, p_tjf.source_cde, p_tjf.rt20_txn_sequence);
          add_purse(p_tjf.purse_id_19,p_tjf.purse_ccy_19,p_tjf.purse_amt_19,p_tjf.purse_amt_crdb_19,p_tjf.purse_ccy_exp_19,19, p_tjf.source_cde, p_tjf.rt20_txn_sequence);
          add_purse(p_tjf.purse_id_20,p_tjf.purse_ccy_20,p_tjf.purse_amt_20,p_tjf.purse_amt_crdb_20,p_tjf.purse_ccy_exp_20,20, p_tjf.source_cde, p_tjf.rt20_txn_sequence);

        EXCEPTION
          WHEN OTHERS THEN
            standard_error.when_others_exception
            (
              p_program         => 'settlement_validation.tjf_type20.expand_purses',
              p_additional_info => 'TXN Sequence: '||p_tjf.rt20_txn_sequence,
              p_raise_exception => FALSE
            );
            RAISE;

        END expand_purses;

        ----------------------------------------------------------------------------------------------------------------------------

        -- Converts loyalty columns into a table

        PROCEDURE expand_loyalties
        (
          p_tjf  IN OUT NOCOPY cur_tjf%ROWTYPE
        ) IS

        ----------------------------------------------------------------------------------------------------------------------------

          PROCEDURE add_loyalty
          (
            p_lprog_id            VARCHAR2,
            p_regular_reward_amt  VARCHAR2,
            p_bonus_reward_amt    VARCHAR2,
            p_monetary_ind        VARCHAR2,
            p_lprog_amt_crdb      VARCHAR2
          ) IS

            v_loyalty_record  typ_loyalty_record;
            v_loyalty_amount  NUMBER;
            v_field_in_error  varchar2(200);

          BEGIN
            v_field_in_error := 'p_lprog_id';
            IF LTRIM(p_lprog_id,'0') IS NOT NULL THEN
              v_field_in_error := 'prog_id';
              v_loyalty_record.prog_id            := p_lprog_id;
              v_field_in_error                    := 'regular_reward_amt';
              v_loyalty_record.regular_reward_amt :=
                calculate_amount
                (
                  ip_text_amount  => p_regular_reward_amt,
                  ip_exp          => 0,
                  ip_dbcr_ind     => p_lprog_amt_crdb
                );

              v_field_in_error                    := 'bonus_reward_amt';
              v_loyalty_record.bonus_reward_amt   :=
                calculate_amount
                (
                  ip_text_amount  => p_bonus_reward_amt,
                  ip_exp          => 0,
                  ip_dbcr_ind     => p_lprog_amt_crdb
                );

              v_field_in_error                    := 'monetary_ind';
              v_loyalty_record.monetary_ind       := p_monetary_ind;

              v_field_in_error := 'buf_loyalty';
              buf_loyalty(buf_loyalty.COUNT+1) := v_loyalty_record;
            END IF;

          EXCEPTION
            WHEN OTHERS THEN
              insert_error
              (
                p_check_num   => 0,
                p_type        => 'TYP1',
                p_sequence    => p_tjf.rt01_txn_sequence,
                p_message     => 'Error processing '||v_field_in_error||'. '||SQLERRM
              );
              standard_error.when_others_exception
              (
                p_program         => 'settlement_validation.add_loyalty',
                p_location        => 'Validation Error',
                p_additional_info => 'TXN Sequence: '||p_tjf.rt20_txn_sequence||'Error processing '||v_field_in_error,
                p_raise_exception => FALSE
              );
              RAISE validation_exception;

          END add_loyalty;

        ----------------------------------------------------------------------------------------------------------------------------

        BEGIN
          buf_loyalty.DELETE;

          add_loyalty(p_tjf.lprog_id_1,p_tjf.regular_reward_amt_1,p_tjf.bonus_reward_amt_1,p_tjf.monetary_ind_1,p_tjf.lprog_amt_crdb_1);
          add_loyalty(p_tjf.lprog_id_2,p_tjf.regular_reward_amt_2,p_tjf.bonus_reward_amt_2,p_tjf.monetary_ind_2,p_tjf.lprog_amt_crdb_2);
          add_loyalty(p_tjf.lprog_id_3,p_tjf.regular_reward_amt_3,p_tjf.bonus_reward_amt_3,p_tjf.monetary_ind_3,p_tjf.lprog_amt_crdb_3);
          add_loyalty(p_tjf.lprog_id_4,p_tjf.regular_reward_amt_4,p_tjf.bonus_reward_amt_4,p_tjf.monetary_ind_4,p_tjf.lprog_amt_crdb_4);
          add_loyalty(p_tjf.lprog_id_5,p_tjf.regular_reward_amt_5,p_tjf.bonus_reward_amt_5,p_tjf.monetary_ind_5,p_tjf.lprog_amt_crdb_5);

        EXCEPTION
          WHEN OTHERS THEN
            standard_error.when_others_exception
            (
              p_program         => 'settlement_validation.tjf_type20.expand_loyalties',
              p_additional_info => 'TXN Sequence: '||p_tjf.rt20_txn_sequence,
              p_raise_exception => FALSE
            );
            RAISE;

        END expand_loyalties;

        ----------------------------------------------------------------------------------------------------------------------------

        -- Maps the TJF record type 01 data for the RT01 table

        PROCEDURE map_tjf_rt01 IS
        v_ica_check_01  NUMBER;
        BEGIN
          -- Note that the input_data_id reflects the batch that created the record, not necessarily the record that the data was received (i.e. error data)
          buf_rt01.input_data_id := p_input_data_id;

          -- Convert currencies to alpha
          v_field_in_error := 'ic_ccy_cde';
          v_ic_ccy_cde      := get_currency_alpha(p_tjf.ic_ccy_cde);

          v_field_in_error := 'ch_bill_ccy_cde';
          v_ch_bill_ccy_cde := get_currency_alpha(p_tjf.ch_bill_ccy_cde);

          v_field_in_error := 'recon_ccy_cde';
          v_recon_ccy_cde   := get_currency_alpha(p_tjf.recon_ccy_cde);

          v_field_in_error := 'txn_ccy_cde';
          v_txn_ccy_cde     := get_currency_alpha(p_tjf.txn_ccy_cde);

          -- Simple mapping. Note errors are treated as validation errors as could be down to corrupt data

          v_field_in_error                        := 'txn_seq_nbr';
          buf_rt01.txn_seq_nbr                    := p_tjf.rt01_txn_sequence;
          v_field_in_error                        := 'bin';
          buf_rt01.bin                            := p_tjf.bin;
          v_field_in_error                        := 'acc_nbr';
          buf_rt01.acc_nbr                        := p_tjf.acc_nbr;
          v_field_in_error                        := 'card_nbr';
          buf_rt01.card_nbr                       := p_tjf.card_nbr;
          v_field_in_error                        := 'card_nbr_2';
          buf_rt01.card_nbr_2                     := p_tjf.card_nbr_2;
          v_field_in_error                        := 'multi_purse_flag';
          buf_rt01.multi_purse_flag               := p_tjf.multi_purse_flag;
          v_field_in_error                        := 'alternate_id_1';
          buf_rt01.alternate_id_1                 := LTRIM(p_tjf.alternate_id_1,'0');
          v_field_in_error                        := 'branch';
          buf_rt01.branch                         := p_tjf.branch;
          v_field_in_error                        := 'sys_trace_audit_nbr';
          buf_rt01.sys_trace_audit_nbr            := LTRIM(p_tjf.sys_trace_audit_nbr,'0');
          v_field_in_error                        := 'approval_cde';
          buf_rt01.approval_cde                   := p_tjf.approval_cde;
          v_field_in_error                        := 'sysgenrefno';
          buf_rt01.sysgenrefno                    := LTRIM(p_tjf.sysgenrefno,'0');
          v_field_in_error                        := 'txn_ref_datetime';
          buf_rt01.txn_ref_datetime               := p_tjf.txn_ref_datetime;
          v_field_in_error                        := 'cust_posting_datetime';
          buf_rt01.cust_posting_datetime          := p_tjf.cust_posting_datetime;
          v_field_in_error                        := 'cmpy_id';
          buf_rt01.cmpy_id                        := p_tjf.cmpy_id;
          v_field_in_error                        := 'pat';
          buf_rt01.pat                            := p_tjf.pat;
          v_field_in_error                        := 'pfc';
          buf_rt01.pfc                            := p_tjf.pfc;
          v_field_in_error                        := 'ica_on_bin';
          buf_rt01.ica_on_bin                     := LTRIM(p_tjf.ica_on_bin,'0');
          v_field_in_error                        := 'legal_entity_on_ica';
          buf_rt01.legal_entity_on_ica            := LTRIM(p_tjf.legal_entity_on_ica,'0');
          v_field_in_error                        := 'second_legal_entity_on_ica';
          buf_rt01.second_legal_entity_on_ica     := LTRIM(p_tjf.second_legal_entity_on_ica,'0');
          v_field_in_error                        := 'source_cde';
          buf_rt01.source_cde                     := p_tjf.source_cde;
          v_field_in_error                        := 'category_cde';
          buf_rt01.category_cde                   := p_tjf.category_cde;
          v_field_in_error                        := 'level_3_cde';
          buf_rt01.level_3_cde                    := p_tjf.level_3_cde;
          v_field_in_error                        := 'level_4_cde';
          buf_rt01.level_4_cde                    := p_tjf.level_4_cde;
          v_field_in_error                        := 'level_5_cde';
          buf_rt01.level_5_cde                    := p_tjf.level_5_cde;
          v_field_in_error                        := 'txn_cde';
          buf_rt01.txn_cde                        := NVL(LTRIM(p_tjf.txn_cde,'0'),'0');
          v_field_in_error                        := 'txn_desc';
          buf_rt01.txn_desc                       := p_tjf.txn_desc;
          v_field_in_error                        := 'adj_reason_cde';
          buf_rt01.adj_reason_cde                 := p_tjf.adj_reason_cde;
          v_field_in_error                        := 'channel_cde';
          buf_rt01.channel_cde                    := p_tjf.channel_cde;
          v_field_in_error                        := 'cuid';
          buf_rt01.cuid                           := p_tjf.cuid;
          v_field_in_error                        := 'message_type_id';
          buf_rt01.message_type_id                := p_tjf.message_type_id;
          v_field_in_error                        := 'processing_cde';
          buf_rt01.processing_cde                 := p_tjf.processing_cde;
          v_field_in_error                        := 'function_cde';
          buf_rt01.function_cde                   := p_tjf.function_cde;
          v_field_in_error                        := 'message_reason_cde';
          buf_rt01.message_reason_cde             := p_tjf.message_reason_cde;
          v_field_in_error                        := 'ips_process_cde';
          buf_rt01.ips_process_cde                := p_tjf.ips_process_cde;
          v_field_in_error                        := 'ips_txn_cde';
          buf_rt01.ips_txn_cde                    := p_tjf.ips_txn_cde;
          v_field_in_error                        := 'ips_txn_suffix';
          buf_rt01.ips_txn_suffix                 := p_tjf.ips_txn_suffix;
          v_field_in_error                        := 'ips_source';
          buf_rt01.ips_source                     := p_tjf.ips_source;
          v_field_in_error                        := 'country_of_bin';
          buf_rt01.country_of_bin                 := get_country_of_bin(p_tjf.bin);
          v_field_in_error                        := 'ic_extended_decimal_ccy_cde';
          buf_rt01.ic_extended_decimal_ccy_cde    := v_ic_ccy_cde;
          v_field_in_error                        := 'posted_amt_ccy_cde';
          buf_rt01.posted_amt_ccy_cde             := v_ch_bill_ccy_cde;
          v_field_in_error                        := 'txn_amt_ccy_cde';
          buf_rt01.txn_amt_ccy_cde                := v_txn_ccy_cde;
          v_field_in_error                        := 'recon_amt_ccy_cde';
          buf_rt01.recon_amt_ccy_cde              := v_recon_ccy_cde;
          v_field_in_error                        := 'ch_bill_amt_ccy_cde';
          buf_rt01.ch_bill_amt_ccy_cde            := v_ch_bill_ccy_cde;
          v_field_in_error                        := 'orig_txn_amt_ccy_cde';
          buf_rt01.orig_txn_amt_ccy_cde           := v_txn_ccy_cde;
          v_field_in_error                        := 'ic_amt_ccy_cde';
          buf_rt01.ic_amt_ccy_cde                 := v_ic_ccy_cde;
          v_field_in_error                        := 'ic_cde';
          buf_rt01.ic_cde                         := SUBSTR(p_tjf.txn_desc,38,2);
          v_field_in_error                        := 'cca_amt_ccy_cde';
          buf_rt01.cca_amt_ccy_cde                := v_ch_bill_ccy_cde;
          v_field_in_error                        := 'sett_cca_amt_ccy_cde';
          buf_rt01.sett_cca_amt_ccy_cde           := v_recon_ccy_cde;
          v_field_in_error                        := 'fx_ccy_conv_fee_ccy_cde';
          buf_rt01.fx_ccy_conv_fee_ccy_cde        := v_ch_bill_ccy_cde;
          v_field_in_error                        := 'cashback_amt_ccy_cde';
          buf_rt01.cashback_amt_ccy_cde           := v_ch_bill_ccy_cde;
          v_field_in_error                        := 'txn_crdb_1';
          buf_rt01.txn_crdb_1                     := p_tjf.txn_crdb_1;
          v_field_in_error                        := 'txn_crdb_2';
          buf_rt01.txn_crdb_2                     := p_tjf.txn_crdb_2;
          v_field_in_error                        := 'fx_ccy_conv_fee_ind';
          buf_rt01.fx_ccy_conv_fee_ind            := p_tjf.fx_ccy_conv_fee_ind;
          v_field_in_error                        := 'total_ch_fee_amt';
          buf_rt01.total_ch_fee_amt               := NVL(p_tjf.total_ch_fee_amt, 0);
          v_field_in_error                        := 'issuer_country_cde';
          buf_rt01.issuer_country_cde             := LTRIM(p_tjf.issuer_country_cde,'0');
          v_field_in_error                        := 'ca_terminal_id';
          buf_rt01.ca_terminal_id                 := LTRIM(p_tjf.ca_terminal_id,'0');
          v_field_in_error                        := 'receiving_inst_id';
          buf_rt01.receiving_inst_id              := LTRIM(p_tjf.receiving_inst_id,'0');
          v_field_in_error                        := 'forwarding_inst_id_cde';
          buf_rt01.forwarding_inst_id_cde         := LTRIM(p_tjf.forwarding_inst_id_cde,'0');
          v_field_in_error                        := 'retrieval_ref_nbr';
          buf_rt01.retrieval_ref_nbr              := p_tjf.retrieval_ref_nbr;
          v_field_in_error                        := 'ca_name';
          buf_rt01.ca_name                        := p_tjf.ca_name;
          v_field_in_error                        := 'ca_business_code';
          buf_rt01.ca_business_code               := p_tjf.ca_business_code;
          v_field_in_error                        := 'ca_id_code';
          buf_rt01.ca_id_code                     := p_tjf.ca_id_code;
          v_field_in_error                        := 'ca_addr';
          buf_rt01.ca_addr                        := p_tjf.ca_addr;
          v_field_in_error                        := 'ca_postcode';
          buf_rt01.ca_postcode                    := p_tjf.ca_postcode;
          v_field_in_error                        := 'ca_region';
          buf_rt01.ca_region                      := p_tjf.ca_region;
          v_field_in_error                        := 'ca_country_code';
          buf_rt01.ca_country_code                := get_valid_country_code(LTRIM(p_tjf.ca_country_code,'0'));
          v_field_in_error                        := 'acquiring_country_cde';
          buf_rt01.acquiring_country_cde          := LTRIM(p_tjf.acquiring_country_cde,'0');
          v_field_in_error                        := 'acquirer_ref_data';
          buf_rt01.acquirer_ref_data              := LTRIM(p_tjf.acquirer_ref_data,'0');
          v_field_in_error                        := 'acquirer_inst_id_cde';
          buf_rt01.acquirer_inst_id_cde           := LTRIM(p_tjf.acquirer_inst_id_cde,'0');
          v_field_in_error                        := 'ss_transfer_agent_id_cde';
          buf_rt01.ss_transfer_agent_id_cde       := p_tjf.ss_transfer_agent_id_cde;
          v_field_in_error                        := 'ss_transfer_agent_accnt';
          buf_rt01.ss_transfer_agent_accnt        := p_tjf.ss_transfer_agent_accnt;
          v_field_in_error                        := 'ss_level_cde';
          buf_rt01.ss_level_cde                   := p_tjf.ss_level_cde;
          v_field_in_error                        := 'ss_id_cde';
          buf_rt01.ss_id_cde                      := p_tjf.ss_id_cde;
          v_field_in_error                        := 'network_id';
          buf_rt01.network_id                     := p_tjf.network_id;
          v_field_in_error                        := 'cross_border_ind';
          buf_rt01.cross_border_ind               := p_tjf.cross_border_ind;
          v_field_in_error                        := 'suspended_ind';
          buf_rt01.suspended_ind                  := p_tjf.suspended_ind;
          v_field_in_error                        := 'card_prog_id_brand';
          buf_rt01.card_prog_id_brand             := p_tjf.card_prog_id_brand;
          v_field_in_error                        := 'card_brand_ind';
          buf_rt01.card_brand_ind                 := p_tjf.card_brand_ind;
          v_field_in_error                        := 'issuer_country_cde_on_bin';
          buf_rt01.issuer_country_cde_on_bin      := p_tjf.issuer_country_cde_on_bin;
          v_field_in_error                        := 'prod_cde_on_bin';
          buf_rt01.prod_cde_on_bin                := p_tjf.prod_cde_on_bin;
          v_field_in_error                        := 'card_seq_nbr';
          buf_rt01.card_seq_nbr                   := LTRIM(p_tjf.card_seq_nbr,'0');
          v_field_in_error                        := 'card_expiration_date';
          buf_rt01.card_expiration_date           := p_tjf.card_expiration_date;
          v_field_in_error                        := 'business_service_type_cde';
          buf_rt01.business_service_type_cde      := p_tjf.business_service_type_cde;
          v_field_in_error                        := 'business_service_type_id';
          buf_rt01.business_service_type_id       := p_tjf.business_service_type_id;
          v_field_in_error                        := 'ic_rate_designator';
          buf_rt01.ic_rate_designator             := p_tjf.ic_rate_designator;
          v_field_in_error                        := 'business_cycle';
          buf_rt01.business_cycle                 := p_tjf.business_cycle;
          v_field_in_error                        := 'fin_parent_business_unit_nbr';
          buf_rt01.fin_parent_business_unit_nbr   := LTRIM(p_tjf.fin_parent_business_unit_nbr,'0');
          v_field_in_error                        := 'fin_parent_child_unit_nbr';
          buf_rt01.fin_parent_child_unit_nbr      := LTRIM(p_tjf.fin_parent_child_unit_nbr,'0');
          v_field_in_error                        := 'plastic_type';
          buf_rt01.plastic_type                   := p_tjf.plastic_type;
          v_field_in_error                        := 'financial_trust_ind';
          buf_rt01.financial_trust_ind            := p_tjf.financial_trust_ind;
          v_field_in_error                        := 'dispute_id';
          buf_rt01.dispute_id                     := p_tjf.dispute_id;
          v_field_in_error                        := 'chip_ind';
          buf_rt01.chip_ind                       := p_tjf.chip_ind;
          v_field_in_error                        := 'pos_entry_mode';
          buf_rt01.pos_entry_mode                 := p_tjf.pos_entry_mode;
          v_field_in_error                        := 'emv_terminal_capability';
          buf_rt01.emv_terminal_capability        := p_tjf.emv_terminal_capability;
          v_field_in_error                        := 'sett_wh_key';
          buf_rt01.sett_wh_key                    := p_tjf.sett_wh_key;
          v_field_in_error                        := 'cvc2_validation';
          buf_rt01.cvc2_validation                := p_tjf.cvc2_validation;
          v_field_in_error                        := 'chargeback_eligibility';
          buf_rt01.chargeback_eligibility         := p_tjf.chargeback_eligibility;
          v_field_in_error                        := 'ucaf_indicator';
          buf_rt01.ucaf_indicator                 := p_tjf.ucaf_indicator;
          v_field_in_error                        := 'txn_seq_nbr_parent';
          buf_rt01.txn_seq_nbr_parent             := NULL;
          v_field_in_error                        := 'etl_date';
          buf_rt01.etl_date                       := v_etl_date;

          v_field_in_error                        := 'txn_effective_date';
          buf_rt01.txn_effective_date             := TO_DATE(p_tjf.txn_effective_date, 'YYYYMMDD');

          v_field_in_error                        := 'posting_date';
          buf_rt01.posting_date                   := TO_DATE(p_tjf.posting_date, 'YYYYMMDD');

          v_field_in_error                        := 'datetime_transmission';
          buf_rt01.transmission_datetime          := TO_DATE(p_tjf.transmission_datetime, 'YYMMDDHH24MISS') ;

/*----CMP1268 changes----*/

          v_field_in_error                        := 'carddata_input_capability';
          buf_rt01.carddata_input_capability          := p_tjf.carddata_input_capability;

          v_field_in_error                        := 'cardhldr_auth_capability';
          buf_rt01.cardhldr_auth_capability          := p_tjf.cardhldr_auth_capability;

          v_field_in_error                        := 'cardhldr_present';
          buf_rt01.cardhldr_present          := p_tjf.cardhldr_present;

          v_field_in_error                        := 'carddata_input_mode';
          buf_rt01.carddata_input_mode          := p_tjf.carddata_input_mode;

          v_field_in_error                        := 'cardhldr_auth_method';
          buf_rt01.cardhldr_auth_method          := p_tjf.cardhldr_auth_method;



/*----CMP1268 changes----*/


          v_field_in_error := 'local_datetime';
          IF SUBSTR (p_tjf.local_datetime, 7, 6) = '000000' THEN
            buf_rt01.local_datetime := TO_DATE(SUBSTR (p_tjf.local_datetime, 1, 6) || '010101', 'YYMMDDHH24MISS');
          ELSE
            buf_rt01.local_datetime := TO_DATE(p_tjf.local_datetime, 'YYMMDDHH24MISS');
          END IF;

          v_field_in_error                        := 'business_date';
          buf_rt01.business_date                  := TO_DATE(p_tjf.business_date, 'YYMMDD');

          v_field_in_error                        := 'aqs_date';
          buf_rt01.aqs_date                       := TO_DATE(p_tjf.aqs_date, 'YYYYMMDD');

          v_field_in_error                        := 'ssv_date';
          buf_rt01.ssv_date                       := TO_DATE(p_tjf.ssv_date, 'YYMMDD');

          v_field_in_error := 'rt20_ind';
          IF buf_rt01.source_cde = 'MON' THEN
            buf_rt01.rt20_ind := 'Y';
          ELSE
            buf_rt01.rt20_ind := 'N';
          END IF;

          v_field_in_error := 'ic_extended_decimal';
          buf_rt01.ic_extended_decimal :=
            calculate_amount
            (
              ip_text_amount  => p_tjf.ic_extended_decimal,
              ip_exp          => p_tjf.ic_extended_decimal_exp,
              ip_dbcr_ind     => p_tjf.ic_amt_crdb
            );

          v_field_in_error := 'posted_amt crdb';
          IF p_tjf.source_cde IN ('MON', '998', '999') THEN
            v_crdb := p_tjf.txn_crdb_1;
          ELSE
            v_crdb := p_tjf.txn_crdb_2;
          END IF;

          v_field_in_error := 'posted_amt';
          buf_rt01.posted_amt :=
            calculate_amount
            (
              ip_text_amount  => p_tjf.posted_amt,
              ip_exp          => p_tjf.ch_bill_ccy_cde_exp,
              ip_dbcr_ind     => v_crdb
            );

          v_field_in_error := 'txn_amt';
          buf_rt01.txn_amt :=
            calculate_amount
            (
              ip_text_amount  => p_tjf.txn_amt,
              ip_exp          => p_tjf.txn_ccy_cde_exp,
              ip_dbcr_ind     => v_crdb
            );

          v_field_in_error := 'recon_amt';
          buf_rt01.recon_amt :=
            calculate_amount
            (
              ip_text_amount  => p_tjf.recon_amt,
              ip_exp          => p_tjf.recon_ccy_cde_exp,
              ip_dbcr_ind     => v_crdb
            );

          v_field_in_error := 'ch_bill_amt';
          buf_rt01.ch_bill_amt :=
            calculate_amount
            (
              ip_text_amount  => p_tjf.ch_bill_amt,
              ip_exp          => p_tjf.ch_bill_ccy_cde_exp,
              ip_dbcr_ind     => v_crdb
            );

          v_field_in_error := 'orig_txn_amt';
          buf_rt01.orig_txn_amt :=
            calculate_amount
            (
              ip_text_amount  => p_tjf.orig_txn_amt,
              ip_exp          => p_tjf.txn_ccy_cde_exp
            );

          v_field_in_error := 'orig_recon_amt';
          buf_rt01.orig_recon_amt :=
            calculate_amount
            (
              ip_text_amount  => p_tjf.orig_recon_amt,
              ip_exp          => p_tjf.recon_ccy_cde_exp
            );

          v_field_in_error := 'conv_rate_recon_factor';
          buf_rt01.conv_rate_recon_factor :=
            calculate_amount
            (
              ip_text_amount  => p_tjf.conv_rate_recon_factor,
              ip_exp          => p_tjf.conv_rate_recon_exp
            );

          v_field_in_error := 'factor_conv_rate_ch_bill';
          buf_rt01.conv_rate_ch_bill_factor :=
            calculate_amount
            (
              ip_text_amount  => p_tjf.conv_rate_ch_bill_factor,
              ip_exp          => p_tjf.conv_rate_ch_bill_exp
            );

          v_field_in_error := 'ic_amt';
          buf_rt01.ic_amt :=
            calculate_amount
            (
              ip_text_amount  => p_tjf.ic_amt,
              ip_exp          => p_tjf.recon_ccy_cde_exp,
              ip_dbcr_ind     => p_tjf.ic_amt_crdb
            );

          v_field_in_error := 'cca_amt';
          buf_rt01.cca_amt :=
            calculate_amount
            (
              ip_text_amount  => p_tjf.cca_amt,
              ip_exp          => p_tjf.ch_bill_ccy_cde_exp,
              ip_dbcr_ind     => p_tjf.cca_amt_crdb
            );

          v_field_in_error := 'sett_cca_amt';
          buf_rt01.sett_cca_amt :=
            calculate_amount
            (
              ip_text_amount  => p_tjf.sett_cca_amt,
              ip_exp          => p_tjf.recon_ccy_cde_exp,
              ip_dbcr_ind     => p_tjf.cca_amt_crdb
            );

          v_field_in_error := 'fx_ccy_conv_fee';
          buf_rt01.fx_ccy_conv_fee :=
            calculate_amount
            (
              ip_text_amount  => p_tjf.fx_ccy_conv_fee,
              ip_exp          => p_tjf.ch_bill_ccy_cde_exp
            );

          v_field_in_error := 'cashback_amt';
          buf_rt01.cashback_amt :=
            calculate_amount
            (
             ip_text_amount  => p_tjf.cashback_amt,
              ip_exp          => p_tjf.ch_bill_ccy_cde_exp
            );

          v_field_in_error    := 'purses_used';
          buf_rt01.purses_used  := buf_purse.COUNT;

          --Default fx_event_txn to N then change later if this is not correct
          v_field_in_error            := 'fx_event_txn';
          buf_rt01.fx_event_txn       := 'N';
          v_field_in_error            := 'fx_event_txn_type';
          buf_rt01.fx_event_txn_type  := 'N/A';

          v_field_in_error := 'fx_event_txn/fx_event_txn_type';
          FOR v_ptr IN 1..buf_purse.COUNT LOOP
            IF get_currency_alpha(buf_purse(v_ptr).purse_currency) <> buf_rt01.txn_amt_ccy_cde THEN
              buf_rt01.fx_event_txn         := 'Y';
              IF is_currency_offered(buf_rt01.pat,buf_rt01.txn_amt_ccy_cde) = 'Y' THEN
                buf_rt01.fx_event_txn_type  := 'PCO';
              ELSE
                buf_rt01.fx_event_txn_type  := 'PCN';
                EXIT;
              END IF;
            END IF;
          END LOOP;

          v_field_in_error                  := 'scheme_region_id_of_bin';
          buf_rt01.scheme_region_id_of_bin  := get_scheme_region_id(buf_rt01.country_of_bin);

          v_field_in_error := 'scheme_region_id_of_txn';
          IF p_tjf.source_cde  IN ('998', '999') THEN
            buf_rt01.scheme_region_id_of_txn := NULL;
          ELSE
            buf_rt01.scheme_region_id_of_txn := get_scheme_region_id(buf_rt01.ca_country_code);
          END IF;

          v_field_in_error := 'txn_region_type';
          IF (p_tjf.source_cde = '998' OR p_tjf.source_cde = '999' OR buf_rt01.country_of_bin IS NULL) THEN
            buf_rt01.txn_region_type  := NULL;
          ELSIF buf_rt01.country_of_bin = buf_rt01.ca_country_code THEN
            buf_rt01.txn_region_type    := 'DOM';
          ELSE
            IF buf_rt01.scheme_region_id_of_bin = buf_rt01.scheme_region_id_of_txn THEN
              buf_rt01.txn_region_type  := 'INTRA';
            ELSE
              buf_rt01.txn_region_type  := 'INTER';
            END IF;
          END IF;

          -- Processor Client ID
          v_field_in_error := 'processor_client_id';
          v_processor_client_id :=
            fn_processor_client_id
            (
              pINIClient1 => p_tjf.ol1,
              pINIClient2 => p_tjf.ol2,
              pINIClient3 => p_tjf.ol3,
              pINOClient1 => NULL,
              pINOClient2 => NULL,
              pINOClient3 => NULL
            );

          IF NVL(v_processor_client_id,0) = 0 THEN
            v_processor_count := 0;
          ELSE
            buf_rt01.ol1 := TRIM(SUBSTR(v_processor_client_id,1,5));
            buf_rt01.ol2 := TRIM(SUBSTR(v_processor_client_id,6,5));
            buf_rt01.ol3 := TRIM(SUBSTR(v_processor_client_id,11,5));

            SELECT  COUNT(1)
            INTO    v_processor_count
            FROM    client_coding
            WHERE   processor_client_code = v_processor_client_id;
          END IF;

          v_field_in_error := 'txn_cde_prefix';
          buf_rt01.txn_cde_prefix :=
            get_rt01_prefix
            (
              p_txn_cde                     => buf_rt01.txn_cde,
              p_source_cde                  => buf_rt01.source_cde,
              p_process_cde                 => buf_rt01.ips_process_cde,
              p_ips_transaction_cde         => buf_rt01.ips_txn_cde,
              p_message_type_id             => buf_rt01.message_type_id,
              p_function_cde                => buf_rt01.function_cde,
              p_txn_crdb_1                  => buf_rt01.txn_crdb_1,
              p_txn_crdb_2                  => buf_rt01.txn_crdb_2
            );

          v_field_in_error := '998';
          IF (buf_rt01.ic_amt <> 0 AND buf_rt01.source_cde = '998') THEN

            buf_rt01.txn_cde_prefix := 'ICD'; /*('I' || SUBSTR(buf_rt01.txn_desc, 38, 2)) ;*/  --- Commected code by Renu as part of JIRA-13340 and added new code
            buf_rt01.ic_cde         := SUBSTR(buf_rt01.txn_desc, 38, 2);

            BEGIN
              -- Assumption that the txn_ref_datetime will be in the same order as txn_seq_nbr
             SELECT  MIN (rt1.txn_ref_datetime),
                      MIN (rt1.txn_seq_nbr)
              INTO    v_txn_ref_datetime,
                      v_txn_sequence
              FROM    rt01 rt1
              WHERE   rt1.acc_nbr             = buf_rt01.acc_nbr
              AND     rt1.sys_trace_audit_nbr = buf_rt01.sys_trace_audit_nbr
              AND     txn_ref_datetime IS NOT NULL;

              IF v_txn_ref_datetime IS NULL THEN
                buf_rt01.sys_trace_audit_nbr := -1;
              ELSE
                buf_rt01.txn_ref_datetime   := v_txn_ref_datetime;
                buf_rt01.txn_seq_nbr_parent := v_txn_sequence;
              END IF;

            EXCEPTION
              WHEN OTHERS THEN
                buf_rt01.txn_ref_datetime    := NULL;
                buf_rt01.sys_trace_audit_nbr := -1;
            END;

            v_field_in_error := 'processor_transaction_type';
            buf_rt01.processor_transaction_type  :=
              get_processor_transaction_type
              (
                p_txn_cde => buf_rt01.txn_cde,
                p_prefix  => buf_rt01.txn_cde_prefix,
                p_dbcr    => p_tjf.ic_amt_crdb,
                p_suffix  => buf_rt01.ips_txn_suffix
              );

          ELSE

            v_field_in_error := '999';
            IF (buf_rt01.ic_amt <> 0 AND buf_rt01.source_cde = '999') THEN
              buf_rt01.txn_cde_prefix := ('D' || SUBSTR (buf_rt01.txn_desc, 38, 2));
              buf_rt01.ic_cde         := SUBSTR (buf_rt01.txn_desc, 38, 2);
            END IF;

            v_field_in_error := 'processor_transaction_type';
            buf_rt01.processor_transaction_type  :=
              get_processor_transaction_type
              (
                p_txn_cde => buf_rt01.txn_cde,
                p_prefix  => buf_rt01.txn_cde_prefix,
                p_dbcr    => buf_rt01.txn_crdb_2,
                p_suffix  => buf_rt01.ips_txn_suffix
              );

          END IF;

          -- Specific Validation
          v_error := FALSE;

          v_field_in_error := 'validating bin';
          IF get_bin(buf_rt01.bin) IS NULL THEN
            v_error := TRUE;
            insert_error
            (
              p_check_num   => 101,
              p_type        => 'TYP1',
              p_sequence    => p_tjf.rt01_txn_sequence,
              p_message     => 'Missing Bin'
            );
            standard_error.raise_program_error
            (
              p_program         => 'settlement_validation.map_tjf_rt01',
              p_message         => 'Missing Bin '|| buf_rt01.bin,
              p_location        => 'Validation Error',
              p_additional_info => 'TXN Sequence: '||p_tjf.rt01_txn_sequence||' Error processing '||v_field_in_error,
              p_raise_exception => FALSE
            );
          END IF;

          v_field_in_error := 'validating ica_on_bin';
          IF get_ica_number(buf_rt01.ica_on_bin) IS NULL THEN
            v_error := TRUE;
            insert_error
            (
              p_check_num   => 102,
              p_type        => 'TYP1',
              p_sequence    => p_tjf.rt01_txn_sequence,
              p_message     => 'Missing ICA'
            );
            standard_error.raise_program_error
            (
              p_program         => 'settlement_validation.map_tjf_rt01',
              p_message         => 'Missing ICA '|| buf_rt01.ica_on_bin,
              p_location        => 'Validation Error',
              p_additional_info => 'TXN Sequence: '||p_tjf.rt01_txn_sequence||' Error processing '||v_field_in_error,
              p_raise_exception => FALSE
            );
          END IF;

          v_field_in_error := 'validating processor_client_id';
          IF v_processor_count = 0 THEN
            v_error := TRUE;
            insert_error
            (
              p_check_num   => 103,
              p_type        => 'TYP1',
              p_sequence    => p_tjf.rt01_txn_sequence,
              p_message     => 'Missing Client'
            );
            standard_error.raise_program_error
            (
              p_program         => 'settlement_validation.map_tjf_rt01',
              p_message         => 'Missing Client '||v_processor_client_id,
              p_location        => 'Validation Error',
              p_additional_info => 'TXN Sequence: '||p_tjf.rt01_txn_sequence||' Error processing '||v_field_in_error,
              p_raise_exception => FALSE
            );
          END IF;

          v_field_in_error := 'validating pat';
          IF get_account_type(buf_rt01.pat) IS NULL THEN
            v_error := TRUE;
            insert_error
            (
              p_check_num   => 104,
              p_type        => 'TYP1',
              p_sequence    => p_tjf.rt01_txn_sequence,
              p_message     => 'Missing Program Account Type'
            );
            standard_error.raise_program_error
            (
              p_program         => 'settlement_validation.map_tjf_rt01',
              p_message         => 'Missing Program Account Type '||buf_rt01.pat,
              p_location        => 'Validation Error',
              p_additional_info => 'TXN Sequence: '||p_tjf.rt01_txn_sequence||' Error processing '||v_field_in_error,
              p_raise_exception => FALSE
            );
          END IF;

          v_field_in_error := 'validating processor_transaction_type';
          IF buf_rt01.processor_transaction_type IS NULL THEN
            v_error := TRUE;
            insert_error
            (
              p_check_num   => 105,
              p_type        => 'TYP1',
              p_sequence    => p_tjf.rt01_txn_sequence,
              p_message     => 'Missing Internal Transaction Type'
            );
            standard_error.raise_program_error
            (
              p_program         => 'settlement_validation.map_tjf_rt01',
              p_message         => 'Missing Internal Transaction Type',
              p_location        => 'Validation Error',
              p_additional_info =>
                'TXN Sequence: '      ||p_tjf.rt01_txn_sequence
                ||' Error processing '||v_field_in_error
                ||' txn cde: '        ||buf_rt01.txn_cde
                ||' prefix: '         ||buf_rt01.txn_cde_prefix
                ||' dbcr: '           ||buf_rt01.txn_crdb_2
                ||' IC crdb: '        ||p_tjf.ic_amt_crdb,
              p_raise_exception => FALSE
            );
          END IF;
          
          /*** SCP and MCP merge*****/
          
          SELECT COUNT(1)
          INTO   v_ica_check_01
          FROM   gfl_mcp_core.ica_agreement
          WHERE  TO_CHAR(ica_nbr) = TO_CHAR(buf_rt01.ica_on_bin)
          AND    migrated_scp = 'Y';          


        

         v_field_in_error := 'validating type 20 exists';
          IF buf_rt01.rt20_ind = 'Y' 
             AND (buf_purse.COUNT = 0 AND v_ica_check_01 != 1)
          THEN
            IF p_tjf.rt20_txn_sequence IS NULL THEN
              v_error := TRUE;
              insert_error
              (
                p_check_num   => 106,
                p_type        => 'TYP1',
                p_sequence    => p_tjf.rt01_txn_sequence,
                p_message     => 'Missing Type 20 record where one is expected'
              );
              standard_error.raise_program_error
              (
                p_program         => 'settlement_validation.map_tjf_rt01',
                p_message         => 'Missing Type 20 record where one is expected',
                p_location        => 'Validation Error',
                p_additional_info => 'TXN Sequence: '||p_tjf.rt01_txn_sequence||' Error processing '||v_field_in_error,
                p_raise_exception => FALSE
              );
            ELSE
              v_error := TRUE;
              insert_error
             (
                p_check_num   => 106,
                p_type        => 'TYP1',
                p_sequence    => p_tjf.rt01_txn_sequence,
                p_message     => 'No purse amounts have been found when at least 1 is expected'
              );
              standard_error.raise_program_error
              (
                p_program         => 'settlement_validation.map_tjf_rt01',
                p_message         => 'No non-zero purse amounts have been found when at least 1 is expected',
                p_location        => 'Validation Error',
                p_additional_info => 'TXN Sequence: '||p_tjf.rt01_txn_sequence||' Error processing '||v_field_in_error,
                p_raise_exception => FALSE
              );
            END IF;
          END IF;

         v_field_in_error := 'validating type 20 not exists';
          IF buf_rt01.rt20_ind = 'N' AND buf_purse.COUNT > 0 THEN
            v_error := TRUE;
            insert_error
            (
              p_check_num   => 108,
              p_type        => 'TYP1',
              p_sequence    => p_tjf.rt01_txn_sequence,
              p_message     => 'Matching Type 20 record where one was not expected'
            );
            standard_error.raise_program_error
            (
              p_program         => 'settlement_validation.map_tjf_rt01',
              p_message         => 'Matching Type 20 record where one was not expected',
              p_location        => 'Validation Error',
              p_additional_info => 'TXN Sequence: '||p_tjf.rt01_txn_sequence||' Error processing '||v_field_in_error,
              p_raise_exception => FALSE
            );
          END IF;

          IF v_error THEN
            RAISE validation_exception;
          END IF;

        EXCEPTION
          WHEN validation_exception THEN
            RAISE validation_exception;

          WHEN OTHERS THEN
            standard_error.when_others_exception
            (
              p_program         => 'settlement_validation.map_tjf_rt01',
             p_location        => 'Validation Error',
              p_additional_info => 'TXN Sequence: '||p_tjf.rt01_txn_sequence||' Error processing '||v_field_in_error,
              p_raise_exception => FALSE
            );
            insert_error
            (
              p_check_num   => 0,
              p_type        => 'TYP1',
              p_sequence    => p_tjf.rt01_txn_sequence,
              p_message     => 'Error processing '||v_field_in_error||'. '||SQLERRM
            );
            RAISE validation_exception;

        END map_tjf_rt01;

        ----------------------------------------------------------------------------------------------------------------------------

        PROCEDURE map_tjf_purse IS
        v_ica_check NUMBER;
        BEGIN
          store_rt20_purse.DELETE;

          buf_rt20_purse.input_data_id        := p_input_data_id;
          buf_rt20_purse.txn_seq_nbr          := p_tjf.rt20_txn_sequence;
          buf_rt20_purse.txn_seq_nbr_parent   := NULL;
          buf_rt20_purse.rt01_seq_nbr         := p_tjf.rt01_txn_sequence;
          buf_rt20_purse.pat                  := p_tjf.pat;
          buf_rt20_purse.pfc                  := p_tjf.pfc;
          buf_rt20_purse.ol1                  := p_tjf.ol1;
          buf_rt20_purse.acc_nbr              := p_tjf.acc_nbr;
          buf_rt20_purse.card_nbr             := p_tjf.card_nbr;
          buf_rt20_purse.txn_ref_datetime     := p_tjf.txn_ref_datetime;
          buf_rt20_purse.txn_effective_date   := TO_DATE(p_tjf.txn_effective_date,'YYYYMMDD');
          buf_rt20_purse.alternate_id_1       := LTRIM(p_tjf.alternate_id_1,'0');
          buf_rt20_purse.branch               := p_tjf.branch;
          buf_rt20_purse.addendum_id          := p_tjf.addendum_id;
          buf_rt20_purse.etl_date             := v_etl_date;

          FOR v_ptr IN 1..buf_purse.COUNT LOOP

            IF v_ptr > 1 THEN
              buf_rt20_purse.txn_seq_nbr        := tjf_txn_sequence.NEXTVAL;
              buf_rt20_purse.txn_seq_nbr_parent := p_tjf.rt20_txn_sequence;
            END IF;

            buf_rt20_purse.purse_id           := buf_purse(v_ptr).purse_id;
            buf_rt20_purse.purse_amt          := buf_purse(v_ptr).purse_amount;
            buf_rt20_purse.purse_ccy          := get_currency_alpha(buf_purse(v_ptr).purse_currency);
            buf_rt20_purse.purse_sequence     := buf_purse(v_ptr).purse_sequence;
            buf_rt20_purse.fx_event_ind       := 'N';
            buf_rt20_purse.fx_event_type      := 'N/A';

            IF buf_rt20_purse.purse_ccy <> buf_rt01.txn_amt_ccy_cde THEN
              buf_rt20_purse.fx_event_ind   := 'Y';
              IF is_currency_offered(buf_rt20_purse.pat,buf_rt01.txn_amt_ccy_cde) = 'Y' THEN
                buf_rt20_purse.fx_event_type  := 'PCO';
              ELSE
                buf_rt20_purse.fx_event_type  := 'PCN';
              END IF;
            END IF;

            store_rt20_purse(store_rt20_purse.COUNT+1) := buf_rt20_purse;

          END LOOP;
          
          
          /*** Added below as part of F57475 - SCP and MCP merge***/
          
          
          
          SELECT COUNT(1)
          INTO  v_ica_check
          FROM  gfl_mcp_core.ica_agreement
          WHERE migrated_scp = 'Y'
          AND   TO_CHAR(ica_nbr) = TO_CHAR(buf_rt01.ica_on_bin);
          
          
          
          IF buf_rt01.source_cde = 'MON'
             AND v_ica_check > 0 
          THEN
          
            buf_rt20_purse.txn_seq_nbr        := tjf_txn_sequence.NEXTVAL;
            buf_rt20_purse.txn_seq_nbr_parent := p_tjf.rt20_txn_sequence;
            buf_rt20_purse.purse_id           := 'TRAVEL';
            buf_rt20_purse.purse_amt          := buf_rt01.ch_bill_amt;
            buf_rt20_purse.purse_ccy          := buf_rt01.ch_bill_amt_ccy_cde;
            buf_rt20_purse.purse_sequence     := '1';
            buf_rt20_purse.fx_event_ind       := 'N';
            buf_rt20_purse.fx_event_type      := 'N/A';   
            
            IF buf_rt20_purse.purse_ccy <> buf_rt01.txn_amt_ccy_cde THEN
              buf_rt20_purse.fx_event_ind   := 'Y';
              IF is_currency_offered(buf_rt20_purse.pat,buf_rt01.txn_amt_ccy_cde) = 'Y' THEN
                buf_rt20_purse.fx_event_type  := 'PCO';
              ELSE
                buf_rt20_purse.fx_event_type  := 'PCN';
              END IF;
            END IF;

            store_rt20_purse(store_rt20_purse.COUNT+1) := buf_rt20_purse;  
          
          END IF;

        EXCEPTION
          WHEN OTHERS THEN
            standard_error.when_others_exception
            (
              p_program         => 'settlement_validation.map_tjf_purse',
              p_location        => 'Validation Error',
              p_additional_info => 'TXN Sequence: '||p_tjf.rt20_txn_sequence||' Error processing '||v_field_in_error,
              p_raise_exception => FALSE
            );
            insert_error
            (
              p_check_num   => 0,
              p_type        => 'TYP1',
              p_sequence    =>  p_tjf.rt01_txn_sequence,
              p_message     => 'Error processing '||v_field_in_error||'. '||SQLERRM
            );
            RAISE validation_exception;

        END map_tjf_purse;

        ----------------------------------------------------------------------------------------------------------------------------

        PROCEDURE map_tjf_loyalty IS

        BEGIN
          store_rt20_loyalty.DELETE;

          buf_rt20_loyalty.input_data_id      := p_input_data_id;
          buf_rt20_loyalty.txn_seq_nbr        := p_tjf.rt20_txn_sequence;
          buf_rt20_loyalty.txn_seq_nbr_parent := NULL;
          buf_rt20_loyalty.rt01_seq_nbr       := p_tjf.rt01_txn_sequence;
          buf_rt20_loyalty.pat                := p_tjf.pat;
          buf_rt20_loyalty.pfc                := p_tjf.pfc;
          buf_rt20_loyalty.ol1                := p_tjf.ol1;
          buf_rt20_loyalty.account_nbr        := p_tjf.acc_nbr;
          buf_rt20_loyalty.card_nbr           := p_tjf.card_nbr;
          buf_rt20_loyalty.txn_ref_datetime   := p_tjf.txn_ref_datetime;
          buf_rt20_loyalty.txn_effective_date := TO_DATE(p_tjf.txn_effective_date,'YYYYMMDD');
          buf_rt20_loyalty.alternate_id_1     := LTRIM(p_tjf.alternate_id_1,'0');
          buf_rt20_loyalty.branch             := p_tjf.branch;
          buf_rt20_loyalty.etl_date           := v_etl_date;

          FOR v_ptr IN 1..buf_loyalty.COUNT LOOP

            IF v_ptr > 1 THEN
              buf_rt20_loyalty.txn_seq_nbr        := tjf_txn_sequence.NEXTVAL;
              buf_rt20_loyalty.txn_seq_nbr_parent := p_tjf.rt20_txn_sequence;
            END IF;

            buf_rt20_loyalty.lprog_id                    := buf_loyalty(v_ptr).prog_id;
            buf_rt20_loyalty.monetary_ind                := buf_loyalty(v_ptr).monetary_ind;
            buf_rt20_loyalty.regular_reward_amt          := buf_loyalty(v_ptr).regular_reward_amt;
            buf_rt20_loyalty.bonus_reward_amt            := buf_loyalty(v_ptr).bonus_reward_amt;

            store_rt20_loyalty(store_rt20_loyalty.COUNT+1) := buf_rt20_loyalty;

          END LOOP;

        EXCEPTION
          WHEN OTHERS THEN
            standard_error.when_others_exception
            (
              p_program         => 'settlement_validation.map_tjf_loyalty',
              p_location        => 'Validation Error',
              p_additional_info => 'TXN Sequence: '||p_tjf.rt20_txn_sequence||' Error processing '||v_field_in_error,
              p_raise_exception => FALSE
            );
            insert_error
            (
              p_check_num   => 0,
              p_type        => 'TYP1',
              p_sequence    => p_tjf.rt01_txn_sequence,
              p_message     => 'Error processing '||v_field_in_error||'. '||SQLERRM
            );
            RAISE validation_exception;

        END map_tjf_loyalty;

      ------------------------------------------------------------------------------------------------------------------------------

      BEGIN
        SAVEPOINT before_rt01_record;

        expand_purses(p_tjf);
        expand_loyalties(p_tjf);

        IF p_tjf.tjf_rowid IS NULL THEN
          v_in_out_indicator := 0;
        ELSE
          v_in_out_indicator := 1;
        END IF;

        recycling_report.add_record
        (
          p_txn_sequence      => p_tjf.rt01_txn_sequence,
          p_purse_txn_seq     => 0,
          p_business_date     => v_etl_date,
          p_record_type       => '01',
          p_in_out_indicator  => v_in_out_indicator,
          p_bin               => p_tjf.bin,
          p_ica               => LTRIM(p_tjf.ica_on_bin,'0'),
          p_txn_cde           => NVL(LTRIM(p_tjf.txn_cde,'0'),'0'),
          p_ssv_date          => p_tjf.ssv_date,
          p_amount_type       => recycling_report.amount_type_ic,
          p_currency_code     => p_tjf.ic_ccy_cde,
          p_amout_string      => p_tjf.ic_amt,
          p_exp               => p_tjf.recon_ccy_cde_exp,
          p_db_cr             => p_tjf.ic_amt_crdb,
          p_input_data_id     => p_input_data_id
        );

        recycling_report.add_record
        (
          p_txn_sequence      => p_tjf.rt01_txn_sequence,
          p_purse_txn_seq     => 0,
          p_business_date     => v_etl_date,
          p_record_type       => '01',
          p_in_out_indicator  => v_in_out_indicator,
          p_bin               => p_tjf.bin,
          p_ica               => LTRIM(p_tjf.ica_on_bin,'0'),
          p_txn_cde           => NVL(LTRIM(p_tjf.txn_cde,'0'),'0'),
          p_ssv_date          => p_tjf.ssv_date,
          p_amount_type       => recycling_report.amount_type_sett_cca,
          p_currency_code     => p_tjf.recon_ccy_cde,
          p_amout_string      => p_tjf.sett_cca_amt,
          p_exp               => p_tjf.recon_ccy_cde_exp,
          p_db_cr             => p_tjf.cca_amt_crdb,
          p_input_data_id     => p_input_data_id
        );

        recycling_report.add_record
        (
          p_txn_sequence      => p_tjf.rt01_txn_sequence,
          p_purse_txn_seq     => 0,
         p_business_date     => v_etl_date,
          p_record_type       => '01',
          p_in_out_indicator  => v_in_out_indicator,
          p_bin               => p_tjf.bin,
          p_ica               => LTRIM(p_tjf.ica_on_bin,'0'),
          p_txn_cde           => NVL(LTRIM(p_tjf.txn_cde,'0'),'0'),
          p_ssv_date          => p_tjf.ssv_date,
          p_amount_type       => recycling_report.amount_type_recon,
          p_currency_code     => p_tjf.recon_ccy_cde,
          p_amout_string      => p_tjf.recon_amt,
          p_exp               => p_tjf.recon_ccy_cde_exp,
          p_db_cr             => p_tjf.txn_crdb_2,
          p_input_data_id     => p_input_data_id
        );

        IF buf_purse.COUNT > 0 THEN
          FOR v_ptr IN buf_purse.FIRST..buf_purse.LAST LOOP
            recycling_report.add_record
            (
              p_txn_sequence      => p_tjf.rt01_txn_sequence,
              p_purse_txn_seq     => p_tjf.rt20_txn_sequence,
              p_business_date     => v_etl_date,
              p_record_type       => '01',
              p_in_out_indicator  => v_in_out_indicator,
              p_bin               => p_tjf.bin,
              p_ica               => LTRIM(p_tjf.ica_on_bin,'0'),
              p_txn_cde           => NVL(LTRIM(p_tjf.txn_cde,'0'),'0'),
              p_ssv_date          => p_tjf.ssv_date,
              p_amount_type       => recycling_report.amount_type_purse,
              p_currency_code     => buf_purse(v_ptr).purse_currency,
              p_amount            => buf_purse(v_ptr).purse_amount,
              p_input_data_id     => p_input_data_id
            );
          END LOOP;
        END IF;

        map_tjf_rt01;
        map_tjf_purse;
        map_tjf_loyalty;

        write_rt01(buf_rt01);

        -- Spin Off Section
        IF (buf_rt01.ic_amt <> 0  AND buf_rt01.source_cde NOT IN ('998', '999')) THEN
          spin_off_ic_amt(buf_rt01,p_tjf.ic_amt_crdb);
        END IF;

        IF buf_rt01.cca_amt <> 0  THEN
          spin_off_cca_amt(buf_rt01);
        END IF;

        IF buf_rt01.sett_cca_amt <> 0 THEN
          spin_off_sett_cca_amt(buf_rt01);
        END IF;

        IF buf_rt01.fx_ccy_conv_fee <> 0 THEN
          spin_off_fx_ccy_conv_fee(buf_rt01);
        END IF;

        IF buf_rt01.cashback_amt <> 0 THEN
          spin_off_cashback_amt(buf_rt01);
        END IF;

        IF buf_rt01.processor_transaction_type IN (428,429) THEN
          spin_off_cashback_rebate(buf_rt01);
        END IF;

        -- Write type 20 data
        
        
        
        IF store_rt20_purse.COUNT > 0 THEN
          FOR v_ptr IN store_rt20_purse.FIRST..store_rt20_purse.LAST LOOP
            write_rt20_purse(store_rt20_purse(v_ptr));
          END LOOP;
        END IF;

        IF store_rt20_loyalty.COUNT > 0 THEN
          FOR v_ptr IN store_rt20_loyalty.FIRST..store_rt20_loyalty.LAST LOOP
            write_rt20_loyalty(store_rt20_loyalty(v_ptr));
          END LOOP;
        END IF;

        recycling_report.process_buffer
        (
          p_exiting_recycling    => TRUE
        );

      EXCEPTION
        WHEN validation_exception THEN
          ROLLBACK TO before_rt01_record;
          insert_error_data
          (
            p_txn_sequence  => p_tjf.rt01_txn_sequence,
            p_rowid         => p_tjf.tjf_rowid
          );

        WHEN OTHERS THEN
          ROLLBACK TO before_rt01_record;
          standard_error.when_others_exception
          (
            p_program         => 'settlement_validation.process_tjf_record',
            p_additional_info => 'Transaction Sequence '||p_tjf.rt01_txn_sequence,
            p_raise_exception => FALSE
          );
          insert_error
          (
            p_check_num   => 0,
            p_type        => 'TYP1',
            p_sequence    => p_tjf.rt01_txn_sequence,
            p_message     => 'Error processing tjf_record. '||SQLERRM
          );
          insert_error_data
          (
            p_txn_sequence  => p_tjf.rt01_txn_sequence,
            p_rowid         => p_tjf.tjf_rowid
          );

      END process_tjf_record;

      ------------------------------------------------------------------------------------------------------------------------------

    -- Main Routine

    BEGIN
      standard_audit.log_entry
      (
        p_program     => 'settlement_validation.process_tjf_records',
        p_step_name   => 'open cur_tjf',
        p_log_entry   => '',
        p_level       => 2
      );

      OPEN cur_tjf;

      standard_audit.log_entry
      (
        p_program     => 'settlement_validation.process_tjf_records',
        p_step_name   => 'process cur_tjf',
        p_log_entry   => '',
        p_level       => 2
      );

      LOOP
        FETCH cur_tjf
        BULK COLLECT
        INTO buf_tjf
        LIMIT 1000;

        IF buf_tjf.COUNT = 0 THEN
          EXIT;
        END IF;

        standard_audit.log_entry
        (
          p_program     => 'settlement_validation.process_tjf_records',
          p_step_name   => 'Fetch Loop',
          p_log_entry   => 'Fetched '||buf_tjf.COUNT||' records',
          p_level       => 3
        );

        FOR v_source_ptr IN 1..buf_tjf.count LOOP
          process_tjf_record(buf_tjf(v_source_ptr));
        END LOOP;

        COMMIT;

      END LOOP;

      CLOSE cur_tjf;

    EXCEPTION
      WHEN OTHERS THEN
        IF (cur_tjf%ISOPEN ) THEN
          CLOSE cur_tjf;
        END IF;
        ROLLBACK;
        standard_error.when_others_exception
        (
          p_program         => 'settlement_validation.process_tjf_records',
          p_additional_info => 'p_input_data_id: '||p_input_data_id
        );

    END process_tjf_records;

    --------------------------------------------------------------------------------------------------------------------------------

    PROCEDURE purse_missing_settlement IS

      v_ptr                     BINARY_INTEGER;
      v_number_of_orphaned_20   PLS_INTEGER;

      CURSOR  cur_rt20_recycle IS
      SELECT  v_stg_tjf_type20.txn_sequence,
              (
                SELECT  NVL(MAX(1),0)
                FROM    typ1_tjf_error_data
                WHERE   v_stg_tjf_type20.acc_nbr           = typ1_tjf_error_data.acc_nbr(+)
                AND     v_stg_tjf_type20.txn_ref_datetime  = typ1_tjf_error_data.txn_ref_datetime(+)
              ) rt01_exists
      FROM    v_stg_tjf_type20
      WHERE   v_stg_tjf_type20.input_data_id = p_input_data_id
      AND NOT EXISTS
      (
        SELECT  1
        FROM    rt20_purse
        WHERE   rt20_purse.txn_seq_nbr = v_stg_tjf_type20.txn_sequence
      );

      TYPE typ_rt20_recycle IS TABLE OF cur_rt20_recycle%ROWTYPE
      INDEX BY BINARY_INTEGER;

      buf_rt20_recycle   typ_rt20_recycle;

    BEGIN
      INSERT INTO typ20_tjf_error_data
      (
        cmpy_id,
        pat,
        pfc,
        acc_nbr,
        ol1,
        ol2,
        ol3,
        txn_effective_date,
        card_nbr,
        addendum_id,
        purse_id_1,
        purse_ccy_1,
        purse_amt_1,
        purse_amt_crdb_1,
        purse_ccy_exp_1,
        purse_id_2,
        purse_ccy_2,
        purse_amt_2,
        purse_amt_crdb_2,
        purse_ccy_exp_2,
        purse_id_3,
        purse_ccy_3,
        purse_amt_3,
        purse_amt_crdb_3,
        purse_ccy_exp_3,
        purse_id_4,
        purse_ccy_4,
        purse_amt_4,
        purse_amt_crdb_4,
        purse_ccy_exp_4,
        purse_id_5,
        purse_ccy_5,
        purse_amt_5,
        purse_amt_crdb_5,
        purse_ccy_exp_5,
        purse_id_6,
        purse_ccy_6,
        purse_amt_6,
        purse_amt_crdb_6,
        purse_ccy_exp_6,
        purse_id_7,
        purse_ccy_7,
        purse_amt_7,
        purse_amt_crdb_7,
        purse_ccy_exp_7,
        purse_id_8,
        purse_ccy_8,
        purse_amt_8,
        purse_amt_crdb_8,
        purse_ccy_exp_8,
        purse_id_9,
        purse_ccy_9,
        purse_amt_9,
        purse_amt_crdb_9,
        purse_ccy_exp_9,
        purse_id_10,
        purse_ccy_10,
        purse_amt_10,
        purse_amt_crdb_10,
        purse_ccy_exp_10,
        purse_id_11,
        purse_ccy_11,
        purse_amt_11,
        purse_amt_crdb_11,
        purse_ccy_exp_11,
        purse_id_12,
        purse_ccy_12,
        purse_amt_12,
        purse_amt_crdb_12,
        purse_ccy_exp_12,
        purse_id_13,
        purse_ccy_13,
        purse_amt_13,
        purse_amt_crdb_13,
        purse_ccy_exp_13,
        purse_id_14,
        purse_ccy_14,
        purse_amt_14,
        purse_amt_crdb_14,
        purse_ccy_exp_14,
        purse_id_15,
        purse_ccy_15,
        purse_amt_15,
        purse_amt_crdb_15,
        purse_ccy_exp_15,
        purse_id_16,
        purse_ccy_16,
        purse_amt_16,
        purse_amt_crdb_16,
        purse_ccy_exp_16,
        purse_id_17,
        purse_ccy_17,
        purse_amt_17,
        purse_amt_crdb_17,
        purse_ccy_exp_17,
        purse_id_18,
        purse_ccy_18,
        purse_amt_18,
        purse_amt_crdb_18,
        purse_ccy_exp_18,
        purse_id_19,
        purse_ccy_19,
        purse_amt_19,
        purse_amt_crdb_19,
        purse_ccy_exp_19,
        purse_id_20,
        purse_ccy_20,
        purse_amt_20,
        purse_amt_crdb_20,
        purse_ccy_exp_20,
        alternate_id_1,
        branch,
        lprog_id_1,
        regular_reward_amt_1,
        bonus_reward_amt_1,
        monetary_ind_1,
        lprog_amt_crdb_1,
        lprog_id_2,
        regular_reward_amt_2,
        bonus_reward_amt_2,
        monetary_ind_2,
        lprog_amt_crdb_2,
        lprog_id_3,
        regular_reward_amt_3,
        bonus_reward_amt_3,
        monetary_ind_3,
        lprog_amt_crdb_3,
        lprog_id_4,
        regular_reward_amt_4,
        bonus_reward_amt_4,
        monetary_ind_4,
        lprog_amt_crdb_4,
        lprog_id_5,
        regular_reward_amt_5,
        bonus_reward_amt_5,
        monetary_ind_5,
        lprog_amt_crdb_5,
        txn_ref_datetime,
        txn_sequence,
        input_data_id
      )
      SELECT  cmpy_id,
              pat,
              pfc,
              acc_nbr,
              ol1,
              ol2,
              ol3,
              txn_effective_date,
              card_nbr,
              addendum_id,
              purse_id_1,
              purse_ccy_1,
              purse_amt_1,
              purse_amt_crdb_1,
              purse_ccy_exp_1,
              purse_id_2,
              purse_ccy_2,
              purse_amt_2,
              purse_amt_crdb_2,
              purse_ccy_exp_2,
              purse_id_3,
              purse_ccy_3,
              purse_amt_3,
              purse_amt_crdb_3,
              purse_ccy_exp_3,
              purse_id_4,
              purse_ccy_4,
              purse_amt_4,
              purse_amt_crdb_4,
              purse_ccy_exp_4,
              purse_id_5,
              purse_ccy_5,
              purse_amt_5,
              purse_amt_crdb_5,
              purse_ccy_exp_5,
              purse_id_6,
              purse_ccy_6,
              purse_amt_6,
              purse_amt_crdb_6,
              purse_ccy_exp_6,
              purse_id_7,
              purse_ccy_7,
              purse_amt_7,
              purse_amt_crdb_7,
              purse_ccy_exp_7,
              purse_id_8,
              purse_ccy_8,
              purse_amt_8,
              purse_amt_crdb_8,
             purse_ccy_exp_8,
              purse_id_9,
              purse_ccy_9,
              purse_amt_9,
              purse_amt_crdb_9,
              purse_ccy_exp_9,
              purse_id_10,
              purse_ccy_10,
              purse_amt_10,
              purse_amt_crdb_10,
              purse_ccy_exp_10,
              purse_id_11,
              purse_ccy_11,
              purse_amt_11,
              purse_amt_crdb_11,
              purse_ccy_exp_11,
              purse_id_12,
              purse_ccy_12,
              purse_amt_12,
              purse_amt_crdb_12,
              purse_ccy_exp_12,
              purse_id_13,
              purse_ccy_13,
              purse_amt_13,
              purse_amt_crdb_13,
              purse_ccy_exp_13,
              purse_id_14,
              purse_ccy_14,
              purse_amt_14,
              purse_amt_crdb_14,
              purse_ccy_exp_14,
              purse_id_15,
              purse_ccy_15,
              purse_amt_15,
              purse_amt_crdb_15,
              purse_ccy_exp_15,
              purse_id_16,
              purse_ccy_16,
              purse_amt_16,
              purse_amt_crdb_16,
              purse_ccy_exp_16,
              purse_id_17,
              purse_ccy_17,
              purse_amt_17,
              purse_amt_crdb_17,
              purse_ccy_exp_17,
              purse_id_18,
              purse_ccy_18,
              purse_amt_18,
              purse_amt_crdb_18,
              purse_ccy_exp_18,
              purse_id_19,
              purse_ccy_19,
              purse_amt_19,
              purse_amt_crdb_19,
              purse_ccy_exp_19,
              purse_id_20,
              purse_ccy_20,
              purse_amt_20,
              purse_amt_crdb_20,
              purse_ccy_exp_20,
              alternate_id_1,
              branch,
              lprog_id_1,
              regular_reward_amt_1,
              bonus_reward_amt_1,
              monetary_ind_1,
              lprog_amt_crdb_1,
              lprog_id_2,
              regular_reward_amt_2,
              bonus_reward_amt_2,
              monetary_ind_2,
              lprog_amt_crdb_2,
              lprog_id_3,
              regular_reward_amt_3,
              bonus_reward_amt_3,
              monetary_ind_3,
              lprog_amt_crdb_3,
              lprog_id_4,
              regular_reward_amt_4,
              bonus_reward_amt_4,
              monetary_ind_4,
              lprog_amt_crdb_4,
              lprog_id_5,
              regular_reward_amt_5,
              bonus_reward_amt_5,
              monetary_ind_5,
              lprog_amt_crdb_5,
              txn_ref_datetime,
              txn_sequence,
              input_data_id
      FROM  v_stg_tjf_type20
      WHERE v_stg_tjf_type20.input_data_id = p_input_data_id
      AND   NOT EXISTS
      (
        SELECT  1
        FROM    rt20_purse
        WHERE   rt20_purse.txn_seq_nbr = v_stg_tjf_type20.txn_sequence
      ) ;

      v_number_of_orphaned_20 := SQL%ROWCOUNT;

      standard_audit.log_entry
      (
        p_program     => 'settlement_validation.purse_missing_settlement',
        p_step_name   => 'Orphaned type 20',
        p_log_entry   => 'Number of Orphaned type 20 records found '||v_number_of_orphaned_20,
        p_level       => 2
      );

      OPEN cur_rt20_recycle;
      FETCH cur_rt20_recycle
      BULK COLLECT
      INTO buf_rt20_recycle;
      CLOSE cur_rt20_recycle;

      standard_audit.log_entry
      (
        p_program     => 'settlement_validation.purse_missing_settlement',
        p_step_name   => 'purse_missing_settlement',
        p_log_entry   => '',
        p_level       => 2
      );

      IF buf_rt20_recycle.COUNT > 0 THEN
        FOR v_ptr IN buf_rt20_recycle.FIRST..buf_rt20_recycle.LAST LOOP
          insert_tran_recycle
          (
            p_sequence  => buf_rt20_recycle(v_ptr).txn_sequence,
            p_txn_type  => 'TYP20',
            p_rel_date  => NULL,
            p_status    => 'REC'
          );

          IF buf_rt20_recycle(v_ptr).rt01_exists = 0 THEN
            insert_error
            (
              p_check_num   => 107,
              p_type        => 'TYP1',
              p_sequence    => buf_rt20_recycle(v_ptr).txn_sequence,
              p_message     => 'Type 20 record without a matching Type 01 record'
            );

            standard_error.raise_program_error
            (
              p_program         => 'settlement_validation.purse_missing_settlement',
              p_message         => 'Type 20 record without a matching Type 01 record',
              p_location        => 'Validation Error',
              p_additional_info => 'TXN Sequence: '||buf_rt20_recycle(v_ptr).txn_sequence,
              p_raise_exception => FALSE
            );
          END IF;
        END LOOP;
      END IF;

      COMMIT;

    EXCEPTION
      WHEN OTHERS THEN
        standard_error.when_others_exception
        (
          p_program => 'settlement_validation.purse_missing_settlement'
        );

    END purse_missing_settlement;

  ----------------------------------------------------------------------------------------------------------------------------------

    PROCEDURE clean_error_data_tables IS

    BEGIN
      DELETE typ1_tjf_error_data
      WHERE EXISTS
      (
        SELECT  1
        FROM    rt01
        WHERE   rt01.txn_seq_nbr = typ1_tjf_error_data.txn_sequence
      );

      standard_audit.log_entry
      (
        p_program     => 'settlement_validation.clean_error_data_tables',
        p_step_name   => 'Delete type 01',
        p_log_entry   => 'Record type 01 records deleted '||SQL%ROWCOUNT,
        p_level       => 4
      );

      DELETE typ20_tjf_error_data
      WHERE EXISTS
      (
        SELECT  1
        FROM    rt20_purse
        WHERE   rt20_purse.txn_seq_nbr = typ20_tjf_error_data.txn_sequence
      );

      standard_audit.log_entry
      (
        p_program     => 'settlement_validation.clean_error_data_tables',
        p_step_name   => 'Delete type 20',
        p_log_entry   => 'Record type 20 records deleted '||SQL%ROWCOUNT,
        p_level       => 4
      );

    EXCEPTION
      WHEN OTHERS THEN
        standard_error.when_others_exception
        (
          p_program => 'settlement_validation.clean_error_data_tables'
        );

    END clean_error_data_tables;

  ----------------------------------------------------------------------------------------------------------------------------------

  BEGIN
    standard_audit.open_audit_trail
    (
      p_namespace   => 'SETTLEMENT_VALIDATION.PROCESS_TJF',
      p_program     => 'settlement_validation.process_tjf'
    );

    BEGIN
      v_etl_date := utl_etl_control.get_current_etl_date;
    EXCEPTION
      WHEN OTHERS THEN
        v_etl_date := NULL;
    END;

    -- If ETL date is not set then try business date off of the header record.
    IF v_etl_date IS NULL THEN
      SELECT  MAX(TRIM(SUBSTR(the_record,5,6)))
      INTO    v_etl_date_string
      FROM    tjf_records_stg
      WHERE   input_data_id=p_input_data_id
      AND     RECORD_TYPE = '00';

      IF v_etl_date_string IS NULL THEN
        standard_error.raise_program_error
        (
          p_program          => 'settlement_validation.process_tjf',
          p_message          => 'TJF header record is not found.'
        );
      END IF;

      BEGIN
        v_etl_date := TO_DATE(v_etl_date_string,'YYMMDD');
      EXCEPTION
        WHEN OTHERS THEN
          standard_error.raise_program_error
          (
            p_program          => 'settlement_validation.process_tjf',
            p_message          => 'TJF header record date is invalid.'
          );
      END;
    END IF;

    IF v_etl_date IS NULL THEN
      standard_error.raise_program_error
      (
        p_program         => 'settlement_validation.process_tjf',
        p_message         => 'ETL date is not set and business date off of Header record could not be found',
        p_additional_info => 'p_input_data_id: '||p_input_data_id
      );
    END IF;

    recycling_report.initialise
    (
      p_business_date     => v_etl_date,
      p_input_data_id     => p_input_data_id,
      p_record_type       => '01'
    );

    standard_audit.log_entry
    (
      p_program     => 'settlement_validation.process_tjf',
      p_step_name   => 'process_tjf_records',
      p_log_entry   => 'p_input_data_id: '||p_input_data_id,
      p_level       => 2
    );

    process_tjf_records;

    standard_audit.log_entry
    (
      p_program     => 'settlement_validation.process_tjf',
      p_step_name   => 'purse_missing_settlement',
      p_log_entry   => '',
      p_level       => 2
    );

    purse_missing_settlement;

    standard_audit.log_entry
    (
      p_program     => 'settlement_validation.process_tjf',
      p_step_name   => 'clean_error_data_tables',
      p_log_entry   => '',
      p_level       => 2
    );

    clean_error_data_tables;

    recycling_report.finalise;

    standard_audit.close_audit_trail
    (
      p_program => 'settlement_validation.process_tjf'
    );

  EXCEPTION
    WHEN OTHERS THEN
      standard_audit.close_audit_trail
      (
        p_program => 'settlement_validation.process_tjf'
      );
      standard_error.when_others_exception
      (
        p_program         => 'settlement_validation.process_tjf',
        p_additional_info => 'p_input_data_id: '||p_input_data_id
      );

  END process_tjf;

  ----------------------------------------------------------------------------------------------------------------------------------

  PROCEDURE control(pINEnv IN VARCHAR2) IS

    v_file_seq1             NUMBER;
    v_file_seq2             NUMBER;

    --------------------------------------------------------------------------------------------------------------------------------

    FUNCTION get_file_seq(p_env IN VARCHAR2) RETURN NUMBER IS

      v_ret_val NUMBER;

    BEGIN
      IF p_env = 'PROD' THEN
        SELECT  file_seq
        INTO    v_ret_val
        FROM    mcp_etl_control
        WHERE   file_type = 'CRDTXN'
        AND     file_name LIKE '%TVXSP%'
        AND     processed = 'N';
      ELSE
        SELECT  file_seq
        INTO    v_ret_val
        FROM    mcp_etl_control
        WHERE   file_type = 'CRDTXN'
        AND     file_name LIKE '%TVXST%'
        AND     processed = 'N';
      END IF;

      RETURN (v_ret_val);

    EXCEPTION
      WHEN OTHERS THEN
        standard_error.when_others_exception
        (
          p_program         => 'settlement_validation.get_file_seq',
          p_additional_info => 'p_env: '||p_env
        );

    END get_file_seq;

    --------------------------------------------------------------------------------------------------------------------------------

  BEGIN
   --F32524 chnages NAB/ISRAELPOST
   utl_etl_control.create_process_control_log_id ('SETTLEMENT_VALIDATION','STARTED');

   /* INSERT INTO gfl_common_data.etl_process_control_log
      (
        process_control_id,
        data_set_log_id,
        process_name,
        start_date_time,
        end_date_time,
        status,
        last_updated)
    VALUES
     (
        SEQ_PROCESS_CONTROL_LOG_ID.nextval,
        (SELECT MAX(data_set_log_id) FROM  etl_data_set_log ) ,
        'SETTLEMENT_VALIDATION',
        SYSDATE,
        SYSDATE,
        'STARTED',
        SYSDATE);
    COMMIT;*/
     --F32524 chnages NAB/ISRAELPOST
    IF pINEnv IN ('BOTH', 'PROD', 'TEST') THEN
      IF pINEnv = 'BOTH' THEN
        v_file_seq1 := get_file_seq('PROD');
        v_file_seq2 := get_file_seq('TEST');
      ELSE
        v_file_seq1 := get_file_seq(pINEnv);
        v_file_seq2 := -1;
      END IF;
    ELSE
      standard_error.raise_program_error
      (
        p_program          => 'settlement_validation.control',
        p_message          => 'Invalid Environment Parameter '|| pINEnv
      );
    END IF;

    IF v_file_seq1 = -99 OR v_file_seq2 = -99 THEN
      standard_error.raise_program_error
      (
        p_program          => 'settlement_validation.control',
        p_message          => 'Invalid TJF file'
      );
    END IF;

    process_tjf(v_file_seq1);

    IF v_file_seq2 > 0 THEN
      process_tjf(v_file_seq2);
    END IF;
   --F32524 chnages NAB/ISRAELPOST

    UTL_ETL_CONTROL.update_process_control_log_id ('SETTLEMENT_VALIDATION','COMPLETED') ;
   /* UPDATE etl_process_control_log
      SET   status = 'COMPLETED',
            end_date_time = sysdate,
            last_updated = sysdate
      WHERE process_name = 'SETTLEMENT_VALIDATION'
      AND   TRUNC(last_updated) = TRUNC(SYSDATE)
      AND   status ='STARTED';

    COMMIT;*/
   --F32524 chnages NAB/ISRAELPOST

  EXCEPTION
    WHEN OTHERS THEN
   --F32524 chnages NAB/ISRAELPOST
      UTL_ETL_CONTROL.update_process_control_log_id ('SETTLEMENT_VALIDATION','FAILED') ;
   --F32524 chnages NAB/ISRAELPOST
      standard_error.when_others_exception
      (
        p_program         => 'settlement_validation.control',
        p_additional_info => 'pINEnv: '||pINEnv
      );

  END control;

  ----------------------------------------------------------------------------------------------------------------------------------

  -- Note that clean must not be used if settlement validation completed
  -- as processed error data will be lost.

  PROCEDURE clean_run(ip_input_data_id NUMBER) IS

  BEGIN
    DELETE FROM rt20_loyalty
    WHERE input_data_id = ip_input_data_id;

    DELETE FROM rt20_purse
    WHERE input_data_id = ip_input_data_id;

    DELETE FROM rt01
    WHERE input_data_id = ip_input_data_id;

    DELETE FROM spend_purse_tjf_recycle
    WHERE input_data_id = ip_input_data_id;

    DELETE FROM typ20_tjf_error_data
    WHERE input_data_id = ip_input_data_id;

    DELETE FROM typ1_tjf_error_data
    WHERE input_data_id = ip_input_data_id;

    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      standard_error.when_others_exception
      (
        p_program => 'settlement_validation.clean_run'
      );

  END clean_run;

  ----------------------------------------------------------------------------------------------------------------------------------

END settlement_validation;
/