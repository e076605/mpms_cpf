CREATE OR REPLACE PROCEDURE CPRADMIN.PR_SYN_RA_CLIENT_EXTRA_ATTR (i_unique_id NUMBER,i_audit_group_id NUMBER)
IS
 /* *********************************************************
 REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.1        08/02/2018  Swati      Added two columns for Feature F57475 SCP and MCP Merge     
************************************************************/
  v_pk_column_name VARCHAR2(30);
BEGIN
  FOR c1 IN (SELECT * FROM RA_CLIENT_EXTRA_ATTR_A
    WHERE ID = i_unique_id
      AND audit_group_id = i_audit_group_id)
  LOOP
    IF c1.dml_type = 'I' THEN
      INSERT INTO RA_CLIENT_EXTRA_ATTR (ID,
        RA_CLIENT_ID,
        BIN_SPONSOR_ID,
        CLIENT_MCP_FLAG,
        CLIENT_EXCEL_INDICATOR,
        CLIENT_SETTLEMENT_FORMAT,
        INCLUDE_REV_NET_SETTLE,
        FEES_COMM_INV_TEMPLATE,
        ALTERNATE_ID,
        CARDNUMBER_REQ,
        PROXY_CARDNBR_REQ,
        TRANSACTION_TIMESTAMP_REQ, -- Added for CMP1191 Canada POST
        LAST_UPDATED,
        LAST_UPDATED_BY,
        AUDIT_GROUP_ID,
        MIGRATED_SCP,
        EXCLUDE_MATCHING
       )
        VALUES(c1.ID,
        c1.RA_CLIENT_ID,
        c1.BIN_SPONSOR_ID,
        c1.CLIENT_MCP_FLAG,
        c1.CLIENT_EXCEL_INDICATOR,
        c1.CLIENT_SETTLEMENT_FORMAT,
        c1.INCLUDE_REV_NET_SETTLE,
        c1.FEES_COMM_INV_TEMPLATE,
        c1.ALTERNATE_ID,
        c1.CARDNUMBER_REQ,
        c1.PROXY_CARDNBR_REQ,
        c1.TRANSACTION_TIMESTAMP_REQ, -- Added for CMP1191 Canada POST
        c1.LAST_UPDATED,
        c1.LAST_UPDATED_BY,
        c1.AUDIT_GROUP_ID,
        c1.MIGRATED_SCP,
        c1.EXCLUDE_MATCHING
   
        );
    ELSIF c1.dml_type = 'U' THEN
      UPDATE RA_CLIENT_EXTRA_ATTR SET AUDIT_GROUP_ID=c1.AUDIT_GROUP_ID,
        RA_CLIENT_ID = c1.RA_CLIENT_ID,
        BIN_SPONSOR_ID = c1.BIN_SPONSOR_ID,
        CLIENT_MCP_FLAG = c1.CLIENT_MCP_FLAG,
        CLIENT_EXCEL_INDICATOR = c1.CLIENT_EXCEL_INDICATOR,
        CLIENT_SETTLEMENT_FORMAT = c1.CLIENT_SETTLEMENT_FORMAT,
        INCLUDE_REV_NET_SETTLE = c1.INCLUDE_REV_NET_SETTLE,
        FEES_COMM_INV_TEMPLATE = c1.FEES_COMM_INV_TEMPLATE,
        ALTERNATE_ID =c1.ALTERNATE_ID,
        CARDNUMBER_REQ=c1.CARDNUMBER_REQ,
        PROXY_CARDNBR_REQ=c1.PROXY_CARDNBR_REQ,
        TRANSACTION_TIMESTAMP_REQ=c1.TRANSACTION_TIMESTAMP_REQ, -- Added for CMP 1191 Canada POST
        LAST_UPDATED = c1.LAST_UPDATED,
        LAST_UPDATED_BY = c1.LAST_UPDATED_BY,
        MIGRATED_SCP =c1.MIGRATED_SCP,
        EXCLUDE_MATCHING = c1.EXCLUDE_MATCHING
     WHERE ID = i_unique_id;
    ELSIF c1.dml_type = 'D' THEN
      DELETE RA_CLIENT_EXTRA_ATTR
        WHERE ID = i_unique_id;
    END IF;
  END LOOP;
END;
/
