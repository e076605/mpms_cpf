DROP VIEW CPRADMIN.RA_CLIENT_EXTRA_ATTR_VW;
/* *********************************************************
 REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.1        07/03/2018  Swati      Added one column CORPORATE_REFERENCES for correct invoicing options for Corporate Client
************************************************************/
/* Formatted on 3/2/2018 6:32:18 PM (QP5 v5.227.12220.39724) */
CREATE OR REPLACE FORCE VIEW CPRADMIN.RA_CLIENT_EXTRA_ATTR_VW
(
   ID,
   RA_CLIENT_ID,
   BIN_SPONSOR_ID,
   CLIENT_MCP_FLAG,
   CLIENT_EXCEL_INDICATOR,
   CLIENT_SETTLEMENT_FORMAT,
   INCLUDE_REV_NET_SETTLE,
   FEES_COMM_INV_TEMPLATE,
   ALTERNATE_ID,
   CARDNUMBER_REQ,
   PROXY_CARDNBR_REQ,
   TRANSACTION_TIMESTAMP_REQ,
   LAST_UPDATED,
   LAST_UPDATED_BY,
   AUDIT_GROUP_ID,
   MIGRATED_SCP,
   EXCLUDE_MATCHING,
   CORPORATE_REFERENCES  -----------Correct option of invoicing for Corporate Client
)
AS
   SELECT ID,
          RA_CLIENT_ID,
          BIN_SPONSOR_ID,
          CLIENT_MCP_FLAG,
          CLIENT_EXCEL_INDICATOR,
          CLIENT_SETTLEMENT_FORMAT,
          INCLUDE_REV_NET_SETTLE,
          FEES_COMM_INV_TEMPLATE,
          ALTERNATE_ID,
          CARDNUMBER_REQ,
          PROXY_CARDNBR_REQ,
          TRANSACTION_TIMESTAMP_REQ,
          LAST_UPDATED,
          LAST_UPDATED_BY,
          AUDIT_GROUP_ID,
          MIGRATED_SCP,
          EXCLUDE_MATCHING,
          CORPORATE_REFERENCES  -----------Correct option of invoicing for Corporate Client
     FROM RA_CLIENT_EXTRA_ATTR;