DECLARE
    v_count  NUMBER;

BEGIN
    select count(*) 
    into 
    v_count from all_tables where table_name = 'F_IPS_ICCR_DUAL_MSG_BKP0152018' and OWNER = 'CPR_IPS_STAGING';
    
    IF v_count > 0 THEN
    
        EXECUTE IMMEDIATE 'DROP TABLE F_IPS_ICCR_DUAL_MSG_BKP0152018';
            
    END IF;

END;
/


DECLARE
    v_count  NUMBER;

BEGIN
    select count(*) 
    into 
    v_count from all_tables where table_name = 'F_IPS_ICCR_SINGLE_MSG_BKP0152018' and OWNER = 'CPR_IPS_STAGING';
    
    IF v_count > 0 THEN
    
        EXECUTE IMMEDIATE 'DROP TABLE F_IPS_ICCR_SINGLE_MSG_BKP0152018';
            
    END IF;

END;
/

DECLARE
    v_count  NUMBER;

BEGIN
    select count(*) 
    into 
    v_count from all_tables where table_name = 'STG_ACOF_CANDIDATES_BKP0152018' and OWNER = 'CPR_IPS_STAGING';
    
    IF v_count > 0 THEN
    
        EXECUTE IMMEDIATE 'DROP TABLE STG_ACOF_CANDIDATES_BKP0152018';
            
    END IF;

END;
/



DECLARE
    v_count  NUMBER;

BEGIN
    select count(*) 
    into 
    v_count from all_tables where table_name = 'STG_IPS_ACOF_DELTAS_BKP0152018' and OWNER = 'CPR_IPS_STAGING';
    
    IF v_count > 0 THEN
    
        EXECUTE IMMEDIATE 'DROP TABLE STG_IPS_ACOF_DELTAS_BKP0152018';
            
    END IF;

END;
/


DECLARE
    v_count  NUMBER;

BEGIN
    select count(*) 
    into 
    v_count from all_tables where table_name = 'STG_CURRENT_ACOF_BKP0152018' and OWNER = 'CPR_IPS_STAGING';
    
    IF v_count > 0 THEN
    
        EXECUTE IMMEDIATE 'DROP TABLE STG_CURRENT_ACOF_BKP0152018';
            
    END IF;

END;
/


