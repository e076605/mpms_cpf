	DECLARE
	v_cnt_check NUMBER;
	BEGIN
	SELECT count(1) into v_cnt_check
	from all_tab_columns
	where column_name = 'CHILD_BUSINESS_UNIT_NAME' 
	and table_name = 'RA_BIN_CURRENCY'
	and OWNER = 'CPRADMIN';

	IF v_cnt_check = 0 THEN


	execute immediate 'ALTER TABLE RA_BIN_CURRENCY ADD CHILD_BUSINESS_UNIT_NAME varchar2(30)';
	execute immediate 'grant select on RA_BIN_CURRENCY to GFL_MCP_CORE with grant option';


	END IF;
	exception
	When others then
	dbms_output.put_line(SQLERRM||'  Column creation failed for table  RA_CLIENT_EXTRA_ATTR');
	End;

	/
