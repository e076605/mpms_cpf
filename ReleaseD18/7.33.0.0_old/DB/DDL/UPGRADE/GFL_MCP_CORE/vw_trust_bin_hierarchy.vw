CREATE OR REPLACE FORCE VIEW GFL_MCP_CORE.VW_TRUST_BIN_HIERARCHY
(
   BIN_COUNTRY,
   BIN,
   ICA_NBR,
   PARENT_BUSINESS_UNIT_NAME,
   CHILD_BUSINESS_UNIT_NAME,
   NAME,
   ID,
   IS_MCP_BIN_SPONSOR_FLAG,
   BIN_SPONSOR_TYPE_CODE,
   IS_PRIMARY_ICA,
   COUNTRY_TAX,
   IS_PRIMARY_BIN
)
AS
   SELECT BIN_C.name AS BIN_COUNTRY,
          B.BIN,
          I.ICA_NBR,
          I.PARENT_BUSINESS_UNIT_NAME,
          B.CHILD_BUSINESS_UNIT_NAME,
          BS.name,
          BS.id,
          bs.mcp_flag IS_MCP_BIN_SPONSOR_FLAG,
          bs.bin_sponsor_type_code,
         NVL (i.IS_PRIMARY_ICA, 'N') AS IS_PRIMARY_ICA,
          NVL (bin_c.manage_country_tax, 0) COUNTRY_TAX,
          NVL (b.is_primary_bin, 'N') IS_PRIMARY_BIN
     FROM ica_agreement i,
          bin b,
          BIN_SPONSOR BS,
          ORG_REGION BIN_R,
          COUNTRY bin_c
    WHERE     BS.id = i.BIN_SPONSOR_ID(+)
          AND i.bin_sponsor_id = b.bin_sponsor_id(+)
          AND I.ICA_NBR = B.ICA_NBR(+)
          AND B.ORG_REGION_CODE = BIN_R.CODE(+)
          AND BIN_R.COUNTRY_CODE = BIN_C.ISO_CODE(+)
          AND BS.MCP_FLAG = 'Y'
UNION
SELECT BIN_C.name AS BIN_COUNTRY,
          B.BIN,
          I.ICA_NBR,
          I.PARENT_BUSINESS_UNIT_NAME,
          UPPER(TRIM(rbc.CHILD_BUSINESS_UNIT_NAME)),
          BS.name,
          BS.id,
          bs.mcp_flag IS_MCP_BIN_SPONSOR_FLAG,
          bs.bin_sponsor_type_code,
         NVL (i.IS_PRIMARY_ICA, 'N') AS IS_PRIMARY_ICA,
          NVL (bin_c.manage_country_tax, 0) COUNTRY_TAX,
          NVL (b.is_primary_bin, 'N') IS_PRIMARY_BIN
     FROM ica_agreement i,
          bin b,
          BIN_SPONSOR BS,
          ORG_REGION BIN_R,
          COUNTRY bin_c,
         Ra_bin_currency rbc
    WHERE     BS.id = i.BIN_SPONSOR_ID(+)
          AND i.bin_sponsor_id = b.bin_sponsor_id(+)
          AND I.ICA_NBR = B.ICA_NBR(+)
          AND B.ORG_REGION_CODE = BIN_R.CODE(+)
          AND BIN_R.COUNTRY_CODE = BIN_C.ISO_CODE(+)
          AND BS.MCP_FLAG = 'Y'
        And   b.bin = rbc.bin
        AND rbc.CHILD_BUSINESS_UNIT_NAME IS NOT NULL;
