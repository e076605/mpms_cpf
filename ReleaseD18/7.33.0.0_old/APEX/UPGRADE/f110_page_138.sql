set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_050100 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2016.08.24'
,p_release=>'5.1.1.00.08'
,p_default_workspace_id=>2094526905262116
,p_default_application_id=>110
,p_default_owner=>'CPRADMIN'
);
end;
/
prompt --application/set_environment
 
prompt APPLICATION 110 - Mastercard Stage
--
-- Application Export:
--   Application:     110
--   Name:            Mastercard Stage
--   Date and Time:   15:18 Friday March 9, 2018
--   Exported By:     ADMIN
--   Flashback:       0
--   Export Type:     Page Export
--   Version:         5.1.1.00.08
--   Instance ID:     69309211345420
--

prompt --application/pages/delete_00138
begin
wwv_flow_api.remove_page (p_flow_id=>wwv_flow.g_flow_id, p_page_id=>138);
end;
/
prompt --application/pages/page_00138
begin
wwv_flow_api.create_page(
 p_id=>138
,p_user_interface_id=>wwv_flow_api.id(69579080781817559)
,p_tab_set=>'TS_OPS'
,p_name=>'Maintain RA_BIN_CURRENCY'
,p_page_mode=>'NORMAL'
,p_step_title=>'Maintain RA_BIN_CURRENCY'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title=>'Maintain RA_BIN_CURRENCY'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'NO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script language="JavaScript" type="text/javascript">',
'<!--',
'',
' htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';',
'',
'//-->',
'</script>'))
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'ADMIN'
,p_last_upd_yyyymmddhh24miss=>'20180309151403'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(143399345221514640)
,p_plug_name=>'Create New BIN CURRENCY'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(163489526479947943)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>1
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(143399539119514644)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(143399345221514640)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(163488237414947942)
,p_button_image_alt=>'Cancel'
,p_button_position=>'REGION_TEMPLATE_CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:602:&SESSION.::&DEBUG.:::'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(143400144876514646)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(143399345221514640)
,p_button_name=>'CREATE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(163488237414947942)
,p_button_image_alt=>'Create'
,p_button_position=>'REGION_TEMPLATE_CREATE'
,p_button_condition=>'P138_BIN_CURRENCY'
,p_button_condition_type=>'ITEM_IS_NULL'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(143402645098514670)
,p_branch_action=>'f?p=&APP_ID.:602:&SESSION.::&DEBUG.::P138_BIN:&P138_BIN.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1
,p_branch_condition_type=>'REQUEST_IN_CONDITION'
,p_branch_condition=>'CREATE, SAVE'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(143402849701514671)
,p_branch_action=>'f?p=&APP_ID.:602:&SESSION.::&DEBUG.::P138_BIN:&P138_BIN.'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
,p_branch_condition_type=>'REQUEST_IN_CONDITION'
,p_branch_condition=>'DELETE'
,p_branch_comment=>'Created 02-NOV-2010 16:14 by USER1'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2713378316788948)
,p_name=>'P138_CHILD_BUSINESS_UNIT_NAME'
,p_item_sequence=>12
,p_item_plug_id=>wwv_flow_api.id(143399345221514640)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Child business unit name'
,p_source=>'CHILD_BUSINESS_UNIT_NAME'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>30
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(163502726737947949)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(143400331881514646)
,p_name=>'P138_BIN'
,p_item_sequence=>2
,p_item_plug_id=>wwv_flow_api.id(143399345221514640)
,p_prompt=>'Bin'
,p_source=>'BIN'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(163502726737947949)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(143400538520514649)
,p_name=>'P138_BIN_CURRENCY'
,p_item_sequence=>3
,p_item_plug_id=>wwv_flow_api.id(143399345221514640)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Bin Currency'
,p_source=>'BIN_CURRENCY'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LST_CURRENCY_ISO'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select CURRENCY_ISO_CODE d, CURRENCY_ISO_CODE r',
'from   RA_CURRENCY',
'order by 1'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'None'
,p_lov_null_value=>'None'
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(163502726737947949)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(143400747137514650)
,p_name=>'P138_NOMINATED_SETTLEMENT_CURR'
,p_item_sequence=>4
,p_item_plug_id=>wwv_flow_api.id(143399345221514640)
,p_use_cache_before_default=>'NO'
,p_item_default=>'GBP'
,p_prompt=>'Nominated Settlement Currency'
,p_source=>'NOMINATED_SETTLEMENT_CURRENCY'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LST_CURRENCY_ISO'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select CURRENCY_ISO_CODE d, CURRENCY_ISO_CODE r',
'from   RA_CURRENCY',
'order by 1'))
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(163502928419947949)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(143400943392514655)
,p_name=>'P138_TRAVELEX_CURRENCY_CODE'
,p_item_sequence=>5
,p_item_plug_id=>wwv_flow_api.id(143399345221514640)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Travelex Currency Code'
,p_source=>'TRAVELEX_CURRENCY_CODE'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>32
,p_cMaxlength=>2
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(163502726737947949)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(143401153905514655)
,p_name=>'P138_PORTFOLIO_CODE'
,p_item_sequence=>6
,p_item_plug_id=>wwv_flow_api.id(143399345221514640)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Portfolio Code'
,p_source=>'PORTFOLIO_CODE'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>32
,p_cMaxlength=>50
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(163502726737947949)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(143401348857514656)
,p_name=>'P138_BIN_MARGIN'
,p_item_sequence=>7
,p_item_plug_id=>wwv_flow_api.id(143399345221514640)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Bin Margin'
,p_source=>'BIN_MARGIN'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>32
,p_cMaxlength=>255
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(163502726737947949)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(143401548914514656)
,p_name=>'P138_BIN_MARGIN_DISPLAY'
,p_item_sequence=>8
,p_item_plug_id=>wwv_flow_api.id(143399345221514640)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Bin Margin Display'
,p_source=>'BIN_MARGIN_DISPLAY'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>32
,p_cMaxlength=>255
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(163502726737947949)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(143401749021514656)
,p_name=>'P138_TRAVELEX_BANK_ACCOUNT_ID'
,p_item_sequence=>9
,p_item_plug_id=>wwv_flow_api.id(143399345221514640)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Travelex Bank Account'
,p_source=>'TRAVELEX_BANK_ACCOUNT_ID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select distinct ''(''||RA_TRAVELEX_BANK_ACCOUNT.currency||'') ''||BANK_ACCOUNT_NAME||'' ''||TRAVELEX_BANK_ACCOUNT_NUMBER||'' ''||TRAVELEX_LEGAL_ENTITY d, RA_TRAVELEX_BANK_ACCOUNT_ID r',
'from   RA_TRAVELEX_BANK_ACCOUNT, vw_d_bin_ips',
'where ',
'vw_d_bin_ips.bin_currency = RA_TRAVELEX_BANK_ACCOUNT.currency and',
'vw_d_bin_ips.history_status = ''C'' ',
'order by 1',
'',
''))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'None'
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(163502928419947949)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(143401949798514656)
,p_name=>'P138_USE_BIN_MARGIN_FLAG'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(143399345221514640)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Use Bin Margin Flag'
,p_source=>'USE_BIN_MARGIN_FLAG'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>1
,p_cMaxlength=>255
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(163502726737947949)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(143402135511514656)
,p_name=>'P138_BIN_REFERENCE_NAME'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_api.id(143399345221514640)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Bin Reference Name'
,p_source=>'BIN_REFERENCE_NAME'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>32
,p_cMaxlength=>60
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(163502726737947949)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(144903843482941671)
,p_validation_name=>'Portfolio Code Exists'
,p_validation_sequence=>10
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select distinct vw_d_bin_ips.portfolio_code , vw_d_bin_ips.bin_number, vw_d_bin_ips.bin_currency',
'from ra_bin_currency, vw_d_bin_ips where ',
'ra_bin_currency.bin(+) = vw_d_bin_ips.bin_number and',
'ra_bin_currency.portfolio_code(+) = vw_d_bin_ips.portfolio_code and',
':P138_BIN = vw_d_bin_ips.bin_number and',
':P138_PORTFOLIO_CODE = vw_d_bin_ips.portfolio_code'))
,p_validation_type=>'NOT_EXISTS'
,p_error_message=>'Portfolio Code for this BIN already exists.'
,p_always_execute=>'N'
,p_associated_item=>wwv_flow_api.id(143401153905514655)
,p_error_display_location=>'ON_ERROR_PAGE'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(145050637719338189)
,p_validation_name=>'BIN Currency exists'
,p_validation_sequence=>20
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select distinct vw_d_bin_ips.bin_currency',
'from vw_d_bin_ips where ',
':P138_BIN = vw_d_bin_ips.bin_number and',
':P138_BIN_CURRENCY = vw_d_bin_ips.bin_currency'))
,p_validation_type=>'NOT_EXISTS'
,p_error_message=>'BIN Currency for this BIN already exists.'
,p_always_execute=>'N'
,p_validation_condition_type=>'NEVER'
,p_associated_item=>wwv_flow_api.id(143400538520514649)
,p_error_display_location=>'ON_ERROR_PAGE'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(70581061529784000)
,p_validation_name=>'P138_TRAVELEX_CURRENCY_CODE'
,p_validation_sequence=>30
,p_validation=>'RETURN FN_WHITELIST_VALIDATION(''APEX_CUST_WHITELIST_VALIDATION'',''CHARACTERS_ALLOWED'',:P138_TRAVELEX_CURRENCY_CODE);'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_error_message=>'err'
,p_always_execute=>'N'
,p_validation_condition=>'P138_TRAVELEX_CURRENCY_CODE'
,p_validation_condition_type=>'ITEM_IS_NOT_NULL'
,p_only_for_changed_rows=>'Y'
,p_associated_item=>wwv_flow_api.id(143400943392514655)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(70581247727790327)
,p_validation_name=>'P138_PORTFOLIO_CODE'
,p_validation_sequence=>40
,p_validation=>'RETURN FN_WHITELIST_VALIDATION(''APEX_CUST_WHITELIST_VALIDATION'',''CHARACTERS_ALLOWED'',:P138_PORTFOLIO_CODE);'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_error_message=>'err'
,p_always_execute=>'N'
,p_validation_condition=>'P138_PORTFOLIO_CODE'
,p_validation_condition_type=>'ITEM_IS_NOT_NULL'
,p_only_for_changed_rows=>'Y'
,p_associated_item=>wwv_flow_api.id(143401153905514655)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(70581470574794931)
,p_validation_name=>'P138_BIN_MARGIN'
,p_validation_sequence=>50
,p_validation=>'RETURN FN_WHITELIST_VALIDATION(''APEX_CUST_WHITELIST_VALIDATION'',''CHARACTERS_ALLOWED'',:P138_BIN_MARGIN);'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_error_message=>'err'
,p_always_execute=>'N'
,p_validation_condition=>'P138_BIN_MARGIN'
,p_validation_condition_type=>'ITEM_IS_NOT_NULL'
,p_only_for_changed_rows=>'Y'
,p_associated_item=>wwv_flow_api.id(143401348857514656)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(70581661517799178)
,p_validation_name=>'P138_BIN_MARGIN_DISPLAY'
,p_validation_sequence=>60
,p_validation=>'RETURN FN_WHITELIST_VALIDATION(''APEX_CUST_WHITELIST_VALIDATION'',''CHARACTERS_ALLOWED'',:P138_BIN_MARGIN_DISPLAY);'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_error_message=>'err'
,p_always_execute=>'N'
,p_validation_condition=>'P138_BIN_MARGIN_DISPLAY'
,p_validation_condition_type=>'ITEM_IS_NOT_NULL'
,p_only_for_changed_rows=>'Y'
,p_associated_item=>wwv_flow_api.id(143401548914514656)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(70581852243803445)
,p_validation_name=>'P138_USE_BIN_MARGIN_FLAG'
,p_validation_sequence=>70
,p_validation=>'RETURN FN_WHITELIST_VALIDATION(''APEX_CUST_WHITELIST_VALIDATION'',''CHARACTERS_ALLOWED'',:P138_USE_BIN_MARGIN_FLAG);'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_error_message=>'err'
,p_always_execute=>'N'
,p_validation_condition=>'P138_USE_BIN_MARGIN_FLAG'
,p_validation_condition_type=>'ITEM_IS_NOT_NULL'
,p_only_for_changed_rows=>'Y'
,p_associated_item=>wwv_flow_api.id(143401949798514656)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(70599170541586611)
,p_validation_name=>'P138_BIN_REFERENCE_NAME'
,p_validation_sequence=>80
,p_validation=>'RETURN FN_WHITELIST_VALIDATION(''APEX_CUST_WHITELIST_VALIDATION'',''CHARACTERS_ALLOWED'',:P138_BIN_REFERENCE_NAME);'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_error_message=>'error'
,p_always_execute=>'N'
,p_validation_condition=>'P138_BIN_REFERENCE_NAME'
,p_validation_condition_type=>'ITEM_IS_NOT_NULL'
,p_only_for_changed_rows=>'Y'
,p_associated_item=>wwv_flow_api.id(143402135511514656)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(143402341482514666)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_FORM_PROCESS'
,p_process_name=>'Process Row of RA_BIN_CURRENCY'
,p_attribute_02=>'RA_BIN_CURRENCY'
,p_attribute_03=>'P138_BIN'
,p_attribute_04=>'BIN'
,p_attribute_05=>'P138_BIN_CURRENCY'
,p_attribute_06=>'BIN_CURRENCY'
,p_attribute_11=>'I'
,p_process_error_message=>'Unable to process row of table RA_BIN_CURRENCY.'
,p_process_success_message=>'Action Processed.'
);
end;
/
begin
wwv_flow_api.import_end(p_auto_install_sup_obj => nvl(wwv_flow_application_install.get_auto_install_sup_obj, false));
commit;
end;
/
set verify on feedback on define on
prompt  ...done
