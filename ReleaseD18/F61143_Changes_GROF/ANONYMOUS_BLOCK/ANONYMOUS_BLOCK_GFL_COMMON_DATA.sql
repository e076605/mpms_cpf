declare
l_val varchar2(50);
begin
select value into l_val from gfl_common_data.stored_parameter_value where name='ENVIRONMENT';
IF l_val='MTF' THEN
UPDATE gfl_common_data.gft_framework_config
SET OUTBOUND_DIRECTORY='REPFRM'
WHERE report_name='RMST_ANPOST' and client_name='ANPOST';
COMMIT;
END IF;
END ;
/
