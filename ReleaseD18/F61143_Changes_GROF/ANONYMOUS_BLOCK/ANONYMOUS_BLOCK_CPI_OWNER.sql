/* Formatted on 3/20/2018 12:11:34 AM (QP5 v5.227.12220.39724) */
DECLARE
   l_env     VARCHAR2 (50);
   l_count   NUMBER;
BEGIN
   SELECT VALUE
     INTO l_env
     FROM cpi_owner.stored_parameter_value
    WHERE name = 'ENVIRONMENT';

   IF l_env = 'MTF'
   THEN
      UPDATE cpi_owner.PROCESS_FRAMEWORK_PROCESS_STEP
         SET status = 'ENABLED'
       WHERE program_name = 'RMST_ANPOST_REPORT';

      SELECT COUNT (1)
        INTO l_count
        FROM stored_parameter_value
       WHERE namespace = 'ANPOST' AND name = 'DIRECTORY';

      IF l_count = 0
      THEN
         INSERT INTO STORED_PARAMETER_NAMESPACE
              VALUES ('ANPOST', 'ANPOST DIRECTORY');

         INSERT INTO STORED_PARAMETER_VALUE
              VALUES ('ANPOST',
                      'DIRECTORY',
                      'ANPOST DIRECTORY',
                      'REPFRM',
                      SYSDATE,
                      'CPI_OWNER');
      END IF;
   END IF;
   COMMIT;
END;
/