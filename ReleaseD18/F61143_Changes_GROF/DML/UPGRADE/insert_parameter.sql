delete from CPI_OWNER.stored_parameter_value where namespace='RMST_ANPOST_CONFIG';
delete from CPI_OWNER.stored_parameter_namespace where namespace='RMST_ANPOST_CONFIG';


INSERT INTO CPI_OWNER.STORED_PARAMETER_NAMESPACE (NAMESPACE, DESCRIPTION)
     VALUES ('RMST_ANPOST_CONFIG', 'Parameter Values To Configure Stock Transfer File');

INSERT INTO CPI_OWNER.stored_parameter_value  (NAMESPACE,
                                     NAME,
                                     DESCRIPTION,
                                     VALUE,
                                     LAST_UPDATED,
                                     LAST_UPDATED_BY)
     VALUES ('RMST_ANPOST_CONFIG',
                   'ORGID1',
                   'Parameter Values To Configure Stock Transfer File',
                   '20951',
                   SYSDATE,
                   'CPI_OWNER');
				   
				   
INSERT INTO CPI_OWNER.stored_parameter_value  (NAMESPACE,
                                     NAME,
                                     DESCRIPTION,
                                     VALUE,
                                     LAST_UPDATED,
                                     LAST_UPDATED_BY)
     VALUES ('RMST_ANPOST_CONFIG',
                   'ORGID2',
                   'Parameter Values To Configure Stock Transfer File',
                   '23089',
                   SYSDATE,
                   'CPI_OWNER');

COMMIT;