CREATE OR REPLACE PACKAGE BODY CPI_OWNER.RMST_ANPOST_REPORT
AS
/****************************************************************************
  PURPOSE:   Routine to create RAMSTA reports

  REVISIONS:
  Ver        Date        Author           Description
  ---------  ----------  ---------------
  1.0        18/12/2014      Veeru           Initial Version
  2.0        02/0 5/2015     Veeru           GFT Setup
  3.0        03/03/2015      Lavanya         Added logic to stop sending empty file on mail
  4.0        06/06/2015      Srinivas        Added audit log trail
  4.0        14/06/2015      Srinivas        Send mail when no file to send
*****************************************************************************/

  PROCEDURE anpost_report
  (
  p_report_name VARCHAR2,
  p_client VARCHAR2
  )
  IS
    v_output_file      utl_file.file_type;

    v_line             VARCHAR2(32567);
    v_output_dir       VARCHAR2(30);
    v_file_name        VARCHAR2(200);
    v_order_date       VARCHAR2(30);
    v_subject          VARCHAR2(1000);
    v_environment      VARCHAR2(30);
    v_message          VARCHAR2(1000);
    v_recipient        VARCHAR2(1000);
    v_sender_email     VARCHAR2(300);

    v_record_count     NUMBER;
    v_blob_store_id    NUMBER;

    CURSOR cur_report_data IS
      SELECT oh.external_order_number as Order_No,
             oh.insert_date as order_date,
             TO_NUMBER(REGEXP_SUBSTR(rcbo.longname, 'A.*?(\d\d\d\d)',1,1,'i',1)) as grof,
             ip.name as Product_Name,
             SUM(stk.quantity) AS quantity
      FROM  order_line_fulfilment olf,
            order_line ol,
            order_header oh,
            client_order_header coh,
            ims_stock_location_client_v islv,
            rds_client_branch_office_v rcbo,
            stock stk,
            ims_stocked_product_v isp,
            ims_product_v ip
      WHERE ol.order_line_id = olf.order_line_id
        AND ol.order_header_id = oh.order_header_id
        AND coh.order_header_id = oh.order_header_id
        AND coh.client_id = islv.client_id
        AND olf.stock_id = stk.stock_id
        AND stk.stocked_product_id = isp.stocked_product_id
        AND isp.product_id = ip.product_id
        AND rcbo.cp_org_branch_office_id = islv.ips_owner_level_3
        AND rcbo.cp_org_reg_office_id = islv.ips_owner_level_2
        AND rcbo.cp_org_id = islv.ips_owner_level_1
        AND oh.update_date > SYSDATE - 2
        AND oh.order_status_id = 2
        AND rcbo.cp_org_id = '20951'
        GROUP BY oh.external_order_number,
                 oh.insert_date,
                 TO_NUMBER(REGEXP_SUBSTR(rcbo.longname, 'A.*?(\d\d\d\d)',1,1,'i',1)),
                 ip.name
        ORDER BY order_no;

    TYPE typ_report_data IS TABLE OF cur_report_data%ROWTYPE
    INDEX BY BINARY_INTEGER;

    buf_report_data typ_report_data;


  BEGIN

    standard_audit.open_audit_trail (
         p_namespace   => 'PROCESS_FRAMEWORK',
         p_program     => 'rmst_anpost_report.anpost_report',
         p_override    => FALSE);


    v_output_dir      := stored_parameter.get_parameter('ANPOST','DIRECTORY');
    v_file_name       := 'ANPostReport_'||TO_CHAR(SYSDATE,'YYYYMMDD')||'.csv';
    v_record_count    := 0;

    standard_audit.log_entry (
         p_program     => 'rmst_anpost_report.anpost_report',
         p_step_name   => 'Set Parameters',
         p_log_entry   => 'Parameters v_output_dir -'||v_output_dir||'v_file_name = '||v_file_name,
         p_level       => 3);

    v_output_file     := utl_file.fopen(v_output_dir,v_file_name,'W');

    standard_audit.log_entry (
         p_program     => 'rmst_anpost_report.anpost_report',
         p_step_name   => 'After open file',
         p_log_entry   => 'After open file',
         p_level       => 3);


    v_line := 'ORDER_NO,ORDER_DATE,GROF,PRODUCT_NAME,QUANTITY'||CHR(13);

    IF utl_file.is_open(v_output_file) THEN
      utl_file.put_line(v_output_file,v_line,TRUE);
    END IF;

    standard_audit.log_entry (
         p_program     => 'rmst_anpost_report.anpost_report',
         p_step_name   => 'Before Open Cursor',
         p_log_entry   => 'Before Open Cursor',
         p_level       => 3);

    OPEN cur_report_data;
    LOOP
      FETCH cur_report_data
      BULK COLLECT INTO
      buf_report_data LIMIT 1000;
       IF buf_report_data.COUNT = 0 THEN
         EXIT;
       END IF;
      FOR v_ptr in buf_report_data.FIRST..buf_report_data.LAST LOOP

        v_order_date := TO_CHAR(buf_report_data(v_ptr).order_date,'DD/MM/YYYY hh24:mi:ss');

        v_line := buf_report_data(v_ptr).order_no    ||','||
                  v_order_date  ||','||
                  buf_report_data(v_ptr).grof        ||','||
                  buf_report_data(v_ptr).product_name||','||
                  buf_report_data(v_ptr).quantity    ||CHR(13);

        IF utl_file.is_open(v_output_file) THEN
          utl_file.put_line(v_output_file,v_line,TRUE);
        END IF;

        v_line := NULL;
        v_record_count  := v_record_count + 1;

      END LOOP;
    END LOOP;

    IF cur_report_data%ISOPEN THEN
      CLOSE cur_report_data;
    END IF;

    standard_audit.log_entry (
         p_program     => 'rmst_anpost_report.anpost_report',
         p_step_name   => 'Close Cursor',
         p_log_entry   => 'Close Cursor : v_record_count - '||v_record_count,
         p_level       => 3);

    utl_file.fclose(v_output_file);

    standard_audit.log_entry (
         p_program     => 'rmst_anpost_report.anpost_report',
         p_step_name   => 'Close file',
         p_log_entry   => 'Close file',
         p_level       => 3);

  IF v_record_count > 0 THEN

    standard_audit.log_entry (
      p_program     => 'rmst_anpost_report',
      p_step_name   => 'Start - GFT Process',
      p_log_entry   => 'Start - GFT Process RMST_ANPOST :  p_report_name -'||p_report_name||' p_client_name-'||p_client||' v_file_name -'||v_file_name ,
      p_level       => 3);

      pkg_gft_framework.send_report (p_report_name   => p_report_name,
                                     p_client_name   => p_client,
                                     p_file_name     => v_file_name);

    standard_audit.log_entry (
      p_program     => 'rmst_anpost_report',
      p_step_name   => 'End - GFT Process',
      p_log_entry   => 'End - GFT Process-Environment RMST_ANPOST',
      p_level       => 3);


    v_blob_store_id :=
      blob_store.store_file
      (
        p_content_type          => 'RMST_ANPOST_REPORT',
        p_directory             => v_output_dir,
        p_file_name             => v_file_name,
        p_file_type             => 'CSV',
        p_blob_date             => SYSDATE,
        p_blob_store_comment    => 'Anpost report for '||TO_CHAR(SYSDATE,'DD/MM/YYYY')
      );
  ELSE
      standard_audit.log_entry (
      p_program     => 'rmst_anpost_report',
      p_step_name   => 'End',
      p_log_entry   => 'No records in the report.Hence sending no data mail',
      p_level       => 3);

      v_subject  := 'There is no data for the ANPost report today';
      v_message  := 'There is no data for the ANPost report today';
      v_sender_email := NVL(ramsta_utilities.get_es_parameter('SendingEMailAddress'),'CVP_IT_Support@mastercard.com');

      email_interface.initialise_message
      (
        p_subject_title     => v_subject,
        p_sender_email      => v_sender_email,
        p_encryt_email      => FALSE,
        p_message           => v_message
      );

      IF NVL(stored_parameter.get_parameter('SYSTEM','ENVIRONMENT'),'STAGE') <> 'PROD' THEN
        v_recipient := stored_parameter.get_parameter('RAMSTA_TEST_ENV_MAIL','EMAIL_OK');
      ELSE
        v_recipient := ramsta_utilities.get_es_parameter('MailANPostReport_');
      END IF;

      email_interface.add_recipient
      (
        p_email               => v_recipient,
        p_send_level          => 'TO'  -- TO, CC OR BCC
      );

      email_interface.send_mail;

  END IF;

  standard_audit.close_audit_trail (
            p_program => 'rmst_anpost_report.anpost_report');

  EXCEPTION
    WHEN OTHERS THEN
      standard_audit.close_audit_trail
      (
      p_program => 'rmst_anpost_report.anpost_report'
      );

      IF cur_report_data%ISOPEN THEN
        CLOSE cur_report_data;
      END IF;

      standard_error.when_others_exception
      (
      p_program     => 'rmst_anpost_report.anpost_report'
      );

  END anpost_report;

END rmst_anpost_report;
/