DROP VIEW GFL_MCP_CORE.PDF_STAT_REVSHR_VIEW_CT_ALL;

/* Formatted on 4/19/2018 12:16:37 PM (QP5 v5.227.12220.39724) */
CREATE OR REPLACE FORCE VIEW GFL_MCP_CORE.PDF_STAT_REVSHR_VIEW_CT_ALL
(
   CLIENT_LEGAL_ENTITY,
   COMPANY_LOGO,
   BUSINESS_NAME,
   ADDRESS_LINE_1,
   ADDRESS_LINE_2,
   ADDRESS_LINE_3,
   CITY,
   COMPANY_NBR,
   REG_ADDRESS_LINE_1,
   REG_ADDRESS_LINE_2,
   REG_ADDRESS_LINE_3,
   REG_CITY,
   REG_ZIP_POST_CODE,
   VAT_NBR,
   ZIP_POST_CODE,
   EMAIL,
   INVOICE_NUMBER,
   CLIENT_NAME,
   INVOICE_CREATE_DATE,
   BUSINESS_DATE,
   PURSE_CURRENCY,
   PURSE_AMOUNT,
   SETTLEMENT_CURRENCY,
   TOTAL_AMOUNT,
   PERMIT_RATE,
   TOTAL_FX_REVENUE,
   TOTAL_FEES_COMMISSION,
   FEES,
   COMMISSION,
   TOTAL_TAKEN,
   CLIENT_SHARE_OF_FX_REVENUE,
   CLIENT_SHARE_OF_FEE,
   CLIENT_SHARE_OF_FEE_COMM,
   CLIENT_SHARE_OF_COMM,
   PORTAL_ORDER_REFERENCE_NUMBER,
   REFERENCE_NUMBER,
   CARD_NUMBER,
   CARDHOLDER_NAME,
   QUANTITY,
   CASHIER_NAME,
   BANK_NAME,
   ACCOUNT_NAME,
   ACCOUNT_NUMBER,
   SWIFT_CODE,
   IBAN_NUMBER,
   TRANSACTION_TYPE,
   BUSINESS_START_DATE,
   BUSINESS_END_DATE,
   INVOICE_TERMS,
   PAYMENT_REFERENCE,
   DETAIL_PROC_CLIENT_ID,
   DETAIL_CLIENT_NAME,
   SUMMARY_CLIENT_ID,
   SELLING_LOCATION_ID,
   TJF_TXN_DESC,
   INVOICE_ITEM_TYPE,
   INVOICE_ITEM_TYPE_ASIS,
   TRANSACTION_TYPE_ORD,
   ORD_SEQ,
   ORDER_NAME_DISPLAY,
   ETL_DATE
)
AS
     SELECT b.client_legal_entity,
            d.company_logo,
            d.business_name,
            d.address_line_1,
            d.address_line_2,
            d.address_line_3,
            d.city,
            d.company_nbr,
            d.reg_address_line_1,
            d.reg_address_line_2,
            d.reg_address_line_3,
            d.reg_city,
            d.reg_zip_post_code,
            d.vat_nbr,
            d.zip_post_code,
            d.email,
            b.invoice_number invoice_number,
            b.client_name,
            TO_CHAR (b.invoice_create_date, 'dd-Mon-yyyy') invoice_create_date,
            TO_CHAR (a.original_trxn_date, 'dd/mm/yyyy') business_date,
            a.purse_currency,
            NVL (a.transaction_amount, 0) purse_amount,
            b.settlement_currency,
--            NVL (a.sett_amt_excl_margin, 0) total_amount,
            CASE
               WHEN sys_source = 'PORTAL' AND order_or_purse = 'O' THEN 0
               ELSE NVL (a.sett_amt_excl_margin, 0)
            END
               total_amount,            
            NVL (a.invoice_rate, 1) permit_rate,
            a.sett_amt_fx_revenue total_fx_revenue,
            a.total_fees_comm total_fees_commission,
            CASE
               WHEN NVL (order_or_purse, 'P') = 'P'
               THEN
                  (SELECT NVL (SUM (total_fees_comm), a.total_fees_comm)
                     FROM invoice_lines inl, invoice_transaction_type itt
                    WHERE     inl.reference_number = a.reference_number
                          AND inl.sk_invoice_header_id = a.sk_invoice_header_id
                          AND itt.ID = inl.transaction_type
                          AND invoice_group = 'FEE'
                          AND itt.order_or_purse = 'P'
                          AND itt.sys_source = 'PORTAL')
               WHEN (    order_or_purse = 'O'
                     AND invoice_group = 'FEE'
                     AND sys_source = 'PORTAL')
               THEN
                  NVL (a.total_fees_comm, 0)
               ELSE
                  0
            END
               fees,
            CASE
               WHEN NVL (order_or_purse, 'P') = 'P'
               THEN
                  (SELECT NVL (SUM (total_fees_comm), 0)
                     FROM invoice_lines inl, invoice_transaction_type itt
                    WHERE     inl.reference_number = a.reference_number
                          AND inl.sk_invoice_header_id = a.sk_invoice_header_id
                          AND itt.ID = inl.transaction_type
                          AND invoice_group = 'COMMISSION'
                          AND itt.order_or_purse = 'P'
                          AND itt.sys_source = 'PORTAL')
               WHEN (    order_or_purse = 'O'
                     AND invoice_group = 'COMMISSION'
                     AND sys_source = 'PORTAL')
               THEN
                  NVL (a.total_fees_comm, 0)
               ELSE
                  0
            END
               commission,
              NVL (a.sett_amt_excl_margin, 0)
            + a.sett_amt_fx_revenue
            + a.total_fees_comm
               total_taken,
            a.client_fx_revenue_share client_share_of_fx_revenue,
            CASE
               WHEN NVL (order_or_purse, 'P') = 'P'
               THEN
                  (SELECT NVL (SUM (client_fees_comm_share),
                               a.client_fees_comm_share)
                     FROM invoice_lines inl, invoice_transaction_type itt
                    WHERE     inl.reference_number = a.reference_number
                          AND inl.sk_invoice_header_id = a.sk_invoice_header_id
                          AND itt.ID = inl.transaction_type
                          AND invoice_group = 'FEE'
                          AND itt.order_or_purse = 'P'
                          AND itt.sys_source = 'PORTAL')
               WHEN (    order_or_purse = 'O'
                     AND invoice_group = 'FEE'
                     AND sys_source = 'PORTAL')
               THEN
                  NVL (a.client_fees_comm_share, 0)
               ELSE
                  0
            END
               client_share_of_fee,
            a.client_fees_comm_share client_share_of_fee_comm,
            CASE
               WHEN NVL (order_or_purse, 'P') = 'P'
               THEN
                  (SELECT NVL (SUM (client_fees_comm_share), 0)
                     FROM invoice_lines inl, invoice_transaction_type itt
                    WHERE     inl.reference_number = a.reference_number
                          AND inl.sk_invoice_header_id = a.sk_invoice_header_id
                          AND itt.ID = inl.transaction_type
                          AND invoice_group = 'COMMISSION'
                          AND itt.order_or_purse = 'P'
                          AND itt.sys_source = 'PORTAL')
               WHEN (    order_or_purse = 'O'
                     AND invoice_group = 'COMMISSION'
                     AND sys_source = 'PORTAL')
               THEN
                  NVL (a.client_fees_comm_share, 0)
               ELSE
                  0
            END
               client_share_of_comm,
            a.mcp_portal_order_no portal_order_reference_number,
            a.reference_number,
            a.card_number,
            a.cardholder_name,
            a.quantity,
            a.cashier_name,
            b.bank_name,
            b.account_name,
            b.account_number,
            b.swift_code,
            b.iban_number,
            a.transaction_type,
            TO_CHAR (b.business_start_date, 'dd-Mon-yyyy') business_start_date,
            TO_CHAR (b.business_end_date, 'dd-Mon-yyyy') business_end_date,
            CASE
               WHEN NVL (b.net_total_value_due, 0) < 0
               THEN
                  'Payments will be made to your Account with'
               ELSE
                  'Payments should be made to our Account with'
            END
               invoice_terms,
               TO_CHAR (b.invoice_create_date, 'yymmdd')
            || '-'
            || b.processor_client_id
               payment_reference,
            a.processor_client_id detail_proc_client_id,
            a.client_name detail_client_name,
            b.client_id summary_client_id,
            a.client_id selling_location_id,
            a.tjf_txn_desc,
            DECODE (
               NVL (c.fee_transaction_type, a.transaction_type),
               '1', 'Purse Loads',
               '2', 'Purse Reloads',
               '3', 'Purse Cashouts',
               (SELECT MIN (
                          DECODE (TO_NUMBER (b.transaction_type),
                                  1, 'Purse Loads',
                                  2, 'Purse Reloads',
                                  3, 'Purse Cashouts'))
                  FROM invoice_lines b
                 WHERE     b.sk_invoice_header_id = a.sk_invoice_header_id
                       AND b.reference_number LIKE
                              '%' || a.reference_number || '%'))
               invoice_item_type,
            invoice_txn_type invoice_item_type_asis,
            NVL (fee_transaction_type, transaction_type) transaction_type_ord,
            NVL (order_or_purse, 'P') ord_seq,
            DECODE (order_or_purse, 'O', process_txn_type) order_name_display,
            a.ETL_DATE --added by nagesh bhat 14-07-2017 to fix long running invoices
       FROM invoice_lines a
            LEFT JOIN invoice_summary b
               ON a.sk_invoice_header_id = b.sk_invoice_header_id
            LEFT JOIN legal_entity d ON b.client_legal_entity = d.code
            LEFT JOIN invoice_transaction_type c ON a.transaction_type = c.ID
      WHERE     NVL (b.sk_inv_format_id, 1) IN (2)
            AND (   a.transaction_type IN (1, 2, 3, 8, 9, 10, 11)
                 OR (sys_source = 'PORTAL' AND order_or_purse = 'O'))
   ORDER BY selling_location_id,
            portal_order_reference_number,
            reference_number;


CREATE OR REPLACE SYNONYM RAL_MCP_DATA.PDF_STAT_REVSHR_VIEW_CT_ALL FOR GFL_MCP_CORE.PDF_STAT_REVSHR_VIEW_CT_ALL;


GRANT SELECT ON GFL_MCP_CORE.PDF_STAT_REVSHR_VIEW_CT_ALL TO GFL_MCP_CORE_READ_RL;

GRANT SELECT ON GFL_MCP_CORE.PDF_STAT_REVSHR_VIEW_CT_ALL TO GFL_MCP_CORE_USR_RL;
