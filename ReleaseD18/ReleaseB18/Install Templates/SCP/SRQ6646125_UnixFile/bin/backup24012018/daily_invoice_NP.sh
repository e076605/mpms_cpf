#!/bin/sh
. initialize_env
export xml_data_template_file="$BIP_HOME/cfg/daily_invoice_data_template_np.xml"
export xml_output_file="$BIP_HOME/tmp/xml/daily_invoice_np.xml"
export bursting_ctrl_file="$BIP_HOME/cfg/daily_invoice_fs_np_ctrl.xml"
export tmp_folder="/tmp"
$JAVA_HOME/bin/java CreateXMLData $db_server $db_port $db_sid $db_username $db_password $xml_data_template_file $xml_output_file
$JAVA_HOME/bin/java BurstingFile $bursting_ctrl_file $xml_output_file $tmp_folder
