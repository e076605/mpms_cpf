#!/bin/sh
cd /app/cpr_data/scripts/bursting/bin/
. initialize_env
export PATH=$PATH:/bin:/usr/bin
##################################################

INPUT_DIR=$BIP_HOME/output
OUTPUT_DIR=$BIP_ARCHIVE
DDMMYY=`date +"%y%m%d"`
HHMISS=`date +"%H%M%S"`
for i in `ls -1 ../output/*.pdf | cut -f3 -d"/" | cut -c2,3,4,5,6,7 | sort -u`
do
echo "creating directory for " $OUTPUT_DIR/$i
mkdir $OUTPUT_DIR/$i
echo "moving all files for " $i "into appropriate direcotry."
echo
mv $INPUT_DIR/*$i*.* $OUTPUT_DIR/$i
echo "files moved to directory " $OUTPUT_DIR/$i
echo "-------------------------------------------------------------------"
echo " GFT Started"
zip -r -j $OUTPUT_DIR/$i.zip $OUTPUT_DIR/$i
if [ $? -eq 0 ]
  then
/app/cpr_data/scripts/gft_wrapper_framework.sh $OUTPUT_DIR/$i.zip ${DDMMYY} ${HHMISS} TFU2 E0085917 RECFM=U,LRECL=0,BLKSIZE=27998 -i -z $i.zip
         if [ $? -eq 0 ]
            then
	      echo "GFT is successfull , file name ${i}.zip"
	      rm $OUTPUT_DIR/$i.zip
	 fi
fi
echo "GFT Finishaed"
done

