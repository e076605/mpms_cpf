DECLARE
CURSOR c_get_name_doc is
select a.template_id,file_name
 from gfl_mcp_core.BI_PUBLISHER_TEMPLATE  a, gfl_mcp_core.BI_PUB_DOC b,gfl_mcp_core.BI_PUB_SOURCE_BLOB c
where A.TEMPLATE_ID = B.DOC_TEMPLATE_ID
and C.SK_SOURCE_BLOB_ID = b.sk_source_blob_id
and file_name not in 
(
'Bin_Sponsor_Statement_PDF_v10.rtf',
'Bin_Sponsor_Statement_Excel.rtf',
'MCP_Non_Daily_Invoice.rtf',
'MCP_Affiliate_Monthly_Statement.rtf'
);

V_TEMPLATE_ID number; -- template id to replace : 1 = daily pdf, 2 = daily excel etc - see bi_publisher_template
V_FILENAME VARCHAR2(100) ; -- name of file including extension
V_FILETYPE varchar2(6) := 'RTF'; -- XML or RTF depending on type of file
v_blobid number := null; -- id of the bi_pub_source_blob raw blob
BEGIN
FOR i in c_get_name_doc LOOP
V_TEMPLATE_ID  := i.template_id; -- template id to replace : 1 = daily pdf, 2 = daily excel etc - see bi_publisher_template
V_FILENAME  := i.file_name; -- name of file including extension
    IF V_FILETYPE IN ('XML','RTF') THEN  -- only allow the allowable filetypes through
      IF V_FILETYPE = 'XML' THEN      
         DELETE BI_PUB_DATA WHERE DATA_TEMPLATE_ID = V_TEMPLATE_ID;
         delete bi_pub_source_blob where FILE_NAME = v_filename;
         COMMIT;
        ELSE
         DELETE BI_PUB_DOC WHERE DOC_TEMPLATE_ID = V_TEMPLATE_ID;
         DELETE BI_PUB_SOURCE_BLOB WHERE FILE_NAME = V_FILENAME;
         COMMIT;
      END IF;
      -- get the raw blob loaded
      LOAD_BI_BLOB(V_FILETYPE,V_FILENAME);
      -- get the blob id of the loaded blob
      SELECT MAX(SK_SOURCE_BLOB_ID) INTO V_BLOBID
        FROM BI_PUB_SOURCE_BLOB 
       WHERE FILE_NAME = V_FILENAME AND FILE_TYPE = V_FILETYPE;
      -- now convert the template int data xml
      BIPUBAPI.TEMPLATECONVERT(V_BLOBID,V_TEMPLATE_ID,V_TEMPLATE_ID);
    END IF;
END LOOP;
EXCEPTION WHEN OTHERS then
raise;
END;
/
commit;
/
