<dataTemplate name="INVOICE" description="COOP specific template" dataSourceRef="TRAVELX_DS">
<parameters>
	<parameter name="P_INVOICE_NUMBER" dataType="character"/>
</parameters>
<dataQuery>
	<sqlStatement name="Q1">
		<![CDATA[ WITH SAWITH0 AS (select sum(T1872.CURRENCY_AMOUNT) as c1,
     						  sum(T1872.SETTLEMENT_AMOUNT) as c2,
     						  T2157.DATE_NAME as c3,
     						  T1872.INVOICE_NUMBER as c4,
     						  T1872.PAN as c5,
     						  T1872.TRANSACTION_TYPE as c6,
     						  T1872.SELLING_LOCATION as c7,
     						  T1872.CARDHOLDER_NAME as c8,
     						  cast(T1872.PERMIT_COVER_RATE as  DOUBLE PRECISION  ) as c9,
     						  T1872.CURRENCY as c10,
     						  nullif( cast(T1872.PERMIT_COVER_RATE as  DOUBLE PRECISION  ), 0) as c11
					  from D_TIME T2157 /* a_Dim_D_Time_Transaction */ ,
     					       D_ORGANIZATION T2069 /* a_Dim_D_Organization */ ,
     					       FD_INVOICE T1872 /* a_Fact_FD_Invoice */ 
					  where  ( T1872.D_TIME_TRANSACTION = T2157.TIME_DIMENSION_KEY 
					  and 	   T1872.D_ORGANIZATION_ID = T2069.D_ORGANIZATION_ID 
          				  and 	   T2069.CLIENT_NAME = 'Co op Travel' 
          				  and 	   T2157.DATE_NAME = to_date(sysdate-1,'DD-MM-YY')
          				  and      (T1872.TRANSACTION_TYPE in ('InitialLoad', 'Reload')) 
          				  and T1872.SELLING_LOCATION <> 'Co op Test' ) 
					  group by T1872.INVOICE_NUMBER, 
					           T1872.SELLING_LOCATION, 
					           T1872.PAN, 
					           T1872.CARDHOLDER_NAME, 
					           T1872.CURRENCY, 
					           T1872.TRANSACTION_TYPE, 
					           T2157.DATE_NAME, 
					           cast(T1872.PERMIT_COVER_RATE as  DOUBLE PRECISION  )),
			       SAWITH1 AS (select sum(T1872.SETTLEMENT_AMOUNT) as c1,
     				                  sum(T1872.CURRENCY_AMOUNT) as c2
					   from D_TIME T2157 /* a_Dim_D_Time_Transaction */ ,
     						D_ORGANIZATION T2069 /* a_Dim_D_Organization */ ,
     						FD_INVOICE T1872 /* a_Fact_FD_Invoice */ 
					   where  ( T1872.D_TIME_TRANSACTION = T2157.TIME_DIMENSION_KEY 
					   and      T1872.D_ORGANIZATION_ID = T2069.D_ORGANIZATION_ID 
					   and      T2069.CLIENT_NAME = 'Co op Travel' 
					   and      T2157.DATE_NAME = to_date(sysdate-1,'DD-MM-YY') 
					   and      (T1872.TRANSACTION_TYPE in ('InitialLoad', 'Reload')) 
					   and      T1872.SELLING_LOCATION <> 'Co op Test' ) ),
			       SAWITH2 AS (select distinct SAWITH0.c3 as c1,
     						  SAWITH0.c4 as c2,
     						  SAWITH0.c5 as c3,
     						  SAWITH0.c6 as c4,
     						  SAWITH0.c7 as c5,
     						  SAWITH0.c8 as c6,
     						  SAWITH0.c9 as c7,	
     						  SAWITH0.c10 as c8,
     						  SAWITH0.c1 as c9,
     						  SAWITH0.c1 / nullif( 100, 0) * 0.225 as c10,
     						  SAWITH0.c1 / nullif( 100, 0) * 0.225 / SAWITH0.c11 as c11,
     						  SAWITH0.c1 / nullif( 100, 0) * 0.225 / SAWITH0.c11 + SAWITH0.c2 as c12,
     						  SAWITH0.c11 as c13
					   from SAWITH0,
     						SAWITH1),
			       SAWITH3 AS (select sum(T1872.CURRENCY_AMOUNT) as c1,
     						  sum(T1872.SETTLEMENT_AMOUNT) as c2,
     						  T2157.DATE_NAME as c3,
     						  T1872.INVOICE_NUMBER as c4,
     						  T1872.PAN as c5,
     						  T1872.TRANSACTION_TYPE as c6,
     						  T1872.SELLING_LOCATION as c7,
     						  T1872.CARDHOLDER_NAME as c8,	
     						  cast(T1872.PERMIT_COVER_RATE as  DOUBLE PRECISION  ) as c9,
     						  T1872.CURRENCY as c10,
     						  nullif( cast(T1872.PERMIT_COVER_RATE as  DOUBLE PRECISION  ), 0) as c11
					   from D_TIME T2157 /* a_Dim_D_Time_Transaction */ ,
     						D_ORGANIZATION T2069 /* a_Dim_D_Organization */ ,
     						FD_INVOICE T1872 /* a_Fact_FD_Invoice */ 
					   where  ( T1872.D_TIME_TRANSACTION = T2157.TIME_DIMENSION_KEY 
					   and 	  T1872.D_ORGANIZATION_ID = T2069.D_ORGANIZATION_ID 
					   and    T2069.CLIENT_NAME = 'Co op Travel' 
					   and    T2157.DATE_NAME = to_date(sysdate-1,'DD-MM-YY') 
					   and    (T1872.TRANSACTION_TYPE in ('InitialLoad', 'Reload')) 
					   and    T1872.SELLING_LOCATION <> 'Co op Test' ) 
					   group by T1872.INVOICE_NUMBER, 
					   	    T1872.SELLING_LOCATION, 
					   	    T1872.PAN, 
					   	    T1872.CARDHOLDER_NAME, 
					   	    T1872.CURRENCY, 
					   	    T1872.TRANSACTION_TYPE, 
					   	    T2157.DATE_NAME, 
					   	    cast(T1872.PERMIT_COVER_RATE as  DOUBLE PRECISION  )),
			       SAWITH4 AS (select sum(T1872.SETTLEMENT_AMOUNT) as c1,
     					     	  sum(T1872.CURRENCY_AMOUNT) as c2
					   from D_TIME T2157 /* a_Dim_D_Time_Transaction */ ,
     						D_ORGANIZATION T2069 /* a_Dim_D_Organization */ ,
     						FD_INVOICE T1872 /* a_Fact_FD_Invoice */ 
					   where  ( T1872.D_TIME_TRANSACTION = T2157.TIME_DIMENSION_KEY 
					   and    T1872.D_ORGANIZATION_ID = T2069.D_ORGANIZATION_ID 
					   and    T2069.CLIENT_NAME = 'Co op Travel' 
					   and    T2157.DATE_NAME = to_date(sysdate-1,'DD-MM-YY') 
					   and    (T1872.TRANSACTION_TYPE in ('InitialLoad', 'Reload')) 
					   and T1872.SELLING_LOCATION <> 'Co op Test' ) ),
			       SAWITH5 AS (select max(SAWITH4.c2) / nullif( 100, 0) * 0.225 as c14,
     						  max(SAWITH4.c1) as c15
					   from SAWITH3,
     						SAWITH4)
			       select D1.c1 as c1,
     				      D1.c2 as c2,
     				      D1.c3 as c3,
     				      D1.c4 as c4,
     				      D1.c5 as c5,
     				      D1.c6 as c6,
     				      D1.c7 as c7,
     				      D1.c8 as c8,
     				      D1.c9 as c9,
     				      D1.c10 as c10,
     				      D1.c11 as c11,
     				      D1.c12 as c12,
     				      D1.c13 as c13,
     				      D1.c14 as c14,
     				      D1.c15 as c15,
     				      D1.c16 as c16
			       from 
     				  (select SAWITH2.c1 as c1,
               				  SAWITH2.c2 as c2,
               				  SAWITH2.c3 as c3,
               				  SAWITH2.c4 as c4,
               				  SAWITH2.c5 as c5,
               				  SAWITH2.c6 as c6,
               				  SAWITH2.c7 as c7,
               				  SAWITH2.c8 as c8,
               				  SAWITH2.c9 as c9,
               				  SAWITH2.c10 as c10,
               				  SAWITH2.c11 as c11,
               				  SAWITH2.c12 as c12,
               				  SAWITH2.c9 as c13,
               				  SAWITH5.c14 as c14,
               				  SAWITH2.c11 as c15,
               				  SAWITH5.c14 / SAWITH2.c13 + SAWITH5.c15 as c16,
               				  ROW_NUMBER() OVER (PARTITION BY SAWITH2.c1, SAWITH2.c2, SAWITH2.c3, SAWITH2.c4, SAWITH2.c5, SAWITH2.c6, SAWITH2.c7, SAWITH2.c8 ORDER BY SAWITH2.c1 ASC, SAWITH2.c2 ASC, SAWITH2.c3 ASC, SAWITH2.c4 ASC, SAWITH2.c5 ASC, SAWITH2.c6 ASC, SAWITH2.c7 ASC, SAWITH2.c8 ASC) as c17
          			   from SAWITH2,
                      			SAWITH5
     				  ) D1
			       where  ( D1.c17 = 1 ) 
		]]>
	</sqlStatement>
	<sqlStatement name="Q2">
				<![CDATA[ SELECT invoice_number,
						 company.legal_entity,
						 company_name,
						 site_address_1,
						 site_address_2,
						 site_Address_3,
						 site_address_4,
						 site_Postcode,
						 site_email_address,
						 company_Logo,
						 company_registered_no,
						 company_vat_no,
						 registered_address_1,
						 registered_Address_2,
						 case when(registered_address_3 is NULL) then registered_Postcode 
						      else registered_address_3 
						 end as registered_address_3,
						 case when(registered_address_3 is null) then null 
						      when(registered_address_4 is null) then registered_postcode 
						      else registered_address_4
						 end as registered_Address_4,
						 case when (registered_address_3 is null) then null
						      when (registered_address_4 is null) then null
						      else registered_postcode
               					 end as registered_postcode  
					 FROM
						(SELECT distinct invoice_number,legal_entity
						 FROM fd_invoice a,
						      ra_client c,
						      ra_client_currency d,
						      d_organization o,
					              d_Client cl, 
						      d_time t
						 WHERE a.d_Client_id = cl.dimension_key
					         and cl.rds_Client_id = c.rds_client_id
						 and a.D_TIME_TRANSACTION = t.TIME_DIMENSION_KEY
						 and a.D_ORGANIZATION_ID = o.D_ORGANIZATION_ID 
						 and o.CLIENT_NAME = 'Co op Travel' 
						 and t.DATE_NAME = to_date(sysdate-1,'DD-MM-YY') 
						 AND c.rds_Client_id = d.ra_Client_id
						 AND c.client_level in ('CLIENT','LEVEL2')
						 AND d.active_status in ('ACT','TEST')
						 AND a.currency = d.ra_settlement_currency) invoice,
						 ra_company_details company
					WHERE invoice.legal_entity = company.legal_entity
					  AND invoice_number = :INVOICE_NUMBER
				]]>
		</sqlStatement>
	</dataQuery>
	<dataStructure>
		<group name="c5" source="Q1">
			<element name="INVOICE_DATE" value="c1"/>
			<element name="INVOICE_NUMBER" value="c2"/>
			<element name="PAN" value="c3"/>
			<element name="TRANSACTION_TYPE " value="c4"/>
			<element name="SELLING_LOCATION" value="c5"/>
			<element name="CARDHOLDER_NAME" value="c6"/>
			<element name="PERMIT_COVER_RATE" value="c7"/>
			<element name="CURRENCY" value="c8"/>
			<element name="CURRENCY_AMOUNT" value="c9"/>
			<element name="CURRENCY_COMMISSION" value="c10"/>
			<element name="GBP_COMMISSION" value="c11"/>
			<element name="TOTAL_IN_GBP" value="c12"/>
		</group>
		<group name="G_Company" source="Q2">
		     <element name="legal_entity" value="legal_entity"/>
		     <element name="COMPANY_NAME" value="COMPANY_NAME"/>
		     <element name="SITE_ADDRESS_1" value="SITE_ADDRESS_1"/>
		     <element name="SITE_ADDRESS_2" value="SITE_ADDRESS_2"/>
		     <element name="SITE_ADDRESS_3" value="SITE_ADDRESS_3"/>
		     <element name="SITE_ADDRESS_4" value="SITE_ADDRESS_4"/>
		     <element name="SITE_POSTCODE" value="SITE_POSTCODE"/>
		     <element name="site_email_address" value="site_email_address"/>
		     <element name="company_Logo" value="company_Logo"/>
		     <element name="COMPANY_REGISTERED_NO" value="COMPANY_REGISTERED_NO"/>
		     <element name="COMPANY_VAT_NO" value="COMPANY_VAT_NO"/>
		     <element name="REGISTERED_ADDRESS_1" value="REGISTERED_ADDRESS_1"/>
		     <element name="REGISTERED_ADDRESS_2" value="REGISTERED_ADDRESS_2"/>
		     <element name="REGISTERED_ADDRESS_3" value="REGISTERED_ADDRESS_3"/>
		     <element name="REGISTERED_ADDRESS_4" value="REGISTERED_ADDRESS_4"/>
		     <element name="REGISTERED_POSTCODE" value="REGISTERED_POSTCODE"/>
		</group>
	</dataStructure>
</dataTemplate>