#!/bin/sh
################################################
# burst the pdf files and emails them
################################################
BIN_DIR=`dirname $0`
DATE_SUFFIX=`date '+%Y%m%d_%H:%M'`
cd $BIN_DIR
. initialize_env

################################################
echo "Starting bursting " `date` > $BIP_HOME/log/main_daily_$DATE_SUFFIX.log

$BIP_HOME/bin/daily_invoice.sh > $BIP_HOME/log/daily_invoice_$DATE_SUFFIX.log
$BIP_HOME/bin/daily_statement.sh > $BIP_HOME/log/daily_statement_$DATE_SUFFIX.log
$BIP_HOME/bin/daily_invoice_NP.sh > $BIP_HOME/log/daily_invoice_NP_$DATE_SUFFIX.log

#
# generate the client specific invoices
# Currently for BA,TC &VA
# 
$BIP_HOME/bin/burst_va.sh    > $BIP_HOME/log/VirginAtlantic_invoice_$DATE_SUFFIX.log
$BIP_HOME/bin/burst_tc.sh    > $BIP_HOME/log/ThomasCook_invoice_$DATE_SUFFIX.log
$BIP_HOME/bin/burst_ba.sh    > $BIP_HOME/log/BritishAirways_invoice_$DATE_SUFFIX.log
$BIP_HOME/bin/burst_vships.sh    > $BIP_HOME/log/Vships_invoice_$DATE_SUFFIX.log
$BIP_HOME/bin/burst_coop.sh    > $BIP_HOME/log/coop_invoice_$DATE_SUFFIX.log
$BIP_HOME/bin/burst_coop_mid.sh    > $BIP_HOME/log/coop_mid_invoice_$DATE_SUFFIX.log
$BIP_HOME/bin/burst_scfb_kor.sh    > $BIP_HOME/log/scfb_kor_invoice_$DATE_SUFFIX.log
$BIP_HOME/bin/burst_mid_coop.sh    > $BIP_HOME/log/mid_coop_invoice_$DATE_SUFFIX.log

echo "complete bursting files " `date` >> $BIP_HOME/log/main_daily_$DATE_SUFFIX.log
echo "Start sending emails " `date` >> $BIP_HOME/log/main_daily_$DATE_SUFFIX.log

#
# Email the invoices generated
$BIP_HOME/bin/send_email.sh >  $BIP_HOME/log/send_email_$DATE_SUFFIX.log
#

echo "completed sending emails " `date` >> $BIP_HOME/log/main_daily_$DATE_SUFFIX.log

#
# Archive the files appropriately
#
$BIP_HOME/bin/archive_files.sh > $BIP_HOME/log/archive_files_$DATE_SUFFIX.log

exit 0
