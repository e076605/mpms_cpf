#!/bin/bash
export PATH=$PATH:/bin:/usr/bin:.
cd /app/cpr_data/scripts/bursting/bin/ 
. initialize_env
/app/cpr_data/scripts/bursting/bin/send_email.sh >  /app/cpr_data/scripts/bursting/log/send_email_test.log
#

/app/cpr_data/scripts/bursting/bin/archive_files.sh > /app/cpr_data/scripts/bursting/log/archive_files_test.log

exit 0
