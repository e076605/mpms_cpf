CREATE OR REPLACE TRIGGER CPRADMIN.TR_RA_INVOICE_CLIENT_FEE_VW INSTEAD OF
INSERT OR UPDATE OR DELETE
  ON RA_INVOICE_CLIENT_FEE_VW FOR EACH ROW
DECLARE V_DML_TYPE VARCHAR2(1);
v_LOCKED       VARCHAR2(1);
  v_timeStamp TIMESTAMP     := SysTimeStamp;
 BEGIN
 IF INSERTING
THEN V_DML_TYPE:='I';
ELSIF UPDATING
 THEN V_DML_TYPE:='U';
v_LOCKED:=static_data_auth_utils.get_locked_status('RA_INVOICE_CLIENT_FEE_VW', :NEW.SK_CLIENT_FEE_ID);
ELSIF
DELETING THEN V_DML_TYPE:='D';
v_LOCKED:= static_data_auth_utils.get_locked_status('RA_INVOICE_CLIENT_FEE_VW' ,:OLD.SK_CLIENT_FEE_ID);
END IF;
IF v_LOCKED='Y' THEN RAISE_APPLICATION_ERROR(-20001,
'RECORD IS LOCKED, THERE IS A PENDING CHANGE');
END IF;
 IF INSERTING  THEN
INSERT
INTO
  ra_invoice_client_fee_a
  (
  SK_CLIENT_FEE_ID         ,
  CLIENT_ID                ,
  TRANSACTION_CURRENCY     ,
  CHANNEL_TYPE             ,
  FIXED_OR_PERCENT         ,
  FEE_VALUE                ,
  INVOICE_FREQUENCY        ,
  Fees_Comm_Client_Share   ,
  TAX_DISCOUNT             ,
  FEE_TYPE_ID              ,
  FEE_SETTLEMENT_CURRENCY  ,
  ORDER_OR_PURSE           ,
  MIN_VALUE                ,
  MAX_VALUE                ,
  TIER_BAND_START          ,
  TIER_BAND_END            ,
  TIER_FLAG                ,
  TIER_PERIOD              ,
  VALUE_CONVERSION_RULE    ,
  STATUS                   ,
  DATE_INSERTED            ,
  LAST_UPDATED             ,
  UPDATED_BY               ,
  AUDIT_GROUP_ID           ,
  DML_TYPE
    )
  VALUES
  (
   CPRADMIN.RA_INVOICE_CLIENT_FEE_SEQ.nextval        ,
  :NEW.CLIENT_ID                ,
  :NEW.TRANSACTION_CURRENCY     ,
  :NEW.CHANNEL_TYPE             ,
  :NEW.FIXED_OR_PERCENT         ,
  :NEW.FEE_VALUE                ,
  :NEW.INVOICE_FREQUENCY        ,
  :NEW.Fees_Comm_Client_Share   ,
  :NEW.TAX_DISCOUNT             ,
  :NEW.FEE_TYPE_ID              ,
  :NEW.FEE_SETTLEMENT_CURRENCY  ,
  :NEW.ORDER_OR_PURSE           ,
  :NEW.MIN_VALUE                ,
  :NEW.MAX_VALUE                ,
  :NEW.TIER_BAND_START          ,
  :NEW.TIER_BAND_END            ,
  :NEW.TIER_FLAG                ,
  :NEW.TIER_PERIOD              ,
  :NEW.VALUE_CONVERSION_RULE    ,
  :NEW.STATUS                   ,
  :NEW.DATE_INSERTED            ,
  :NEW.LAST_UPDATED             ,
  :NEW.UPDATED_BY               ,
  :NEW.AUDIT_GROUP_ID           ,
   V_DML_TYPE
  );
ELSIF UPDATING THEN
INSERT
INTO
  ra_invoice_client_fee_a
   (
  SK_CLIENT_FEE_ID         ,
  CLIENT_ID                ,
  TRANSACTION_CURRENCY     ,
  CHANNEL_TYPE             ,
  FIXED_OR_PERCENT         ,
  FEE_VALUE                ,
  INVOICE_FREQUENCY        ,
  Fees_Comm_Client_Share   ,
  TAX_DISCOUNT             ,
  FEE_TYPE_ID              ,
  FEE_SETTLEMENT_CURRENCY  ,
  ORDER_OR_PURSE           ,
  MIN_VALUE                ,
  MAX_VALUE                ,
  TIER_BAND_START          ,
  TIER_BAND_END            ,
  TIER_FLAG                ,
  TIER_PERIOD              ,
  VALUE_CONVERSION_RULE    ,
  STATUS                   ,
  DATE_INSERTED            ,
  LAST_UPDATED             ,
  UPDATED_BY               ,
  AUDIT_GROUP_ID           ,
  DML_TYPE
    )
  VALUES
  (
   :NEW.SK_CLIENT_FEE_ID         ,
  :NEW.CLIENT_ID                ,
  :NEW.TRANSACTION_CURRENCY     ,
  :NEW.CHANNEL_TYPE             ,
  :NEW.FIXED_OR_PERCENT         ,
  :NEW.FEE_VALUE                ,
  :NEW.INVOICE_FREQUENCY        ,
  :NEW.Fees_Comm_Client_Share   ,
  :NEW.TAX_DISCOUNT             ,
  :NEW.FEE_TYPE_ID              ,
  :NEW.FEE_SETTLEMENT_CURRENCY  ,
  :NEW.ORDER_OR_PURSE           ,
  :NEW.MIN_VALUE                ,
  :NEW.MAX_VALUE                ,
  :NEW.TIER_BAND_START          ,
  :NEW.TIER_BAND_END            ,
  :NEW.TIER_FLAG                ,
  :NEW.TIER_PERIOD              ,
  :NEW.VALUE_CONVERSION_RULE    ,
  :NEW.STATUS                   ,
  :OLD.DATE_INSERTED               ,
  :NEW.LAST_UPDATED             ,
  :NEW.UPDATED_BY               ,
  :NEW.AUDIT_GROUP_ID           ,
   V_DML_TYPE
    );
ELSIF DELETING THEN
INSERT
INTO ra_invoice_client_fee_a
   (
  SK_CLIENT_FEE_ID         ,
  CLIENT_ID                ,
  TRANSACTION_CURRENCY     ,
  CHANNEL_TYPE             ,
  FIXED_OR_PERCENT         ,
  FEE_VALUE                ,
  INVOICE_FREQUENCY        ,
  Fees_Comm_Client_Share   ,
  TAX_DISCOUNT             ,
  FEE_TYPE_ID              ,
  FEE_SETTLEMENT_CURRENCY  ,
  ORDER_OR_PURSE           ,
  MIN_VALUE                ,
  MAX_VALUE                ,
  TIER_BAND_START          ,
  TIER_BAND_END            ,
  TIER_FLAG                ,
  TIER_PERIOD              ,
  VALUE_CONVERSION_RULE    ,
  STATUS                   ,
  DATE_INSERTED            ,
  LAST_UPDATED             ,
  UPDATED_BY               ,
  AUDIT_GROUP_ID           ,
  DML_TYPE
    )
  VALUES
  (
   :OLD.SK_CLIENT_FEE_ID         ,
  :OLD.CLIENT_ID                ,
  :OLD.TRANSACTION_CURRENCY     ,
  :OLD.CHANNEL_TYPE             ,
  :OLD.FIXED_OR_PERCENT         ,
  :OLD.FEE_VALUE                ,
  :OLD.INVOICE_FREQUENCY        ,
  :OLD.Fees_Comm_Client_Share   ,
  :OLD.TAX_DISCOUNT             ,
  :OLD.FEE_TYPE_ID              ,
  :OLD.FEE_SETTLEMENT_CURRENCY  ,
  :OLD.ORDER_OR_PURSE           ,
  :OLD.MIN_VALUE                ,
  :OLD.MAX_VALUE                ,
  :OLD.TIER_BAND_START          ,
  :OLD.TIER_BAND_END            ,
  :OLD.TIER_FLAG                ,
  :OLD.TIER_PERIOD              ,
  :OLD.VALUE_CONVERSION_RULE    ,
  :OLD.STATUS                   ,
  :OLD.DATE_INSERTED               ,
  :OLD.LAST_UPDATED             ,
  :OLD.UPDATED_BY               ,
  :OLD.AUDIT_GROUP_ID           ,
   V_DML_TYPE
    );
END IF;
 END;
/