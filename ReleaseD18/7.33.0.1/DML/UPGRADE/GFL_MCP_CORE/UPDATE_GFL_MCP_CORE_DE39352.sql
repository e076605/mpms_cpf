BEGIN
   UPDATE gfl_mcp_core.currency
      SET MC_SETTLEMENT_FLAG = 'N',
          pref_settlement_curr_flag = 'N',
          pref_deal_curr_flag = 'N'
    WHERE iso_num_code IN (703, 000, 716, 454);

   UPDATE GFL_MCP_CORE.CLIENT
      SET INCLUDE_REV_NET_SETTLE = 'N'
    WHERE INCLUDE_REV_NET_SETTLE IS NULL;

   COMMIT;
END;
/