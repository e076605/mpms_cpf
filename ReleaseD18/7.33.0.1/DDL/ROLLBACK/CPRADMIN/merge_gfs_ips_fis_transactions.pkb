CREATE OR REPLACE PACKAGE BODY CPRADMIN."MERGE_GFS_IPS_FIS_TRANSACTIONS" AS 

  g_etl_date DATE := get_etl_date();
  TYPE t_name_tokens IS TABLE OF INTEGER INDEX BY VARCHAR2(60);
    
  PROCEDURE name_parsing (p_name IN VARCHAR2
                         ,p_name_tokens OUT t_name_tokens 
                         ) IS
    v_beg PLS_INTEGER := 1; 
    v_end PLS_INTEGER := 0;
    v_len PLS_INTEGER := LENGTH(p_name);
    v_index VARCHAR2(60);
  BEGIN
    LOOP
      v_beg := v_end + 1;
      v_end := INSTR(p_name, ' ', v_beg, 1);
      -- dbms_output.put_line ('v_beg = '||v_beg || '  p_end = '||v_end);
      v_index := SUBSTR(p_name, v_beg, v_end - v_beg + ABS(SIGN (v_end) - 1)*100);
      IF v_index NOT IN ('MR','MRS', 'MS') AND LENGTH (v_index) > 1 THEN 
        -- dbms_output.put_line ('v_index = '||v_index);
        p_name_tokens (v_index) := LENGTH(v_index );
      END IF;  
      EXIT WHEN v_end = 0;
    END LOOP;
    -- p_name_tokens (SUBSTR(p_name, v_beg)) := LENGTH(SUBSTR(p_name, v_beg));
  END name_parsing;  

  FUNCTION cleanse (p_str IN VARCHAR2
                   ) RETURN VARCHAR2 IS
  BEGIN
    RETURN UPPER(REGEXP_REPLACE(TRANSLATE(TRIM(p_str),',.','  '), '( ){2,}', ' '));
  END cleanse;


  FUNCTION arrange_name (p_name IN VARCHAR2      -->  source_name
                        ) RETURN VARCHAR IS
    v_name VARCHAR2(100) := cleanse(p_name);
    v_name_tokens t_name_tokens;
    v_index VARCHAR2(60);
  BEGIN
    IF v_name IS NOT NULL THEN
      name_parsing (p_name => v_name
                   ,p_name_tokens => v_name_tokens
                   );
      v_name := NULL;             
      v_index := v_name_tokens.FIRST;
      WHILE v_index IS NOT NULL LOOP
       -- dbms_output.put_line ('v_index = '||v_index|| ' length = '||v_name_tokens(v_index));
         v_name := v_name||v_index||' ';
         v_index := v_name_tokens.NEXT(v_index);
       END LOOP;
    END IF;
    
    RETURN RTRIM(v_name);
      
  END arrange_name;
                          

  FUNCTION is_the_same_names (p_name_1 IN VARCHAR2 --> consumer_name
                             ,p_name_2 IN VARCHAR2 --> cardholder_name
                             ) RETURN INTEGER IS
    v_name_1 VARCHAR2(100) := arrange_name(p_name_1);
    v_name_2 VARCHAR2(100) := arrange_name(p_name_2);
    v_name_tokens_1 t_name_tokens;
    v_name_tokens_2 t_name_tokens;
    is_the_same INTEGER := 0;
   BEGIN
    IF v_name_1 IS NULL OR v_name_2 IS NULL OR v_name_1 = v_name_2 THEN
      is_the_same := 1;
    END IF;
      
    RETURN is_the_same;
      
  END is_the_same_names;


  FUNCTION is_the_same_trx_dates (p_gfs_trx_date IN DATE --> gfs trx date
                                 ,p_if_trx_date IN DATE  --> ips/fis trx date
                              ) RETURN INTEGER IS
  BEGIN
  
    IF ABS(p_gfs_trx_date - p_if_trx_date) <= 0.5 THEN
      RETURN 1;
    ELSE  
      RETURN 0;
    END IF;
    
  END is_the_same_trx_dates;

  PROCEDURE do_rec_merge IS 
    v_merge_datetime DATE := SYSDATE;
    v_loop_flag VARCHAR2(1);
    v_insert_count number:=0;
    v_count number := 0;
    --25 Nov 2011 Sarath - Introduced below changes to force many to many matches 
     cursor c1 is
     SELECT gfs_rec_id, if_rec_id, i_counter_x, g_counter_x
      --,v_merge_datetime, g_etl_date, '4i'
        FROM (SELECT gfs_rec_id
                    ,if_rec_id
                    ,COUNT(1) OVER (PARTITION BY if_rec_id)  i_counter_x 
                    ,COUNT(1) OVER (PARTITION BY gfs_rec_id) g_counter_x                   
                FROM v_basic_if_gfs_merge_query 
                WHERE 
                      (-- card details match
                          bin = 
                          CASE --If Cardnumber less than 10 digits then bin not checked
                             WHEN LENGTH(obfuscated_card_no) < 10 THEN bin 
                             ELSE TO_NUMBER(SUBSTR(obfuscated_card_no,1,6)) 
                          END  
                          AND SUBSTR(cardnumber,-4) =SUBSTR(obfuscated_card_no,-4)                      
                      )
                        OR 
                      (--name + date match
                          merge_gfs_ips_fis_transactions.arrange_name(g_cardholder_name) =
                             merge_gfs_ips_fis_transactions.arrange_name(i_cardholder_name) 
                          AND abs(
                                trunc(g_transaction_timestamp)- trunc( i_transaction_timestamp)
                                )<=2
                      )
             )        
       WHERE i_counter_x > 1 OR g_counter_x > 1 ;
       
        --25 Nov 2011 End of changes         
    --------------------------------------------------------------------------  
    
  BEGIN
    pre_handle_if_gfs_merge_log;  
    
    v_loop_flag :='Y';--Initialize
    
    While v_loop_flag='Y'
    LOOP
        v_insert_count:=0;

    --------------------------------------------------------------------------  
      --  iteration for additional conditions - Card details 
      INSERT /*+ APPEND NO_PARALLEL(l) */  INTO if_gfs_transactions_merge_log l
        (gfs_rec_id, if_rec_id, merge_datetime, etl_date, comments)
      SELECT gfs_rec_id, if_rec_id, v_merge_datetime, g_etl_date, '1i'
        FROM (SELECT gfs_rec_id
                    ,if_rec_id
                    ,COUNT(1) OVER (PARTITION BY if_rec_id)  i_counter_x 
                    ,COUNT(1) OVER (PARTITION BY gfs_rec_id) g_counter_x                   
                FROM v_basic_if_gfs_merge_query 
                WHERE  bin = 
                    CASE --If Cardnumber less than 10 digits then bin not checked
                       WHEN LENGTH(obfuscated_card_no) < 10 THEN bin 
                       ELSE TO_NUMBER(SUBSTR(obfuscated_card_no,1,6)) 
                    END  
               AND SUBSTR(cardnumber,-4) =SUBSTR(obfuscated_card_no,-4)
             )        
       WHERE i_counter_x = 1 AND g_counter_x = 1;

      v_insert_count:=      v_insert_count + sql%rowcount; 
      COMMIT;   
      
      ----------------
      --  iteration for additional conditions - name and trx timestamp 
      INSERT /*+ APPEND NO_PARALLEL(l) */  INTO if_gfs_transactions_merge_log l
        (gfs_rec_id, if_rec_id, merge_datetime, etl_date, comments)
      SELECT gfs_rec_id, if_rec_id, v_merge_datetime, g_etl_date, '2i.1'
        FROM (SELECT gfs_rec_id
                    ,if_rec_id
                    ,COUNT(1) OVER (PARTITION BY if_rec_id)  i_counter_x 
                    ,COUNT(1) OVER (PARTITION BY gfs_rec_id) g_counter_x                   
                FROM v_basic_if_gfs_merge_query 
                WHERE merge_gfs_ips_fis_transactions.arrange_name(g_cardholder_name) =
                       merge_gfs_ips_fis_transactions.arrange_name(i_cardholder_name) 
                  --AND merge_gfs_ips_fis_transactions.is_the_same_trx_dates 
                  --    (g_transaction_timestamp, i_transaction_timestamp)= 1 
                    AND abs(
                          trunc(g_transaction_timestamp)- trunc( i_transaction_timestamp)
                          )=0
 
             )        
       WHERE i_counter_x = 1 AND g_counter_x = 1;

      v_insert_count:=      v_insert_count + sql%rowcount; 
      COMMIT;   
  
      ----------------
      --  iteration for additional conditions - name and trx timestamp (1 day gap)
      --slowly increase window of match
      INSERT /*+ APPEND NO_PARALLEL(l) */  INTO if_gfs_transactions_merge_log l
        (gfs_rec_id, if_rec_id, merge_datetime, etl_date, comments)
      SELECT gfs_rec_id, if_rec_id, v_merge_datetime, g_etl_date, '2i.2'
        FROM (SELECT gfs_rec_id
                    ,if_rec_id
                    ,COUNT(1) OVER (PARTITION BY if_rec_id)  i_counter_x 
                    ,COUNT(1) OVER (PARTITION BY gfs_rec_id) g_counter_x                   
                FROM v_basic_if_gfs_merge_query 
                WHERE merge_gfs_ips_fis_transactions.arrange_name(g_cardholder_name) =
                       merge_gfs_ips_fis_transactions.arrange_name(i_cardholder_name) 
                  AND abs(
                          trunc(g_transaction_timestamp)- trunc( i_transaction_timestamp)
                        )<=1 
             )        
       WHERE i_counter_x = 1 AND g_counter_x = 1;

      v_insert_count:=      v_insert_count + sql%rowcount; 
      COMMIT;   
  
      ----------------
      --  iteration for additional conditions - name and trx timestamp (2 day gap)
      --slowly increase window of match
      INSERT /*+ APPEND NO_PARALLEL(l) */  INTO if_gfs_transactions_merge_log l
        (gfs_rec_id, if_rec_id, merge_datetime, etl_date, comments)
      SELECT gfs_rec_id, if_rec_id, v_merge_datetime, g_etl_date, '2i.3'
        FROM (SELECT gfs_rec_id
                    ,if_rec_id
                    ,COUNT(1) OVER (PARTITION BY if_rec_id)  i_counter_x 
                    ,COUNT(1) OVER (PARTITION BY gfs_rec_id) g_counter_x                   
                FROM v_basic_if_gfs_merge_query 
                WHERE merge_gfs_ips_fis_transactions.arrange_name(g_cardholder_name) =
                       merge_gfs_ips_fis_transactions.arrange_name(i_cardholder_name) 
                  AND abs(
                          trunc(g_transaction_timestamp)- trunc( i_transaction_timestamp)
                          )<=2
             )        
       WHERE i_counter_x = 1 AND g_counter_x = 1;

      v_insert_count:=      v_insert_count + sql%rowcount; 
      COMMIT;   
  
      ----------------
      --  iteration for additional conditions -  (Prim+Secondary) Card details 
      INSERT /*+ APPEND NO_PARALLEL(l) */  INTO if_gfs_transactions_merge_log l
        (gfs_rec_id, if_rec_id, merge_datetime, etl_date, comments)
      SELECT gfs_rec_id, if_rec_id, v_merge_datetime, g_etl_date, '3i'
        FROM (SELECT gfs_rec_id
                    ,if_rec_id
                    ,COUNT(1) OVER (PARTITION BY if_rec_id)  i_counter_x 
                    ,COUNT(1) OVER (PARTITION BY gfs_rec_id) g_counter_x                   
                FROM v_basic_if_gfs_merge_query v,
                     re_cardholder rc1,
                     re_cardholder rc2
                WHERE  v.cardnumber=rc1.cardnumber
                And    rc1.pan=rc2.pan
                AND
                v.bin = 
                    CASE 
                       WHEN LENGTH(v.obfuscated_card_no) < 10 THEN v.bin 
                       ELSE TO_NUMBER(SUBSTR(obfuscated_card_no,1,6)) 
                    END  
               AND SUBSTR(rc2.cardnumber,-4)= SUBSTR(obfuscated_card_no,-4)
             )        
       WHERE i_counter_x = 1 AND g_counter_x = 1;

      v_insert_count:=      v_insert_count + sql%rowcount; 
      COMMIT;   

--commented out due to duplicates issue in Live
/*
      ----------------
      --  iteration for additional conditions 
      --if there are still many to many matches, match off the minimum gfs_rec_id from the group
      -- with the minimum if_rec_id to solve the deadlock.
      INSERT --+ APPEND NO_PARALLEL(l) 
      INTO if_gfs_transactions_merge_log l
        (gfs_rec_id, if_rec_id, merge_datetime, etl_date, comments)
      SELECT gfs_rec_id, if_rec_id, v_merge_datetime, g_etl_date, '4i'
        FROM (SELECT gfs_rec_id
                    ,if_rec_id
                    ,COUNT(1) OVER (PARTITION BY if_rec_id)  i_counter_x 
                    ,COUNT(1) OVER (PARTITION BY gfs_rec_id) g_counter_x                   
                FROM v_basic_if_gfs_merge_query 
                WHERE 
                      (-- card details match
                          bin = 
                          CASE --If Cardnumber less than 10 digits then bin not checked
                             WHEN LENGTH(obfuscated_card_no) < 10 THEN bin 
                             ELSE TO_NUMBER(SUBSTR(obfuscated_card_no,1,6)) 
                          END  
                          AND SUBSTR(cardnumber,-4) =SUBSTR(obfuscated_card_no,-4)                      
                      )
                        OR 
                      (--name + date match
                          merge_gfs_ips_fis_transactions.arrange_name(g_cardholder_name) =
                             merge_gfs_ips_fis_transactions.arrange_name(i_cardholder_name) 
                          AND abs(
                                trunc(g_transaction_timestamp)- trunc( i_transaction_timestamp)
                                )<=2
                      )
             )        
       WHERE i_counter_x > 1 OR g_counter_x > 1 --imp do only for many to many or 1 to many
        ;
      v_insert_count:=      v_insert_count + sql%rowcount; 
      COMMIT;   

*/
    --------------------------------------------------------------------------
    --25 Nov 2011 Sarath - Introduced below changes to force many to many matches
    
        For c1_rec in c1
           LOOP 
           
               v_count := 0;
               
       --        dbms_output.put_line('gfs_rec_id'||':'||c1_rec.gfs_rec_id||';''if_rec_id'||':'||c1_rec.if_rec_id);
               
               SELECT COUNT(1) INTO v_count
                 from if_gfs_transactions_merge_log
                where gfs_rec_id = c1_rec.gfs_rec_id
                   or if_rec_id  = c1_rec.if_rec_id
                 ;                 
        --         dbms_output.put_line('Before if check v_count'||':'||v_count); 
                if  v_count = 0 then
                
        --        dbms_output.put_line('v_count'||':'||v_count);
        --        dbms_output.put_line('Insert values:'||c1_rec.gfs_rec_id||','||c1_rec.if_rec_id||','||v_merge_datetime||','||g_etl_date||','||'5i');
                
                     INSERT /*+ APPEND NO_PARALLEL(l) */  INTO if_gfs_transactions_merge_log 
                           (gfs_rec_id, if_rec_id, merge_datetime, etl_date, comments) values
                           (c1_rec.gfs_rec_id, c1_rec.if_rec_id, v_merge_datetime, g_etl_date, '5i') ;                                                                                       
                      commit;
                      
                      v_insert_count :=   v_insert_count + 1; 
                             
                 end if; 
                
                END LOOP;   
    --25 Nov 2011 End of changes         
    --------------------------------------------------------------------------             
       
       
      
      IF v_insert_count>0
      THEN 
        v_loop_flag:='Y';
        v_insert_count:=0;
      ELSE v_loop_flag:='N';
      END IF;
    END LOOP;  
  
  END do_rec_merge;   


  FUNCTION do_merge (p_non_one_to_one_rec IN VARCHAR2 DEFAULT NULL
                    ) RETURN table_of_couple_of_int PIPELINED IS
    out_rec couple_of_int  := couple_of_int (1,2);
    CURSOR c1 IS SELECT * FROM v_basic_if_gfs_merge_query;
 --      AND merge_gfs_ips_fis_transactions.is_the_same_names (g.consumer_name,c.first_name||' '||c.last_name) = 1

    TYPE t_merge_result IS TABLE OF c1%ROWTYPE;
    v_merge_result t_merge_result;
    i PLS_INTEGER;
  BEGIN

    OPEN c1;
    FETCH c1 BULK COLLECT INTO v_merge_result;
    CLOSE c1;
    FOR i IN 1..v_merge_result.COUNT LOOP
      IF (v_merge_result(i).i_counter = 1 AND v_merge_result(i).g_counter = 1 AND p_non_one_to_one_rec IS NULL) 
        OR (v_merge_result(i).i_counter + v_merge_result(i).g_counter > 2 AND p_non_one_to_one_rec = 'Y') THEN 
        out_rec.int_1 := v_merge_result(i).gfs_rec_id;
        out_rec.int_2 := v_merge_result(i).if_rec_id;
        PIPE ROW (out_rec);
      END IF;  
     
    END LOOP;
     
  END do_merge;

  FUNCTION get_etl_date
    RETURN DATE IS
  BEGIN
  
    RETURN utl_registry_management.get_value('DWH.GENERAL','CURRENT_ETL_DATE');
    
  END get_etl_date;

  
  PROCEDURE handle_partition (p_partition_name IN VARCHAR2
                             ,p_par_value IN VARCHAR2
                             ,p_table_name IN VARCHAR2
                             ) IS
    v_part_exist PLS_INTEGER;
    v_sql_stmt VARCHAR2(2000);
  BEGIN
  
    SELECT COUNT(1) INTO v_part_exist 
      FROM user_tab_partitions 
     WHERE table_name = p_table_name 
       AND partition_name = p_partition_name;
       
    CASE v_part_exist 
      WHEN 0 THEN                             --> create parttition
        v_sql_stmt := 'ALTER TABLE '||p_table_name||' ADD PARTITION '||p_partition_name||
                      ' VALUES (TO_DATE ('''||p_par_value||''',''YYYY-MM-DD''))';
      ELSE
        v_sql_stmt := 'ALTER TABLE '||p_table_name||' TRUNCATE PARTITION "'||p_partition_name||'"';                  --> truncate parttition
      END CASE;
     
      dbms_output.put_line(v_sql_stmt);
      EXECUTE IMMEDIATE v_sql_stmt;
     
  END handle_partition;
  
  
  PROCEDURE handle_date_list_partition (p_part_tab_name IN VARCHAR2
                                       ) IS
    v_partition_name VARCHAR2(30);
  BEGIN
 
    v_partition_name := 'P_'||TO_CHAR(g_etl_date,'YYYY_MM_DD');

    handle_partition (p_partition_name => v_partition_name
                     ,p_par_value => TO_CHAR(g_etl_date,'YYYY-MM-DD')
                     ,p_table_name => p_part_tab_name                                   
                     );
                                 
  END handle_date_list_partition;
 

  PROCEDURE pre_handle_if_for_merge IS
  BEGIN
    handle_date_list_partition (p_part_tab_name => 'IF_TRANSACTIONS_FOR_GFS_MERGE'                                    
                               );
  END pre_handle_if_for_merge;
  
  PROCEDURE pre_handle_if_gfs_merge_log IS
  BEGIN
    handle_date_list_partition (p_part_tab_name => 'IF_GFS_TRANSACTIONS_MERGE_LOG'                                    
                               );
  END pre_handle_if_gfs_merge_log;


  FUNCTION is_merged (p_gfs_rec_id IN INTEGER 
                     ,p_if_rec_id IN INTEGER
                      ) RETURN INTEGER IS
    v_yes_merge CONSTANT NUMBER := 1;                  
    v_no_merge CONSTANT NUMBER := 0;                  
  BEGIN
  
    RETURN v_no_merge;
  
  END is_merged;

  PROCEDURE purge_merge_data (p_purge_before_date IN DATE
                             ,p_purge_type IN VARCHAR2 DEFAULT 'BOTH'
                             ) IS
  BEGIN
  
    NULL;
  
  END purge_merge_data;

END merge_gfs_ips_fis_transactions;
/
