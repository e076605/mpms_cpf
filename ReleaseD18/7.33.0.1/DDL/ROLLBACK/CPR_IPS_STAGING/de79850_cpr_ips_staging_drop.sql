--CPR_IPS_STAGING

begin

execute immediate 'create table F_IPS_ICCR_DUAL_MSG_bkp0152018 as select * from  F_IPS_ICCR_DUAL_MSG';


EXCEPTION
WHEN OTHERS THEN
dbms_output.put_line('CREATE failed for F_IPS_ICCR_DUAL_MSG_bkp0152018 = '||SQLERRM); 

end;
/

begin

execute immediate 'create table F_IPS_ICCR_SINGLE_MSG_bkp0152018 as select * from  F_IPS_ICCR_SINGLE_MSG';


EXCEPTION
WHEN OTHERS THEN
dbms_output.put_line('CREATE failed for F_IPS_ICCR_SINGLE_MSG_bkp0152018 = '||SQLERRM); 

end;
/

begin

execute immediate 'create table STG_ACOF_CANDIDATES_bkp0152018 as select * from  STG_ACOF_CANDIDATES';


EXCEPTION
WHEN OTHERS THEN
dbms_output.put_line('CREATE failed for STG_ACOF_CANDIDATES_bkp0152018 = '||SQLERRM); 

end;
/

begin

execute immediate 'create table STG_IPS_ACOF_DELTAS_bkp0152018 as select * from  STG_IPS_ACOF_DELTAS';


EXCEPTION
WHEN OTHERS THEN
dbms_output.put_line('CREATE failed for STG_IPS_ACOF_DELTAS_bkp0152018 = '||SQLERRM); 

end;
/

begin

execute immediate 'create table STG_CURRENT_ACOF_bkp0152018 as select * from  STG_CURRENT_ACOF';


EXCEPTION
WHEN OTHERS THEN
dbms_output.put_line('CREATE failed for STG_CURRENT_ACOF_bkp0152018 = '||SQLERRM); 

end;
/