DECLARE
    v_count  NUMBER;

BEGIN
    select count(*) 
    into 
    v_count from all_tables where table_name = 'CLIENT_CONTACT_DATA_BKP0152018' and OWNER = 'GFL_MCP_CORE';
    
    IF v_count > 0 THEN
    
        EXECUTE IMMEDIATE 'DROP TABLE CLIENT_CONTACT_DATA_BKP0152018';
            
    END IF;

END;
