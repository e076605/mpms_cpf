DECLARE
    v_count  NUMBER;

BEGIN
    select count(*) 
    into 
    v_count from all_tables where table_name = 'DTO_TABLEA_BKP0152018' and OWNER = 'GLOBAL_STAGING_AREA';
    
    IF v_count > 0 THEN
    
        EXECUTE IMMEDIATE 'DROP TABLE DTO_TABLEA_BKP0152018';
            
    END IF;

END;
/


DECLARE
    v_count  NUMBER;

BEGIN
    select count(*) 
    into 
    v_count from all_tables where table_name = 'DTO_TABLEB_BKP0152018' and OWNER = 'GLOBAL_STAGING_AREA';
    
    IF v_count > 0 THEN
    
        EXECUTE IMMEDIATE 'DROP TABLE DTO_TABLEB_BKP0152018';
            
    END IF;

END;
/


DECLARE
    v_count  NUMBER;

BEGIN
    select count(*) 
    into 
    v_count from all_tables where table_name = 'DTO_TABLEC_BKP0152018' and OWNER = 'GLOBAL_STAGING_AREA';
    
    IF v_count > 0 THEN
    
        EXECUTE IMMEDIATE 'DROP TABLE DTO_TABLEC_BKP0152018';
            
    END IF;

END;
/



DECLARE
    v_count  NUMBER;

BEGIN
    select count(*) 
    into 
    v_count from all_tables where table_name = 'IRISH_STAMP_DUTY_BKP0152018' and OWNER = 'GLOBAL_STAGING_AREA';
    
    IF v_count > 0 THEN
    
        EXECUTE IMMEDIATE 'DROP TABLE IRISH_STAMP_DUTY_BKP0152018';
            
    END IF;

END;
/



DECLARE
    v_count  NUMBER;

BEGIN
    select count(*) 
    into 
    v_count from all_tables where table_name = 'MCP_PORTAL_BK' and OWNER = 'GLOBAL_STAGING_AREA';
    
    IF v_count > 0 THEN
    
        EXECUTE IMMEDIATE 'DROP TABLE MCP_PORTAL_BK';
            
    END IF;

END;
/

DECLARE
    v_count  NUMBER;

BEGIN
    select count(*) 
    into 
    v_count from all_tables where table_name = 'MCP_PORTAL_STG_BK' and OWNER = 'GLOBAL_STAGING_AREA';
    
    IF v_count > 0 THEN
    
        EXECUTE IMMEDIATE 'DROP TABLE MCP_PORTAL_STG_BK';
            
    END IF;

END;
/
