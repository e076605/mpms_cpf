DECLARE
    v_count  NUMBER;

BEGIN
    select count(*) 
    into 
    v_count from all_tables where table_name = 'CARD_XREF_BKP0152018' and OWNER = 'CPRADMIN';
    
    IF v_count > 0 THEN
    
        EXECUTE IMMEDIATE 'DROP TABLE  CPRADMIN.CARD_XREF_BKP0152018';
            
    END IF;

END;
/

DECLARE
    v_count  NUMBER;

BEGIN
    select count(*) 
    into 
    v_count from all_tables where table_name = 'CLIENT_XRF_BKP0152018' and OWNER = 'CPRADMIN';
    
    IF v_count > 0 THEN
    
        EXECUTE IMMEDIATE 'DROP TABLE  CPRADMIN.CLIENT_XRF_BKP0152018';
            
    END IF;

END;
/
DECLARE
    v_count  NUMBER;

BEGIN
    select count(*) 
    into 
    v_count from all_tables where table_name = 'CPRADMIN_STATS_BKP0152018' and OWNER = 'CPRADMIN';
    
    IF v_count > 0 THEN
    
        EXECUTE IMMEDIATE 'DROP TABLE  CPRADMIN.CPRADMIN_STATS_BKP0152018';
            
    END IF;

END;
/
DECLARE
    v_count  NUMBER;

BEGIN
    select count(*) 
    into 
    v_count from all_tables where table_name = 'D_FOXWEB_CODES_BKP0152018' and OWNER = 'CPRADMIN';
    
    IF v_count > 0 THEN
    
        EXECUTE IMMEDIATE 'DROP TABLE  CPRADMIN.D_FOXWEB_CODES_BKP0152018';
            
    END IF;

END;
/

DECLARE
    v_count  NUMBER;

BEGIN
    select count(*) 
    into 
    v_count from all_tables where table_name = 'ES_PARAMETERS_VP_BKP0152018' and OWNER = 'CPRADMIN';
    
    IF v_count > 0 THEN
    
        EXECUTE IMMEDIATE 'DROP TABLE  CPRADMIN.ES_PARAMETERS_VP_BKP0152018';
            
    END IF;

END;
/

DECLARE
    v_count  NUMBER;

BEGIN
    select count(*) 
    into 
    v_count from all_tables where table_name = 'FD_FEE_COST_TVX_BKP0152018' and OWNER = 'CPRADMIN';
    
    IF v_count > 0 THEN
    
        EXECUTE IMMEDIATE 'DROP TABLE  CPRADMIN.FD_FEE_COST_TVX_BKP0152018';
            
    END IF;

END;
/

DECLARE
    v_count  NUMBER;

BEGIN
    select count(*) 
    into 
    v_count from all_tables where table_name = 'FS_AGENT_PROFIT_TVX_BKP0152018' and OWNER = 'CPRADMIN';
    
    IF v_count > 0 THEN
    
        EXECUTE IMMEDIATE 'DROP TABLE  CPRADMIN.FS_AGENT_PROFIT_TVX_BKP0152018';
            
    END IF;

END;
/

DECLARE
    v_count  NUMBER;

BEGIN
    select count(*) 
    into 
    v_count from all_tables where table_name = 'FS_AGENT_PROFITABILITY_TVX_BKP0152018' and OWNER = 'CPRADMIN';
    
    IF v_count > 0 THEN
    
        EXECUTE IMMEDIATE 'DROP TABLE  CPRADMIN.FS_AGENT_PROFITABILITY_TVX_BKP0152018';
            
    END IF;

END;
/


DECLARE
    v_count  NUMBER;

BEGIN
    select count(*) 
    into 
    v_count from all_tables where table_name = 'GP_FESALE_LOG_BKP0152018' and OWNER = 'CPR_ADMIN';
    
    IF v_count > 0 THEN
    
        EXECUTE IMMEDIATE 'DROP TABLE  CPRADMIN.GP_FESALE_LOG_BKP0152018';
            
    END IF;

END;
/

DECLARE
    v_count  NUMBER;

BEGIN
    select count(*) 
    into 
    v_count from all_tables where table_name = 'GP_TX_TXNS_BKP0152018' and OWNER = 'CPR_ADMIN';
    
    IF v_count > 0 THEN
    
        EXECUTE IMMEDIATE 'DROP TABLE  CPRADMIN.GP_TX_TXNS_BKP0152018';
            
    END IF;

END;

/


DECLARE
    v_count  NUMBER;

BEGIN
    select count(*) 
    into 
    v_count from all_tables where table_name = 'MSTIF_DATA_FLOW_LOG_PREPRD_BKP0152018' and OWNER = 'CPR_ADMIN';
    
    IF v_count > 0 THEN
    
        EXECUTE IMMEDIATE 'DROP TABLE  CPRADMIN.MSTIF_DATA_FLOW_LOG_PREPRD_BKP0152018';
            
    END IF;

END;
/


DECLARE
    v_count  NUMBER;

BEGIN
    select count(*) 
    into 
    v_count from all_tables where table_name = 'RA_CLIENT_CONTACTS_VP_BKP0152018' and OWNER = 'CPR_ADMIN';
    
    IF v_count > 0 THEN
    
        EXECUTE IMMEDIATE 'DROP TABLE  CPRADMIN.RA_CLIENT_CONTACTS_VP_BKP0152018';
            
    END IF;

END;
/


DECLARE
    v_count  NUMBER;

BEGIN
    select count(*) 
    into 
    v_count from all_tables where table_name = 'TEMP_CUSTOMER_GROUP_BKP0152018' and OWNER = 'CPR_ADMIN';
    
    IF v_count > 0 THEN
    
        EXECUTE IMMEDIATE 'DROP TABLE  CPRADMIN.TEMP_CUSTOMER_GROUP_BKP0152018';
            
    END IF;

END;
/


DECLARE
    v_count  NUMBER;

BEGIN
    select count(*) 
    into 
    v_count from all_tables where table_name = 'TEMP_CUSTOMER_REFERENCE_BKP0152018' and OWNER = 'CPR_ADMIN';
    
    IF v_count > 0 THEN
    
        EXECUTE IMMEDIATE 'DROP TABLE  CPRADMIN.TEMP_CUSTOMER_REFERENCE_BKP0152018';
            
    END IF;

END;
/