DECLARE
v_cnt_check NUMBER;
BEGIN
SELECT count(1) into v_cnt_check
from all_tab_columns
where column_name = 'B24_ON_US_LOADS_AMNT'
and table_name = 'PREPAID_POOL_BAL_RECONC'
and OWNER = 'GLOBAL_STAGING_AREA';

IF v_cnt_check = 0 THEN


EXECUTE IMMEDIATE 'ALTER TABLE GLOBAL_STAGING_AREA.PREPAID_POOL_BAL_RECONC ADD 
(
B24_ON_US_LOADS_DESC  VARCHAR2(255 CHAR),
B24_ON_US_LOADS_AMNT   VARCHAR2(255 CHAR)
)';

END IF;
exception
When others then
dbms_output.put_line(SQLERRM||'  Column creation failed for table  GLOBAL_STAGING_AREA.PREPAID_POOL_BAL_RECONC');
End;

