DECLARE
    v_count  NUMBER;

BEGIN
    select count(*) 
    into 
    v_count from all_tables where table_name = 'FS_MCP_EOD_NEG_BAL_BK' and OWNER = 'MCP_ISSUER_REP';
    
    IF v_count > 0 THEN
    
        EXECUTE IMMEDIATE 'DROP TABLE FS_MCP_EOD_NEG_BAL_BK';
            
    END IF;

END;
/