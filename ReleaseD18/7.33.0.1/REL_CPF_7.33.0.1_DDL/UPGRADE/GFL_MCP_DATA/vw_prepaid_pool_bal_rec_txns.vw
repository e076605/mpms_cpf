CREATE OR REPLACE FORCE VIEW GFL_MCP_DATA.VW_PREPAID_POOL_BAL_REC_TXNS
(
   DATA_SOURCE,
   ICA,
   CURRENCY_ALPHA,
   RECONCILIATION_DATE,
   TRANSACTION_DESC,
   TRANSACTION_AMOUNT,
   PARSER_RUN_ID,
   PARSER_RUN_DATE
)
AS
   SELECT DECODE (report_number, 'IB184N50-BB', 'IB284010-BB', 'IB284010-BB')
             AS DATA_SOURCE,
          ica,
          purse_currency_alpha AS currency_alpha,
          reconciliation_date,
          (CASE
              WHEN TRANSACTION_DESC = 'ACCNT_REPLSHMNT_RELOAD_AMOUNT'
              THEN
                 'ACCOUNT REPLENISHMENT (RELOAD)'
              WHEN TRANSACTION_DESC = 'TRANSNS_PROCESS_OTP_REJ_AMNT'
              THEN
                 'TRANSACTIONS PROCESSED OTP REJ'
              WHEN TRANSACTION_DESC = 'CARDHLDR_ADJ_CRED_RECON_AMNT'
              THEN
                 'CARDHOLDER ADJUSTMENTS - CREDIT (RECON)'
              WHEN TRANSACTION_DESC = 'CARDHLDR_ADJ_CRED_EXCP_AMOUNT'
              THEN
                 'CARDHOLDER ADJUSTMENTS - CREDIT (EXCP)'
              WHEN TRANSACTION_DESC = 'CARDHLDR_ADJ_CRED_MISC_AMOUNT'
              THEN
                 'CARDHOLDER ADJUSTMENTS - CREDIT (MISC)'
              WHEN TRANSACTION_DESC = 'CARDHLDR_ADJ_CRED_P2P_TRF_AMNT'
              THEN
                 'CARDHOLDER ADJUSTMENTS - CREDIT P2P TRF'
              WHEN TRANSACTION_DESC = 'CARDHLDR_ADJ_CRED_P2P_TRF_F_AM'
              THEN
                 'CARDHOLDER ADJUSTMENTS - CREDIT P2P TRF FEE'
              WHEN TRANSACTION_DESC = 'CARDHLDR_ADJ_DEB_EXCP_AMNT'
              THEN
                 'CARDHOLDER ADJUSTMENTS - DEBIT (EXCP)'
              WHEN TRANSACTION_DESC = 'CARDHLDR_ADJ_DEB_MISC_AMNT'
              THEN
                 'CARDHOLDER ADJUSTMENTS - DEBIT (MISC)'
              WHEN TRANSACTION_DESC = 'CARDHLDR_ADJ_DEB_RECON_AMNT'
              THEN
                 'CARDHOLDER ADJUSTMENTS - DEBIT (RECON)'
              WHEN TRANSACTION_DESC = 'CARDHLDR_ADJ_DEB_P2P_TRF_AMNT'
              THEN
                 'CARDHOLDER ADJUSTMENTS - DEBIT P2P TRF'
              WHEN TRANSACTION_DESC = 'CARDHLDR_ADJ_DEB_P2P_TRF_F_AMT'
              THEN
                 'CARDHOLDER ADJUSTMENTS - DEBIT P2P TRF FEE'
              WHEN TRANSACTION_DESC IN
                      ('CARDHOLDER_FEES_AMNT', 'CARDHOLDER_FEES_REVERSAL_AMT')
              THEN
                 'CARDHOLDER FEES.'
              WHEN TRANSACTION_DESC IN
                      ('CARDS_LOAD_AMOUNT', 'SOCIAL_BEN_LOAD_RELOAD_AMT')
              THEN
                 'CARDS LOAD'
              WHEN TRANSACTION_DESC IN
                      ('CASHOUT_UNLOAD_AMOUNT',
                       'ABANDONED_CASH_OUT_AMT',
                       'CORPCNTLACC_UNLDLOAD_CORR_AMT')
              THEN
                 'CASHOUT (UNLOAD)'
              WHEN TRANSACTION_DESC = 'ENDING_POOL_BALANCE_AMNT'
              THEN
                 'ENDING POOL BALANCE'
                 
                             
              WHEN TRANSACTION_DESC = 'FOREIGN_EXC_CONV_FEE_AMNT'
              THEN
                 'FOREIGN EXCHANGE CONVERSION FEE'
              WHEN TRANSACTION_DESC = 'FORGN_EXCH_CROSS_BRDR_FEE_AMNT'
              THEN
                 'FOREIGN EXCH CROSS BORDER FEE'
              WHEN TRANSACTION_DESC IN ('NETWORK_LOAD_AMNT','B24_ON_US_LOADS_AMNT')
              THEN
                 'NETWORK LOAD'
              WHEN TRANSACTION_DESC = 'TODAYS_OTP_REJ_CCA_AMT_AMNT'
              THEN
                 'TODAYS OTP REJECTS - CCA  AMT'
              WHEN TRANSACTION_DESC = 'TODAYS_OTP_REJ_TRAN_AMT_AMNT'
              THEN
                 'TODAYS OTP REJECTS - TRAN AMT'
              WHEN TRANSACTION_DESC IN
                      ('TRANSACTIONS_PROCESSED_AMNT',
                       'CONVENIENCE_CHECK_AUTH_AMT',
                       'CONVENIENCE_CHECK_STOP_PAY_AMT')
              THEN
                 'TRANSACTIONS PROCESSED'
              WHEN TRANSACTION_DESC = 'TRANSFERS_AMOUNT'
              THEN
                 'TRANSFERS'
              WHEN TRANSACTION_DESC = 'BEG_POOL_BALANCE_AMOUNT'
              THEN
                 'BEGINNING POOL BALANCE'
              WHEN TRANSACTION_DESC IN
                      ('TRANSFERS_CREDIT_AMNT', 'TRANSFER_CRE_LOD_CORR_AMT')
              THEN
                 'TRANSFERS_CREDIT_DESC'
              WHEN TRANSACTION_DESC IN
                      ('TRANSFERS_DEBIT_AMNT', 'TRANSFER_DEBIT_LOD_CORR_AMT')
              THEN
                 'TRANSFERS_DEBIT_DESC'
           END)
             TRANSACTION_DESC,
          NVL (transaction_amount, 0) AS transaction_amount,
          run_id AS parser_run_id,
          RUN_DATE_TIME AS PARSER_RUN_DATE
     FROM (SELECT /*+ no_merge */
                 run_id,
                  report_number,
                  COMP_DIST_ID,
                  ica,
                  RECONCILIATION_DATE,
                  purse_currency_alpha,
                  NVL (ACCNT_REPLSHMNT_RELOAD_AMOUNT, 0)
                     AS ACCNT_REPLSHMNT_RELOAD_AMOUNT,
                  NVL (BEG_POOL_BALANCE_AMOUNT, 0) AS BEG_POOL_BALANCE_AMOUNT,
                  NVL (CARDHLDR_ADJ_CRED_RECON_AMNT, 0)
                     AS CARDHLDR_ADJ_CRED_RECON_AMNT,
                  NVL (CARDHLDR_ADJ_CRED_EXCP_AMOUNT, 0)
                     AS CARDHLDR_ADJ_CRED_EXCP_AMOUNT,
                  NVL (CARDHLDR_ADJ_CRED_MISC_AMOUNT, 0)
                     AS CARDHLDR_ADJ_CRED_MISC_AMOUNT,
                  NVL (CARDHLDR_ADJ_CRED_P2P_TRF_AMNT, 0)
                     AS CARDHLDR_ADJ_CRED_P2P_TRF_AMNT,
                  NVL (CARDHLDR_ADJ_CRED_P2P_TRF_F_AM, 0)
                     AS CARDHLDR_ADJ_CRED_P2P_TRF_F_AM,
                  NVL (CARDHLDR_ADJ_DEB_EXCP_AMNT, 0)
                     AS CARDHLDR_ADJ_DEB_EXCP_AMNT,
                  NVL (CARDHLDR_ADJ_DEB_MISC_AMNT, 0)
                     AS CARDHLDR_ADJ_DEB_MISC_AMNT,
                  NVL (CARDHLDR_ADJ_DEB_RECON_AMNT, 0)
                     AS CARDHLDR_ADJ_DEB_RECON_AMNT,
                  NVL (CARDHLDR_ADJ_DEB_P2P_TRF_AMNT, 0)
                     AS CARDHLDR_ADJ_DEB_P2P_TRF_AMNT,
                  NVL (CARDHLDR_ADJ_DEB_P2P_TRF_F_AMT, 0)
                     AS CARDHLDR_ADJ_DEB_P2P_TRF_F_AMT,
                  NVL (CARDHOLDER_FEES_AMNT, 0) AS CARDHOLDER_FEES_AMNT,
                  NVL (CARDS_LOAD_AMOUNT, 0) AS CARDS_LOAD_AMOUNT,
                  NVL (CASHOUT_UNLOAD_AMOUNT, 0) AS CASHOUT_UNLOAD_AMOUNT,
                  NVL (ENDING_POOL_BALANCE_AMNT, 0)
                     AS ENDING_POOL_BALANCE_AMNT,
                  NVL (B24_ON_US_LOADS_AMNT, 0)
                     AS B24_ON_US_LOADS_AMNT,   
                  NVL (FOREIGN_EXC_CONV_FEE_AMNT, 0)
                     AS FOREIGN_EXC_CONV_FEE_AMNT,
                  NVL (FORGN_EXCH_CROSS_BRDR_FEE_AMNT, 0)
                     AS FORGN_EXCH_CROSS_BRDR_FEE_AMNT,
                  NVL (NETWORK_LOAD_AMNT, 0) AS NETWORK_LOAD_AMNT,
                  NVL (TODAYS_OTP_REJ_CCA_AMT_AMNT, 0)
                     AS TODAYS_OTP_REJ_CCA_AMT_AMNT,
                  NVL (TODAYS_OTP_REJ_TRAN_AMT_AMNT, 0)
                     AS TODAYS_OTP_REJ_TRAN_AMT_AMNT,
                  NVL (TRANSACTIONS_PROCESSED_AMNT, 0)
                     AS TRANSACTIONS_PROCESSED_AMNT,
                  NVL (TRANSFERS_AMOUNT, 0) AS TRANSFERS_AMOUNT,
                  NVL (TRANSNS_PROCESS_OTP_REJ_AMNT, 0)
                     AS TRANSNS_PROCESS_OTP_REJ_AMNT,
                  NVL (TRANSFERS_CREDIT_AMNT, 0) AS TRANSFERS_CREDIT_AMNT,
                  NVL (TRANSFERS_DEBIT_AMNT, 0) AS TRANSFERS_DEBIT_AMNT,
                  NVL (SOCIAL_BEN_LOAD_RELOAD_AMT, 0)
                     AS SOCIAL_BEN_LOAD_RELOAD_AMT,
                  NVL (ABANDONED_CASH_OUT_AMT, 0) AS ABANDONED_CASH_OUT_AMT,
                  NVL (TRANSFER_CRE_LOD_CORR_AMT, 0)
                     AS TRANSFER_CRE_LOD_CORR_AMT,
                  NVL (TRANSFER_DEBIT_LOD_CORR_AMT, 0)
                     AS TRANSFER_DEBIT_LOD_CORR_AMT,
                  NVL (CORPCNTLACC_UNLDLOAD_CORR_AMT, 0)
                     AS CORPCNTLACC_UNLDLOAD_CORR_AMT,
                  NVL (CARDHOLDER_FEES_REVERSAL_AMT, 0)
                     AS CARDHOLDER_FEES_REVERSAL_AMT,
                  NVL (CONVENIENCE_CHECK_AUTH_AMT, 0)
                     AS CONVENIENCE_CHECK_AUTH_AMT,
                  NVL (CONVENIENCE_CHECK_STOP_PAY_AMT, 0)
                     AS CONVENIENCE_CHECK_STOP_PAY_AMT,
                  run_date_time
             FROM (SELECT src.*,
                          RANK ()
                          OVER (PARTITION BY RECONCILIATION_DATE, run_id
                                ORDER BY RECONCILIATION_DATE, run_id DESC)
                             rnk
                     FROM PREPAID_POOL_BAL_RECONC_DTL src           --IB284010
                                                         )
            WHERE rnk = 1) UNPIVOT (transaction_amount
                           FOR transaction_desc
                           IN  (accnt_replshmnt_reload_amount,
                               beg_pool_balance_amount,
                               cardhldr_adj_cred_recon_amnt,
                               cardhldr_adj_cred_excp_amount,
                               cardhldr_adj_cred_misc_amount,
                               cardhldr_adj_cred_p2p_trf_amnt,
                               cardhldr_adj_cred_p2p_trf_f_am,
                               cardhldr_adj_deb_excp_amnt,
                               cardhldr_adj_deb_misc_amnt,
                               cardhldr_adj_deb_recon_amnt,
                               cardhldr_adj_deb_p2p_trf_amnt,
                               cardhldr_adj_deb_p2p_trf_f_amt,
                               cardholder_fees_amnt,
                               cards_load_amount,
                               cashout_unload_amount,
                               ending_pool_balance_amnt,
                               b24_on_us_loads_amnt,
                               foreign_exc_conv_fee_amnt,
                               forgn_exch_cross_brdr_fee_amnt,
                               network_load_amnt,
                               todays_otp_rej_cca_amt_amnt,
                               todays_otp_rej_tran_amt_amnt,
                               transactions_processed_amnt,
                               transfers_amount,
                               transns_process_otp_rej_amnt,
                               transfers_credit_amnt,
                               transfers_debit_amnt,
                               social_ben_load_reload_amt,
                               abandoned_cash_out_amt,
                               transfer_cre_lod_corr_amt,
                               transfer_debit_lod_corr_amt,
                               corpcntlacc_unldload_corr_amt,
                               cardholder_fees_reversal_amt,
                               convenience_check_auth_amt,
                               convenience_check_stop_pay_amt));
                               
                               
CREATE OR REPLACE SYNONYM RAL_MCP_DATA.VW_PREPAID_POOL_BAL_REC_TXNS FOR GFL_MCP_DATA.VW_PREPAID_POOL_BAL_REC_TXNS;

