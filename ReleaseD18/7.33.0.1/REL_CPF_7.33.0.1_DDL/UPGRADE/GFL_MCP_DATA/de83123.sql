DECLARE
v_cnt_check NUMBER;
BEGIN
SELECT count(1) into v_cnt_check
from all_tab_columns
where column_name = 'B24_ON_US_LOADS_DESC'
and table_name = 'PREPAID_POOL_BAL_RECONC_DTL'
and OWNER = 'GFL_MCP_DATA';

IF v_cnt_check = 0 THEN


EXECUTE IMMEDIATE 'ALTER TABLE GFL_MCP_DATA.PREPAID_POOL_BAL_RECONC_DTL ADD 

(
B24_ON_US_LOADS_DESC  VARCHAR2(255 CHAR),
B24_ON_US_LOADS_AMNT   NUMBER)
)';


END IF;
exception
When others then
dbms_output.put_line(SQLERRM||'  Column creation failed for table  GFL_MCP_DATA.PREPAID_POOL_BAL_RECONC_DTL');
End;

