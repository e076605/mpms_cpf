7.29.0.1 - F62010	GDPR � Remove Cardholder name from all non-self issuer invoice templates (CPF)

7.32.0.1 F61143	An Post Groff File Change when moving to MCP

7.33.0.0 � F58423 : Corporate CPF changes -- This release is dependent on 7.29.0.1

7.33.0.1 � JIRA/Spikes/Team Supreme Support
DE79849 Excel scientific notification - Related to SCP and MCP merge
DE79850 Defect or Improvement yet to create - drop the backup tables of A18 cycle in C18 cycle � Related to GDPR and unused backup tables
DE81609	GFS Step in SCP Daily Run failing due to unusable indexes
DE82800  - Migrated SCP flag OBIEE hierarchy
DE79842 DA718 add credit net totals
DE83123 - IB184 report include B24 on Network loads
DE57813	APEX: Stop allowing for duplicate GL account codes
DE77927	APEX: PROD: Client Extra Attr - Approvals. Review / Reject and Entry Screen values inconsistent
DE39352	Error in APEX "Operations" - "RA CURRENCY" Tab coding to DB.  should populate with 'N' where checkbox in not ticked.
DE86555 630 Statement segregated fees
