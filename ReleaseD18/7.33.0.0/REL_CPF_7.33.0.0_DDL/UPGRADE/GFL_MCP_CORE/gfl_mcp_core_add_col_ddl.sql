DECLARE
v_cnt_check NUMBER;
BEGIN
SELECT count(1) into v_cnt_check
from all_tab_columns
where column_name = 'CORPORATE_REFERENCES' 
and table_name = 'CLIENT'
and OWNER = 'GFL_MCP_CORE';

IF v_cnt_check = 0 THEN


EXECUTE IMMEDIATE 'ALTER TABLE GFL_MCP_CORE.CLIENT
ADD (CORPORATE_REFERENCES VARCHAR2(1) default ''N'')';


END IF;
exception
When others then
dbms_output.put_line(SQLERRM||'  Column creation failed for table  CLIENT');
End;

/