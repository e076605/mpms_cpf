CREATE OR REPLACE TRIGGER CPRADMIN.TR_RA_CLIENT_EXTRA_ATTR_VW
/* *********************************************************
 REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.1        08/02/2018  Swati      Added two columns MIGRATED_SCP,EXCLUDE_MATCHING  for Feature F57475 SCP and MCP Merge
************************************************************/
   INSTEAD OF INSERT OR UPDATE OR DELETE
   ON CPRADMIN.RA_CLIENT_EXTRA_ATTR_VW
   FOR EACH ROW
DECLARE
   V_DML_TYPE   VARCHAR2 (1);
   v_LOCKED     VARCHAR2 (1);
BEGIN
   IF INSERTING
   THEN
      V_DML_TYPE := 'I';
   ELSIF UPDATING
   THEN
      V_DML_TYPE := 'U';
      v_LOCKED :=
         static_data_auth_utils.get_locked_status ('RA_CLIENT_EXTRA_ATTR_VW',
                                                   :NEW.ID);
   ELSIF DELETING
   THEN
      V_DML_TYPE := 'D';
      v_LOCKED :=
         static_data_auth_utils.get_locked_status ('RA_CLIENT_EXTRA_ATTR_VW',
                                                   :OLD.ID);
   END IF;

   IF v_LOCKED = 'Y'
   THEN
      RAISE_APPLICATION_ERROR (-20001,
                               'RECORD IS LOCKED, THERE IS A PENDING CHANGE');
   END IF;

   IF INSERTING OR UPDATING
   THEN
      INSERT INTO RA_CLIENT_EXTRA_ATTR_A (AUDIT_GROUP_ID,
                                          ID,
                                          RA_CLIENT_ID,
                                          BIN_SPONSOR_ID,
                                          CLIENT_MCP_FLAG,
                                          CLIENT_EXCEL_INDICATOR,
                                          CLIENT_SETTLEMENT_FORMAT,
                                          INCLUDE_REV_NET_SETTLE,
                                          FEES_COMM_INV_TEMPLATE,
                                          ALTERNATE_ID,
                                          CARDNUMBER_REQ,
                                          PROXY_CARDNBR_REQ,
                                          TRANSACTION_TIMESTAMP_REQ, -- Added for Canada POST
                                          LAST_UPDATED,
                                          LAST_UPDATED_BY,
                                          DML_TYPE,
                                          MIGRATED_SCP,
                                          EXCLUDE_MATCHING)
           VALUES (:NEW.AUDIT_GROUP_ID,
                   :NEW.ID,
                   :NEW.RA_CLIENT_ID,
                   :NEW.BIN_SPONSOR_ID,
                   :NEW.CLIENT_MCP_FLAG,
                   :NEW.CLIENT_EXCEL_INDICATOR,
                   :NEW.CLIENT_SETTLEMENT_FORMAT,
                   :NEW.INCLUDE_REV_NET_SETTLE,
                   :NEW.FEES_COMM_INV_TEMPLATE,
                   :NEW.ALTERNATE_ID,
                   :NEW.CARDNUMBER_REQ,
                   :NEW.PROXY_CARDNBR_REQ,
                   :NEW.TRANSACTION_TIMESTAMP_REQ,    -- Added for Canada POST
                   :NEW.LAST_UPDATED,
                   :NEW.LAST_UPDATED_BY,
                   V_DML_TYPE,
                   :NEW.MIGRATED_SCP,
                   :NEW.EXCLUDE_MATCHING);
   ELSIF DELETING
   THEN
      INSERT INTO RA_CLIENT_EXTRA_ATTR_A (AUDIT_GROUP_ID,
                                          ID,
                                          RA_CLIENT_ID,
                                          BIN_SPONSOR_ID,
                                          CLIENT_MCP_FLAG,
                                          CLIENT_EXCEL_INDICATOR,
                                          CLIENT_SETTLEMENT_FORMAT,
                                          INCLUDE_REV_NET_SETTLE,
                                          FEES_COMM_INV_TEMPLATE,
                                          ALTERNATE_ID,
                                          CARDNUMBER_REQ,
                                          PROXY_CARDNBR_REQ,
                                          TRANSACTION_TIMESTAMP_REQ, --Added for Canada POST
                                          LAST_UPDATED,
                                          LAST_UPDATED_BY,
                                          DML_TYPE,
                                          MIGRATED_SCP,
                                          EXCLUDE_MATCHING)
           VALUES (:OLD.AUDIT_GROUP_ID,
                   :OLD.ID,
                   :OLD.RA_CLIENT_ID,
                   :OLD.BIN_SPONSOR_ID,
                   :OLD.CLIENT_MCP_FLAG,
                   :OLD.CLIENT_EXCEL_INDICATOR,
                   :OLD.CLIENT_SETTLEMENT_FORMAT,
                   :OLD.INCLUDE_REV_NET_SETTLE,
                   :OLD.FEES_COMM_INV_TEMPLATE,
                   :OLD.ALTERNATE_ID,
                   :OLD.CARDNUMBER_REQ,
                   :OLD.PROXY_CARDNBR_REQ,
                   :OLD.TRANSACTION_TIMESTAMP_REQ,     --Added for Canada POST
                   :OLD.LAST_UPDATED,
                   :OLD.LAST_UPDATED_BY,
                   V_DML_TYPE,
                   :OLD.MIGRATED_SCP,
                   :OLD.EXCLUDE_MATCHING);
   END IF;
END;
/