DECLARE
v_cnt_check NUMBER;
BEGIN
SELECT count(1) into v_cnt_check
from all_tab_columns
where column_name = 'CORPORATE_REFERENCES' 
and table_name = 'RA_CLIENT_EXTRA_ATTR'
and OWNER = 'CPRADMIN';

IF v_cnt_check = 0 THEN


EXECUTE IMMEDIATE 'ALTER TABLE CPRADMIN.RA_CLIENT_EXTRA_ATTR
ADD (CORPORATE_REFERENCES VARCHAR2(1) default ''N'')';


END IF;
exception
When others then
dbms_output.put_line(SQLERRM||'  Column creation failed for table  RA_CLIENT_EXTRA_ATTR');
End;

/

DECLARE
v_cnt_check NUMBER;
BEGIN
SELECT count(1) into v_cnt_check
from all_tab_columns
where column_name = 'CORPORATE_REFERENCES' 
and table_name = 'RA_CLIENT_EXTRA_ATTR_A'
and OWNER = 'CPRADMIN';

IF v_cnt_check = 0 THEN


EXECUTE IMMEDIATE 'ALTER TABLE CPRADMIN.RA_CLIENT_EXTRA_ATTR_A
ADD (CORPORATE_REFERENCES VARCHAR2(1) default ''N'')';


END IF;
exception
When others then
dbms_output.put_line(SQLERRM||'  Column creation failed for table  RA_CLIENT_EXTRA_ATTR_A');
End;

/